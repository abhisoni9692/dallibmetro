{
	"targets": [
		{
			"target_name": "posappnode",
			"cflags": ["-Wall", "-std=c++11"],
			"sources": [
				"src/Wrapper.cpp",
				"src/Util.cpp",
				"src/Util_PC.cpp",
				"src/model/utils/ConversionUtil.cpp",
				"src/model/utils/EnumUtil.cpp",
				"src/ErrorList.cpp",
				"src/Validator.cpp",
				"src/card/Card.cpp",
				"src/card/DesfireEV1Card.cpp",
				"src/card/FelicaCard.cpp",
				"src/model/Activity.cpp",
				"src/model/CardActivation.cpp",
				"src/model/CardStatus.cpp",
				"src/model/Pass.cpp",
				"src/model/PassCriteria.cpp",
				"src/model/PersonalData.cpp",	
				"src/model/VehicleData.cpp",
				"src/model/Ticket.cpp",
				"src/model/Transaction.cpp",
				"src/model/TollLog.cpp",
				"src/model/ParkingService.cpp",
				"src/model/TransactionsList.cpp",
				"src/model/TransactionsCriteria.cpp",
				"src/model/CardDetail.cpp",
				"src/model/GenericData.cpp",
				"src/model/List.cpp",
				"src/model/Periodicity.cpp",
				"src/model/ErrorMessage.cpp",
				"src/model/Balance.cpp",
				"src/model/CardRemove.cpp",
				"src/reader/Reader.cpp",
				"src/sam/SAM.cpp",
				"src/reader/SCSProReader.cpp",
				"src/reader/SonyReader.cpp",
				"src/reader/BuarohReader.cpp",
				"src/reader/DualiReader.cpp",
				"src/sam/MifareAV2SAM.cpp",
				"src/sam/FelicaSAM.cpp"
			],
			"include_dirs": [
		         "./include/",
		        "./openssl/include",
		        "./openssl/lib",
		        "./include/reader",
		        "./include/card",
		        "./include/model",
		        "./include/sam"
    	    ],
	    	 'libraries': [
				'../openssl/lib/libeay32MDd.lib',
				'../openssl/lib/ssleay32MDd.lib'
			],
		}
	]
}