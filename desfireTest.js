var data1 = {
				'readerType':"SCSPRO_READER",
				'readerIndex':0,
				'firstName': 'Sainath',
				'middleName': 'Sai',
				'lastName': 'Kommu',
				'gender': 'MALE',
				'dateOfBirth':809785844,
				'mobile': 9642296713,
				'aadhaar':123456789123,
				'vehicleType':'NON_COMMERCIAL_CAR',
				'vehicleNumber':'AP04AM0722',
				'status' : 3,
				'posTerminalId' : 'DOE5',
				'timeInMilliSeconds':1520318175
			};
var data2 = {
				'readerType':"SCSPRO_READER",
				'readerIndex':0,
				'amount':500,
				// 'firstName': 'Sainath',
				// 'middleName': 'Sai',
				// 'lastName': 'Kommu',
				// 'gender': 'M',
				// 'dateOfBirth':1234567,
				// 'mobile': 9642296713,
				// 'aadhaar':123456789123
				// 'vehicleType':1,
				// 'vehicleNumber':'AP04AM0722'
			};
var data3 = {
				'readerType':"SCSPRO_READER",
				'readerIndex':0,
				'amount':500,
				'action':'recharge',
				'serviceType': "TOLL",
				'posTerminalId':'DOE1',
				'sourceBranchId':'DOE1',
				'destinationBranchId':'DOE9',
				'periodicity':[{'limitPeriodicity': "MONTHLY",'limitMaxCount':15},{'limitPeriodicity' : "DAILY" ,'limitMaxCount':2}],
				'expiryDate': 1527773369,// Thursday, May 31, 2018 6:59:29 PM GMT+05:30
				// 'expiryTimeInMilliSeconds' : 1525844410000,//Tuesday, March 6, 2018 12:06:15 PM GMT+05:30
				'timeInMilliSeconds':1520860604000,//TODAY
				'passType': "LOCAL",
				'maxTrips' : 30,
				'partnerTxnId': 9876543210,
				'isRenewal':false
			};

const binding = require('./build/Release/posappnode');


var handleResponse = function(response) {
						if(response.error) {
							console.log(response.error);
						} else {
							console.log('no error');
							console.log(response);
						}
					}

function initializeReader(data) {
	binding.initializeReader(data, function(response) {
						if(response.error) {
							console.log(response.error);
						} else {
							console.log('no error');
							console.log(response);
							getPass(data3);
						}
					});
}

function disconnectReader(data) {
	binding.disconnectReader(data, handleResponse);
}

function activateCard(data){
	binding.activateCard(data, handleResponse);
}

function updatePersonalData(data){
	binding.updatePersonalData(data, handleResponse);
}

function updateVehicleData(data){
	binding.updateVehicleData(data, handleResponse);
}

function getCardDetails(data) {
	binding.getCardDetails(data, handleResponse);
}

function activatePass(data) {
	binding.activatePass(data, handleResponse);
}

function usePass(data) {
	binding.usePass(data, handleResponse);
}

function recharge(data) {
	binding.recharge(data, handleResponse);
}

function pay(data) {
	binding.pay(data, handleResponse);
}

function getTransactions(data) {
	binding.getTransactions(data, handleResponse);
}


function getTicket(data){
	binding.getTicket(data, function(response) {
		if(response.error) {
			console.log(response.error);
		} else {
			console.log('no error');
		}
	});
}

function purchaseTicket(data){
	binding.purchaseTicket(data, function(response) {
		if(response.error) {
			console.log(response.error);
		} else {
			console.log('no error');
		}
	});
}

function getPass(data){
	binding.getPass(data, function(response) {
		if(response.error) {
			console.log(response.error);
		} else {
			console.log('no error');
			console.log(response);
		}
	});
}

initializeReader(data2);
// recharge(data3);
// activateCard(data3);
// updatePersonalData(data1);
// updateVehicleData(data1);
// activatePass(data3);
// usePass(data3);
getPass(data3);
// getTransactions(data3);
// purchaseTicket(data3);
// getTicket(data3);
// getCardDetails(data1);
//pay(data3);
// initializeReader({
// 				'readerType':"SCSPRO_READER",
// 				'readerIndex':"1"});
// disconnectReader({
// 				'readerType':"1",
// 				'readerIndex':"0"});
// disconnectReader({
// 				'readerType':"1",
// 				'readerIndex':"1"});
// binding.usePass(data1, function(response) {
// 	if(response.error) {
// 		console.log(response.error);
// 	} else {
// 		console.log('no error');
// 	}
// });



// initializeReader({
// 				'readerType':"SCSPRO_READER",
// 				'readerIndex':1});

// disconnectReader({
// 				'readerType':"1",
// 				'readerIndex':"0"});

// disconnectReader({
// 				'readerType':"1",
// 				'readerIndex':"1"});
// getCardDetails({
// 				'readerType':"ALL",
// 				'readerIndex':-1});