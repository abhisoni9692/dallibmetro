#ifndef     _Adapter_H_
#define     _Adapter_H_

#include "../ErrorList.h"
#include "node.h"

using v8::Local;
using v8::Object;

class Adapter
{
public:
	virtual ErrorList deserialize(Local<Object> object) = 0;
	virtual Local<Object> serialize() = 0;
};

#endif