#ifndef     _ERROR_LIST_H_
#define     _ERROR_LIST_H_

#include <v8.h>

using v8::Local;
using v8::Array;
using v8::Handle;
using v8::Value;

class ErrorList
{
	public:
		ErrorList();
		void add(Handle<Value> pathValue, Handle<Value> value, Handle<Value> messageValue);
		void addAll(ErrorList *errorList);
		void addAll(Local<Array> errors);
		Local<Array> getErrors();
		int Size();
	private:
		Local<Array> errors;
};
#endif