#ifndef     _PARAMETER_H_
#define     _PARAMETER_H_

#define READ_WO_ENC			0
#define READ_ENC			1

#define METHOD_CHARGE		0x00
#define METHOD_PAYMENT		0x01

static unsigned char SYSTEM_CODE[] = {0x92, 0xCF};

// Mutual Authentication key
// ######################################################################################
// # WARNING: Key value should not be written in the source code in commercial version. #
// ###################################################################################### 
static const unsigned char AuthNormalKey[16] = {0x12,0x34,0x12,0x34,0x12,0x34,0x12,0x34,0x12,0x34,0x12,0x34,0x12,0x34,0x12,0x34};


#endif