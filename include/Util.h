#ifndef     _UTIL_H_
#define     _UTIL_H_

#define APP_ERROR		-1
#define APP_CANCEL		-2
#define APP_SUCCESS		0


// FeliCa card command related definition
#define CARD_DATA_TRANSMIT_SUCCEEDED				10000
#define CARD_COMMAND_SUCCEEDED						10001

#define CARD_NOT_FOUND_ERROR						-10000
#define CARD_RESPONSE_APDU_DATA_ERROR				-10001
#define CARD_READER_ERROR							-10002
#define CARD_READ_DATA_ERROR						-10003


// SAM Related constants.
#define SAM_DATA_TRANSMIT_SUCCEEDED				20000
#define SAM_COMMAND_SUCCEEDED					20001
#define DECRYPTION_SUCCEEDED					20002
#define SAM_RESPONSE_NO_SYNTAX_ERROR		    20003
#define SAM_RECONNECTION_SUCCEEDED				20004



#define DECRYPTION_FAILED					   -20000
#define SNR_VERIFICATION_FAILED				   -20001
#define SAM_RESPONSE_BUFFER_ERROR              -20002
#define SAM_DATA_TRANSMIT_UNKOWN_ERROR		   -20003
#define SAM_DATA_TRANSMIT_LCLE_ERROR		   -20004
#define SAM_DATA_TRANSMIT_P1P2_ERROR		   -20005
#define SAM_DATA_TRANSMIT_INS_ERROR		       -20006
#define SAM_DATA_TRANSMIT_CLA_ERROR		       -20007
#define SAM_DATA_TRANSMIT_MAC_ERROR		       -20008
#define SAM_BRIDGE_ERROR                       -20009
#define SAM_RESPONSE_HAS_SYNTAX_ERROR		   -20010
#define SAM_ERROR_REASON_FETCH_ERROR		-20011
#define SAM_DRIVER_SNR_OVERFLOW_ERROR		-20012
#define SAM_RECONNECTION_FAILED				-20013

constexpr static int arraySize = 256; 

/**
Convert big endian byte array to int value.
*/
unsigned int CharArrayToInt(unsigned char in[],
							unsigned int len);

long long CharArrayToLong(unsigned char in[], 
							int len);

/**
Convert int value to 4 bytes big endian byte array.
*/
void IntToCharArray(unsigned int in, 
					unsigned char out[4]);


void IntToCharArrayToBytes(int in,
	unsigned char out[2]);
/**
Convert little endian byte array to int value.
*/
int CharArrayToIntLE(unsigned char in[],
							unsigned int len);

/**
Convert int value to 4 bytes little endian byte array.
*/
void IntToCharArrayLE(unsigned int in, 
					unsigned char out[4]);

bool Equals(unsigned char a[], int sizeOfa, unsigned char b[], int sizeOfb);


/**
Print out unsigned char array. 
*/
void PrintHexArray(char* header, 
				   unsigned long len, 
				   unsigned char byte_array[]);

/**
Print out text.
*/
void PrintText (char* text, ...);

/**
Get numeric value input from keyboard.
*/
int GetNumericValue(void);

/**
Get current time in BCD format

\param [out] timeData	current time in BCD format ([DD][MM][YY][YY][hh][mm][ss])
*/
void GetTimeAtBCD(unsigned char* timeData);

/**
Generate Random value.
*/
void GenerateRandom (unsigned char *buf,int num);

void getdateFromEpochTime(long long now,int* date,int* month,int* year,int* hour);

void longtobytes6(long long var, unsigned char out[]);

long long trimMilliseconds(long long timeInMilliSec);

unsigned long long CharArrayTolonglong(unsigned char in[], unsigned int len);

void IntToCharArrayTwelveDigit(long long in,
	unsigned char out[5]);

void IntToCharArrayDigit(long long in,
	unsigned char out[6]);

extern unsigned char Snr[4];	// Sequence number

#endif
