#ifndef     _VALIDATOR_H_
#define     _VALIDATOR_H_

#include <v8.h>
#include "model/enum/Comparison.h"
#include "ErrorList.h"

using v8::Local;
using v8::Array;
using v8::Handle;
using v8::Value;

class Validator
{
	public:
		static bool Defined(Local<Value> value, const char *path, const char *message, ErrorList *errorList);
		static bool NotNull(Local<Value> value, const char *path, const char *message, ErrorList *errorList);
		static bool Number(Local<Value> value, const char *path, const char *message, ErrorList *errorList);
		static bool NotNullNumber(Local<Value> value, const char *path, const char *message, ErrorList *errorList);
		static bool String(Local<Value> value, const char *path, const char *message, ErrorList *errorList);
		static bool NotNullString(Local<Value> value, const char *path, const char *message, ErrorList *errorList);
		static bool Object(Local<Value> value, const char *path, const char *message, ErrorList *errorList);
		static bool NotNullObject(Local<Value> value, const char *path, const char *message, ErrorList *errorList);
		static bool Function(Local<Value> value, const char *path, const char *message, ErrorList *errorList);
		static bool NotNullFunction(Local<Value> value, const char *path, const char *message, ErrorList *errorList);
		static bool Bool(Local<Value> value, const char *path, const char *message, ErrorList *errorList);
		static bool NumberOfDigits(Local<Value> value, const char *path, const char *message,const int digitCount,Comparison checkType, ErrorList *errorList);
		static bool LengthOfString(Local<Value> value, const char *path, const char *message,const int digitCount,Comparison checkType, ErrorList *errorList);
	private:
		static void addToErrorList(Local<Value> value, const char *path, const char *message, ErrorList *errorList);
};
#endif