#ifndef     __Card_H__
#define     __Card_H__

#include "model/CardDetail.h"
#include "model/CardActivation.h"
#include "model/PersonalData.h"
#include "model/GenericData.h"
#include "model/VehicleData.h"
#include "model/CardStatus.h"
#include "model/Ticket.h"
#include "model/Pass.h"
#include "model/Transaction.h"
#include "model/TransactionsList.h"
#include "constants/ErrorConstants.h"
#include "reader/Reader.h"
#include "model/PassCriteria.h"
#include "model/TransactionsCriteria.h"
#include "model/TransactionsList.h"
class Reader;

class Card {
public:
	static const int unknownCard = 0;
	static const int DESFIRE_EV1_CARD = 1;
	static const int FELICA_RCSA01_CARD = 2;
	int type = unknownCard;
	static Card* GetInstance(int type);
	virtual ErrorConstants GetBalance(Reader *reader, long *amount) = 0;
	virtual ErrorConstants GetCardDetails(Reader *reader, CardDetail *cardDetail) = 0;
	virtual	ErrorConstants GetGenericData(Reader *reader, GenericData *genericData) = 0;
	virtual ErrorConstants ActivateCard(Reader *reader, CardActivation *cardActivation) = 0;
	virtual ErrorConstants UpdatePersonalData(Reader *reader, PersonalData *personalData) = 0;
	virtual ErrorConstants UpdateVehicleData(Reader *reader, VehicleData *vehicleData) = 0;
	virtual ErrorConstants UpdateCardStatus(Reader *reader, CardStatus *cardStatus) = 0; 
	virtual ErrorConstants Pay(Reader *reader, Activity *activity) = 0;
	virtual ErrorConstants Recharge(Reader *reader, Activity *activity) = 0;
	virtual ErrorConstants PurchaseTicket(Reader *reader, Ticket *ticket) = 0;
	virtual ErrorConstants GetTicket(Reader *reader, Ticket *ticket) = 0;
	virtual ErrorConstants ActivatePass(Reader *reader, Pass *pass) = 0;
	virtual ErrorConstants UsePass(Reader *reader, Pass *pass) = 0;
	virtual ErrorConstants GetPass(Reader *reader, Pass *pass,PassCriteria *passCriteria) = 0;
	virtual ErrorConstants GetTransactions(Reader *reader,TransactionsList *transactionsList,TransactionsCriteria *transactionsCriteria) = 0;
	int GetSAMType(int cardType);
	

};

#endif