

class DesfireCardCmdUtil
{
public:
	constexpr static unsigned char ISO_threePass[]         = {0x90,0xAA,0x00,0x00,0x01,0x00,0x00};
	constexpr static unsigned char ISO_selectApplication[] = {0x90,0x5A,0x00,0x00};
	constexpr static unsigned char ISO_getValue[]          = {0x90,0x6C,0x00,0x00,0x01,0x00,0x00};
	constexpr static unsigned char ISO_getCardVersion[]    = {0x90,0x60,0x00,0x00,0x00};
	constexpr static unsigned char ISO_additinalFrame[]     = {0x90,0xAF,0x00,0x00};
	constexpr static unsigned char ISO_read[]              = {0x90,0xBD,0x00,0x00,0x07};
	constexpr static unsigned char ISO_write[]             = {0x90,0x00,0x00,0x00};
	constexpr static unsigned char ISO_credit[]            = {0x90,0x0C,0x00,0x00};
	constexpr static unsigned char ISO_debit[]             = {0x90,0xDC,0x00,0x00};
	constexpr static unsigned char ISO_commit[]            = {0x90,0xC7,0x00,0x00,0x00};
	constexpr static unsigned char ISO_readRecord[]        = {0x90,0xBB,0x00,0x00,0x07};
	constexpr static unsigned char ISO_le                  =  0x00;
	constexpr static unsigned char ISO_zero                =  0x00;
	constexpr static unsigned char ISO_readRecordCmd       =  0xBB;
	constexpr static unsigned char ISO_writeRecordCmd      =  0x3B;
	constexpr static unsigned char ISO_formatCard[]        = {0x90,0xFC,0x00,0x00,0x00};
	constexpr static unsigned char ISO_createApp[] 		   = {0x90,0xCA,0x00,0x00};
	constexpr static unsigned char ISO_generalHeader[]     = {0x90,0x00,0x00,0x00};
	constexpr static unsigned char ISO_valueFile           =  0xCC;
	constexpr static unsigned char ISO_getFileSettings     =  0XF5;
};