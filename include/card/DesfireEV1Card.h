#ifndef     __DESFIRE_Card_H__
#define     __DESFIRE_Card_H__
#include<iostream>
#include <stdbool.h>
#include "reader/Reader.h"
#include "card/Card.h"
#include "constants/ErrorConstants.h"
#include "model/PassCriteria.h"
#include "model/TransactionsCriteria.h"
#include "model/TransactionsList.h"

class DesfireEV1Card : public Card {
	
public:
	DesfireEV1Card();
	ErrorConstants GetBalance(Reader *reader, long *amount);
	ErrorConstants GetCardDetails(Reader *reader, CardDetail *cardDetail);
	ErrorConstants GetGenericData(Reader *reader, GenericData *genericData);
	ErrorConstants ActivateCard(Reader *reader, CardActivation *cardActivation);
	ErrorConstants UpdatePersonalData(Reader *reader, PersonalData *personalData);
	ErrorConstants UpdateVehicleData(Reader *reader, VehicleData *vehicleData);
	ErrorConstants UpdateCardStatus(Reader *reader, CardStatus *cardStatus);
	ErrorConstants Pay(Reader *reader, Activity *activity);
	ErrorConstants Recharge(Reader *reader, Activity *activity);
	ErrorConstants PurchaseTicket(Reader *reader, Ticket *ticket);
	ErrorConstants GetTicket(Reader *reader, Ticket *ticket);
	ErrorConstants ActivatePass(Reader *reader, Pass *pass);
	ErrorConstants UsePass(Reader *reader, Pass *pass);
	ErrorConstants GetPass(Reader *reader, Pass *pass,PassCriteria *passCriteria);
	ErrorConstants GetTransactions(Reader *reader,TransactionsList *transactionsList,TransactionsCriteria *transactionsCriteria);
	
private:
	struct fileKeys
	{
		unsigned char readKey ;
		unsigned char writeKey ;
		unsigned char readWriteKey;
		unsigned char changeKey;
	};
	
	constexpr static fileKeys App_1_File_1 ={0x01,0x02,0x03,0x00};
	constexpr static fileKeys App_1_File_2 ={0x01,0x00,0x00,0x00};

	constexpr static fileKeys App_2_File_1 ={0x0b,0x0c,0x0d,0x00};
	constexpr static fileKeys App_2_File_2 ={0x0c,0x0b,0x0d,0x00};
	constexpr static fileKeys App_2_File_3 ={0x01,0x0A,0x0A,0x00};
	constexpr static fileKeys App_2_File_4 ={0x01,0x02,0x02,0x00};
	constexpr static fileKeys App_2_File_5 ={0x01,0x02,0x02,0x00};
	constexpr static fileKeys App_2_File_6 ={0x01,0x03,0x03,0x00};
	constexpr static fileKeys App_2_File_7 ={0x01,0x03,0x03,0x00};
	constexpr static fileKeys App_2_File_8 ={0x01,0x04,0x04,0x00};
	constexpr static fileKeys App_2_File_9 ={0x01,0x04,0x04,0x00};
	constexpr static fileKeys App_2_File_A ={0x01,0x05,0x05,0x00};
	constexpr static fileKeys App_2_File_B ={0x01,0x05,0x05,0x00};
	constexpr static fileKeys App_2_File_C ={0x01,0x06,0x06,0x00};
	constexpr static fileKeys App_2_File_D ={0x01,0x06,0x06,0x00};

	constexpr static unsigned char CARD_AUTH_KEY_NO =0x00;
	unsigned char Plain_communication = 0x00;
	unsigned char MAC_communication = 0x01;
	unsigned char Encrypted_communication = 0x03;

	constexpr static unsigned char app0_ID[] = { 0x00,0x00,0x00 };
	constexpr static unsigned char app1_ID[] = { 0x01,0x00,0x00 };
	constexpr static unsigned char app2_ID[] = { 0x02,0x00,0x00 };

	long authenticateCard(Reader *reader, unsigned char keyno, unsigned char samkey[]);
	long getValue(Reader *reader, unsigned char fileno, long *amount);
	long selectApplication(Reader *reader, unsigned char applicationId[]);
	long GetCardUID(Reader *reader, unsigned char UID[]);
	long readApp1File2(Reader *reader, unsigned char cardData[], unsigned long cardDataLen);
	long readFromDataFile(Reader *reader, unsigned char fileid, int offset, int len, unsigned char output[], unsigned long outputLen);
	long writeToDataFile(Reader *reader, unsigned char fileno, unsigned int offset, unsigned char data[], unsigned int data_len);
	void Write_to_card(Reader *reader,unsigned char fileno,unsigned char encData[],unsigned long enc_data_len,unsigned char CMD_Write, unsigned char cmd_res[], unsigned long cmd_res_len,int offset, int data_len);
	long creditAmount(Reader *reader,unsigned char fileno, long amount);
	long debitAmount(Reader *reader,unsigned char fileno, long amount);
	long readRecord(Reader *reader,unsigned char fileno, int offset, int len,int recordSize, unsigned char output[], unsigned long *outputLen);
	long writeRecord(Reader *reader,unsigned char fileno,unsigned long offset,unsigned char payLoad[], unsigned long payLoadLength);
	long commit(Reader *reader);
	long addRecordInTxnLog(Reader *reader,unsigned char fileno,unsigned char txnType,long amount,long runningBalance,string bizLevelID,string terminalId,ServiceType domainType,long long timeInMsec);
	long activateCard(Reader *reader,unsigned char posTerminalId[],long long issueDate);
	long updateCardHoldersData(Reader *reader,unsigned char fileNo,unsigned char FName[],unsigned char MName[],unsigned char LName[],unsigned char Gender,long DOB, long long MobileNumber, long long Adhar);
	long updateVehicleData(Reader *reader,unsigned char fileNo, int VehicleType, unsigned char vehicleNo[]);
	bool isCardActive(Reader *reader,long long timeInMsec);
	long buyTollTicket(Reader *reader,unsigned char branchId[],long long txnTime,long long expiryTime);
	long buyRailwayTkt(Reader *reader,unsigned char stationID[],long long pnrNo,long long bookedDate);
	long buyMetroTkt(Reader *reader,unsigned char stationID[],long long startDatetime);
	long buyBusTkt(Reader *reader,unsigned char stationID[],long long startDatetime);
	long buyParkingTkt(Reader *reader,long long startTime,long long endTime);
	long createPass(Reader *reader, ServiceType serviceType, unsigned char FromBranchID[], unsigned char ToBranchId[],
		unsigned char NoofTrips[], unsigned char ExpiryDate[], unsigned char Passtype,
		unsigned char limitPeriodicity, int limitMaxCount[],
		unsigned char limitStartTime[], unsigned char limitStartTime1[],bool isRenewal);
	long readBranchIDofPass(Reader *reader,ServiceType serviceType,unsigned char fileno,int passId, unsigned char output[]);
	long readPass(Reader *reader,ServiceType serviceType,unsigned char srcBranchID[],unsigned char outPut[],int *outputLen);
	long decrementPassTrips(Reader *reader,ServiceType serviceType,unsigned char branchId[],long long timeInMsec,int passType);
	long readPassType(Reader *reader,unsigned char fileno,unsigned char branchId[],int passType,int *passId,unsigned char passData[]);
	long passActions(Reader *reader,unsigned char payLoad[],long long timeInMsec);
	long readTxnLog(Reader *reader,unsigned char fileNo,int offset,int noOfTxns,unsigned char output[],unsigned long *outputLen);
	long getFileSettings(Reader *reader,unsigned char fileNo,unsigned char response[],unsigned long *responseslen);
	long createRailwayPass(Reader *reader,unsigned char CRISID[],unsigned char noOfTrips[], unsigned char ExpiryDate[], unsigned char passType,
					unsigned char limitPeriodicity, unsigned char limitMaxCount[],  
					unsigned char limitStartTime[],unsigned char limitsMaxCount1[], unsigned char limitStartTime1[],long long currentTime);
	long checkRailWayPassExpired(Reader *reader,unsigned char fileNo,bool isExpired, long long timeInsec,int passId);
	long createParkingPass(Reader *reader,unsigned char BranchId[],unsigned char noOfTrips[],long long ExpiryDate,unsigned char passType,
							unsigned char limitPeriodicity, unsigned char limitMaxCount[],  
					unsigned char limitStartTime[],unsigned char limitsMaxCount1[], unsigned char limitStartTime1[],long long currentTime);
	long checkifParkingPassExpired(Reader *reader,unsigned char fileNo,bool isExpired, long long timeInsec,int passId);
	ErrorConstants payForActivityAndAddRecordInTxnLog(Reader *reader, Activity *activity,unsigned char samKey[]);
};

#endif
