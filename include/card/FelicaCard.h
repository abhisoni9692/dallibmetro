#ifndef     _FELICA_Card_
#define     _FELICA_Card_

#include <stdbool.h>
#include "reader/Reader.h"
#include "card/Card.h"
#include "constants/ErrorConstants.h"
#include "model/PassCriteria.h"
#include "model/TransactionsCriteria.h"
#include "model/TransactionsList.h"

class FelicaCard : public Card {
public:
	FelicaCard();
	ErrorConstants GetBalance(Reader *reader, long *amount);
	ErrorConstants GetCardDetails(Reader *reader, CardDetail *cardDetail);
	ErrorConstants GetGenericData(Reader *reader, GenericData *genericData);
	ErrorConstants ActivateCard(Reader *reader, CardActivation *cardActivation);
	ErrorConstants UpdatePersonalData(Reader *reader, PersonalData *personalData);
	ErrorConstants UpdateVehicleData(Reader *reader, VehicleData *vehicleData);
	ErrorConstants UpdateCardStatus(Reader *reader, CardStatus *cardStatus);
	ErrorConstants Pay(Reader *reader, Activity *activity);
	ErrorConstants Recharge(Reader *reader, Activity *activity);
	ErrorConstants PurchaseTicket(Reader *reader, Ticket *ticket);
	ErrorConstants GetTicket(Reader *reader, Ticket *ticket);
	ErrorConstants ActivatePass(Reader *reader, Pass *pass);
	ErrorConstants UsePass(Reader *reader, Pass *pass);
	ErrorConstants GetPass(Reader *reader, Pass *pass, PassCriteria *passCriteria);
	ErrorConstants GetTransactions(Reader *reader,TransactionsList *transactionsList,TransactionsCriteria *transactionsCriteria);
	
	
private:
	long ReadDataWOEnc(const unsigned char serviceNum, 
				   const unsigned char serviceList[], 
				   const unsigned char blockNum, 
				   const unsigned char blockList[], 
				   unsigned long* readLen,
				   unsigned char readData[],
				   Reader *reader);
	long PollingFeliCaCard(Reader *reader);
	long ReadDataBlock(const unsigned char blockNum,
				   const unsigned char blockList[], 
				   unsigned long* readLen,
				   unsigned char readData[],
				   Reader* reader);
	long MutualAuthV2WithFeliCa(const unsigned char systemCode[2],
							const unsigned char serviceCodeNum, 
							const unsigned char serviceCodeKeyVerList[],
							Reader* reader);
	long WriteDataBlock(const unsigned char	blockNum,
					const int             blockLen,
					const unsigned char	blockList[], 
					const unsigned char	blockData[],
					Reader* reader);
	long ReadBalance(unsigned int* balance, Reader* reader);
	long UpdateBalance(unsigned int newBalance,
						const int amount, 
						const unsigned char method, 
						Reader *reader);
	long WriteGenericaDataToCard(unsigned char name [48], unsigned char vehicleNo[16],
								unsigned char mobileNo[5],
								unsigned char adhaarNo[5],unsigned char amount[4], unsigned char VehicleType,
								Reader* reader);
	long UpdataCardHoldersData(unsigned char name [48], unsigned char mobileNo[5],
								unsigned char adhaarNo[5],unsigned char vehicleNo[16], Reader* reader);
	void _update_exec_id( unsigned char execID[]);
	long createPass(Reader *reader,ServiceType serviceType,unsigned char FromBranchID[], unsigned char ToBranchId[],
					unsigned char NoofTrips[], unsigned char ExpiryDate[], unsigned char Passtype,
					unsigned char limitPeriodicity, int limitMaxCount[],  
					unsigned char limitStartTime[], unsigned char limitStartTime1[],unsigned char payload[]);
};
#endif