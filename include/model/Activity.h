#ifndef     _Activity_
#define     _Activity_

#include "enum/ServiceType.h"
#include "enum/Action.h"
#include "enum/PassType.h"
#include "../Adapter.h"
#include "../ErrorList.h"
#include "node.h"

using v8::Local;
using v8::Object;

using namespace std;

class Activity : Adapter {
public:
	ServiceType getServiceType();	
	void setServiceType(ServiceType serviceType);
	Action getAction();
	void setAction(Action action);
	string getPosTerminalId();
	void setPosTerminalId(string posTerminalId);
	long getAmount();
	void setAmount(long amount);
	string getSrcBranchId();
	void setSrcBranchId(string srcBranchId);
	string getDestBranchId();
	void setDestBranchId(string destBranchId);
	long long getTimeInMilliSeconds();
	void setTimeInMilliSeconds(long long timeInMilliSeconds);

	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
private:
	ServiceType serviceType;
	Action action;
	string posTerminalId;
	long amount;
	string srcBranchId;
	string destBranchId;
	long long timeInMilliSeconds;
};

#endif