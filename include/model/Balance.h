#ifndef     _Balance_
#define     _Balance_
#include <iostream>
#include "../Adapter.h"
#include "../ErrorList.h"
#include "node.h"

using v8::Local;
using v8::Object;

using namespace std;

class Balance : Adapter {
public:
	Balance();
	long getAmount();
	void setAmount(long balance);

	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
private:
	long amount;
};

#endif