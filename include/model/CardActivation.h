#ifndef     _Card_Activation_
#define     _Card_Activation_

#include "../Adapter.h"
#include "../ErrorList.h"
#include "model/enum/VehicleType.h"
#include "node.h"
#include <iostream>
#include <string>

using v8::Local;
using v8::Object;

using namespace std;

class CardActivation : Adapter {
public:
	string getPosTerminalId();
	void setPosTerminalId(string posTerminalId);
	long long getTimeInMilliSeconds();
	void setTimeInMilliSeconds(long long timeInMilliSeconds);

	string getName();
	void setName(string name);
	long getAmount();
	void setAmount(long amount);
	VehicleType getVehicleType();
	void setVehicleType(VehicleType vehicleType);
	string getVehicleNumber();
	void setVehicleNumber(string vehicleNumber);
	long long getMobile();
	void setMobile(long long mobile);
	long long getAadhaar();
	void setAadhaar(long long aadhaar);

	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
private:
	string posTerminalId;
	long long timeInMilliSeconds;
	long amount;
	long long mobile;
	long long aadhaar;
	VehicleType vehicleType;
	string vehicleNumber;
	string name;
};

#endif
