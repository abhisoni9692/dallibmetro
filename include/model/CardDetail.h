#ifndef     _Card_Detail_
#define     _Card_Detail_
#include <iostream>
#include "../Adapter.h"
#include "../ErrorList.h"
#include "node.h"

using v8::Local;
using v8::Object;

using namespace std;

class CardDetail : Adapter {
public:
	CardDetail();
	string getCardUID();
	void setCardUID(string cardUID);
	string getCardNumber();
	void setCardNumber(string cardNumber);
	int getBalance();
	void setBalance(int balance);
	int getStatus();
	void setStatus(int status);
	string getVersion();
	void setVersion(string version);

	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
private:
	string cardUID ;
	string cardNumber;
	int balance;
	int status;
	string version;
};

#endif