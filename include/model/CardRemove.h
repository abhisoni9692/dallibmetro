#ifndef     _Card_Remove_
#define     _Card_Remove_
#include <iostream>
#include "../Adapter.h"
#include "../ErrorList.h"
#include "node.h"

using v8::Local;
using v8::Object;

using namespace std;

class CardRemove : Adapter {
public:
	CardRemove();
	bool isCardRemoved();
	void setCardRemoved(bool cardRemoved);

	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
private:
	bool cardRemoved;
};

#endif