#ifndef     _Card_Status_
#define     _Card_Status_

#include "../Adapter.h"
#include "../ErrorList.h"
#include "node.h"
#include "model/enum/CardStatusEnum.h"

using v8::Local;
using v8::Object;

using namespace std;

class CardStatus : Adapter {
public:
	CardStatusEnum getStatus();
	void setStatus(CardStatusEnum status);

	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
private:
	CardStatusEnum status;
};

#endif