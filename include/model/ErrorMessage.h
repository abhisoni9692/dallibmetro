#ifndef _ERROR_MSG_
#define _ERROR_MSG_
#include "constants/ErrorConstants.h"
class ErrorMessage{
public:
	std::string errorCode;
	std::string errorMsg;
	ErrorMessage();
	ErrorMessage(std::string errorCode,std::string errorMsg);
};

ErrorMessage *getErrorMessage(ErrorConstants errorConstants);

#endif