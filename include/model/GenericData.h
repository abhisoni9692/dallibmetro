#ifndef     _Generic_Data_
#define     _Generic_Data_
#include <iostream>
#include "../Adapter.h"
#include "model/enum/ServiceType.h"
#include "../ErrorList.h"
#include "enum/PassType.h"
#include "model/enum/VehicleType.h"
#include "model/enum/CardStatusEnum.h"
#include "node.h"
#include "model/Pass.h"
#include "model/Transaction.h"
#include "model/TollLog.h"
#include "model/ParkingService.h"
#include "model/enum/TransactionType.h"


using v8::Local;
using v8::Object;

using namespace std;

struct PARKING_DATA{
		long long branchId;
		long long inDateTime;
		long long exitDateTime;
	};

class GenericData : Adapter {
public:
	GenericData();
	string getName();
	void setName(string name);
	string getCardNumber();
	void setCardNumber(string cardNumber);
	int getBalance();
	void setBalance(int balance);
	VehicleType getVehicleType();
	void setVehicleType(VehicleType vehicleType);
	CardStatusEnum getCardStatus();
	void setCardStatus(CardStatusEnum cardStatus);
	string getVehicleNumber();
	void setVehicleNumber(string vehicleNumber);
	long long getMobile();
	void setMobile(long long mobile);
	long long getAadhaar();
	void setAadhaar(long long aadhaar);
	string getReaderType();
	void setReaderType(string readerType);
	int getReaderIndex();
	void setReaderIndex(int readerIndex);
	void setServiceType(ServiceType serviceType);
	ServiceType getServiceType();
	void setBranchId(string branchId);
	string getBranchId();
	void setpassCount(int passesCount);
	int getpassCount();
	void setPass(vector<Pass> passes);
	vector<Pass> getPass();
	void setTransactionLog(vector<Transaction> transactions);
	vector<Transaction> getTransactionLog();
	void setTollLog(vector<TollLog> tollLog);
	vector<TollLog> getTollLog();
	void setParkingData(ParkingService *parkingData);
	ParkingService getParkingData();

	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
private:
	string name;
	string cardNumber;
	long long mobile;
	long long aadhaar;
	int balance;
	VehicleType vehicleType;
	CardStatusEnum cardStatus;
	string vehicleNumber;
	int readerIndex;
	string readerType;
	ServiceType serviceType ;
	string branchId;
	int passesCount;
	vector<Pass> passes;
	vector<Transaction> transactions;
	vector<TollLog> tollLogs;
	ParkingService parkingData;
};

#endif
