#ifndef     _LIST_H_
#define     _LIST_H_

#include <v8.h>

using v8::Local;
using v8::Array;

template <class T>
class List
{
	public:
		List();
		void add(T item);
		void addAll(List<T> items);
		int Size();
	private:
		int size;
		T* items;
};
#endif