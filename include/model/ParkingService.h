#ifndef     _PARKING_SERVICE_
#define     _PARKING_SERVICE_

#include "enum/ServiceType.h"
#include "enum/TransactionType.h"
#include "../Adapter.h"
#include "../ErrorList.h"
#include "node.h"
#include <iostream>
#include <string>

using v8::Local;
using v8::Object;

using namespace std;

class ParkingService : Adapter {
public:
	ParkingService();
	long long getExitDateTime();
	void setExitDateTime(long long exitDateTime);
	string getBranchId();
	void setBranchId(string branchId);
	long long getInDateTime();
	void setInDateTime(long long inDateTime);
	
	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
private:
	string branchId;
	long long inDateTime;
	long long exitDateTime;
};

#endif