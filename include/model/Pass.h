#ifndef     _Pass_
#define     _Pass_

#include<iostream>
#include <vector>
#include "Activity.h"
#include "enum/PassType.h"
#include "../ErrorList.h"
#include "model/Periodicity.h"
#include "node.h"

using v8::Local;
using v8::Object;

class Pass :public Activity {
public:
	Pass();
	PassType getPassType();
	void setPassType(PassType passType);
	int getMaxTrips();
	void setMaxTrips(int maxTrips);
	long long getExpiryDate();
	void setExpiryDate(long long expiryDate);
	bool getIsRenewal();
	void setIsRenewal(bool isRenewal);
	vector<Periodicity> getPeriodicity();
	void setPeriodicity(vector<Periodicity> periodicities);
	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
private:
	PassType passType;
	std::vector<Periodicity> periodicities={};
	int maxTrips;
	long long expiryDate;
	bool isRenewal;
};

#endif