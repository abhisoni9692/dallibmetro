#ifndef _PASS_CRITERIA_
#define _PASS_CRITERIA_
#include <iostream>
#include <string>
#include "enum/ServiceType.h"
#include "enum/PassType.h"
#include "model/Activity.h"
#include "../Adapter.h"
#include "../ErrorList.h"
#include "node.h"

using v8::Local;
using v8::Object;

using namespace std;

class PassCriteria: Adapter {
public:

	PassCriteria();
	ServiceType getServiceType();
	void setServiceType(ServiceType serviceType);
	string getSrcBranchId();
	void setSrcBranchId(string srcBranchId);
	PassType getPassType();
	void setPassType(PassType passType);
	long long getTimeInMilliSeconds();
	void setTimeInMilliSeconds(long long timeInMilliSeconds);
	
	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
	
private:
	ServiceType serviceType;
	string srcBranchId;
	PassType passType;	
	long long timeInMilliSeconds;
};
#endif