#ifndef _PERIODICITY_H
#define _PERIODICITY_H

#include "../Adapter.h"
#include "../ErrorList.h"
#include "enum/LimitPeriodicity.h"
#include  "model/Activity.h"
#include "node.h"

using v8::Local;
using v8::Object;

class Periodicity : Adapter {
public:
	Periodicity();
	LimitPeriodicity getLimitPeriodicity();
	void setLimitPeriodicity(LimitPeriodicity limitPeriodicity);
	int getLimitsMaxCount();
	void setLimitsMaxCount(int limitsMaxCount);
	int getLimitsRemainingCount();
	void setLimitsRemainingCount(int limitsRemainingCount);
	long long getLimitsStartTime();
	void setLimitsStartTime(int limitsStartTime);
	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();

private:
		LimitPeriodicity limitPeriodicity;
		int limitsMaxCount;
		int limitsRemainingCount;
		long long limitsStartTime;
	
};
#endif