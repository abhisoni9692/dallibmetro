#ifndef     _Personal_Data_
#define     _Personal_Data_

#include "../Adapter.h"
#include "../ErrorList.h"
#include "node.h"
#include "model/enum/Gender.h"
#include <iostream>
#include <string>

using v8::Local;
using v8::Object;

using namespace std;

class PersonalData : Adapter {
public:
	string getFirstName();
	void setFirstName(string firstName);
	string getMiddleName();
	void setMiddleName(string middleName);
	string getLastName();
	void setLastName(string lastName);
	Gender getGender();
	void setGender(Gender gender);
	long getDateOfBirth();
	void setDateOfBirth(long dateOfBirth);
	long long getMobile();
	void setMobile(long long mobile);
	long long getAadhaar();
	void setAadhaar(long long aadhaar);
	string getVehicleNumber();
	void setVehicleNumber(string vehicleNumber);
	long long getTimeInMilliSeconds();
	void setTimeInMilliSeconds(long long timeInMilliSeconds);

	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
private:
	string firstName;
	string middleName;
	string lastName;
	Gender gender;
	long dateOfBirth;
	long long mobile;
	long long aadhaar;
	string vehicleNumber;
	long long timeInMilliSeconds;
};

#endif