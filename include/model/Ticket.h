#ifndef     _Ticket_
#define     _Ticket_

#include "Activity.h"
#include "ErrorList.h"
#include "node.h"

using v8::Local;
using v8::Object;

class Ticket : public Activity {
public:
	long long getExpiryTimeInMilliSeconds();
	void setExpiryTimeInMilliSeconds(long long expiryTimeInMilliSeconds);
	long long getPartnerTxnId();
	void setPartnerTxnId(long long partnerTxnId);

	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
private:
	long long expiryTimeInMilliSeconds;
	long long partnerTxnId;
};

#endif