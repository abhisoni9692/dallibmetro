#ifndef     _TOLL_LOG_
#define     _TOLL_LOG_

#include "enum/ServiceType.h"
#include "enum/TransactionType.h"
#include "../Adapter.h"
#include "../ErrorList.h"
#include "node.h"
#include <iostream>
#include <string>

using v8::Local;
using v8::Object;

using namespace std;

class TollLog : Adapter {
public:
	TollLog();
	long long getExpiryDateTime();
	void setExpiryDateTime(long long expiryDateTime);
	string getBranchId();
	void setBranchId(string branchId);
	long long getInDateTime();
	void setInDateTime(long long inDateTime);
	
	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
private:
	string branchId;
	long long inDateTime;
	long long expiryDateTime;
};

#endif