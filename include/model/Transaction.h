#ifndef     _Transaction_
#define     _Transaction_

#include "enum/ServiceType.h"
#include "enum/TransactionType.h"
#include "../Adapter.h"
#include "../ErrorList.h"
#include "node.h"
#include <iostream>
#include <string>

using v8::Local;
using v8::Object;

using namespace std;

class Transaction : Adapter {
public:
	Transaction();
	ServiceType getServiceType();
	void setServiceType(ServiceType serviceType);
	TransactionType getType();
	void setType(TransactionType type);
	long long getTimeInMilliSeconds();
	void setTimeInMilliSeconds(long long timeInMilliSeconds);
	long getAmount();
	void setAmount(long amount);
	long getRunningBalance();
	void setRunningBalance(long runningBalance);
	string getPosTerminalId();
	void setPosTerminalId(string posTerminalId);

	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
private:
	ServiceType serviceType;
	TransactionType type;
	long long timeInMilliSeconds;
	long amount;
	long runningBalance;
	string posTerminalId;
};

#endif