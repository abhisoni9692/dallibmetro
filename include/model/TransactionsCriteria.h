#ifndef _TRANSACTIONS_CRITERIA_H
#define _TRANSACTIONS_CRITERIA_H
#include <iostream>
#include <string>
#include "model/Activity.h"
#include "../Adapter.h"
#include "../ErrorList.h"
#include "node.h"

using v8::Local;
using v8::Object;

class TransactionsCriteria: Adapter
{
public:
	TransactionsCriteria();
	string getSourceBranchID();
	void setSourceBranchID(string posTerminalId);
	long long getTimeInMilliSeconds();
	void setTimeInMilliSeconds(long long timeInMilliSeconds);

	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
private:
	string posTerminalId;
	long long timeInMilliSeconds;
	
};

#endif