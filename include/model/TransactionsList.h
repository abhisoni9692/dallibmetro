#ifndef _TRANSACTIONS_LIST_H_
#define _TRANSACTIONS_LIST_H_

#include <iostream>
#include <vector>
#include "Transaction.h"
#include "../Adapter.h"
#include "../ErrorList.h"
#include "node.h"

using v8::Local;
using v8::Object;

using namespace std;


class TransactionsList : Adapter
{
public:
	TransactionsList();
	vector<Transaction> getTransactions();
	void setTransactions(vector<Transaction> transactions);

	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();

private:
	vector<Transaction> transactions;
};

#endif