#ifndef     _Vehicle_Data_
#define     _Vehicle_Data_

#include "../Adapter.h"
#include "../ErrorList.h"
#include "node.h"
#include "model/enum/VehicleType.h"
#include <iostream>
#include <string>

using v8::Local;
using v8::Object;

using namespace std;

class VehicleData : Adapter {
public:
	VehicleType getVehicleType();
	void setVehicleType(VehicleType vehicleType);
	string getVehicleNumber();
	void setVehicleNumber(string vehicleNumber);
	long long getTimeInMilliSeconds();
	void setTimeInMilliSeconds(long long timeInMilliSeconds);
	
	ErrorList deserialize(Local<Object> object);
	Local<Object> serialize();
private:
	VehicleType vehicleType;
	string vehicleNumber;
	long long timeInMilliSeconds;
};

#endif