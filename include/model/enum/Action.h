#ifndef     _Action_H_
#define     _Action_H_

enum class Action {
	UNKNOWN =0,
	PAY =1,
	RECHARGE =2,
	FINE =3
};

#endif