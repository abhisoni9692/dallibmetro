#ifndef     _CARDSTATUSENUM_H_
#define     _CARDSTATUSENUM_H_
#include <string>


enum class CardStatusEnum{
	UNKNOWN = 0,
	personalize = 1,
	allocate = 2,
	activate = 3,
	block = 4
};

#endif