#ifndef     _COMPARISON_H_
#define     _COMPARISON_H_

enum class Comparison {
	EQUAL_TO = 0,
	LESS_THAN_EQUAL_TO =1,
	GREATER_THAN_EQUAL_TO =2
};

#endif