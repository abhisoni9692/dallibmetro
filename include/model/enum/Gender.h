#ifndef _GENDER_H_
#define _GENDER_H_
enum class Gender{
	UNKNOWN = 0,
	MALE = 1,
	FEMALE = 2
};
#endif