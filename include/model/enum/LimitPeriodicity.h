#ifndef _LIMIT_PERIODICITY_H
#define _LIMIT_PERIODICITY_H

enum class LimitPeriodicity{
	UNKNOWN_PERIODICITY = 0,
	HOURLY = 1,
	DAILY  = 2,
	WEEKLY = 3,
	MONTHLY = 4,
	QUARTLY = 5,
	HALFYEARLY = 6,
	YEARLY = 7
};
#endif