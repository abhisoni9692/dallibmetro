#ifndef     _Pass_H_
#define     _Pass_H_

enum class PassType {
	UNKNOWN = 0,
	DAILY = 1,
	MONTHLY = 2,
	LOCAL = 3
};

#endif