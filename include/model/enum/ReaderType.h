#ifndef     _READER_TYPE_H_
#define     _READER_TYPE_H_

enum class ReaderType {
	UNKNOWN = 0,
	ALL = 1,
	SCSPRO_READER = 2,
	SONY_READER = 3, 
	BUAROH_READER = 4,
	DUALI_READER = 5
};

#endif