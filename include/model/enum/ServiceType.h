#ifndef     _Service_Type_
#define     _Service_Type_

enum class ServiceType {
	UNKNOWN = 0,
	TOLL = 1,
	BUS =2,
	METRO =3,
	RAIL =4,
	PARKING =5
};

#endif