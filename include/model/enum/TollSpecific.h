#ifndef     _Toll_Specific_
#define     _Toll_Specific_

enum class TollSpecific {
	TOTAL_SERVICES =2,
	PASS_SERVICE =1,
	LOG_SERVICE =1,
	BLOCK_NUM_PASSES=6,
	BLOCK_NUM_LOG=7
};

#endif