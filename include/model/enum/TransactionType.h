#ifndef     _Transaction_Type_H_
#define     _Transaction_Type_H_

enum class TransactionType {
	UNKNOWN =0,
	PAY = 1,
	RECHARGE =2,
	FINE =3
};

#endif