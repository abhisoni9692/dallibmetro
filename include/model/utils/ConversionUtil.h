#ifndef     _ConversionUtil_H_
#define     _ConversionUtil_H_

#include <v8.h>
#include "node.h"
#include <model/enum/ServiceType.h>
#include <model/enum/LimitPeriodicity.h>
using v8::Local;
using v8::String;

using namespace std;

class ConversionUtil {
public:
	static char* toCharArray(Local<String> v8String);
	static unsigned char* toUnsignedCharArray(Local<String> v8String);
	static char* toCharArray(string str);
	static unsigned char* toUnsignedCharArray(string str);
	static string toStdString(Local<String> v8String);
	static char toChar(Local<String> v8String);
	static Local<String> toV8String(char ch);
	static ServiceType toServiceType(int service);
	static LimitPeriodicity toLimitPeriodicity(int limit);
};

#endif