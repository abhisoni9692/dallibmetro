#ifndef     _EnumUtil_H_
#define     _EnumUtil_H_

#include "../enum/Action.h"
#include "../enum/CardStatusEnum.h"
#include "../enum/LimitPeriodicity.h"
#include "../enum/PassType.h"
#include "../enum/ReaderType.h"
#include "../enum/ServiceType.h"
#include "../enum/TransactionType.h"
#include "../enum/Gender.h"
#include "../enum/VehicleType.h"

class EnumUtil {
public:
	static char* ToString(Action action);
	static Action getAction(const char* action);
	static char* ToString(CardStatusEnum cardStatus);
	static CardStatusEnum getCardStatusEnum(const char* cardStatus);
	static char* ToString(LimitPeriodicity limitPeriodicity);
	static LimitPeriodicity getLimitPeriodicity(const char* limitPeriodicity);
	static char* ToString(PassType passType);
	static PassType getPassType(const char* passType);
	static char* ToString(ReaderType readerType);
	static ReaderType getReaderType(const char* readerType);
	static char* ToString(ServiceType serviceType);
	static ServiceType getServiceType(const char* serviceType);
	static char* ToString(TransactionType transactionType);
	static TransactionType getTransactionType(const char* transactionType);
	static char* ToString(Gender gender);
	static Gender getGender(const char* gender);
	static char* ToString(VehicleType vehicleType);
	static VehicleType getVehicleType(const char* vehicleType);

};

#endif