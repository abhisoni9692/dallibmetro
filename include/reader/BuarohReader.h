#ifndef     _BUAROH_READER_H_
#define     _BUAROH_READER_H_

#pragma once


#include "reader/Reader.h"
#include "sam/SAM.h"
#include "card/Card.h"
#include "model/constants/ErrorConstants.h"

#include <tchar.h>
#include <iostream>

#include <vector>
#include <string>

#include <Windows.h>
#include <SetupAPI.h>
#include <initguid.h>
#include <usbiodef.h>

class Card;

#define		DEBUG

class BuarohReader : public Reader {
public:
	BuarohReader();
	long initializeReader(int readerIndex);
	long sendToCard(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int cardType);
	long sendToSAM(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int SAMType);
	long disconnectReader();
	long detectCard(int *cardType);
	long detectInstantCard(int *cardType);
	bool isCardRemoved();
	bool stopDetection();
	long DisconnectFeliCaCard(void);
	long changeObject (SAM *sam);
	bool cancel_thread = false;
	bool cancelDetection_b = false;

	ErrorConstants GetBalance(Card *card, long *amount);
	ErrorConstants GetCardDetails(Card *card, CardDetail *cardDetail);
	ErrorConstants GetGenericData(Card *card, GenericData *genericData);
	ErrorConstants ActivateCard(Card *card, CardActivation *cardActivation);
	ErrorConstants UpdatePersonalData(Card *card, PersonalData *personalData);
	ErrorConstants UpdateVehicleData(Card *card, VehicleData *vehicleData);
	ErrorConstants UpdateCardStatus(Card *card, CardStatus *cardStatus);
	ErrorConstants Pay(Card *card, Activity *activity);
	ErrorConstants Recharge(Card *card, Activity *activity);
	ErrorConstants PurchaseTicket(Card *card, Ticket *ticket);
	ErrorConstants GetTicket(Card *card, Ticket *ticket);
	ErrorConstants ActivatePass(Card *card, Pass *pass);
	ErrorConstants UsePass(Card *card, Pass *pass);
	ErrorConstants GetPass(Card *card, Pass *pass,PassCriteria *passCriteria);
	ErrorConstants GetTransactions(Card *card,TransactionsList *transactionsList,TransactionsCriteria *transactionsCriteria);
	ErrorConstants EncryptData(Card *card, unsigned char data[16]);

private:

	static const int slot1 = 1;
	static const int slot2 = 2;
	static SAM *samRefSlot1;
	static SAM *samRefSlot2;
	//
	

	HANDLE hCom = NULL;

	int NAD_ADDR = 0;
	int PCB_ADDR = 1;
	int LEN_ADDR = 2;
	int LEN2_ADDR = 3;
	int DATA_BASE = (LEN_ADDR + 1);
	int DATA2_BASE = (LEN2_ADDR + 1);
	int EDC_LEN = 1;
	int SW_LEN = 2;

	int APDU_PCBTypeA = 0;
	int APDU_PCBTypeB = 1;

	unsigned char APDU_OK_SW1 = 0x90;
	unsigned char APDU_OK_SW2 = 0x00;
	unsigned char APDU_ERR_FELICA_TEST_SW1        = 0x62;
	unsigned char APDU_ERR_FELICA_TEST_ACTIVATE   = 0x00;
	unsigned char APDU_ERR_FELICA_TEST_BALANCE    = 0x01;
	unsigned char APDU_ERR_FELICA_TEST_DEDUCT     = 0x02;
	unsigned char APDU_ERR_FELICA_TEST_TOPUP      = 0x03;
	unsigned char APDU_ERR_FELICA_TEST_CARD_DATA  = 0x04;
	unsigned char APDU_ERR_FELICA_TEST_PASS       = 0x05;
	unsigned char APDU_ERR_FELICA_TEST_POLLING    = 0x06;
	unsigned char APDU_ERR_FELICA_TEST_READ_DATA  = 0x07;
	unsigned char APDU_ERR_FELICA_TEST_AUTH       = 0x08;
	unsigned char APDU_ERR_FELICA_TEST_LOW_BAL    = 0x09;
	unsigned char APDU_ERR_FELICA_TEST_WRITE_DATA = 0x10;


	void ReadUsbData(HANDLE hCom, unsigned char SerialBuffer[], unsigned long *ReadBufferLen, unsigned long length, int PCB_Type);
	void WriteUsbData(HANDLE hCom, unsigned char SerialBuffer[], DWORD WriteBufferLen);
	bool FindBaoruhDevice(vector<wstring> &vPortNames);
		
};

#endif
