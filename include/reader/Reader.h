#ifndef     __READER_H__
#define     __READER_H__

#include <string>
#include "../model/enum/ReaderType.h"
#include "../model/constants/ErrorConstants.h"
#include "../model/CardDetail.h"
#include "../model/CardActivation.h"
#include "../model/PersonalData.h"
#include "../model/GenericData.h"
#include "../model/VehicleData.h"
#include "../model/CardStatus.h"
#include "../model/Activity.h"
#include "../model/Ticket.h"
#include "../model/Pass.h"
#include "../model/Transaction.h"
#include "../model/PassCriteria.h"
#include "../model/TransactionsCriteria.h"
#include "../model/TransactionsList.h"
#include "card/Card.h"
#include "sam/SAM.h"

class Card;
class SAM;

#define		DEBUG

#define IOCTL_CCID_ESCAPE SCARD_CTL_CODE(3500)

// Reader Writer related definition
#define UNKOWN_ERROR			1
#define RCS500_LCLE_ERROR		2
#define RCS500_P1P2_ERROR		3
#define RCS500_INS_ERROR		4
#define RCS500_CLA_ERROR		5
#define RCS500_MAC_ERROR		8

#define SMPL_ESC_KEY			0x1B


//SDR-10 Command
#define PCSC_CMD_CARD_FECLICA		0x01
#define PCSC_CMD_FECLICA_GET_IDM	0x02
#define PCSC_CMD_GET_FW_VER			0x03
#define PCSC_CMD_SAM_1_ON			0x04
#define PCSC_CMD_SAM_2_ON			0x05
#define PCSC_CMD_SAM_1_OFF			0x06
#define PCSC_CMD_SAM_2_OFF			0x07
#define PCSC_CMD_SAM_1_APDU			0x08
#define PCSC_CMD_SAM_2_APDU			0x09

using namespace std;
extern unsigned char g_CMD_SAM_APDU; 

class Reader
{
public:
	ReaderType type = ReaderType::UNKNOWN;
	static Reader *cardTapped;
	bool hasAdditionalFrames = false;
	bool cancelDetection = false;
	bool cancel_thread = false;

	static Reader* getInstance(int type);
	static Reader* getInstance(ReaderType type);
	virtual long initializeReader(int readerIndex) = 0;
	virtual long disconnectReader() = 0;
	virtual long detectCard(int *cardType) = 0;
	virtual long detectInstantCard(int *cardType) = 0;
	virtual bool isCardRemoved() = false;
	virtual bool stopDetection() = false;
	virtual long DisconnectFeliCaCard(void) = 0;
	virtual long sendToCard(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int cardType) = 0;
	virtual long sendToSAM(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int SAMType) = 0;
	virtual long changeObject (SAM *sam)=0;

	virtual ErrorConstants GetBalance(Card *card, long *amount) = 0;
	virtual ErrorConstants GetCardDetails(Card *card, CardDetail *cardDetail) = 0;
	virtual	ErrorConstants GetGenericData(Card *card, GenericData *genericData) = 0;
	virtual ErrorConstants ActivateCard(Card *card, CardActivation *cardActivation) = 0;
	virtual ErrorConstants UpdatePersonalData(Card *card, PersonalData *personalData) = 0;
	virtual ErrorConstants UpdateVehicleData(Card *card, VehicleData *vehicleData) = 0;
	virtual ErrorConstants UpdateCardStatus(Card *card, CardStatus *cardStatus) = 0;
	virtual ErrorConstants Pay(Card *card, Activity *activity) = 0;
	virtual ErrorConstants Recharge(Card *card, Activity *activity) = 0;
	virtual ErrorConstants PurchaseTicket(Card *card, Ticket *ticket) = 0;
	virtual ErrorConstants GetTicket(Card *card, Ticket *ticket) = 0;
	virtual ErrorConstants ActivatePass(Card *card, Pass *pass) = 0;
	virtual ErrorConstants UsePass(Card *card, Pass *pass) = 0;
	virtual ErrorConstants GetPass(Card *card, Pass *pass,PassCriteria *passCriteria) = 0;
	virtual ErrorConstants GetTransactions(Card *card,TransactionsList *transactionsList,TransactionsCriteria *transactionsCriteria) = 0;
	virtual ErrorConstants EncryptData(Card *card, unsigned char data[16])=0;
};
#endif