#ifndef     __SCSPRO_H__
#define     __SCSPRO_H__

#include "reader/Reader.h"
#include "sam/SAM.h"
#include "card/Card.h"
#include "model/constants/ErrorConstants.h"
#include "model/TransactionsCriteria.h"
#include "model/TransactionsList.h"

#define		DEBUG

class SCSProReader : public Reader {
public:
	
	SCSProReader();
	long initializeReader(int readerIndex);
	long sendToCard(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int cardType);
	long sendToSAM(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int SAMType);
	long disconnectReader();
	long detectCard(int *cardType);
	long detectInstantCard(int *cardType);
	bool isCardRemoved();
	bool stopDetection();
	long DisconnectFeliCaCard(void);
	long changeObject (SAM *sam);
	bool cancel_thread = false;
	bool cancelDetection = false;

	ErrorConstants GetBalance(Card *card, long *amount);
	ErrorConstants GetCardDetails(Card *card, CardDetail *cardDetail);
	ErrorConstants GetGenericData(Card *card, GenericData *genericData);    //GetGenericData
	ErrorConstants ActivateCard(Card *card, CardActivation *cardActivation);
	ErrorConstants UpdatePersonalData(Card *card, PersonalData *personalData);
	ErrorConstants UpdateVehicleData(Card *card, VehicleData *vehicleData);
	ErrorConstants UpdateCardStatus(Card *card, CardStatus *cardStatus);
	ErrorConstants Pay(Card *card, Activity *activity);
	ErrorConstants Recharge(Card *card, Activity *activity);
	ErrorConstants PurchaseTicket(Card *card, Ticket *ticket);
	ErrorConstants GetTicket(Card *card, Ticket *ticket);
	ErrorConstants ActivatePass(Card *card, Pass *pass);
	ErrorConstants UsePass(Card *card, Pass *pass);
	ErrorConstants GetPass(Card *card, Pass *pass,PassCriteria *passCriteria);
	ErrorConstants GetTransactions(Card *card,TransactionsList *transactionsList,TransactionsCriteria *transactionsCriteria);
	ErrorConstants EncryptData(Card *card, unsigned char data[16]);
private:

	// PC/SC context
	//
	
	SCARDCONTEXT g_hContext = NULL;
	SCARDHANDLE	 g_hCardReader = NULL;
	SCARDHANDLE	 g_hSAM = NULL;
	// Buffer to store PC/SC Reader Names

	LPTSTR  g_mszReaders = NULL;
	wchar_t CARD_IF_SDR10[100];
	bool scsproReaderIniti = false;
	

	unsigned char BUZZER_MUTE=0x00;
	unsigned char BUZZER_ERR=0x01;
	unsigned char BUZZER_GOOD=0x02;
	unsigned char BUZZER_START=0x03;
	unsigned char BUZZER_NORMAL=0x04;
	unsigned char BUZZER_END=0x05;
	unsigned char BUZZER_WARN=0x06;
	
	static const int slot1 = 1;
	static const int slot2 = 2;
	SAM *samRefSlot1 = NULL;
	SAM *samRefSlot2 = NULL;
	SAM* initializeSAM(int slotNo);
	long TransmitDataToSAM(unsigned char samCmdBuf[], unsigned long samCmdLen, unsigned char samResBuf[], unsigned long* samResLen, unsigned char sdr10_cmd);
	long TransmitDataToFelicaSAM(unsigned char samCmdBuf[], unsigned long samCmdLen, unsigned char samResBuf[], unsigned long* samResLen, unsigned char sdr10_cmd);
	long TransmitDataToCard(unsigned long cmdLen, unsigned char cmdBuf[], unsigned long* resLen, unsigned char resBuf[]);
	long TransmitDataToFeliCaCard(unsigned long felicaCmdLen, unsigned char felicaCmdBuf[], unsigned long* felicaResLen, unsigned char felicaResBuf[]);
	long _ConnectToSAMInDirectMode();
	long _DisconnectWithSAMInDirectMode(void);
	long _send_SetNormalMode(unsigned char samResBuf[], unsigned long* samResLen);
	long _send_Attention(unsigned char samResBuf[], unsigned long* samResLen);
	long _send_SAMPowerON(unsigned char samNo, unsigned char samResBuf[], unsigned long* samResLen);
	long EscapeDataToSAM(unsigned char samCmdBuf[], unsigned long samCmdLen, unsigned char samResBuf[], unsigned long* samResLen, unsigned char sdr10_cmd);
	long EscapeDataToFelicaSAM(unsigned char samCmdBuf[], unsigned long samCmdLen, unsigned char samResBuf[], unsigned long* samResLen, unsigned char sdr10_cmd);
	void readerSpecific(unsigned char sdr10_cmd,unsigned char _send_buf[],unsigned int* nIdx);
	bool playBuzzer (unsigned char buzzerType);
};

#endif
