#ifndef     _SONY_H_
#define     _SONY_H_

#include "reader/Reader.h"
#include "sam/SAM.h"
#include "card/Card.h"
#include "model/constants/ErrorConstants.h"

class Card;

#define		DEBUG

class SonyReader : public Reader {
public:
	SonyReader();
	long initializeReader(int readerIndex);
	long sendToCard(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int cardType);
	long sendToSAM(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int SAMType);
	long disconnectReader();
	long detectCard(int *cardType);
	long detectInstantCard(int *cardType);
	bool isCardRemoved();
	bool stopDetection();
	long DisconnectFeliCaCard(void);
	long changeObject (SAM *sam);
	bool cancelDetection = false;
	bool cancel_thread = false;

	ErrorConstants GetBalance(Card *card, long *amount);
	ErrorConstants GetCardDetails(Card *card, CardDetail *cardDetail);
	ErrorConstants GetGenericData(Card *card, GenericData *genericData);
	ErrorConstants ActivateCard(Card *card, CardActivation *cardActivation);
	ErrorConstants UpdatePersonalData(Card *card, PersonalData *personalData);
	ErrorConstants UpdateVehicleData(Card *card, VehicleData *vehicleData);
	ErrorConstants UpdateCardStatus(Card *card, CardStatus *cardStatus);
	ErrorConstants Pay(Card *card, Activity *activity);
	ErrorConstants Recharge(Card *card, Activity *activity);
	ErrorConstants PurchaseTicket(Card *card, Ticket *ticket);
	ErrorConstants GetTicket(Card *card, Ticket *ticket);
	ErrorConstants ActivatePass(Card *card, Pass *pass);
	ErrorConstants UsePass(Card *card, Pass *pass);
	ErrorConstants GetPass(Card *card, Pass *pass,PassCriteria *passCriteria);
	ErrorConstants GetTransactions(Card *card,TransactionsList *transactionsList,TransactionsCriteria *transactionsCriteria);
	ErrorConstants EncryptData(Card *card, unsigned char data[16]);
private:
	// PC/SC context
	static SCARDCONTEXT g_hContext;
	static SCARDHANDLE	g_hCard;
	static SCARDHANDLE	g_hSAM;

	// Buffer to store PC/SC Reader Names
	static LPTSTR g_mszReaders;

	static const int slot1 = 1;
	static const int slot2 = 2;
	static SAM *samRefSlot1;
	static SAM *samRefSlot2;
	SAM* initializeSAM();
	bool isCardPresent();
	
};

#endif
