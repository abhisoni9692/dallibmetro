#ifndef     _FEL_SAM_
#define     _FEL_SAM_

#include <stdlib.h>
#include "openssl/aes.h"
#include "openssl/rand.h"
#include "sam/SAM.h"
#include "reader/Reader.h"

#define SAM_COMMAND_CODE_POLLING					0x80
#define SAM_RESPONSE_CODE_POLLING					0x81

#define SAM_COMMAND_CODE_MUTUAL_AUTH_RWSAM			0xe2
#define SAM_COMMAND_CODE_MUTUAL_AUTH_V2_RWSAM		0xe4
#define SAM_SUB_COMMAND_CODE_MUTUAL_AUTH_V2_RWSAM	0x80
#define SAM_COMMAND_CODE_MUTUAL_AUTH_V2				0x96
#define SAM_COMMAND_CODE_MUTUAL_AUTH				0x86
#define SAM_SUB_COMMAND_CODE_REQUEST_SERVICE_V2		0x8A
#define SAM_SUB_COMMAND_CODE_REGISTER_AREA_V2		0x02
#define SAM_SUB_COMMAND_CODE_REQUEST_SERVICE_V2_RWSAM	0x8A

#define SAM_COMMAND_CODE_REGISTER_AREA_v2			0xB6
#define SAM_COMMAND_CODE_REGISTER_AREA				0xC2
#define SAM_COMMAND_CODE_REQUEST_SERVICE_V2_EX		0xD2
#define SAM_COMMAND_CODE_REQUEST_SERVICE			0x82
#define SAM_COMMAND_CODE_POLLING					0x80
#define SAM_RESPONSE_CODE_POLLING					0x81

#define SAM_RESPONSE_CODE_READ_WO_ENC				0x99

#define SAM_COMMAND_CODE_WRITE						0x8a
#define SAM_COMMAND_CODE_WRITE_WO_ENC				0x9a
#define SAM_COMMAND_CODE_READ						0x88
#define SAM_COMMAND_CODE_READ_WO_ENC				0x98

#define SAM_COMMAND_CODE_REG_ISSUE_IDEX_v2			0xB6
#define SAM_COMMAND_CODE_REG_ISSUE_IDEX				0xC6
#define SAM_COMMAND_CODE_REG_ISSUE_ID				0xC0
#define SAM_SUB_COMMAND_CODE_REGISTER_SERVICE_V2	0x04
#define SAM_COMMAND_CODE_REGISTER_SERVICE_V2		0xB6
#define SAM_COMMAND_CODE_REGISTER_SERVICE			0xC4

using namespace std;

void _calc_mac(	unsigned char key[16], unsigned char msg[], unsigned long size, unsigned char mac[16]);
void encrypt_payload (unsigned char commandCode, unsigned char subCommandCode, 
						  unsigned long felicaCmdParamsLen, unsigned char felicaCmdParams[],
						  unsigned char payload[], unsigned char mac[8]);	
long _decrypt_sam_response(unsigned char samResponse[], unsigned int samResLen, unsigned char plainPackets[]);
long AskFeliCaCmdToSAM(unsigned char commandCode, 
				   unsigned long felicaCmdParamsLen,
				   unsigned char felicaCmdParams[],
				   unsigned long* felicaCommandLen,
				   unsigned char felicaCommand[],
				   Reader *reader);
long AskFeliCaCmdToSAMSC(unsigned char commandCode, 
				   unsigned char subCommandCode, 
				   unsigned long felicaCmdParamsLen,
				   unsigned char felicaCmdParams[],
				   unsigned long* felicaCommandLen,
				   unsigned char felicaCommand[],
				   Reader *reader);
long SendRWSAMCmd(unsigned char commandCode,
			  unsigned char subcommndCode,
			  unsigned long felicaCmdParamsLen,
			  unsigned char felicaCmdParams[],
			  unsigned long* felicaCommandLen,
			  unsigned char felicaCommand[],
			  Reader *reader);
long SendPollingResToSAM(unsigned long felicaResLen, 
					 unsigned char felicaResponse[],
					 Reader *reader);
long SendAuth1V2ResultToSAM(unsigned long felicaResLen, 
						unsigned char felicaResponse[], 
						unsigned long* auth2V2CommandLen,
						unsigned char auth2V2Command[],
						Reader *reader);
long SendCardResultToSAM(unsigned long felicaResLen, 
					 unsigned char felicaResponse[], 
					 unsigned long* resultLen, 
					 unsigned char result[],
					 Reader *reader);
long SendCardErrorToSAM( unsigned long* resultLen, 
					 unsigned char result[],
					 Reader *reader);

void update_SNR_For_NextCommand( Reader* reader);
	long hasSyntaxErrorInSamResponse(unsigned char samResBuf[], unsigned long* samResLen);
	long checkAndHandleSyntaxErrorInSamResponse(unsigned char _sam_res[], unsigned long* _sam_res_len,Reader *reader);
	long checkAndHandleForSNROverFlow(Reader *reader);
class FelicaSAM : public SAM {
public:
	FelicaSAM();
	long hostAuthentication(Reader *reader,bool AuthOnlyFlag=false);
	long getVersion(Reader *reader);


private:
	static unsigned char HOST_AUTH_KEY_NO;
	
	long _DisconnectWithSAMInDirectMode(void);
	long _send_Auth1(unsigned char samResBuf[], unsigned long* samResLen, Reader *reader);
	long _send_SetNormalMode(unsigned char samResBuf[], unsigned long* samResLen, Reader *reader);
	long _send_Attention(unsigned char samResBuf[], unsigned long* samResLen, Reader *reader);
	long _check_Auth1_result(unsigned char samResBuf[], unsigned long samResLen);
	long _send_Auth2(unsigned char samResBuf[], unsigned long* samResLen, Reader *reader);
	long _check_Auth2_result(unsigned char samResBuf[], unsigned long samResLen);
long _get_error_reason(unsigned char samResBuf[], unsigned long* samResLen, Reader *reader);
};

#endif