#ifndef     _DES_SAM_
#define     _DES_SAM_

#include <stdlib.h>
#include "openssl/aes.h"
#include "openssl/rand.h"
#include "sam/SAM.h"
#include "reader/Reader.h"

using namespace std;

class MifareAV2SAM : public SAM {
public:
	MifareAV2SAM();
	long hostAuthentication(Reader *reader,bool AuthOnlyFlag=false);
	long getVersion(Reader *reader);
private:
	static unsigned char HOST_AUTH_KEY_NO;
};

#endif