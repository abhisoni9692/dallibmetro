#ifndef     _SAM_H_
#define     _SAM_H_ 

#include "reader/Reader.h"

class SAM {
public:
	static const int unknownSAM = 0;
	static const int MIFARE_AV2_SAM = 1;
	static const int FELICA_RCS500_SAM = 2;
	int type = unknownSAM;
	static SAM* getInstance(int type);
	virtual long hostAuthentication(Reader *reader,bool AuthOnlyFlag=false) = 0;
	virtual long getVersion(Reader *reader) = 0;
};

#endif