*******************************************************************************
FeliCa Standard and RW-SAM Sample Application

Copyright 2013 Sony Corporation
*******************************************************************************

<Important Notice>
This sample application is for learning purpose only. You will need to modify 
the source code according to your environment. Please note that this sample 
application implements mostly nominal case so you may want to add adequate 
exception handling depending on your purpose.


<Terms and conditions>
This sample application is provided to you "as is" without warranties or 
conditions of any kind, whether oral or written, express or implied.

<History>
2013/12/10 Initial creation
2013/12/12 Reader_PC.c Modified to support bigger data more than 128 bytes in TransmitDataToFeliCaCard()
2013/12/12 FeliCa.c Fixed Authentication 2 response check in MutualAuthV2WithFeliCa()


--- Contents ---------------------
1. Required System Environment
2. File Organization
3. Program Structure
4. Command Sequence
----------------------------------


1. Required System Environment
 Software
   - Microsoft Visual C++ 2010 Express
     http://www.microsoft.com/visualstudio/eng/downloads#d-2010-express
   
   - NFC Port Software (for SONY RC-S380)
     http://www.sony.net/Products/felica/business/products/RC-S380.html#Driver

   - PC/SC Contact Reader/Writer driver
      Ex.) PC USB-TR driver from Gemalto
           http://support.gemalto.com/index.php?id=pc_usb_tr_and_pc_twin

   - OpenSSL
     http://www.openssl.org/source/
       or there are number of organizations that provides pre-compiled library for Windows
       such as http://slproweb.com/products/Win32OpenSSL.html
       Please place necessary files under "openssl" folder.
       
       |
       *-- openssl
            | 
            *-- include   Place all the header files about SSL.
            | 
            *-- lib       Place static libraries "libeay32MDd.lib" and "ssleay32MDd.lib".
 

 <<NOTE>>
	   

 Hardware
   - Sony RC-S380 (USB NFC Reader)
   - SONY RC-S888/RC-S100 (FeliCa Standard Card)
   - SONY RC-S251/RC-S500 (FeliCa RW-SAM Card)
   - PC/SC contact reader/writer (ex. Gemalto PC-USB-TR)
   - Windows PC (Windows 7 x86 or x64 is recommended)
   
   
2. File Organization

<ROOT>
  |
  Windows <DIR>
  |
  *-- src <DIR>                     C source codes
  |
  *-- include <DIR>                 C header files
  |
  *-- openssl <DIR>                 OpenSSL header files and static libraries
  | 
  *-- RCS500SampleApp.sln           Solution file for Visual Studo 2010
  |
  *-- Doxyfile						Doxygen definition file
  |
  *-- readme.txt					This file
  

  

3. Program Structure

Main-PC.c:			Application logic of this sample code.
SAM.c: 				SAM access functions.
FeliCa.c:			FeliCa access functions.
Reader_PC.c:		IC Card Reader Access functions for PASORI(SONY RC-S380)
Util.c/Util_PC.c:	Utility functions

Main-PC.c defines application logic, it calls functions defined in SAM.c, 
FeliCa.c or Reader_PC.c.

Felica.c provides FeliCa command execution functions such as
 MutualAuthWithFeliCa or ReadDataWOEnc.
These functions call SAM access function defined in SAM.c to generate FeliCa 
command packets first.

SAM.c provides SAM command execution functions such as AskFeliCaCmdToSAM or 
SendCardResultToSAM.
These functions generate command packets sent to SAM and call IC card access 
function defined in Reader_PC.c

Reader_PC.c provides both contact/contactless IC Card access functions such 
as DetectFeliCaCard, TransmitDataToFeliCaCard or TransmitDataToSAM.
Contact IC Card access function receives APDU packets starts with CLA, INS, 
... and sends it to a card.
Contactless IC Card access function receives "FeliCa Command" starts with 
"Command Code" of FeliCa command and wraps it with "Reader Control Command"
 to send it to a FeliCa card.

Once FeliCa.c function gets FeliCa command packet from SAM, it sends command 
packets to a FeliCa card using Reader_PC.c and receives FeliCa card response.
Then, sends it to the SAM using SAM.c to decrypt or verify FeliCa response.


4. Command Sequence

4-1. Payment
 Polling
 Read without encryption (Service 4097) for balance inquiry
 Authentication1+2 (Area 4080, Area 4081, Service 4092, and Service 408C)
 Read (Service 4092) to get execution ID and balance
 Write (Service 4092) for decrement and (Service 408C) for history record
 Read (Service 408C) for history inquiry

4-2. Charge
 Polling
 Read without encryption (Service 4097) for balance inquiry
 Authentication1+2 (Area 4080, Area 4081, Service 4090, and Service 408C)
 Read (Service 4090) to get block data
 Write (Service 4090) for top-up and (Service 408C) for history record
 Read (Service 408C) for history inquiry

4-3. Balance
 Polling
 Read without encryption (Service 4097) for balance inquiry
 Read without encryption (Service 408F) for history inquiry

EOF
