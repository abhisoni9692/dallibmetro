const ipcMain  = require('electron').ipcMain;
const electron = require('electron');
const url = require('url');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
var mainWindow = null;
var path=require('path');
var bindingPath = path.join(__dirname, '/build', '/Release', '/posappnode');
var binding = require(bindingPath);
// Quit when all windows are closed
app.on('window-all-closed', function () {
 	app.quit();
});

app.on('ready', function () {
	//var screen = require(‘screen’);
	//var size = screen.getPrimaryDisplay().size
	//var height = size.height;
	//var width = size.width;
	mainWindow = new BrowserWindow({
		// name: "Doe-toll",
		// 'web-preferences':{'web-security':false},
		height: 800,
		width: 1280,
		// 'max-width':1280,
		// 'max-height':800,
		// fullscreen:false,
		// kiosk:false,
		show:true,
		// transparent:true,
		// resizable: false,
		// frame: false,
		// centre:false,
		// webPreferences: {
		// zoomFactor:0.0
		// }
	});

	 mainWindow.loadURL(url.format ({
      pathname: path.join(__dirname, '/views/index.html'),
      protocol: 'file:',
      slashes: true
   }));

});

ipcMain.on('readerData',function(event,data){
	console.log('data inside electron '+data);
	initializeReader(data);
})
var handleResponse = function(response) {
	if(response.error) {
		console.log(response.error);
	} else {
		console.log('no error');
		console.log(response);
	}
}

function initializeReader(data) {
binding.initializeReader(data, handleResponse);
}
// 	var handleResponse = function(response) {
// 						if(response.error) {
// 							console.log(response.error);
// 						} else {
// 							console.log('no error');
// 							console.log(response);
// 						}
// 					}
// initializeReader({
// 				'readerType':"SCSPRO_READER",
// 				'readerIndex':0}); 

// ipcMain.on('obj:item',function(e,obj) {
// 	console.log("obj"+obj);
// });
// app.post('/initialize', function (req, res) {
// 	var obj = req.body;
// 	console.log(obj.reader);
// 	var ret = binding.initializeReader({
// 				'readerType':"SCSPRO_READER",
// 				'readerIndex':0});
// 	console.log(ret);
// 	if(ret != 'success') {
// 		res.status(400).send(ret);
// 	} else {
// 	    res.status(200).send();
// 	}
// });
// const binding = require('./build/Release/binding');

// var data1 = {
// 				'readerType':"SCSPRO_READER",
// 				'readerIndex':0,
// 				'firstName': 'Sainath',
// 				'middleName': 'Sai',
// 				'lastName': 'Kommu',
// 				'gender': 'M',
// 				'dateOfBirth':1234567,
// 				'mobile': 9642296713,
// 				'aadhaar':123456789123,
// 				'vehicleType':1,
// 				'vehicleNumber':'AP04AM0722',
// 				'status' : 3,
// 				'posTerminalId' : 'DOE5',
// 				'timeInMilliSeconds':3546047656123
// 			};
// var data2 = {
// 				'readerType':"SCSPRO_READER",
// 				'readerIndex':0,
// 				// 'firstName': 'Sainath',
// 				// 'middleName': 'Sai',
// 				// 'lastName': 'Kommu',
// 				// 'gender': 'M',
// 				// 'dateOfBirth':1234567,
// 				// 'mobile': 9642296713,
// 				// 'aadhaar':123456789123
// 				// 'vehicleType':1,
// 				// 'vehicleNumber':'AP04AM0722'
// 			};
// var data3 = {
// 				'readerType':"SCSPRO_READER",
// 				'readerIndex':0,
// 				'amount':5,
// 				'action':'recharge',
// 				'serviceType': 1,
// 				'posTerminalId':'DOE5',
// 				'srcBranchId':'DOE5',
// 				'destBranchId':'DOE4',
// 				'periodicity':[{'limitPeriodicity': 2,'limitsMaxCount':15},{'limitPeriodicity' : 3 ,'limitsMaxCount':15}],
// 				'expiryDate': 1519363470,//(  Friday, February 23, 2018 10:54:30 AM GMT+05:30)
// 				'expiryTimeInMilliSeconds' : 1519363470123,//(Friday, February 23, 2018 10:54:30.123 AM GMT+05:30)
// 				'timeInMilliSeconds':1516123458123,//( Friday, February 23, 2018 10:53:55.123 AM GMT+05:30)
// 				'passType': 5,
// 				'maxTrips' : 30,
// 				'partnerTxnId': 9876543210,
// 				'isRenewal':false
// 			};




// function disconnectReader(data) {
// 	binding.disconnectReader(data, handleResponse);
// }
// function getCardDetails(data) {
// 	binding.getCardDetails(data, handleResponse);
// }
// function pay(data){
// 	binding.recharge(data, function(response) {
// 		if(response.error) {
// 			console.log(response.error);
// 		} else {
// 			console.log('no error');
// 		}
// 	});
// }

// function getTicket(data){
// 	binding.getTicket(data, function(response) {
// 		if(response.error) {
// 			console.log(response.error);
// 		} else {
// 			console.log('no error');
// 		}
// 	});
// }

// function purchaseTicket(data){
// 	binding.purchaseTicket(data, function(response) {
// 		if(response.error) {
// 			console.log(response.error);
// 		} else {
// 			console.log('no error');
// 		}
// 	});
// }

// initializeReader(data2);
// // purchaseTicket(data3);
// getTicket(data3);
// // getCardDetails(data1);
// //pay(data3);
// // initializeReader({
// // 				'readerType':"SCSPRO_READER",
// // 				'readerIndex':"1"});
// // disconnectReader({
// // 				'readerType':"1",
// // 				'readerIndex':"0"});
// // disconnectReader({
// // 				'readerType':"1",
// // 				'readerIndex':"1"});
// // binding.usePass(data1, function(response) {
// // 	if(response.error) {
// // 		console.log(response.error);
// // 	} else {
// // 		console.log('no error');
// // 	}
// // });



// // initializeReader({
// // 				'readerType':"SCSPRO_READER",
// // 				'readerIndex':1});

// // disconnectReader({
// // 				'readerType':"1",
// // 				'readerIndex':"0"});

// // disconnectReader({
// // 				'readerType':"1",
// // 				'readerIndex':"1"});
// // getCardDetails({
// // 				'readerType':"ALL",
// // 				'readerIndex':-1});