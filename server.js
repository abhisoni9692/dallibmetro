var express = require('express');
var bodyParser = require('body-parser')
var app = express();
const path  = require('path');
const views = path.join(__dirname, 'views');
const binding = require('./build/Release/posappnode');
app.use(bodyParser.json())
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.get('/', function (req, res)
{
    res.sendFile('index.html', { root : views });
});
app.get('/balance', function (req, res)
{
	res.setHeader('Content-Type', 'application/json');
	var readerType = req.query.reader;
	var obj =  {
		readerType: 'BUAROH_READER',
		readerIndex: 0 
	}
	console.log("readerType:"+readerType)
	binding.getBalance(obj, function(response) {
		console.log(response);
		if(typeof  response.error != 'undefined') {
			res.status(400).send(response.error);
		} else {
			var obj = {};
		    obj['amount'] = response.amount;
		    res.status(200).send(JSON.stringify(obj));
		}
	});
});
app.post('/initialize', function (req, res)
{	
	var obj = req.body;
	console.log('here is obj ',obj);
	// if(obj.readerIndex == 0){
	// 	obj.readerType = "ALL"
	// //	obj.readerIndex = 0;
	// }
	if(obj.readerIndex == 1){
		obj.readerType = "SCSPRO_READER"
		obj.readerIndex = 0;
	}
	if(obj.readerIndex == 2){
		obj.readerType = "SONY_READER"
		obj.readerIndex = 0;
	}
	if(obj.readerIndex == 3){
		obj.readerType = "BUAROH_READER"
		obj.readerIndex = 0;
	}
	if(obj.readerIndex == 4){
		obj.readerType = "BUAROH_READER"
		obj.readerIndex = 1;
	}
	
	binding.initializeReader(obj, function(response) {
		console.log(response);
		if(typeof  response.error != 'undefined') {
			res.status(400).send(response.error);
		} else {
			res.status(200).send();		

			binding.isCardRemoved(obj, function (argument) {
				// body...
				console.log("remove result ", argument);
			})
			binding.stopDetection(obj, function (arg) {
				// body...
				console.log("stop  result ", arg);
			})
		}
	});
});


app.post('/getGeneticData', function (req, res)
{	
	var obj = req.body;
	console.log('here is obj ',obj);
	if(obj.readerIndex == 0){
		obj.readerType = "ALL",
		obj.readerIndex = 0;
	}
	// if(obj.readerIndex == 0){
	// 	obj.readerType = "SCSPRO_READER",
	// 	obj.readerIndex = 0;
	// }
	// if(obj.readerIndex == 3){
	// 	obj.readerType = "BUAROH_READER",
	// 	obj.readerIndex = 1;
	// }
	// if(obj.readerIndex == 4){
	// 	obj.readerType = "BUAROH_READER",
	// 	obj.readerIndex = 2;
	// }
	var rmobj =  {
		readerType: 'SCSPRO_READER',
		readerIndex: 0 
	}
	console.log('here is modified obj ',obj);
	console.log("before promise");

	//Promise.resolve(1).then(function() {
		binding.getGenericData(rmobj, function(response) {
			console.log(response);
			if(typeof  response.error != 'undefined') {
				res.status(400).send(response.error);
			} else {
				console.log("card details ", response);
				
			}
		});
		//console.log("after in  promise");
	//});


console.log("after promise");

	
});

app.post('/pay', function (req, res)
{	
	var obj = req.body;

	if(obj.readerIndex == 1){
		obj.readerType = "SCSPRO_READER",
		obj.readerIndex = 0;
	}
	if(obj.readerIndex == 3){
		obj.readerType = "BUAROH_READER",
		obj.readerIndex = 1;
	}
	var paydata = {"serviceType":"TOLL",
				"action":"PAY",
				"posTerminalId":"1234",
				"amount": 1000,
				"sourceBranchId":"231",
				"destinationBranchId":"12",
				"timeInMilliSeconds": 1232343,
				"readerType":"SCSPRO_READER",
              	"readerIndex":0
			}
	console.log('here is obj ',paydata);
	binding.recharge(paydata, function(response) {
		console.log(response);
		if(typeof  response.error != 'undefined') {
			res.status(400).send(response.error);
		} else {
			var data = response;
			console.log("payment result", response);
			res.status(200).send(JSON.stringify(data));
		}
	});
});


app.post('/activateCard', function (req, res)
{	
	var obj = req.body;
	var paydata = {"serviceType":"TOLL",
				"action":"PAY",
				"posTerminalId":"1234",
				"amount": 16000,
				"sourceBranchId":"231",
				"destinationBranchId":"12",
				"timeInMilliSeconds": 1232343,
				"readerType":obj.readerType,
				"readerIndex":obj.readerIndex,
				"cardNumber":"234567890111",
				"name":"Abhishek Maheshwari",
				"mobile":9041322261,
				"aadhaar":123456789009,
				"vehicleType":"UNKNOWN",
				"vehicleNumber":"KA05M9145"
			}
	console.log('here is obj ',paydata);
	binding.activateCard(paydata, function(response) {
		console.log(response);
		if(typeof  response.error != 'undefined') {
			res.status(400).send(response.error);
		} else {
			var data = response;
			console.log("activation result", response);
			res.status(200).send(JSON.stringify(data));
		}
	});
});


app.post('/updatePersonalData', function (req, res)
{	
	var obj = req.body;
	var paydata = {"firstName":"Abhishek Hashtaag",
				"middleName":"PAY",
				"lastName":"1234",
				"gender": "MALE",
				"dateOfBirth":1234567,
				"timeInMilliSeconds": 1232343,
				"readerType":obj.readerType,
				"readerIndex":obj.readerIndex,
				"mobile":9041322261,
				"aadhaar":650965096509,
				"vehicleType":"UNKNOWN",
				"vehicleNumber":"KA05M9145"
			}
	console.log('here is obj ',paydata);
	binding.updatePersonalData(paydata, function(response) {
		console.log(response);
		if(typeof  response.error != 'undefined') {
			res.status(400).send(response.error);
		} else {
			var data = response;
			console.log("updated result", response);
			res.status(200).send(JSON.stringify(data));
		}
	});
});

app.post('/disconnect', function (req, res)
{
	var obj = req.body;
	console.log(obj.reader);
	binding.disconnectReader(obj.reader, function(response) {
		console.log(response);
		if(typeof  response.error != 'undefined') {
			res.status(400).send(response.error);
		} else {
		    res.status(200).send();
		}
	});
});

app.post('/testSNRoverflow', function (req, res)
{
	var i=0
	var obj = req.body;
	console.log(obj.reader);
	function recurse(){
	binding.getGenericData(obj, function(response) {
		console.log(response);
		i+=1
		if(i<2147483650){
		recurse();
	}else{
		res.status(200).send();
	}
	});
}
recurse();
});

app.listen(3000);
console.log('Server running at http://127.0.0.1:3000/');