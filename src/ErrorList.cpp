#include "../include/ErrorList.h"
#include <v8.h>

using v8::Local;
using v8::Array;
using v8::Object;
using v8::String;
using v8::Isolate;
using v8::Handle;
using v8::Value;

ErrorList::ErrorList() {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList::errors = Array::New(isolate);
}

void ErrorList::add(Handle<Value> path, Handle<Value> value, Handle<Value> message) {
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> errorDetail = Object::New(isolate);
	errorDetail->Set(String::NewFromUtf8(isolate, "path"), path);
	errorDetail->Set(String::NewFromUtf8(isolate, "value"), value);
	errorDetail->Set(String::NewFromUtf8(isolate, "message"), message);
	ErrorList::errors->Set(ErrorList::errors->Length(), errorDetail);
}

void ErrorList::addAll(ErrorList *errorList) {
	this->addAll(errorList->getErrors());
}

void ErrorList::addAll(Local<Array> newErrors) {
	int index = ErrorList::errors->Length();
	int length = newErrors->Length();
	for(int i=0; i<length; i++){
		ErrorList::errors->Set(index++, newErrors->Get(i));
	}
}

Local<Array> ErrorList::getErrors() {
	return errors;
}

int ErrorList::Size() {
	return errors->Length();
}