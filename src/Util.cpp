#include "Util.h"
#include <cstring>
#include <iostream>
#include <cctype>
#include <cstdlib>
#include <ctime>
#include <chrono>

unsigned int CharArrayToInt(unsigned char in[], 
							unsigned int len)
{
	unsigned int result = 0;

	for (int i = 0; i<len; i++)
	{
		result += in[i];
		if (i != len-1)
		{
			result =result << 8;
		}
	}

	return result;
}

long long CharArrayToLong(unsigned char in[], 
							int len)
{
	long long result = 0;

	for (int i = 0; i<len; i++)
	{
		result += in[i];
		if (i != len-1)
		{
			result =result << 8;
		}
	}

	return result;
}

void IntToCharArrayToBytes(int in,
	unsigned char out[2])
{
	memset(out, 0, 2);
	out[1] = (unsigned char)in;
	in = in >> 8;
	out[0] = (unsigned char)in;
}

void IntToCharArray(unsigned int in,
					unsigned char out[4])
{
	memset(out, 0, 4);
	out[3] = (unsigned char)in;
	in = in>> 8;
	out[2] = (unsigned char)in;
	in = in>> 8;
	out[1] = (unsigned char)in;
	in = in>> 8;
	out[0] = (unsigned char)in;
}


int CharArrayToIntLE(unsigned char in[], 
							unsigned int len)
{
	int result = 0;

	for (int i = len-1; i>=0; i--)
	{
		result += in[i];
		if (i != 0)
		{
			result =result << 8;
		}
	}

	return result;
}


void IntToCharArrayLE(unsigned int in,
					unsigned char out[4])
{
	memset(out, 0, 4);
	out[0] = (unsigned char)in;
	in = in>> 8;
	out[1] = (unsigned char)in;
	in = in>> 8;
	out[2] = (unsigned char)in;
	in = in>> 8;
	out[3] = (unsigned char)in;
}


bool Equals(unsigned char a[], int sizeOfa, unsigned char b[], int sizeOfb) {
	if (sizeOfa != sizeOfb) {
		return false;
	}
	for (int i = 0; i<sizeOfa; i++) {
		if (a[i] != b[i])
			return false;
	}
	return true;
}

void longtobytes6(long long var, unsigned char out[]) {
	memset(out, 0, 6);
	out[5] = (unsigned char)var;
	var = (var >> 8) ;
	out[4] = (unsigned char)var;
	var = (var >> 8) ;
	out[3] = (unsigned char)var;
	var = (var >> 8);
	out[2] = (unsigned char)var;
	var = (var >> 8) ;
	out[1] = (unsigned char)var;
	var = (var >> 8) ;
	out[0] = (unsigned char)var;
}

void getdateFromEpochTime(long long now,int* date,int* month,int* year,int* hour)
{
	long long now1  = now;
	int digits =0;
	while(now1>0)
	{
		now1=now1/10;
		digits++;
	}
	printf("now:: %lld\n", now);
	if(digits == 13 || digits == 10){
    	if(digits==13) {
    	    now = now / 1000;
    	}
    	std::cout <<digits<<std::endl;
        std::cout<<now<<std::endl;
        struct tm ts = *localtime(&(const time_t )now);
        *month = ts.tm_mon;
        *date  = ts.tm_mday;
        *year  = ts.tm_year;
        *hour  = ts.tm_hour;
        std::cout << " month - " << *month;
        std::cout << " date - " << *date;
        std::cout << " year - " <<*year ;
        std::cout << " hour - " << *hour<<std::endl;
	}
    else{
        std::cout<<"ERROR\n"<<std::endl;
    }

}

long long trimMilliseconds(long long timeInMilliSec)
{
	long long now1  = timeInMilliSec;
	int digits =0;
	while(now1>0)
	{
		now1=now1/10;
		digits++;
	}
	printf("now:: %lld\n", timeInMilliSec);
	if(digits == 13 || digits == 10){
    	if(digits==13) {
    	    timeInMilliSec = timeInMilliSec / 1000;
    	}	
    }
    return timeInMilliSec;
}

unsigned long long CharArrayTolonglong(unsigned char in[],
	unsigned int len)
{
	unsigned long long result = 0;
	int i;

	for (i = 0; i<len; i++) {
		result += in[i];
		if (i != len - 1) {
			result = result << 8;
		}
	}

	return result;
}

void IntToCharArrayTwelveDigit(long long in,
	unsigned char out[5])
{
	memset(out, 0, 5);
	out[4] = (unsigned char)in;
	in = in >> 8;
	out[3] = (unsigned char)in;
	in = in >> 8;
	out[2] = (unsigned char)in;
	in = in >> 8;
	out[1] = (unsigned char)in;
	in = in >> 8;
	out[0] = (unsigned char)in;
}

void IntToCharArrayDigit(long long in,
	unsigned char out[6])
{
	memset(out, 0, 6);
	out[5] = (unsigned char)in;
	in = in >> 8;
	out[4] = (unsigned char)in;
	in = in >> 8;
	out[3] = (unsigned char)in;
	in = in >> 8;
	out[2] = (unsigned char)in;
	in = in >> 8;
	out[1] = (unsigned char)in;
	in = in >> 8;
	out[0] = (unsigned char)in;
}
