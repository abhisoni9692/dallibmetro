#include <cstdio>
#include <conio.h>
#include <cstdarg>
#include <ctime>
#include "openssl/rand.h"
#include "Util.h"

unsigned char to_bcd(int value);

void PrintText (char* text, ...)
{
    va_list args;
    va_start(args, text);
	vprintf(text, args);
	printf("\n");
    va_end(args);
}

int GetNumericValue()
{
	unsigned int val;
	/*int ret;
	char c;
	c = getch();
	printf("%c\n", c);
	val = c - 48;*/
	scanf("%d", &val);

	return val;
}


void GetTimeAtBCD(unsigned char* timeData)
{
	struct tm date;

	time_t timer = time(NULL);
	localtime_s(&date, &timer);
	
	timeData[0] = to_bcd(date.tm_mday);
	timeData[1] = to_bcd(date.tm_mon+1);
	int y0 = (date.tm_year+1900)/100;
	int y1 = (date.tm_year+1900)%100;
	timeData[2] = to_bcd(y0);
	timeData[3] = to_bcd(y1);
	timeData[4] = to_bcd(date.tm_hour);
	timeData[5] = to_bcd(date.tm_min);
	timeData[6] = to_bcd(date.tm_sec);
}

void GenerateRandom(unsigned char *buf,int num)
{
	RAND_bytes(buf, num);
}



void PrintHexArray(char* header, unsigned long len, unsigned char byte_array[])
{
	printf("%s", header);

	for (unsigned long i = 0; i<len; i++)
	{
		printf("%02X ", byte_array[i]);
	}
	printf("\n");
}

unsigned char to_bcd(int value)
{
	unsigned char bcd = 0;

	int d0 = value/10 << 4;
	int d1 = value%10;

	bcd = d0 + d1;

	return bcd;
}
