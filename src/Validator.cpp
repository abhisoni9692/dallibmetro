#include <string>
#include "../include/Validator.h"
#include "model/utils/ConversionUtil.h"
#include "../include/model/enum/comparison.h"
#include "ErrorList.h"
#include <v8.h>

using v8::Local;
using v8::Array;
using v8::Object;
using v8::String;
using v8::Isolate;
using v8::Handle;
using v8::Value;

bool Validator::Defined(Local<Value> value, const char *path, const char *message, ErrorList *errorList) {
	if (value->IsUndefined()) {
		Isolate* isolate = Isolate::GetCurrent();
		addToErrorList(String::NewFromUtf8(isolate, "undefined"), path, message, errorList);
		return false;
    }
    return true;
}

bool Validator::NotNull(Local<Value> value, const char *path, const char *message, ErrorList *errorList) {
	if (value->IsNull() || value->IsUndefined()) {
		Isolate* isolate = Isolate::GetCurrent();
		addToErrorList(String::NewFromUtf8(isolate, "null"), path, message, errorList);
		return false;
    }
    return true;
}

bool Validator::Number(Local<Value> value, const char *path, const char *message, ErrorList *errorList) {
	if(!value->IsUndefined() && !value->IsNull()) {
		if (!value->IsNumber()) {
			addToErrorList(value, path, message, errorList);
			return false;
	    }
	}
    return true;
}

bool Validator::NotNullNumber(Local<Value> value, const char *path, const char *message, ErrorList *errorList) {
	if (!value->IsNumber()) {
		addToErrorList(value, path, message, errorList);
		return false;
    }
    return true;
}

bool Validator::String(Local<Value> value, const char *path, const char *message, ErrorList *errorList) {
	if(!value->IsUndefined() && !value->IsNull()) {
		if (!value->IsString()) {
			addToErrorList(value, path, message, errorList);
			return false;
	    }
	}
    return true;
}

bool Validator::NotNullString(Local<Value> value, const char *path, const char *message, ErrorList *errorList) {
	if (!value->IsString()) {
		addToErrorList(value, path, message, errorList);
		return false;
    }
    return true;
}

bool Validator::Object(Local<Value> value, const char *path, const char *message, ErrorList *errorList) {
	if(!value->IsUndefined() && !value->IsNull()) {
		if (!value->IsObject()) {
			addToErrorList(value, path, message, errorList);
			return false;
	    }
	}
    return true;
}

bool Validator::NotNullObject(Local<Value> value, const char *path, const char *message, ErrorList *errorList) {
	if (!value->IsObject()) {
		addToErrorList(value, path, message, errorList);
		return false;
    }
    return true;
}

bool Validator::Function(Local<Value> value, const char *path, const char *message, ErrorList *errorList) {
	if(!value->IsUndefined() && !value->IsNull()) {
		if (!value->IsFunction()) {
			addToErrorList(value, path, message, errorList);
			return false;
	    }
	}
    return true;
}

bool Validator::NotNullFunction(Local<Value> value, const char *path, const char *message, ErrorList *errorList) {
	if (!value->IsFunction()) {
		addToErrorList(value, path, message, errorList);
		return false;
    }
    return true;
}

bool Validator::Bool(Local<Value> value, const char *path, const char *message, ErrorList *errorList) {
	if(!value->IsBoolean()){
		addToErrorList(value, path, message, errorList);
		return false;
	}
	return true;
}

bool Validator::NumberOfDigits(Local<Value> value, const char *path, const char *message,const int digitCount,Comparison checkType, ErrorList *errorList){
	switch (checkType)
	{
	case Comparison::EQUAL_TO:
		if(std::to_string((long long)value->NumberValue()).length()==digitCount){
			return true;
			break;
		}
		addToErrorList(value,path,message,errorList);
		return false;
		break;

	case Comparison::LESS_THAN_EQUAL_TO:
		if(std::to_string((long long)value->NumberValue()).length()<=digitCount){
			return true;
			break;
		}
		addToErrorList(value,path,message,errorList);
		return false;
		break;
	case Comparison::GREATER_THAN_EQUAL_TO:
		if(std::to_string((long long)value->NumberValue()).length()<=digitCount){
			return true;
			break;
		}
		addToErrorList(value,path,message,errorList);
		return false;
		break;
	}
}

bool Validator::LengthOfString(Local<Value> value, const char *path, const char *message,const int digitCount, Comparison checkType, ErrorList *errorList){
	switch (checkType)
	{
	case Comparison::EQUAL_TO:
	if(ConversionUtil::toStdString(value ->ToString()).length()==digitCount){
		return true;
		break;
	}
	addToErrorList(value,path,message,errorList);
	return false;
	break;
	case Comparison::LESS_THAN_EQUAL_TO:
	if(ConversionUtil::toStdString(value->ToString()).length()<=digitCount){
		return true;
		break;
	}
	addToErrorList(value,path,message,errorList);
	return false;
	break;
	case Comparison::GREATER_THAN_EQUAL_TO:
	if(ConversionUtil::toStdString(value->ToString()).length()>=digitCount){
		return true;
		break;
	}
	addToErrorList(value,path,message,errorList);
	return false;
	break;

	}	
}
void Validator::addToErrorList(Local<Value> value, const char *path, const char *message, ErrorList *errorList) {
	Isolate* isolate = Isolate::GetCurrent();
	errorList->add(
		String::NewFromUtf8(isolate, path), 
		value->ToString(), 
		String::NewFromUtf8(isolate, message)
	);
}