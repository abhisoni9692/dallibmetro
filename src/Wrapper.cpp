#include <cstdio>
#include <thread>
#include <iostream>
#include <node.h>
#include <string>
#include <uv.h>

#include "reader/Reader.h"
#include "model/Activity.h"
#include "model/Balance.h"
#include "model/CardRemove.h"
#include "model/CardDetail.h"
#include "model/CardActivation.h"
#include "model/PersonalData.h"
#include "model/GenericData.h"
#include "model/VehicleData.h"
#include "model/CardStatus.h"
#include "model/Ticket.h"
#include "model/Pass.h"
#include "model/ErrorMessage.h"
#include "Adapter.h"
#include "ErrorList.h"
#include "Validator.h"
#include "card/Card.h"
#include "model/Periodicity.h"
#include "model/utils/ConversionUtil.h"
#include "model/utils/EnumUtil.h"
#include "model/enum/ReaderType.h"
#include "model/CardStatus.h"
#include "model/PassCriteria.h"
#include "sam/SAM.h"
#include "../include/Util.h"
#include "model/constants/ErrorConstants.h"
#include <TransactionsList.h>

using thread = std::thread;

using v8::Local;
using v8::Array;
using v8::Object;
using v8::String;
using v8::Isolate;
using v8::Handle;
using v8::Value;
using v8::Persistent;
using v8::Function;

using std::string;

struct ReaderMap {
	int readerIndex;
	Reader *reader;
};

struct  Work {
	uv_work_t request;
	ReaderType readerType = ReaderType::UNKNOWN;
	int readerIndex = -1;
	Adapter *adapter = NULL;
	Card *card = NULL;
	ErrorConstants error = ErrorConstants::NO__ERROR;
	std::function <void(Work*)> f;
	Persistent<Function> callback;
	string transactionId;
};

void InitializeReader(const v8::FunctionCallbackInfo<v8::Value>& args);
void IsCardRemoved(const v8::FunctionCallbackInfo<v8::Value>& args);
void GetBalance(const v8::FunctionCallbackInfo<v8::Value>& args);
void GetCardDetails(const v8::FunctionCallbackInfo<v8::Value>& args);
void ActivateCard(const v8::FunctionCallbackInfo<v8::Value>& args);
void UpdatePersonalData(const v8::FunctionCallbackInfo<v8::Value>& args);
void UpdateVehicleData(const v8::FunctionCallbackInfo<v8::Value>& args);
void UpdateCardStatus(const v8::FunctionCallbackInfo<v8::Value>& args);
void Pay(const v8::FunctionCallbackInfo<v8::Value>& args);
void CollectFine(const v8::FunctionCallbackInfo<v8::Value>& args);
void Recharge(const v8::FunctionCallbackInfo<v8::Value>& args);
void PurchaseTicket(const v8::FunctionCallbackInfo<v8::Value>& args);
void GetTicket(const v8::FunctionCallbackInfo<v8::Value>& args);
void ActivatePass(const v8::FunctionCallbackInfo<v8::Value>& args);
void UsePass(const v8::FunctionCallbackInfo<v8::Value>& args);
void GetPass(const v8::FunctionCallbackInfo<v8::Value>& args);
void DisconnectReader(const v8::FunctionCallbackInfo<v8::Value>& args);
void GetTransactions(const v8::FunctionCallbackInfo<v8::Value>& args);
void GetGenericData(const v8::FunctionCallbackInfo<v8::Value>& args);
void StopDetection(const v8::FunctionCallbackInfo<v8::Value>& args);
void encryptData(const v8::FunctionCallbackInfo<v8::Value>& args);

void DetectCardAndGetCardInstance(Work *work);
Card* DetectCardAndGetCardInstance(ReaderType readerType, int readerIndex);
long DetectCard(Reader *reader, int *cardType);
void DetectCardInstantAndGetCardInstance(Work *work);
Card* DetectCardInstantAndGetCardInstance(ReaderType readerType, int readerIndex);
long DetectInstantCard(Reader *reader, int *cardType);
long DisconnectReader(int initializedReaderIndex, bool removeFromListEvenOnError);
Local<Object> PrepareError(const char *name, const char *message, ErrorList *errorList);
void PrepareError(const char *name, const char *message, ErrorList *errorList, Local<Object> response);
void Callback(Work *work, Local<Object> response);
void PrepareResponse(const v8::FunctionCallbackInfo<v8::Value>& args, Local<Object> response);
Local<Object> PrepareResponse(Work *work);
void PrintInitializedReader();
long GetInitializedReaderIndex(ReaderType readerType, int readerIndex);
ErrorList DoBasicValidation(const v8::FunctionCallbackInfo<v8::Value>& args);
ErrorList ValidateAndDeserialize(const v8::FunctionCallbackInfo<v8::Value>& args, Adapter *adapter, Card **card, Local<Object> response);
ErrorList ValidateAndDeserialize(const v8::FunctionCallbackInfo<v8::Value>& args, Adapter *adapter, Work *work, Local<Object> response);
ErrorList ValidateFirstArgument(const v8::FunctionCallbackInfo<v8::Value>& args);
void ValidateFirstArgument(const v8::FunctionCallbackInfo<v8::Value>& args, ErrorList *errorList);
void ValidateSecondArgument(const v8::FunctionCallbackInfo<v8::Value>& args, ErrorList *errorList);
void ValidateReaderType(Local<Object> data, ErrorList *errorList);
void ValidateReaderIndex(Local<Object> data, ErrorList *errorList);
void RunAsync(uv_work_t *request);
void RunAsyncAfter(uv_work_t *request, int status);
void CallAsync(Work *work);

// Card *TappedCardOnReader;

ReaderMap initializedReader[10];
int numberOfReadersInitialized;
Validator validator;
uv_async_t async;

void RunAsync(uv_work_t *request) {
	cout<<"Reached runAsync\n";
	Work *work = (Work*) request->data;
	// Isolate *isolate = Isolate::GetCurrent();
	// v8::HandleScope scope(isolate);
	work->f(work);
	// Callback(work, response);

	// uv_close((uv_handle_t *)handle, NULL);
}

void RunAsyncAfter(uv_work_t *request, int status) {
	v8::HandleScope scope(Isolate::GetCurrent());
	Work *work = (Work*) request->data;
	Local<Object> response = PrepareResponse(work);
	Callback(work, response);
}

void CallAsync(Work *work) {
	// async.data = (void *) work;
	// uv_async_init(uv_default_loop(), &async, RunAsync);
	// uv_async_send(&async);
	work->request.data = work;
	uv_queue_work(uv_default_loop(), &work->request, RunAsync, RunAsyncAfter);
}

void DetectCardAndGetCardInstance(Work *work) {
	work->card = DetectCardAndGetCardInstance(work->readerType, work->readerIndex);
	if(work->card == NULL) {
		printf("returnung 5\n");
		work->error = ErrorConstants::CARD_NOT_DETECTED;
	}
}//DetectCardInstantAndGetCardInstance

void DetectCardInstantAndGetCardInstance(Work *work) {
	work->card = DetectCardInstantAndGetCardInstance(work->readerType, work->readerIndex);
	if(work->card == NULL) {
		printf("returnung 5 instant\n");
		work->error = ErrorConstants::CARD_NOT_DETECTED;
	}
}

Card* DetectCardAndGetCardInstance(ReaderType readerType, int readerIndex) {
	//Reader::cardTapped = NULL;
	int cardType = -1;
	if(readerType != ReaderType::ALL) {
		int initializedReaderIndex = GetInitializedReaderIndex(readerType, readerIndex);
		if(initializedReaderIndex == -1) {
			return NULL;
		} else {
			long ret = initializedReader[initializedReaderIndex].reader->detectCard(&cardType);
			if (ret == APP_ERROR || ret == APP_CANCEL) {
				return NULL;
			}
		}
	} else if(numberOfReadersInitialized != 0) {
		std::thread *threads = new std::thread[numberOfReadersInitialized];
		for(int i=0;i<numberOfReadersInitialized;i++) {
			cout<< "ReaderType:"<<EnumUtil::ToString(initializedReader[i].reader->type)<<endl;
			threads[i] = std::thread(DetectCard,initializedReader[i].reader, &cardType);
		}
		for(int i=0;i<numberOfReadersInitialized;i++) {
			threads[i].join();
		}
	}
	return Card::GetInstance(cardType);
}

Card* DetectCardInstantAndGetCardInstance(ReaderType readerType, int readerIndex) {
	//Reader::cardTapped = NULL;
	int cardType = -1;
	if(readerType != ReaderType::ALL) {
		int initializedReaderIndex = GetInitializedReaderIndex(readerType, readerIndex);
		if(initializedReaderIndex == -1) {
			return NULL;
		} else {
			long ret = initializedReader[initializedReaderIndex].reader->detectInstantCard(&cardType);
			if (ret == APP_ERROR || ret == APP_CANCEL) {
				return NULL;
			}
		}
	} else if(numberOfReadersInitialized != 0) {
		std::thread *threads = new std::thread[numberOfReadersInitialized];
		for(int i=0;i<numberOfReadersInitialized;i++) {
			cout<< "ReaderType:"<<EnumUtil::ToString(initializedReader[i].reader->type)<<endl;
			threads[i] = std::thread(DetectInstantCard,initializedReader[i].reader, &cardType);
		}
		for(int i=0;i<numberOfReadersInitialized;i++) {
			threads[i].join();
		}
	}
	return Card::GetInstance(cardType);
}

long DetectCard(Reader *reader, int *cardType) {
	cout<<"Reader "<<reader<<endl;
	return reader->detectCard(cardType);
}

long DetectInstantCard(Reader *reader, int *cardType) {
	cout<<"Reader "<<reader<<endl;
	return reader->detectInstantCard(cardType);
}

void InitializeReader(const v8::FunctionCallbackInfo<v8::Value>& args) {
	v8::Isolate* isolate = args.GetIsolate();
	Local<Object> response = Object::New(isolate);
	ErrorList errorList = DoBasicValidation(args);
	if(errorList.Size() == 0) {
		Local<Object> data = args[0]->ToObject();
		ReaderType readerType = EnumUtil::getReaderType(ConversionUtil::toCharArray(data->Get(String::NewFromUtf8(isolate, "readerType"))->ToString()));
		printf("readerType:%d\n",readerType);
		int readerIndex = data->Get(String::NewFromUtf8(isolate, "readerIndex"))->Int32Value();
		int initializedReaderIndex = GetInitializedReaderIndex(readerType, readerIndex);
		if(initializedReaderIndex != -1) {
			DisconnectReader(initializedReaderIndex, true);	//TODO:handle DisconnectReader error 
		}
		Reader *reader = Reader::getInstance(readerType);
		long ret = reader->initializeReader(readerIndex);
		// printf("ret:%ld\n",ret);
		if(ret != APP_SUCCESS) {
			PrepareError("APP_ERROR", "internal error", &errorList, response);
		} else {
			ReaderMap readerMap;
			readerMap.readerIndex = readerIndex;
			readerMap.reader = reader;
			initializedReader[numberOfReadersInitialized] = readerMap;
			numberOfReadersInitialized++;
		}
	} else {
		PrepareError("VALIDATION_ERROR", "Errors in input", &errorList, response);
	}

	PrintInitializedReader();
	PrepareResponse(args, response);
}

void GetBalance(const v8::FunctionCallbackInfo<v8::Value>& args) {
	Local<Object> response = Object::New(args.GetIsolate());
	Work *work = new Work();
	ErrorList errorList = ValidateAndDeserialize(args, NULL, work, response);
	if(errorList.Size() == 0) {
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [](Work *work){
			DetectCardAndGetCardInstance(work);
			if(work->card != NULL) {
				long amount = -1;
				Reader *reader = Reader::cardTapped;
				work->error = reader->GetBalance(work->card, &amount);
				if(work->error == ErrorConstants::NO__ERROR) {
					Balance *balance = new Balance();
					balance->setAmount(amount);
					work->adapter = (Adapter*) balance;
				}
			}
		};
		CallAsync(work);
	} else {
		PrepareResponse(args, response);
	}
}

void GetCardDetails(const v8::FunctionCallbackInfo<v8::Value>& args) {
	Local<Object> response = Object::New(args.GetIsolate());
	Work *work = new Work();
	ErrorList errorList = ValidateAndDeserialize(args, NULL, work, response);
	if(errorList.Size() == 0){
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [](Work *work){
			DetectCardAndGetCardInstance(work);
			if(work->card != NULL){
				CardDetail *cardDetail = new CardDetail();
				Reader *reader = Reader::cardTapped;
				work->error = reader->GetCardDetails(work->card, cardDetail);
				if (work->error == ErrorConstants::NO__ERROR) {
					work->adapter = (Adapter*) cardDetail;
				}
			}
		};
		CallAsync(work);
	} else {
		PrepareResponse(args, response);
	}
}

void ActivateCard(const v8::FunctionCallbackInfo<v8::Value>& args) {
	Local<Object> response = Object::New(args.GetIsolate());
	Work *work = new Work();
	CardActivation *cardActivation = new CardActivation();
	ErrorList errorList = ValidateAndDeserialize(args, (Adapter*)cardActivation, work, response);
	if(errorList.Size() == 0){
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [cardActivation](Work *work){
			DetectCardInstantAndGetCardInstance(work);
			if(work->card != NULL){
				Reader *reader = Reader::cardTapped;
				work->error = reader->ActivateCard(work->card, cardActivation);
				work->adapter = NULL;
			}
		};
		CallAsync(work);
	} else {
		PrepareError("VALIDATION_ERROR", "Errors in input", &errorList, response);
		PrepareResponse(args, response);
	}
}

void UpdatePersonalData(const v8::FunctionCallbackInfo<v8::Value>& args) {
	Isolate *isolate = args.GetIsolate();
	Local<Object> response = Object::New(isolate);
	PersonalData *personalData = new PersonalData();
	Work *work = new Work();
	ErrorList errorList = ValidateAndDeserialize(args, (Adapter*)personalData, work, response);
	if(errorList.Size() == 0) {
		work->callback.Reset(isolate, Local<Function>::Cast(args[1]));
		work->f = [personalData](Work *work){
			DetectCardInstantAndGetCardInstance(work);//
			if(work->card != NULL){
				Reader *reader = Reader::cardTapped;
				work->error = reader->UpdatePersonalData(work->card, personalData);
				if (work->error == ErrorConstants::NO__ERROR) {
					work->adapter = (Adapter*) personalData;
				}
			}
		};
		CallAsync(work);
	} else {
		PrepareError("VALIDATION_ERROR", "Errors in input", &errorList, response);
		PrepareResponse(args, response);
	}
}

void UpdateVehicleData(const v8::FunctionCallbackInfo<v8::Value>& args) {
	Local<Object> response = Object::New(args.GetIsolate());
	Work *work = new Work();
	VehicleData *vehicleData = new VehicleData();
	ErrorList errorList = ValidateAndDeserialize(args, (Adapter*)vehicleData, work, response);
	if(errorList.Size() == 0){
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [vehicleData](Work *work){
			DetectCardAndGetCardInstance(work);
			if(work->card != NULL){
				Reader *reader = Reader::cardTapped;
				work->error = reader->UpdateVehicleData(work->card, vehicleData);
				work->adapter = NULL;
			}
		};
		CallAsync(work);
	} else {
		PrepareResponse(args, response);
	}
}

void UpdateCardStatus(const v8::FunctionCallbackInfo<v8::Value>& args) {
	Local<Object> response = Object::New(args.GetIsolate());
	CardStatus *cardStatus = new CardStatus();
	Work *work = new Work();
	ErrorList errorList = ValidateAndDeserialize(args, (Adapter*)cardStatus, work, response);
	if(errorList.Size() == 0){
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [cardStatus](Work *work){
			DetectCardAndGetCardInstance(work);
			if(work->card != NULL){
				Reader *reader = Reader::cardTapped;
				work->error = reader->UpdateCardStatus(work->card, cardStatus);
				work->adapter = NULL;
			}
		};
		CallAsync(work);
	} else {
		PrepareResponse(args, response);
	}
}

void Pay(const v8::FunctionCallbackInfo<v8::Value>& args) {
	Local<Object> response = Object::New(args.GetIsolate());
	Activity *activity = new Activity();
	Work *work = new Work();
	ErrorList errorList = ValidateAndDeserialize(args, (Adapter*)activity, work, response);
	if(errorList.Size() == 0){
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [activity](Work *work){
			DetectCardInstantAndGetCardInstance(work);
			if(work->card != NULL){
				Reader *reader = Reader::cardTapped;
				work->error = reader->Pay(work->card, activity);
				work->adapter = (Adapter*) activity;//TODO: Return updated balance
			}
		};
		CallAsync(work);
	} else {
		PrepareError("VALIDATION_ERROR", "Errors in input", &errorList, response);
		PrepareResponse(args, response);
	}
}

void CollectFine(const v8::FunctionCallbackInfo<v8::Value>& args) {
	Local<Object> response = Object::New(args.GetIsolate());
	Activity *activity = new Activity();
	Work *work = new Work();
	ErrorList errorList = ValidateAndDeserialize(args, (Adapter*)activity, work, response);
	if(errorList.Size() == 0){
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [activity](Work *work){
			DetectCardAndGetCardInstance(work);
			if(work->card != NULL){
				Reader *reader = Reader::cardTapped;
				work->error = reader->Pay(work->card, activity);
				work->adapter = (Adapter*) activity;//TODO: Return updated balance
			}
		};
		CallAsync(work);
	} else {
		PrepareError("VALIDATION_ERROR", "Errors in input", &errorList, response);
		PrepareResponse(args, response);
	}
}

void Recharge(const v8::FunctionCallbackInfo<v8::Value>& args) {
	Local<Object> response = Object::New(args.GetIsolate());
	Activity *activity = new Activity();
	Work *work = new Work();
	ErrorList errorList = ValidateAndDeserialize(args, (Adapter*)activity, work, response);
	if(errorList.Size() == 0){
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [activity](Work *work){
			DetectCardInstantAndGetCardInstance(work);

			if(work->card != NULL){
				Reader *reader = Reader::cardTapped;
				work->error = reader->Recharge(work->card, activity);
				work->adapter = (Adapter*) activity;//TODO: Return updated balance
			}
		};
		CallAsync(work);
	} else {
		PrepareError("VALIDATION_ERROR", "Errors in input", &errorList, response);
		PrepareResponse(args, response);
	}
}

void PurchaseTicket(const v8::FunctionCallbackInfo<v8::Value>& args) {
	Local<Object> response = Object::New(args.GetIsolate());
	Ticket *ticket = new Ticket();
	Work *work = new Work();
	ErrorList errorList = ValidateAndDeserialize(args, (Adapter*)ticket, work, response);
	if(errorList.Size() == 0){
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [ticket](Work *work){
			DetectCardAndGetCardInstance(work);
			if(work->card != NULL){
				Reader *reader = Reader::cardTapped;
				work->error = reader->PurchaseTicket(work->card, ticket);
				work->adapter = NULL;
			}
		};
		CallAsync(work);
	} else {
		PrepareResponse(args, response);
	}
}

void GetTicket(const v8::FunctionCallbackInfo<v8::Value>& args) {
	Local<Object> response = Object::New(args.GetIsolate());
	Ticket *ticket = new Ticket();
	Work *work = new Work();
	ErrorList errorList = ValidateAndDeserialize(args, (Adapter*)ticket, work, response);
	if(errorList.Size() == 0) {
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [ticket](Work *work){
			DetectCardAndGetCardInstance(work);
			if(work->card != NULL){
				Reader *reader = Reader::cardTapped;
				work->error = reader->GetTicket(work->card, ticket);
				if (work->error == ErrorConstants::NO__ERROR) {	
					work->adapter = (Adapter*) ticket;
				}
			}
		};
		CallAsync(work);
	} else {
		PrepareResponse(args, response);
	}
}

void ActivatePass(const v8::FunctionCallbackInfo<v8::Value>& args) {
	Local<Object> response = Object::New(args.GetIsolate());
	Pass *pass = new Pass();
	Work *work = new Work();
	ErrorList errorList = ValidateAndDeserialize(args, (Adapter*)pass, work, response);
	if(errorList.Size() == 0){
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [pass](Work *work){
			DetectCardInstantAndGetCardInstance(work);
			if(work->card != NULL){
				Reader *reader = Reader::cardTapped;
				work->error = reader->ActivatePass(work->card, pass);
				if (work->error == ErrorConstants::NO__ERROR) {
					work->adapter = (Adapter*) pass;
				}
			}
		};
		CallAsync(work);
	} else {
		PrepareError("VALIDATION_ERROR", "Errors in input", &errorList, response);
		PrepareResponse(args, response);
	}
}

void UsePass(const v8::FunctionCallbackInfo<v8::Value>& args) {
	Local<Object> response = Object::New(args.GetIsolate());
	Pass *pass = new Pass();
	Work *work = new Work();
	ErrorList errorList = ValidateAndDeserialize(args, (Adapter*)pass, work, response);
	if(errorList.Size() == 0){
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [pass](Work *work){
			DetectCardInstantAndGetCardInstance(work);
			if(work->card != NULL){
				Reader *reader = Reader::cardTapped;
				work->error = reader->UsePass(work->card, pass);
				work->adapter = NULL;
			}
		};
		CallAsync(work);
	} else {
		PrepareResponse(args, response);
	}
}

void GetPass(const v8::FunctionCallbackInfo<v8::Value>& args) {
	Local<Object> response = Object::New(args.GetIsolate());
	cout<<"reached inside PassCriteria"<<endl;
	Pass *pass = new Pass();
	PassCriteria *passCriteria = new PassCriteria();
	Work *work = new Work();
	ErrorList errorList = ValidateAndDeserialize(args, (Adapter*)passCriteria, work, response);
	cout<<"errorList in getPass "<<errorList.Size()<<endl;
	if(errorList.Size() == 0){
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [pass, passCriteria](Work *work){
			DetectCardInstantAndGetCardInstance(work);
			if(work->card != NULL){
				Reader *reader = Reader::cardTapped;
				work->error = reader->GetPass(work->card, pass,passCriteria);
				work->adapter = (Adapter*) pass;
			}
		};
		CallAsync(work);
	} else {
		PrepareResponse(args, response);
	}
}

void GetTransactions(const v8::FunctionCallbackInfo<v8::Value>& args){
	Local<Object> response = Object::New(args.GetIsolate());
	// Transaction *transaction = new Transaction();
	TransactionsList *transactions = new TransactionsList(); 
	TransactionsCriteria *transactionsCriteria = new TransactionsCriteria();
	Work *work = new Work();
	ErrorList errorList = ValidateAndDeserialize(args, (Adapter*)transactionsCriteria, work, response);
	if(errorList.Size() == 0){
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [transactions, transactionsCriteria](Work *work){
			DetectCardAndGetCardInstance(work);
			if(work->card != NULL){
				Reader *reader = Reader::cardTapped;
				work->error = reader->GetTransactions(work->card, transactions,transactionsCriteria);
				if (work->error == ErrorConstants::NO__ERROR) {
					work->adapter = (Adapter*) transactions;
				}
			}
		};
		CallAsync(work);
	} else {
		PrepareResponse(args, response);
	}
}

void GetGenericData(const v8::FunctionCallbackInfo<v8::Value>& args) {
	Local<Object> response = Object::New(args.GetIsolate());
	Work *work = new Work();
	GenericData *genericData = new GenericData();
	ErrorList errorList = ValidateAndDeserialize(args,(Adapter*) genericData, work, response);
	if(errorList.Size() == 0){
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [genericData](Work *work){
			DetectCardAndGetCardInstance(work);
			if(work->card != NULL){
				cout<<"GetCardDetails card "<<work->card<<endl;
				cout<<"GetCardDetails card type "<<work->card->type<<endl;
				cout<<"Reader card tapped "<<Reader::cardTapped<<endl;
				Reader *reader = Reader::cardTapped;
				cout<<"Reader tap is  "<<reader<<endl;
				work->error = reader->GetGenericData(work->card, genericData);
				if (work->error == ErrorConstants::NO__ERROR) {
					work->adapter = (Adapter*) genericData;
				}
			}else {
				work->error = ErrorConstants::APP__ERROR;
			}
		};
		CallAsync(work);
	} else {
		PrepareError("VALIDATION_ERROR", "Errors in input", &errorList, response);
		PrepareResponse(args, response);
	}
}

void DisconnectReader(const v8::FunctionCallbackInfo<v8::Value>& args) {
	printf("Connected\n");
	PrintInitializedReader();

	v8::Isolate* isolate = args.GetIsolate();
	Local<Object> response = Object::New(isolate);
	Work *work = new Work();
	ErrorList errorList = DoBasicValidation(args);
	if(errorList.Size() == 0) {
		Local<Object> data = args[0]->ToObject();
		work->readerType = EnumUtil::getReaderType(ConversionUtil::toCharArray(data->Get(String::NewFromUtf8(isolate, "readerType"))->ToString()));
		work->readerIndex = data->Get(String::NewFromUtf8(isolate, "readerIndex"))->Int32Value();
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [](Work *work){
			ReaderType readerType = work->readerType;
			int readerIndex = work->readerIndex;
			int initializedReaderIndex = GetInitializedReaderIndex(readerType, readerIndex);
			if(initializedReaderIndex != -1) {
				long ret = DisconnectReader(initializedReaderIndex, false);
				if(ret != APP_SUCCESS) {
					work->error = ErrorConstants::APP__ERROR;
				}
			} else {
				work->error = ErrorConstants::READ_NOT_FOUND;
			}
		};
		CallAsync(work);
	} else {
		PrepareError("VALIDATION_ERROR", "Errors in input", &errorList, response);
		printf("Remaining\n");
		PrintInitializedReader();
		PrepareResponse(args, response);
	}
}

void IsCardRemoved(const v8::FunctionCallbackInfo<v8::Value>& args){
	v8::Isolate* isolate = args.GetIsolate();
	Local<Object> response = Object::New(isolate);
	Work *work = new Work();
	ErrorList errorList = DoBasicValidation(args);
	if(errorList.Size() == 0) {
		Local<Object> data = args[0]->ToObject();
		work->readerType = EnumUtil::getReaderType(ConversionUtil::toCharArray(data->Get(String::NewFromUtf8(isolate, "readerType"))->ToString()));
		work->readerIndex = data->Get(String::NewFromUtf8(isolate, "readerIndex"))->Int32Value();
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [](Work *work){
			ReaderType readerType = work->readerType;
			int readerIndex = work->readerIndex;
			int initializedReaderIndex = GetInitializedReaderIndex(readerType, readerIndex);
			if(initializedReaderIndex != -1) {
				bool isCardRemoved = initializedReader[initializedReaderIndex].reader->isCardRemoved();
				if(isCardRemoved == true){
					printf("card removed succesfully\n");
				}else{
					printf("card still present\n");
				}
				CardRemove *cardRemove = new CardRemove();
				cardRemove->setCardRemoved(isCardRemoved);
				work->adapter = (Adapter*) cardRemove;
			} else {
				work->error = ErrorConstants::READ_NOT_FOUND;
			}
		};
		CallAsync(work);
	} else {
		PrepareResponse(args, response);
	}
}


void StopDetection(const v8::FunctionCallbackInfo<v8::Value>& args){
	v8::Isolate* isolate = args.GetIsolate();
	Local<Object> response = Object::New(isolate);
	Work *work = new Work();
	ErrorList errorList = DoBasicValidation(args);
	if(errorList.Size() == 0) {
		Local<Object> data = args[0]->ToObject();
		work->readerType = EnumUtil::getReaderType(ConversionUtil::toCharArray(data->Get(String::NewFromUtf8(isolate, "readerType"))->ToString()));
		work->readerIndex = data->Get(String::NewFromUtf8(isolate, "readerIndex"))->Int32Value();
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		work->f = [](Work *work){
			ReaderType readerType = work->readerType;
			int readerIndex = work->readerIndex;
			int initializedReaderIndex = GetInitializedReaderIndex(readerType, readerIndex);
			if(initializedReaderIndex != -1) {
				bool stop_detection = initializedReader[initializedReaderIndex].reader->stopDetection();
				printf("cancel detection set true\n");
				CardRemove *cardRemove = new CardRemove();
				cardRemove->setCardRemoved(false);
				work->adapter = (Adapter*) cardRemove;
			} else {
				work->error = ErrorConstants::READ_NOT_FOUND;
			}
		};
		CallAsync(work);
	} else {
		PrepareResponse(args, response);
	}
}

void encryptData(const v8::FunctionCallbackInfo<v8::Value>& args){
	v8::Isolate* isolate = args.GetIsolate();
	Local<Object> response = Object::New(isolate);
	Work *work = new Work();
	ErrorList errorList = DoBasicValidation(args);
	if(errorList.Size() == 0) {
		Local<Object> data = args[0]->ToObject();
		work->readerType = EnumUtil::getReaderType(ConversionUtil::toCharArray(data->Get(String::NewFromUtf8(isolate, "readerType"))->ToString()));
		work->readerIndex = data->Get(String::NewFromUtf8(isolate, "readerIndex"))->Int32Value();
		//work->transactionId = data->Get(String::NewFromUtf8(isolate,"transactionId"))->ToString();
		work->callback.Reset(args.GetIsolate(), Local<Function>::Cast(args[1]));
		
	}
}

long DisconnectReader(int initializedReaderIndex, bool removeFromListEvenOnError) {
	int i = initializedReaderIndex;
	Reader *reader = initializedReader[i].reader;
	long ret = reader->disconnectReader();
	if(ret != APP_SUCCESS) {
		return ret;
	}
	if(ret == APP_SUCCESS || removeFromListEvenOnError) {
		for(;i<10;i++) {
			if(i == 9) {
				initializedReader[i] = {0};
			} else {
				initializedReader[i] = initializedReader[i+1];
			}
		}
		numberOfReadersInitialized--;
	}
	return APP_SUCCESS;
}

Local<Object> PrepareError(const char *name, const char *message, ErrorList *errorList) {
	Local<Object> response = Object::New(Isolate::GetCurrent());
	cout<<"reached"<<endl;
	PrepareError(name, message, errorList, response);
	return response;
}

void Callback(Work *work, Local<Object> response) {
	cout<<"Reached Callback\n";
	Isolate *isolate = Isolate::GetCurrent();
	const unsigned argc = 1;
	v8::Local<v8::Value> argv[argc] = {response};
	int amount = response->Get(String::NewFromUtf8(isolate, "amount"))->Int32Value();
	Local<Function>::New(isolate, work->callback)->Call(isolate->GetCurrentContext()->Global(), argc, argv);
}

void PrepareError(const char *name, const char *message, ErrorList *errorList, Local<Object> response) {
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> error = Object::New(Isolate::GetCurrent());
	error->Set(String::NewFromUtf8(isolate, "name"), String::NewFromUtf8(isolate, name));
	error->Set(String::NewFromUtf8(isolate, "message"), String::NewFromUtf8(isolate, message));
	if(errorList && errorList->Size() > 0) {
		error->Set(String::NewFromUtf8(isolate, "errors"), errorList->getErrors());
	}
	response->Set(String::NewFromUtf8(isolate, "error"), error);
}


void PrepareResponse(const v8::FunctionCallbackInfo<v8::Value>& args, Local<Object> response) {
	const unsigned argc = 1;
	v8::Local<v8::Function> cb = v8::Local<v8::Function>::Cast(args[argc]);
	v8::Local<v8::Value> argv[argc] = {response};
	cb->Call(Null(args.GetIsolate()), argc, argv);
}

Local<Object> PrepareResponse(Work *work) {
	ErrorList errorList;
	Isolate *isolate = Isolate::GetCurrent();
	Local<Object> response = Object::New(isolate);
	if(work->error != ErrorConstants::NO__ERROR) {
		cout<<"PrepareResponse: error\n";
		PrepareError(static_cast<const char*>(getErrorMessage(work->error)->errorCode.c_str()), 
			static_cast<const char*>(getErrorMessage(work->error)->errorMsg.c_str()), &errorList, response);
	} else if(work->adapter != NULL) {
		cout<<"PrepareResponse: no error\n";
		response = work->adapter->serialize();
	}
	cout<<"PrepareResponse: adapter null\n";
	return response;
}

void PrintInitializedReader() {
	for(int i=0;i<numberOfReadersInitialized;i++) {
		printf("Reader %d: type %d, index %d\n",i,initializedReader[i].reader->type,initializedReader[i].readerIndex);
	}
}

long GetInitializedReaderIndex(ReaderType readerType, int readerIndex) {
	for(int i=0;i<numberOfReadersInitialized;i++) {
		if(initializedReader[i].reader->type == readerType && initializedReader[i].readerIndex == readerIndex) {
			return i;
		}
	}
	return -1;
}

ErrorList DoBasicValidation(const v8::FunctionCallbackInfo<v8::Value>& args) {
	v8::Isolate* isolate = args.GetIsolate();
	Local<Object> response = Object::New(isolate);
	ErrorList errorList = ValidateFirstArgument(args);
	ValidateSecondArgument(args, &errorList);
	if(errorList.Size() == 0) {
		Local<Object> data = args[0]->ToObject();
		ValidateReaderType(data, &errorList);
		ValidateReaderIndex(data, &errorList);
	}
	return errorList;
}

ErrorList ValidateAndDeserialize(const v8::FunctionCallbackInfo<v8::Value>& args, Adapter *adapter, Card **card, Local<Object> response) {
	Isolate* isolate = args.GetIsolate();
	ErrorList errorList = DoBasicValidation(args);

	ReaderType readerType = ReaderType::UNKNOWN;
	int readerIndex = -1;
	cout<<"errorList " <<errorList.Size()<<endl;
	if(errorList.Size() == 0) {
		Local<Object> data = args[0]->ToObject();
		readerType = EnumUtil::getReaderType(ConversionUtil::toCharArray(data->Get(String::NewFromUtf8(isolate, "readerType"))->ToString()));
		readerIndex = data->Get(String::NewFromUtf8(isolate, "readerIndex"))->Int32Value();
		if(adapter != NULL) {
			errorList = adapter->deserialize(data);
		}
	}

	if(errorList.Size() == 0) {
		*card = DetectCardAndGetCardInstance(readerType, readerIndex);
		if(card == NULL){
			PrepareError("APP_CANCELED_OR_NO_READER", "card detection canceled or no reader connected", &errorList, response);
		}
	} else {
		PrepareError("VALIDATION_ERROR", "Errors in input", &errorList, response);
	}
	
	return errorList;
}

ErrorList ValidateAndDeserialize(const v8::FunctionCallbackInfo<v8::Value>& args, Adapter *adapter, Work *work, Local<Object> response) {
	Isolate* isolate = args.GetIsolate();
	ErrorList errorList = DoBasicValidation(args);
	cout<<"errorList " <<errorList.Size()<<endl;
	if(errorList.Size() == 0) {
		Local<Object> data = args[0]->ToObject();
		work->readerType = EnumUtil::getReaderType(ConversionUtil::toCharArray(data->Get(String::NewFromUtf8(isolate, "readerType"))->ToString()));
		work->readerIndex = data->Get(String::NewFromUtf8(isolate, "readerIndex"))->Int32Value();
		if(adapter != NULL) {
			printf("Going to deserialize");
			errorList = adapter->deserialize(data);
		}
	} else {
		PrepareError("VALIDATION_ERROR", "Errors in input", &errorList, response);
	}

	return errorList;
}

ErrorList ValidateFirstArgument(const v8::FunctionCallbackInfo<v8::Value>& args) {
	ErrorList errorList;
	ValidateFirstArgument(args, &errorList);
	return errorList;
}

void ValidateFirstArgument(const v8::FunctionCallbackInfo<v8::Value>& args, ErrorList *errorList) {
	const char *path = "args0";
	validator.NotNullObject(args[0], path, "args0 is not an object", errorList);
}

void ValidateSecondArgument(const v8::FunctionCallbackInfo<v8::Value>& args, ErrorList *errorList) {
	const char *path = "args1";
	validator.NotNullFunction(args[1], path, "args1 is not an function", errorList);
}

void ValidateReaderType(Local<Object> data, ErrorList *errorList) {
	const char *path = "readerType";
	Local<Value> value = data->Get(String::NewFromUtf8(Isolate::GetCurrent(), "readerType"));
	validator.NotNullString(value, path, "invalid reader type", errorList);
}

void ValidateReaderIndex(Local<Object> data, ErrorList *errorList) {
	const char *path = "readerIndex";
	Local<Value> value = data->Get(String::NewFromUtf8(Isolate::GetCurrent(), "readerIndex"));
	validator.NotNullNumber(value, path, "invalid reader index", errorList);
}

void Initialize(v8::Local<v8::Object> exports) {
	NODE_SET_METHOD(exports, "initializeReader", InitializeReader);
	NODE_SET_METHOD(exports, "getBalance", GetBalance);
	NODE_SET_METHOD(exports, "getCardDetails", GetCardDetails);
	NODE_SET_METHOD(exports, "getGenericData", GetGenericData);
	NODE_SET_METHOD(exports, "activateCard", ActivateCard);
	NODE_SET_METHOD(exports, "updatePersonalData", UpdatePersonalData);
	NODE_SET_METHOD(exports, "updateVehicleData", UpdateVehicleData);
	NODE_SET_METHOD(exports, "updateCardStatus", UpdateCardStatus);
	NODE_SET_METHOD(exports, "pay", Pay);
	NODE_SET_METHOD(exports, "collectFine", CollectFine);
	NODE_SET_METHOD(exports, "recharge", Recharge);
	NODE_SET_METHOD(exports, "purchaseTicket", PurchaseTicket);
	NODE_SET_METHOD(exports, "getTicket", GetTicket);
	NODE_SET_METHOD(exports, "activatePass", ActivatePass);
	NODE_SET_METHOD(exports, "usePass", UsePass);
	NODE_SET_METHOD(exports, "getPass", GetPass);
	NODE_SET_METHOD(exports, "disconnectReader", DisconnectReader);
	NODE_SET_METHOD(exports, "getTransactions", GetTransactions);
	NODE_SET_METHOD(exports, "isCardRemoved", IsCardRemoved);
	NODE_SET_METHOD(exports, "stopDetection", StopDetection);
	NODE_SET_METHOD(exports, "encryptData", encryptData);
}

NODE_MODULE(addon, Initialize);
