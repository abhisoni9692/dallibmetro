#include <cstdio>
#include "sam/SAM.h"
#include "card/Card.h"
#include "card/DesfireEV1Card.h"
#include "card/FelicaCard.h"

Card* Card::GetInstance(int type)
{
	printf("Card::GetInstance cardtype: %d\n", type);
	if (type == Card::DESFIRE_EV1_CARD)
	{
		return new DesfireEV1Card();
	}else if(type == Card::FELICA_RCSA01_CARD){
		return new FelicaCard();
	}

	return NULL;
}
// getSupportedSAMType()
int Card::GetSAMType(int cardType) {
	if (cardType == Card::DESFIRE_EV1_CARD)
	{
		return SAM::MIFARE_AV2_SAM;
	}
	if (cardType == Card::FELICA_RCSA01_CARD)
	{
		return SAM::FELICA_RCS500_SAM;
	}
	return 0;
}

