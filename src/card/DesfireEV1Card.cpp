// #include<stdio.h>
#include<iostream>
#include<string>
#include <chrono>
#include<vector>
#include "reader/Reader.h"
#include "Card.h"
#include "card/DesfireEV1Card.h"
#include "card/DesfireCardCmdUtil.h"
#include "sam/MifareSAMCmdUtil.h"
#include "../include/Util.h"
#include "constants/ErrorConstants.h"
#include "enum/CardStatusEnum.h"
#include "enum/ServiceType.h"
#include "model/CardDetail.h"
#include  "enum/LimitPeriodicity.h"
#include "model/Pass.h"
#include "model/Periodicity.h"
#include "enum/TransactionType.h"
#include "model/CardDetail.h"
#include "model/PassCriteria.h"
#include "utils/ConversionUtil.h"
#include <TransactionsList.h>

#define min(a, b) (((a) < (b)) ? (a) : (b)) 
#define max(a, b) (((a) > (b)) ? (a) : (b)) 

using namespace std;

DesfireEV1Card::DesfireEV1Card()
{
	type = Card::DESFIRE_EV1_CARD;
}

ErrorConstants DesfireEV1Card::GetBalance(Reader *reader, long *amount)
{
	unsigned char fileNo = 0x01;
	unsigned char samKeyNo[2] = { 0x05,0x00 };
	unsigned long ret = authenticateCard(reader, CARD_AUTH_KEY_NO, samKeyNo);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	ret = selectApplication(reader, (unsigned char *)app2_ID);
	if (ret != APP_SUCCESS) 
		return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = authenticateCard(reader, App_2_File_1.readKey, samKeyNo);
	if (ret != APP_SUCCESS) 
		return ErrorConstants::CARD_AUTH_FAILED;
	ret = getValue(reader, fileNo, amount);
	if (ret != APP_SUCCESS) 
		return ErrorConstants::GET_VALUE_FAILED;
	return ErrorConstants::NO__ERROR;
}

ErrorConstants DesfireEV1Card::GetCardDetails(Reader *reader, CardDetail *cardDetail)
{
	unsigned char UID[7], samKey[2] = { 0x05,0x00 },cardData[20];
	unsigned char cardVersion[4],cardNo[16];
	long amount;
	unsigned long cardDataLen;
	long ret = authenticateCard(reader, CARD_AUTH_KEY_NO, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	ret = GetCardUID(reader,UID);
	if (ret != APP_SUCCESS)
		return ErrorConstants::GET_CARD_UID_FAILED;
	ret = selectApplication(reader, (unsigned char *)app1_ID);
	if(ret != APP_SUCCESS)
		return ErrorConstants::SELECT_APPLICATION_FAILED;
	unsigned int offset = 97, len = 3;
	unsigned long outputLen;
	unsigned char fileId =0x01,output[256];
	ret = authenticateCard(reader, App_1_File_1.readKey, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	ret = readFromDataFile(reader,fileId,offset,len,output,outputLen);
	if(ret != APP_SUCCESS)
		return ErrorConstants::READ_FROM_CARD_FAILED;
	int status = output[0];
	ret = authenticateCard(reader, App_1_File_2.readKey, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	ret = readApp1File2(reader, cardData, cardDataLen);
	if (ret != APP_SUCCESS)
		return ErrorConstants::READ_FROM_CARD_FAILED;
	memcpy(cardVersion, &cardData[0], 4);
	memcpy(cardNo, &cardData[4], 20);

	std::string stdUID(reinterpret_cast< char const* >(UID),7);
	std::string stdCardVersion(reinterpret_cast< char const* >(cardVersion),4);
	std::string stdCardNo(reinterpret_cast< char const* >(cardNo),16);

	ret = selectApplication(reader, (unsigned char *)app2_ID);
	if (ret != APP_SUCCESS)
		return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = authenticateCard(reader, App_2_File_1.readKey, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	unsigned char fileNo = 0x01;
	ret = getValue(reader, fileNo, &amount);
	if (ret != APP_SUCCESS)
		return ErrorConstants::GET_VALUE_FAILED;
	cardDetail->setCardUID(stdUID.c_str());
	cardDetail->setVersion(stdCardVersion.c_str());
	cardDetail->setCardNumber(stdCardNo.c_str());
	cardDetail->setBalance(amount);
	cardDetail->setStatus(status);
	return ErrorConstants::NO__ERROR;
}

ErrorConstants DesfireEV1Card::ActivateCard(Reader *reader, CardActivation *cardActivation)
{
	unsigned char samKey[2] = { 0x05,0x00 };
	string posTerminalId = cardActivation->getPosTerminalId();
	unsigned char *ptrByteConvertedPosTerminalId =0;
	ptrByteConvertedPosTerminalId = (unsigned char *)posTerminalId.c_str(); 
	unsigned char byteConvertedPosTerminalId[4];
	for(int i=0;i<sizeof(ptrByteConvertedPosTerminalId);i++)
	{
		byteConvertedPosTerminalId[i] = ptrByteConvertedPosTerminalId[i];
	}
	long long timeInMsec = cardActivation->getTimeInMilliSeconds();
	long ret = authenticateCard(reader, CARD_AUTH_KEY_NO, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	ret = selectApplication(reader, (unsigned char *)app1_ID);
	if (ret != APP_SUCCESS)
		return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = authenticateCard(reader, App_1_File_1.writeKey, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	ret = activateCard(reader,byteConvertedPosTerminalId,timeInMsec);
	if(ret != APP_SUCCESS)
		return ErrorConstants::CARD_ACTICVATION_FAILED;
	ret = commit(reader);
	if(ret != APP_SUCCESS)
		return ErrorConstants::COMMIT_FAILED;
	return ErrorConstants::NO__ERROR;
}
ErrorConstants DesfireEV1Card::UpdatePersonalData(Reader *reader, PersonalData *personalData)
{
	unsigned char samKey[2] = { 0x05,0x00 };
	unsigned char *firstNamePtr = 0,*middleNamePtr = 0,*lastNamePtr= 0;
	unsigned char firstname[16]={0},middleName[16] ={0},lastName[16]={0};
	unsigned char gender = static_cast<unsigned char>(personalData->getGender());
	string fN = personalData->getFirstName();
	string mN = personalData->getMiddleName();
	string lN = personalData->getLastName();
	firstNamePtr = (unsigned char *)fN.c_str();
	middleNamePtr = (unsigned char *)mN.c_str();
	lastNamePtr = (unsigned char *)lN.c_str();
	for(int i=0;i<16;i++)
	{
		firstname[i] = firstNamePtr[i];
		middleName[i] = middleNamePtr[i];
		lastName[i] = lastNamePtr[i];
	}
	unsigned char fileno = 0x01;
	long DOB = personalData->getDateOfBirth();
	long long mobile = personalData->getMobile();
	long long aadhaar = personalData->getAadhaar();
	long ret = authenticateCard(reader, CARD_AUTH_KEY_NO, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	long long currentTime = personalData->getTimeInMilliSeconds();
	ret = selectApplication(reader, (unsigned char *)app1_ID);
	if(ret != APP_SUCCESS)
		return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = authenticateCard(reader, App_1_File_1.readWriteKey, samKey);
	if(!isCardActive(reader,currentTime))
		return ErrorConstants::CARD_INACTIVE;
	ret =updateCardHoldersData(reader,fileno,firstname,middleName,lastName,gender,DOB,mobile,aadhaar);
	if(ret !=APP_SUCCESS)
		return ErrorConstants::UPDATE_PERSONAL_DATA_FAILAED;
	ret = commit(reader);
	if(ret !=APP_SUCCESS)
		return ErrorConstants::COMMIT_FAILED;
	return ErrorConstants::NO__ERROR;
}
ErrorConstants DesfireEV1Card::UpdateVehicleData(Reader *reader, VehicleData *vehicleData)
{
	unsigned char samKey[2] = { 0x05,0x00 };
	unsigned char fileNo = 0x01;
	string vNumber = vehicleData->getVehicleNumber();
	unsigned char *vehicleNumberPtr = (unsigned char *)vNumber.c_str();
	unsigned char vehicleNumber[16];
	int VehicleType = static_cast<int>(vehicleData->getVehicleType());
	for(int i=0;i<16;i++)
			vehicleNumber[i] = vehicleNumberPtr[i];{

	}
	long ret = authenticateCard(reader, CARD_AUTH_KEY_NO, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	long long currentTime = vehicleData->getTimeInMilliSeconds();
	ret = selectApplication(reader, (unsigned char *)app1_ID);
	if(ret != APP_SUCCESS)
		return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = authenticateCard(reader, App_1_File_1.readWriteKey, samKey);
	if(ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	if(!isCardActive(reader,currentTime))
		return ErrorConstants::CARD_INACTIVE;
	ret = updateVehicleData(reader, fileNo, VehicleType, vehicleNumber);
	if(ret !=APP_SUCCESS)
		return ErrorConstants::UPDATE_VEHICLE_DATA_FAILED;
	ret = commit(reader);
	if(ret !=APP_SUCCESS)
		return ErrorConstants::COMMIT_FAILED;
	return ErrorConstants::NO__ERROR;
}
ErrorConstants DesfireEV1Card::UpdateCardStatus(Reader *reader, CardStatus *cardStatus)
{
	unsigned char samKey[2] = { 0x05,0x00 };
	CardStatusEnum status = cardStatus->getStatus();
	long ret = authenticateCard(reader, CARD_AUTH_KEY_NO, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	unsigned char fileno = 0x01;
	unsigned int offset =97;
	unsigned char payload[1];
	payload[0] = (int )status;
	ret = selectApplication(reader,(unsigned char *) DesfireEV1Card::app0_ID);
	if(ret != APP_SUCCESS)
		return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = selectApplication(reader,(unsigned char *) DesfireEV1Card::app1_ID);
	if(ret != APP_SUCCESS)
		return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = authenticateCard(reader, App_1_File_1.writeKey, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	ret = writeToDataFile(reader,fileno,offset,payload,1);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_ACTICVATION_FAILED;
	return ErrorConstants::NO__ERROR;
}

ErrorConstants DesfireEV1Card::Pay(Reader *reader, Activity *activity)
{
	unsigned char fileno = 0x01,samKey[2]= {0x05,0x00};
	long cardBalance=0, amount = activity->getAmount();
	long ret = authenticateCard(reader, CARD_AUTH_KEY_NO, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	long long currentTime = activity->getTimeInMilliSeconds();
	ret = selectApplication(reader, (unsigned char *)app1_ID);
	if(ret != APP_SUCCESS)
		return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = authenticateCard(reader, App_1_File_1.readKey, samKey);
	if(!isCardActive(reader,currentTime))
		return ErrorConstants::CARD_INACTIVE;
	ErrorConstants out = payForActivityAndAddRecordInTxnLog(reader,activity,samKey);
	if(out != ErrorConstants::NO__ERROR)
		return ErrorConstants::ADD_RECORD_IN_TXN_LOG_FAILED;
	ret = commit(reader);
	if(ret !=APP_SUCCESS)
		return ErrorConstants::COMMIT_FAILED;
	return ErrorConstants::NO__ERROR;
}
ErrorConstants DesfireEV1Card::Recharge(Reader *reader, Activity *activity)
{
	unsigned char fileno = 0x01,samKey[2]= {0x05,0x00};
	long cardBalance=0, amount = activity->getAmount();
	auto ret = authenticateCard(reader, CARD_AUTH_KEY_NO, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	long long currentTime = activity->getTimeInMilliSeconds();
	ret = selectApplication(reader, (unsigned char *)app1_ID);
	if(ret != APP_SUCCESS)
		return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = authenticateCard(reader, App_1_File_1.readWriteKey, samKey);
	if(!isCardActive(reader,currentTime))
		return ErrorConstants::CARD_INACTIVE;
	ret = selectApplication(reader,const_cast<unsigned char *>(DesfireEV1Card::app0_ID));
	if(ret != APP_SUCCESS)
		return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = selectApplication(reader,const_cast<unsigned char *>(DesfireEV1Card::app2_ID));
	if(ret != APP_SUCCESS)
		return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = authenticateCard(reader, App_2_File_1.readWriteKey, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	ret = creditAmount(reader,fileno,amount);
	if(ret!=APP_SUCCESS)
		return ErrorConstants::CREDIT_FAILED;
	ret = getValue(reader,fileno,&cardBalance);
	if(ret != APP_SUCCESS)
		return ErrorConstants::GET_VALUE_FAILED;
	ret = authenticateCard(reader, App_2_File_1.writeKey, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	cardBalance += amount;
	fileno = 0x02;
	unsigned char txnType = static_cast<unsigned char>(TransactionType::RECHARGE);
	string bizLevelID = activity->getSrcBranchId();
	string terminalId = activity->getPosTerminalId();
	ServiceType domainType = activity->getServiceType();
	long long timeInMsec =  activity->getTimeInMilliSeconds();
	ret = authenticateCard(reader, App_2_File_2.readWriteKey, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	ret = addRecordInTxnLog(reader,fileno,txnType,amount, cardBalance,bizLevelID,terminalId,domainType,timeInMsec);
	if(ret !=APP_SUCCESS)
		return ErrorConstants::ADD_RECORD_IN_TXN_LOG_FAILED;
	ret = commit(reader);
	if(ret !=APP_SUCCESS)
		return ErrorConstants::COMMIT_FAILED;
	activity->setAmount(cardBalance);
	return ErrorConstants::NO__ERROR;
}

ErrorConstants DesfireEV1Card::PurchaseTicket(Reader *reader, Ticket *ticket)
{
	unsigned char samKey[2] = {0x05,0x00};
	long ret = authenticateCard(reader, CARD_AUTH_KEY_NO, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	long long currentTime = ticket->getTimeInMilliSeconds();
	ret = selectApplication(reader, (unsigned char *)app1_ID);
	ret = authenticateCard(reader, App_1_File_1.readKey, samKey);
	if(!isCardActive(reader,currentTime))
		return ErrorConstants::CARD_INACTIVE;
	ServiceType serviceType = ticket->getServiceType();
	string strSrcBranchId = ticket->getSrcBranchId();
	long long expiryDateTime = ticket->getExpiryTimeInMilliSeconds();
	long long txntime = ticket->getTimeInMilliSeconds();
	unsigned char *ptrByteConvertedPosTerminalId =0,*ptrByteConvertedSrcBranchId = 0;
	ptrByteConvertedSrcBranchId = (unsigned char *)strSrcBranchId.c_str();
	unsigned char byteConvertedPosTerminalId[4],byteConvertedSrcBranchId[4];
	for(int i=0;i<sizeof(ptrByteConvertedSrcBranchId);i++)
	{
		byteConvertedSrcBranchId[i] = static_cast<unsigned char>(ptrByteConvertedSrcBranchId[i]);
	}
	ErrorConstants out = payForActivityAndAddRecordInTxnLog(reader, (Activity*)ticket, samKey);
	if (out != ErrorConstants::NO__ERROR)
	{
		return ErrorConstants::ADD_RECORD_IN_TXN_LOG_FAILED;
	}
	if(serviceType == ServiceType::TOLL)
	{
		ret = authenticateCard(reader, App_2_File_5.readWriteKey, samKey);
		if (ret != APP_SUCCESS)
			return ErrorConstants::CARD_AUTH_FAILED;
		ret = buyTollTicket(reader,byteConvertedSrcBranchId,txntime,expiryDateTime);
		if(ret != APP_SUCCESS)
			return ErrorConstants::BUYING_TICKET_FAILED;
	}
	else if(serviceType ==  ServiceType::BUS)
	{
		ret = authenticateCard(reader, App_2_File_B.readWriteKey, samKey);
		if (ret != APP_SUCCESS)
			return ErrorConstants::CARD_AUTH_FAILED;
		// ret = buyBusTkt(reader,byteConvertedSrcBranchId,txntime);
		// if(ret != APP_SUCCESS)
		// {
		// 	PrintText("Buying Ticket_Failed");
		// 	return BUYING_TICKET_FAILED;
		// }
	}
	else if(serviceType ==  ServiceType::METRO)
	{
		ret = authenticateCard(reader, App_2_File_9.readWriteKey, samKey);
		if (ret != APP_SUCCESS)
			return ErrorConstants::CARD_AUTH_FAILED;
		ret = buyMetroTkt(reader,byteConvertedSrcBranchId,txntime);
		if(ret != APP_SUCCESS)
			return ErrorConstants::BUYING_TICKET_FAILED;
	}
	else if(serviceType ==  ServiceType::RAIL)
	{
		ret = authenticateCard(reader, App_2_File_7.readWriteKey, samKey);
		if (ret != APP_SUCCESS)
			return ErrorConstants::CARD_AUTH_FAILED;
		// ret = buyRailwayTkt(reader,byteConvertedSrcBranchId,long long pnrNo,long long bookedDate);
		// if(ret != APP_SUCCESS)
		// {
		// 	PrintText("Buying Ticket_Failed");
		// 	return BUYING_TICKET_FAILED;
		// }
	}
	else if(serviceType ==  ServiceType::PARKING)
	{
		ret = authenticateCard(reader, App_2_File_7.readWriteKey, samKey);
		if (ret != APP_SUCCESS)
			return ErrorConstants::CARD_AUTH_FAILED;
		ret = buyParkingTkt(reader,txntime,expiryDateTime);
		if(ret != APP_SUCCESS)
			return ErrorConstants::BUYING_TICKET_FAILED;
	}
	else
		return ErrorConstants::INVALID_SERVICE_TYPE;
	ret = commit(reader);
	if(ret != APP_SUCCESS)
			return ErrorConstants::COMMIT_FAILED;
	return ErrorConstants::NO__ERROR;
}
ErrorConstants DesfireEV1Card::GetTicket(Reader *reader, Ticket *ticket)
{
	long long currentTime = ticket->getTimeInMilliSeconds();
	unsigned char fileId,key,samKey[2] = {0x05,0x00};
	long ret = authenticateCard(reader, CARD_AUTH_KEY_NO, samKey);
	if (ret != APP_SUCCESS)
			return ErrorConstants::CARD_AUTH_FAILED;
	ret = selectApplication(reader, (unsigned char *)app1_ID);
	if (ret != APP_SUCCESS)
			return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = authenticateCard(reader, App_1_File_1.readKey, samKey);
	if (ret != APP_SUCCESS)
			return ErrorConstants::CARD_AUTH_FAILED;
	cout<<"currentTime "<<currentTime<<endl;
	if(!isCardActive(reader,currentTime))
		return ErrorConstants::CARD_INACTIVE;
	ServiceType serviceType = ticket->getServiceType();
	string strSrcBranchId = ticket->getSrcBranchId();
	cout<<"strSrcBranchId "<<strSrcBranchId.c_str()<<endl;
	if (serviceType == ServiceType::TOLL)
	{
		fileId = 0x05;
		key = App_2_File_5.readWriteKey;
	}
	else if (serviceType == ServiceType::BUS)
	{
		fileId = 0x0B;
		key = App_2_File_B.readWriteKey;
	}
	else if (serviceType == ServiceType::METRO)
	{
		fileId = 0x09;
		key = App_2_File_9.readWriteKey;
	}
	else if (serviceType == ServiceType::RAIL)
	{
		fileId = 0x07;
		key = App_2_File_7.readWriteKey;
	}
	else if (serviceType == ServiceType::PARKING)
	{
		fileId = 0x0D;
		key = App_2_File_D.readWriteKey;
	}
	else
		return ErrorConstants::INVALID_SERVICE_TYPE;
	int offset =0;
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	ret = selectApplication(reader,(unsigned char *) DesfireEV1Card::app0_ID);
	if (ret != APP_SUCCESS)
		return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = selectApplication(reader,(unsigned char *) DesfireEV1Card::app2_ID);
	if (ret != APP_SUCCESS)
		return ErrorConstants::SELECT_APPLICATION_FAILED;
	unsigned char response[256];
	unsigned long responseslen;
	unsigned char size[3];
	ret = getFileSettings(reader,fileId,response,&responseslen);
	if (ret != APP_SUCCESS)
		return ErrorConstants::GET_FILE_SETTINGS_FAILED;
	int j = responseslen-1;
	for(int i=0;i<3;i++)
	{
		size[i] = response[j--];
	}
	int len = CharArrayToInt(size,3);
	cout<<"No of tkts: "<<len<<endl;
	if(len ==0)
		return ErrorConstants::NO_TICKETS_FOUND;
	ret = authenticateCard(reader,key,samKey);
	if(ret!= APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	unsigned char output[256];
	unsigned long outputLen;
	int recordSize =16;
	ret = readRecord(reader,fileId,offset,len,recordSize,output,&outputLen);
	if(ret != APP_SUCCESS)
		return ErrorConstants::READ_RECORD_FAILED;
	PrintHexArray("TKT ",outputLen,output);
	unsigned char *ptrByteConvertedSrcBranchId = 0;
	cout<<"strSrcBranchId "<<strSrcBranchId.c_str()<<endl;
	ptrByteConvertedSrcBranchId = (unsigned char *)strSrcBranchId.c_str();

	unsigned char byteConvertedSrcBranchId[4];
	for(int i=0;i<sizeof(ptrByteConvertedSrcBranchId);i++)
	{
		byteConvertedSrcBranchId[i] = static_cast<unsigned char>(ptrByteConvertedSrcBranchId[i]);
	}
	int index=16*(len-1);
	unsigned char tkt[16];
	unsigned char cardStoredBranchId[4];
	memcpy(cardStoredBranchId,output,4);
	for(int i=0;i<len;i++)
	{	
		if(Equals(byteConvertedSrcBranchId,4,cardStoredBranchId,4))
		{
			PrintHexArray("TKT: ",16,&output[index]);
			memcpy(tkt,&output[index],16);
			break;
		}
		index -= 16;
		memcpy(cardStoredBranchId,&output[index],4);
	}
	unsigned char byteExpiryTimeInCard[4];
	memcpy(byteExpiryTimeInCard,&tkt[8],4);
	PrintHexArray("byteExpiryTimeInCard ",4,byteExpiryTimeInCard);
	long expiryTimeInCard = CharArrayToInt(byteExpiryTimeInCard,4);
	// cout<<"expiryTimeInCard "<<expiryTimeInCard<<endl;
	// long long currentTime = ticket->getTimeInMilliSeconds();
	currentTime = trimMilliseconds(currentTime);
	// cout<<"currentTime "<<currentTime<<endl;
	// cout<<"expiryTimeInCard"<<expiryTimeInCard<<endl;
	if(currentTime <= expiryTimeInCard)
	{
		PrintText("Ticket is not Expired");
	}
	else
		return ErrorConstants::TICKET_EXPIRED;
	unsigned char purchaseTime[4];
	memcpy(cardStoredBranchId,&tkt[0],4);
	memcpy(purchaseTime,&tkt[4],4);
	PrintHexArray("cardStoredBranchId ",4,cardStoredBranchId);
	long long tktPurchaseTimeInLong= CharArrayToInt(purchaseTime,4);
	std::string stdSrcBranchId(reinterpret_cast< char const* >(cardStoredBranchId),4);
	// cout<<"stdSrcBranchId "<<stdSrcBranchId.c_str()<<endl;
	ticket->setPosTerminalId(stdSrcBranchId.c_str());
	ticket->setExpiryTimeInMilliSeconds(expiryTimeInCard);
	return ErrorConstants::NO__ERROR;
}

ErrorConstants DesfireEV1Card::ActivatePass(Reader *reader, Pass *pass)
{
	unsigned char out[4],byteExpiryDate[4],samKey[2] = {0x05,0x00};
	long ret = authenticateCard(reader,CARD_AUTH_KEY_NO,samKey);
	if(ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	long long currentTime = pass->getTimeInMilliSeconds();
	ret = selectApplication(reader, (unsigned char *)app1_ID);
	if (ret != APP_SUCCESS)
			return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = authenticateCard(reader, App_1_File_1.readKey, samKey);
	if (ret != APP_SUCCESS)
			return ErrorConstants::CARD_AUTH_FAILED;
	if(!isCardActive(reader,currentTime))
	{
		PrintText("Card is not Activated");
		return ErrorConstants::CARD_INACTIVE;
	}
	ServiceType serviceType = pass->getServiceType();
	// ServiceType serviceType =ServiceType::TOLL;
	string strFromBranchID =pass->getSrcBranchId();
	long amount = pass->getAmount();
	string strToBranchId =pass->getDestBranchId();
	int maxNoOfTrips = pass->getMaxTrips();
	PassType passType = pass->getPassType();
	cout << "passType " << (int)passType << endl;
	long long expiryDate = pass->getExpiryDate();
	cout<<"expiryDate "<<expiryDate<<endl;
	ErrorConstants output = payForActivityAndAddRecordInTxnLog(reader, (Activity*)pass, samKey);
	if (output != ErrorConstants::NO__ERROR)
		return ErrorConstants::ADD_RECORD_IN_TXN_LOG_FAILED;
	currentTime = trimMilliseconds(currentTime);
	unsigned char byteConvertedNoOfTrips[2];
	expiryDate = trimMilliseconds(expiryDate);
	IntToCharArray(expiryDate,out);
	memcpy(byteExpiryDate,out,4);
	PrintHexArray("byteExpiryDate: ",4,byteExpiryDate);
	LimitPeriodicity limitPeriodicity; 
	unsigned char lPeriodicity = 0 ;
	int lmaxCount;
	int limitMaxCount[2] = { 0 };//, limitMaxCount1[2] = { 0};
	vector<Periodicity> periodicities = pass->getPeriodicity();
	bool isRenewal = pass->getIsRenewal();
	cout<<"isRenewal "<<isRenewal<<endl;
	auto i = 0;
	for (auto & periodicity : periodicities) {
		
		limitPeriodicity = periodicity.getLimitPeriodicity();
		cout<<"limitPeriodicity " <<static_cast<int>(limitPeriodicity)<<endl;
		lmaxCount = periodicity.getLimitsMaxCount();
		/*In pass we can store 2 limits in single pass first four bits for one limit and next four bits for
		one more limit. Here I am converting the pass periodicities to decimal value and summing them and 
		storing the value as hex in the card. Ex:
		if we have 2 limits one is daily and monthly, the corresponding enum values are 
	    Hourly - 0001, Daily - 0010, Weekly - 0011, Monthly - 0100, Quarterly - 0101, Half-yearly - 0110, Yearly - 0111 
		then the value to be stored in the card in one byte is 0x24 and then its equivalent decimal value is 36 this equation converts to hex.*/ 
		lPeriodicity = lPeriodicity + static_cast<int>(limitPeriodicity)*(16 - (i * 16) + i);
		cout<<"lPeriodicity inside "<<lPeriodicity<<endl;
		limitMaxCount[i] = lmaxCount;
		i++;
	}
	printf("lPeriodicity %02x \n",lPeriodicity);
	cout<<"limitPeriodicity out side " <<lPeriodicity<< endl;
	unsigned char limitStartTime[4];
	unsigned char CRISID[6] = {0};
	IntToCharArray(currentTime,out);
	memcpy(limitStartTime,out,4);
	unsigned char *ptrByteConvertedFromBranchID =0;
	ptrByteConvertedFromBranchID = (unsigned char *)strFromBranchID.c_str(); 
	unsigned char byteConvertedFromBranchID[4] = {0};
	for(int j=0;j<4;j++)
	{
		byteConvertedFromBranchID[j] =(unsigned char ) ptrByteConvertedFromBranchID[j];
	}
	unsigned char *ptrByteConvertedToBranchId =0;
	ptrByteConvertedToBranchId = (unsigned char *)strToBranchId.c_str(); 
	unsigned char byteConvertedToBranchID[4] = {0};
	for(int j=0;j<4;j++)
	{
		byteConvertedToBranchID[j] = (unsigned char )ptrByteConvertedToBranchId[j];
	}
	/*unsigned char byteConvertedFromBranchID[4],byteConvertedToBranchID[4];
	strncpy((char*)byteConvertedFromBranchID, strFromBranchID.c_str(),sizeof(byteConvertedFromBranchID));
	strncpy((char*)byteConvertedToBranchID, strToBranchId.c_str(),sizeof(byteConvertedToBranchID));*/
	PrintHexArray("byteConvertedToBranchID: ",4,byteConvertedToBranchID);
	PrintHexArray("byteConvertedFromBranchID: ",4,byteConvertedFromBranchID);
	unsigned char fileId,key;
	if (serviceType == ServiceType::TOLL)
	{
		fileId = 0x04;
		key = App_2_File_4.readWriteKey;
	}
	else if (serviceType == ServiceType::BUS)
	{
		fileId = 0x0A;
		key = App_2_File_A.readWriteKey;
	}
	else if (serviceType == ServiceType::METRO)
	{
		fileId = 0x08;
		key = App_2_File_8.readWriteKey;
	}
	else if (serviceType == ServiceType::RAIL)
	{
		fileId = 0x06;
		key = App_2_File_6.readWriteKey;
	}
	else if (serviceType == ServiceType::PARKING)
	{
		fileId = 0x0C;
		key = App_2_File_C.readWriteKey;
	}
	ret = authenticateCard(reader, key, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	int noOfTrips = maxNoOfTrips-1;
	IntToCharArrayToBytes(noOfTrips, byteConvertedNoOfTrips);
	if (serviceType ==ServiceType::BUS || serviceType == ServiceType::TOLL || serviceType ==ServiceType::METRO)
	{
		ret = createPass(reader,serviceType,byteConvertedFromBranchID, byteConvertedToBranchID,
					 byteConvertedNoOfTrips, byteExpiryDate, (unsigned char)passType, lPeriodicity, limitMaxCount,
					 limitStartTime, limitStartTime,isRenewal);
		if(ret != APP_SUCCESS)
			return ErrorConstants::CREATE_PASS_FAILED;
	}
	else if(serviceType == ServiceType::RAIL)
	{
		/*ret = createRailwayPass(reader,CRISID,byteConvertedNoOfTrips, byteExpiryDate, (unsigned char)passType,
			lPeriodicity, &limitMaxCount[0][2],limitStartTime,&limitMaxCount[1][2], limitStartTime, currentTime);
		// printf("test 2: %ld\n",ret);
		if(ret != APP_SUCCESS)
		{
			return CREATE_PASS_FAILED;
		}*/
	}
	else if(serviceType == ServiceType::PARKING)
	{
		/*ret = createParkingPass(reader,byteConvertedFromBranchID,byteConvertedNoOfTrips, expiryDate,(unsigned char)passType,
			lPeriodicity, &limitMaxCount[0][2],
					limitStartTime, &limitMaxCount[1][2], limitStartTime,currentTime);
		// printf("test 3: %ld\n",ret);
		if(ret != APP_SUCCESS)
		{
			return CREATE_PASS_FAILED;
		}*/
	}
	// else {
	// 	PrintText("INVALID_SERVICE_TYPE");
	// 	return APP_ERROR;
	// }
	ret = commit(reader);
	if(ret != APP_SUCCESS)
			return ErrorConstants::COMMIT_FAILED;
	return ErrorConstants::NO__ERROR;
}
ErrorConstants DesfireEV1Card::UsePass(Reader *reader, Pass *pass)
{//Done
	unsigned char samKey[2] = {0x05,0x00},key;
	long ret = authenticateCard(reader, CARD_AUTH_KEY_NO, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	long long currentTime = pass->getTimeInMilliSeconds();
	ret = selectApplication(reader, (unsigned char *)app1_ID);
	ret = authenticateCard(reader, App_1_File_1.readKey, samKey);
	if(!isCardActive(reader,currentTime))
		return ErrorConstants::CARD_INACTIVE;
	ServiceType serviceType = pass->getServiceType();
	// ServiceType serviceType =ServiceType::TOLL;
	string posTerminalId =pass->getSrcBranchId();
	long long timeInMsec = pass->getTimeInMilliSeconds();
	int passType = 1;// changetoPassType
	unsigned char *ptrByteConvertedPosTerminalId = (unsigned char *)posTerminalId.c_str();
	// ptrByteConvertedPosTerminalId = (unsigned char *)posTerminalId.c_str(); 
	// printf("%s\n",posTerminalId.c_str() );
	unsigned char byteConvertedPosTerminalId[4] ={0};
	for(int i=0;i<4;i++)
	{
		byteConvertedPosTerminalId[i] = (unsigned char)ptrByteConvertedPosTerminalId[i];
	}
	ret = selectApplication(reader,(unsigned char *) DesfireEV1Card::app0_ID);
	if(ret != APP_SUCCESS)
			return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = selectApplication(reader,(unsigned char *) DesfireEV1Card::app2_ID);
	if(ret != APP_SUCCESS)
			return ErrorConstants::SELECT_APPLICATION_FAILED;
	if(serviceType == ServiceType::TOLL)
	{
		key = App_2_File_4.readWriteKey;
	}else if(serviceType ==ServiceType::BUS)
	{
		key = App_2_File_A.readWriteKey;
	}else if(serviceType == ServiceType::METRO)
	{
		key = App_2_File_8.readWriteKey;
	}else if(serviceType == ServiceType::RAIL)
	{
		key = App_2_File_6.readWriteKey;
	}else if(serviceType == ServiceType::PARKING){
		key = App_2_File_C.readWriteKey;
	}
	else
		return ErrorConstants::INVALID_SERVICE_TYPE;
	ret = authenticateCard(reader, key, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	if(serviceType == ServiceType::TOLL || serviceType == ServiceType::BUS || serviceType == ServiceType::METRO){
		ret = decrementPassTrips(reader,serviceType,byteConvertedPosTerminalId, timeInMsec, passType);
		if(ret != APP_SUCCESS)
			return ErrorConstants::PASS_USE_FAILED;
	}else if(serviceType == ServiceType::RAIL)
	{	
		// printf("test 1: %ld\n",ret);

	}else if (serviceType == ServiceType::PARKING)
	{
		// printf("test 2: %ld\n",ret);

	}
	ret = commit(reader);
	if(ret != APP_SUCCESS)
		return ErrorConstants::COMMIT_FAILED;
	return ErrorConstants::NO__ERROR;
}
ErrorConstants DesfireEV1Card::GetPass(Reader *reader, Pass *pass,PassCriteria *passCriteria)
{
	unsigned char key,samKey[2] = {0x05,0x00};
	long ret = authenticateCard(reader, CARD_AUTH_KEY_NO, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	long long currentTime = passCriteria->getTimeInMilliSeconds();
	ret = selectApplication(reader, (unsigned char *)app1_ID);
	ret = authenticateCard(reader, App_1_File_1.readKey, samKey);
	if(!isCardActive(reader,currentTime))
		return ErrorConstants::CARD_INACTIVE;
	ServiceType serviceType = passCriteria->getServiceType();
	// ServiceType serviceType =ServiceType::TOLL;
	string posTerminalId = passCriteria->getSrcBranchId();
	unsigned char *ptrByteConvertedPosTerminalId =0;
	unsigned char byteConvertedPosTerminalId[4];
	ptrByteConvertedPosTerminalId = (unsigned char *)posTerminalId.c_str(); 
	for(int i=0;i<sizeof(ptrByteConvertedPosTerminalId);i++)
	{
		byteConvertedPosTerminalId[i] = ptrByteConvertedPosTerminalId[i];
	}
	ret = selectApplication(reader, (unsigned char *)app2_ID);
	if (ret != APP_SUCCESS)
		return ErrorConstants::SELECT_APPLICATION_FAILED;
	if(serviceType == ServiceType::TOLL)
	{
		key = App_2_File_4.readWriteKey;
	}
	else if(serviceType == ServiceType::BUS)
	{
		key = App_2_File_A.readWriteKey;
	}
	else if(serviceType == ServiceType::METRO)
	{
		key = App_2_File_8.readWriteKey;
	}
	else if(serviceType == ServiceType::RAIL)
	{
		key = App_2_File_6.readWriteKey;
	}else if(serviceType == ServiceType::PARKING){
		key = App_2_File_C.readWriteKey;
	}
	else
		return ErrorConstants::INVALID_SERVICE_TYPE;
	ret = authenticateCard(reader, key, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	unsigned char passOutPut[256];
	int passOutPutLen;
	ret = readPass(reader,serviceType,byteConvertedPosTerminalId,passOutPut,&passOutPutLen);
	if (ret != APP_SUCCESS)
		return ErrorConstants::READ_FROM_CARD_FAILED;
	if(passOutPutLen == 0)
	{
		return ErrorConstants::PASS_NOT_FOUND;
	}
	PrintHexArray("Passes ",passOutPutLen,passOutPut);
	unsigned char passSrcBranchID[4], passDestBranchID[4],passMaxTrips[2],passExpiryDate[4],Passtype,passLimitsPeriodicity,passLimitsMAxCount[2],passLimitsMAxCount1[2],passLimitsRemainingCount[2],passLimitsRemainingCount1[2],passLimitsStartDate[4],passLimitsStartDate1[4];
	int periodicityLimit[2];
	int maxNoOfTrips;
	memcpy(passSrcBranchID,passOutPut,4);
	memcpy(passDestBranchID,&passOutPut[4],4);
	memcpy(passMaxTrips,&passOutPut[8],2);
	memcpy(passExpiryDate,&passOutPut[10],4);
	Passtype = passOutPut[14];
	passLimitsPeriodicity = passOutPut[15];
	periodicityLimit[0] = passLimitsPeriodicity>>4; //Taking last 4 bits
	periodicityLimit[1] = passLimitsPeriodicity & 0x0f; //Taking first four bits
	memcpy(passLimitsMAxCount,&passOutPut[16],2);
	memcpy(passLimitsRemainingCount,&passOutPut[18],2);
	memcpy(passLimitsStartDate,&passOutPut[20],4);
	int limitsMaxCount[2],limitsRemainingCount[2];
	long long limitsStartDate[2];
	limitsMaxCount[0] = CharArrayToInt(passLimitsMAxCount,2);
	limitsRemainingCount[0] = CharArrayToInt(passLimitsRemainingCount,2);
	limitsStartDate[0] = (long long)CharArrayToInt(passLimitsStartDate,4);
	vector<Periodicity> periodicitiyValue;
	int count = 2;
	if(periodicityLimit[1] == 0)
		count = 1;
	if(count > 1)
	{
		memcpy(passLimitsMAxCount1,&passOutPut[24],2);
		memcpy(passLimitsRemainingCount1,&passOutPut[26],2);
		memcpy(passLimitsStartDate1,&passOutPut[28],4);
		limitsMaxCount[1] = CharArrayToInt(passLimitsMAxCount1,2);
		limitsRemainingCount[1] = CharArrayToInt(passLimitsRemainingCount1,2);
		limitsStartDate[1] = (long long)CharArrayToInt(passLimitsStartDate1,4);
	}
	for(int i=0;i<count;i++){
		Periodicity *periodicityOfPass = new Periodicity();
		periodicityOfPass->setLimitPeriodicity((LimitPeriodicity)periodicityLimit[i]);
		periodicityOfPass->setLimitsMaxCount(limitsMaxCount[i]);
		periodicityOfPass->setLimitsRemainingCount(limitsRemainingCount[i]);
		periodicityOfPass->setLimitsStartTime(limitsStartDate[i]);
		periodicitiyValue.push_back(*periodicityOfPass);
	}
	memcpy(passLimitsMAxCount,&passOutPut[16],2);
	std::string stdSrcBranchId(reinterpret_cast< char const* >(passSrcBranchID),4);
	std::string stdDestBranchId(reinterpret_cast< char const* >(passDestBranchID),4);
	pass->setSrcBranchId(stdSrcBranchId.c_str());
	pass->setDestBranchId(stdDestBranchId.c_str());
	pass->setMaxTrips(CharArrayToInt(passMaxTrips,2));
	pass->setExpiryDate((long long)CharArrayToInt(passExpiryDate,4));
	pass->setPassType((PassType)Passtype);
	pass->setPeriodicity(periodicitiyValue);
	return ErrorConstants::NO__ERROR;
}

ErrorConstants DesfireEV1Card::GetTransactions(Reader *reader,TransactionsList *transactionsList,TransactionsCriteria *transactionsCriteria) 
{
	unsigned char samKey[2] = {0x05,0x00};
	long ret = authenticateCard(reader,CARD_AUTH_KEY_NO , samKey);
	long long currentTime = transactionsCriteria->getTimeInMilliSeconds();
	ret = selectApplication(reader, (unsigned char *)app1_ID);
	ret = authenticateCard(reader, App_1_File_1.readKey, samKey);
	if(!isCardActive(reader,currentTime))
		return ErrorConstants::CARD_INACTIVE;
	unsigned char response[256],out[30],size[3];
	unsigned long responselen,outputLen;
	unsigned char fileNo = 0x02;
	string posTerminalId = transactionsCriteria->getSourceBranchID();
	unsigned char *ptrByteConvertedPosTerminalId =0;
	unsigned char byteConvertedPosTerminalId[4];
	ptrByteConvertedPosTerminalId = (unsigned char *)posTerminalId.c_str(); 
	for(int i=0;i<sizeof(ptrByteConvertedPosTerminalId);i++)
	{
		byteConvertedPosTerminalId[i] = ptrByteConvertedPosTerminalId[i];
	}
	ret = selectApplication(reader, (unsigned char *)app2_ID);
	if(ret != APP_SUCCESS)
			return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = selectApplication(reader, (unsigned char *)app2_ID);
	if(ret != APP_SUCCESS)
			return ErrorConstants::SELECT_APPLICATION_FAILED;
	ret = getFileSettings(reader,fileNo,out,&outputLen);
	if(ret != APP_SUCCESS)
		return  ErrorConstants::GET_FILE_SETTINGS_FAILED;
	int j = outputLen-1;
	for(int i=0;i<3;i++)
	{
		size[i] = out[j--];
	}
	int len = CharArrayToInt(size,3);
	if(len ==0)
	{
		return ErrorConstants::NO_TICKETS_FOUND; 
	}
	ret = authenticateCard(reader, App_2_File_2.readKey, samKey);
	if (ret != APP_SUCCESS)
		return ErrorConstants::CARD_AUTH_FAILED;
	ret = readTxnLog(reader,fileNo,0,len, response, &responselen);
	if(ret != APP_SUCCESS)
		return ErrorConstants::READ_TXNLOG_FAILED;
	int index=19;
	unsigned char byteTransactions[32];
	vector<Transaction> transactions;
	unsigned char byteTxntype,byteTxnTime[6],byteTxnAmount[4],byteTxnRunningBalance[4],byteTxnBizLevelId[4],byteTxnTerminalID[4],byteTxnDomainType;
	ServiceType txnServiceType;
	TransactionType TxnType;
	long long txnTimeInMilliSeconds;
	long txnAmount,TxnRunningBalance;
	for(int i=0;i<len;i++)
	{
		if(Equals(byteConvertedPosTerminalId,4,&response[index],4))
		{
			// PrintHexArray("Transaction : ",32,&response[i*32]);
			memcpy(byteTransactions,&response[i*32],32);
			// PrintHexArray("Transaction Test",32,byteTransactions);
			byteTxntype = byteTransactions[0];
			TxnType = (TransactionType)byteTxntype;
			memcpy(byteTxnTime,&byteTransactions[1],6);
			txnTimeInMilliSeconds = CharArrayToLong(byteTxnTime,6);
			// cout<<"txnTimeInMilliSeconds "<<txnTimeInMilliSeconds<<endl;
			memcpy(byteTxnAmount,&byteTransactions[7],4);
			txnAmount = CharArrayToInt(byteTxnAmount,4);
			memcpy(byteTxnRunningBalance,&byteTransactions[11],4);
			TxnRunningBalance = CharArrayToInt(byteTxnRunningBalance,4);
			memcpy(byteTxnBizLevelId,&byteTransactions[11],4);
			memcpy(byteTxnTerminalID,&byteTransactions[15],4);
			byteTxnDomainType = byteTransactions[23];
			txnServiceType = (ServiceType)byteTxnDomainType;
			Transaction *transaction = new Transaction();
			transaction->setServiceType(txnServiceType);
			transaction->setType(TxnType);
			transaction->setTimeInMilliSeconds(txnTimeInMilliSeconds);
			transaction->setAmount(txnAmount);
			transaction->setRunningBalance(TxnRunningBalance);
			transactions.push_back(*transaction);
		}
		index += 32;
	}
	transactionsList->setTransactions(transactions);
	return ErrorConstants::NO__ERROR;
}

long DesfireEV1Card::authenticateCard(Reader *reader, unsigned char keyno, unsigned char samkey[])
 {
	unsigned int nIdx = 0;
	unsigned char _send_buf[256], receiveBuf[262], resBuf[256];
	unsigned long ret;
	memcpy(_send_buf,DesfireCardCmdUtil::ISO_threePass,sizeof(DesfireCardCmdUtil::ISO_threePass));
	_send_buf[5] = keyno;
	unsigned long _send_len = sizeof(DesfireCardCmdUtil::ISO_threePass);
	unsigned long receiveLen = 0x12;
    PrintHexArray("Send Buff ",_send_len,_send_buf);
	ret = reader->sendToCard(_send_buf, _send_len, receiveBuf, &receiveLen, Card::DESFIRE_EV1_CARD);
	if (ret != APP_SUCCESS)
	{
		printf("SCardTransmit Error\n");
		return APP_ERROR;
	}
	nIdx = 0;			
	memcpy(_send_buf,MifareSAMCmdUtil::ISO_threePass,sizeof(MifareSAMCmdUtil::ISO_threePass));
	nIdx = sizeof(MifareSAMCmdUtil::ISO_threePass);
	_send_buf[nIdx] = (unsigned char)receiveLen +2;
	memcpy(&_send_buf[5],samkey,2);
	memcpy(&_send_buf[7], receiveBuf, receiveLen);	
	nIdx = receiveLen +sizeof(MifareSAMCmdUtil::ISO_threePass)+3;
	_send_buf[nIdx++] = DesfireCardCmdUtil::ISO_le;
	_send_len = nIdx;
	unsigned long _sam_res_len = 262;
	unsigned char _sam_res_buf[262];

	ret = reader->sendToSAM(_send_buf, _send_len, _sam_res_buf, &_sam_res_len, GetSAMType(Card::DESFIRE_EV1_CARD));// Mifare_TransmitDataToSAM(_send_buf, _send_len, _sam_res_buf, &_sam_res_len, g_CMD_SAM_APDU);
	memcpy(_sam_res_buf, &_sam_res_buf, 32);
	unsigned char cmd2[arraySize];
	memcpy(cmd2,DesfireCardCmdUtil::ISO_additinalFrame,sizeof(DesfireCardCmdUtil::ISO_additinalFrame));
	nIdx = sizeof(DesfireCardCmdUtil::ISO_additinalFrame);
	cmd2[nIdx++] = (unsigned char)_sam_res_len;
	memcpy(&cmd2[nIdx],_sam_res_buf,_sam_res_len);
	nIdx = sizeof(DesfireCardCmdUtil::ISO_additinalFrame)+_sam_res_len+1;
	cmd2[nIdx++] =  DesfireCardCmdUtil::ISO_le;
	unsigned long resLen = 256;
	_send_len = nIdx;
	ret = reader->sendToCard(cmd2, _send_len, resBuf, &resLen, Card::DESFIRE_EV1_CARD); 
	unsigned char apdu[256], cardRes[262];
	unsigned long cardres_len = 256;
	memcpy(apdu,MifareSAMCmdUtil::ISO_threePass,sizeof(MifareSAMCmdUtil::ISO_threePass));
	nIdx = sizeof(MifareSAMCmdUtil::ISO_threePass);
	apdu[nIdx++] = (unsigned char)resLen;
	memcpy(&apdu[nIdx],resBuf,resLen);
	nIdx = resLen+ sizeof(MifareSAMCmdUtil::ISO_threePass)+1;
	_send_len = nIdx;
	ret = reader->sendToSAM(apdu, _send_len, cardRes, &cardres_len, GetSAMType(Card::DESFIRE_EV1_CARD));// Mifare_TransmitDataToSAM(apdu, 21, cardRes, &cardres_len, g_CMD_SAM_APDU);
	if (ret != APP_SUCCESS)
	{
		return ret;
	}
	return ret;
}

long DesfireEV1Card::selectApplication(Reader *reader, unsigned char applicationId[])
{
	unsigned char cmd[256], resbuf[256];
	PrintHexArray("APP ID : ", 3, applicationId);
	unsigned long reslen = 256, ret;
	int i = 0;
	memcpy(cmd,DesfireCardCmdUtil::ISO_selectApplication,sizeof(DesfireCardCmdUtil::ISO_selectApplication));
	i = sizeof(DesfireCardCmdUtil::ISO_selectApplication);
	cmd[i++] = 3;
	memcpy(&cmd[i],applicationId,3);
	int nIdx = sizeof(DesfireCardCmdUtil::ISO_selectApplication)+4;
	cmd[nIdx++] = DesfireCardCmdUtil::ISO_le;
	PrintHexArray("CMD : ",nIdx,cmd);
	ret = reader->sendToCard(cmd, nIdx, resbuf, &reslen, Card::DESFIRE_EV1_CARD);
	// printf("ret:%ld\n", ret);
	if (ret != APP_SUCCESS) {
		return APP_ERROR;
	}
	return APP_SUCCESS;
}

long DesfireEV1Card::getValue(Reader *reader, unsigned char fileno, long *amount) {
	unsigned char cmd[arraySize], resbuf[arraySize], sam_cmd[arraySize], sam_res[arraySize];
	int i = 0, j;
	unsigned long reslen = 256, sam_res_len = 256;
	memcpy(cmd,DesfireCardCmdUtil::ISO_getValue,sizeof(DesfireCardCmdUtil::ISO_getValue));
	cmd[5] = fileno;
	int index = sizeof(DesfireCardCmdUtil::ISO_getValue);
	PrintHexArray("CMD: ",index , cmd);
	memcpy(sam_cmd,MifareSAMCmdUtil::ISO_getValue_1,sizeof(MifareSAMCmdUtil::ISO_getValue_1));
	sam_cmd[6] = fileno;
	unsigned long cmdLen = sizeof(MifareSAMCmdUtil::ISO_getValue_1);
	PrintHexArray("SAM_CMD: ", cmdLen, sam_cmd);

	unsigned long ret = reader->sendToSAM(sam_cmd, cmdLen, sam_res, &sam_res_len, GetSAMType(Card::DESFIRE_EV1_CARD));
	PrintHexArray("SAM RES: ", sam_res_len, sam_res);
	if (ret != APP_SUCCESS) {
		return APP_ERROR;
	}
	PrintHexArray("CMD: ", 7, cmd);
	ret = reader->sendToCard(cmd, 7, resbuf, &reslen, Card::DESFIRE_EV1_CARD); 
	// printf("RET == %ld", ret);
	unsigned char TbyeAr[3];
	TbyeAr[0] = 0x00;
	TbyeAr[1] = 0x00;
	TbyeAr[2] = static_cast<unsigned char>(4);
	PrintHexArray("3 byte array : ", 3, TbyeAr);
	PrintHexArray("ENC RES: ", reslen, resbuf);
	index = 0;

	memcpy(sam_cmd,MifareSAMCmdUtil::ISO_getValue_2,sizeof(MifareSAMCmdUtil::ISO_getValue_2));
	for (j = 2, index = 5; j >= 0; j--, index++) {
		sam_cmd[index] = TbyeAr[j];
	}
	for (j = 0, index = 8; j < int(reslen); j++, index++) {
		sam_cmd[index] = resbuf[j];
	}
	sam_cmd[index++] = 0x00;
	sam_cmd[index++] = 0x00;
	PrintHexArray("SAM_CMD: ", index, sam_cmd);
	sam_res_len = arraySize;
	ret = reader->sendToSAM(sam_cmd, index, sam_res, &sam_res_len, GetSAMType(Card::DESFIRE_EV1_CARD));
	PrintHexArray("SAM RES: ", sam_res_len, sam_res);
	*amount = CharArrayToIntLE(sam_res, sam_res_len);
	*amount = *amount / 100;
	// printf("AMOUNT: %ld\n", *amount);	
	return APP_SUCCESS;
}

long DesfireEV1Card::GetCardUID(Reader *reader,unsigned char UID[])
{
	unsigned char cmd[50], result[50];
	unsigned long resLen = arraySize;

	memcpy(cmd,DesfireCardCmdUtil::ISO_getCardVersion,sizeof(DesfireCardCmdUtil::ISO_getCardVersion));
	unsigned long sendLen = sizeof(DesfireCardCmdUtil::ISO_getCardVersion);
	PrintHexArray("getVersion1: ", sendLen, cmd);
	unsigned long ret = reader->sendToCard(cmd, sendLen, result, &resLen, Card::DESFIRE_EV1_CARD);


	memcpy(cmd,DesfireCardCmdUtil::ISO_additinalFrame,sizeof(DesfireCardCmdUtil::ISO_additinalFrame));
	int index = sizeof(DesfireCardCmdUtil::ISO_additinalFrame);
	cmd[index++] = DesfireCardCmdUtil::ISO_le;
	sendLen = index;
	resLen = arraySize;
	PrintHexArray("getVersion2: ", sendLen, cmd);
	ret = reader->sendToCard(cmd, sendLen, result, &resLen, Card::DESFIRE_EV1_CARD);
	index = sizeof(DesfireCardCmdUtil::ISO_additinalFrame);
	cmd[index++] = DesfireCardCmdUtil::ISO_le;
	sendLen = index;
	resLen = arraySize;
	PrintHexArray("getVersion3: ", sendLen, cmd);
	ret = reader->sendToCard(cmd, sendLen, result, &resLen, Card::DESFIRE_EV1_CARD);
	PrintHexArray("getVersion4: ", resLen, result);
	memcpy(UID, result, 7);
	PrintHexArray("UID: ", 7, UID);
	return APP_SUCCESS;
}


long DesfireEV1Card::readApp1File2(Reader *reader,unsigned char cardData[], unsigned long cardDataLen) {
	unsigned char fileId = 0x02;
	long ret = readFromDataFile(reader, fileId, 0, 20, cardData, cardDataLen);
	return ret;
}

long DesfireEV1Card::readFromDataFile(Reader *reader, unsigned char fileid, int offset, int len, unsigned char output[], unsigned long outputLen)
{
	unsigned char apdu[arraySize], apduRes[arraySize], payload[arraySize], bytearr[4], rotbytearr[4];
	int index = 0;

	memcpy(apdu,MifareSAMCmdUtil::ISO_read,sizeof(MifareSAMCmdUtil::ISO_read));
	index = sizeof(MifareSAMCmdUtil::ISO_read);
	IntToCharArray(offset, bytearr);
	PrintHexArray("BYTE Converted ", 4, bytearr);
	payload[0] = fileid;
	for (int i = 1, j = 3; i < 4; i++, j--) {
		rotbytearr[i] = bytearr[j];
		payload[i] = bytearr[j];
	}
	PrintHexArray("BYTE ROT ", 4, rotbytearr);
	IntToCharArray(len, bytearr);
	PrintHexArray("BYTE Converted ", 4, bytearr);
	for (int i = 4, j = 3; i < 7; i++, j--) {
		payload[i] = bytearr[j];
	}
	PrintHexArray("PAYLOAD ", 7, payload);
	memcpy(&apdu[index], payload, 7);
	apdu[13] = DesfireCardCmdUtil::ISO_le;
	PrintHexArray("APDU", 14, apdu);
	unsigned long apduLen = 14;
	unsigned long apduResLen = arraySize;

	unsigned long ret = reader->sendToSAM(apdu, apduLen, apduRes, &apduResLen, GetSAMType(Card::DESFIRE_EV1_CARD));
	if (ret != APP_SUCCESS)
	{
		PrintText("Reading Failed\n");
		return APP_ERROR;
	}
	unsigned char cmd[arraySize], cmdRes[arraySize];
	unsigned long cmdResLen;
	index = 0;
	memcpy(cmd,DesfireCardCmdUtil::ISO_read,sizeof(DesfireCardCmdUtil::ISO_read));
	index = sizeof(DesfireCardCmdUtil::ISO_read);
	memcpy(&cmd[index], payload, 7);
	cmd[12] = DesfireCardCmdUtil::ISO_le;
	PrintHexArray("CMD ", 13, cmd);
	unsigned long cmdLen = 13;
	unsigned char encData[300];
	int encDataLen = 0;
	do {
		ret = reader->sendToCard(cmd, cmdLen, cmdRes, &cmdResLen, Card::DESFIRE_EV1_CARD);
		if (ret != APP_SUCCESS)
		{
			PrintText("Reading Failed\n");
			return APP_ERROR;
		}
		memcpy(&encData[encDataLen], cmdRes, cmdResLen);
		encDataLen += cmdResLen;
		index = 0;
		memcpy(cmd,DesfireCardCmdUtil::ISO_additinalFrame,sizeof(DesfireCardCmdUtil::ISO_additinalFrame));
		index = sizeof(DesfireCardCmdUtil::ISO_additinalFrame);
		cmd[index++] = DesfireCardCmdUtil::ISO_le;
		cmdLen = index;
		PrintHexArray("CMD ", cmdLen, cmd);
		
	} while (reader->hasAdditionalFrames);
	PrintHexArray("EncData", encDataLen, encData);
	PrintHexArray("CMD ", 4, bytearr);
	index = 0;
	memcpy(apdu,MifareSAMCmdUtil::ISO_getValue_2,sizeof(MifareSAMCmdUtil::ISO_getValue_2));
	index = sizeof(MifareSAMCmdUtil::ISO_getValue_2)-1;
	apdu[index++] = (unsigned char)encDataLen + 4;
	for (int i = 5, j = 3; j >= 1; i++, j--) {
		apdu[i] = bytearr[j];
	}
	PrintHexArray("SAM APDU ", 7, apdu);
	memcpy(&apdu[8], encData, encDataLen);
	PrintHexArray("SAM APDU test", encDataLen + 8, apdu);
	index = encDataLen + 8;
	apdu[index++] = DesfireCardCmdUtil::ISO_le;
	apdu[index++] = DesfireCardCmdUtil::ISO_le;
	apduLen =index;
	PrintHexArray("SAM APDU test1", apduLen, apdu);
	apduResLen = arraySize;
	ret = reader->sendToSAM(apdu, apduLen, apduRes, &apduResLen, GetSAMType(Card::DESFIRE_EV1_CARD));
	if (ret != APP_SUCCESS)
	{
		PrintText("Reading Failed\n");
		return APP_ERROR;
	}
	memcpy(output, apduRes, apduResLen);
	outputLen = apduResLen;
	return APP_SUCCESS;
}

long DesfireEV1Card::writeToDataFile(Reader *reader, unsigned char fileno, unsigned int offset, unsigned char data[], unsigned int data_len)
{
	unsigned char apdu[arraySize], payLoad[arraySize]= {0}, out[4],dataSplit[arraySize],encData[300];
	int i = 0, j, index = 0,ind=0,SAM_MAX = 224;
	unsigned char samres[arraySize];
	int enc_data_len = 0;
	unsigned long samres_len = 262, ret;
	IntToCharArray(offset, out);
	payLoad[0] = 0x3D;
	payLoad[1] = fileno;
	for (i = 2, j = 3; j >= 1; i++, j--) {
		payLoad[i] = out[j];
	}
	IntToCharArray(data_len, out);
	for (i = 5, j = 3; j >= 1; i++, j--) {
		payLoad[i] = out[j];
	}
	if (data_len > 1)
		memcpy(&payLoad[8], data, data_len);
	else
		payLoad[8] = data[0];
	int payLoadLength = data_len +8;
	PrintHexArray("Pay Load  ", payLoadLength, payLoad);
	do{
		memcpy(dataSplit, payLoad, min(ind + SAM_MAX, data_len));
		ind = ind + SAM_MAX;
		memcpy(payLoad, dataSplit, min(ind + SAM_MAX, data_len));
		PrintHexArray("Pay Load -2 ", payLoadLength, payLoad);
		memcpy(apdu,MifareSAMCmdUtil::ISO_write_1,sizeof(MifareSAMCmdUtil::ISO_write_1));
		/*apdu[index++] = 0x80;
		apdu[index++] = 0xED;
		apdu[index++] = 0xAF;
		apdu[index++] = 0x08;*/
		index = sizeof(MifareSAMCmdUtil::ISO_write_1);
		apdu[index++] = (unsigned char )payLoadLength;
		// printf("\n\n\npayLoadLength %d\n",payLoadLength );
		memcpy(&apdu[index], payLoad, payLoadLength);
		if (ind >= payLoadLength)
		{
			apdu[2] = 0x00;
		}
		if (ind>SAM_MAX)
		{
			apdu[3] = 0x00;
		}
		PrintHexArray("SEND BUF SAM: ", payLoadLength+index, apdu);
		index += payLoadLength;
		apdu[index++] = DesfireCardCmdUtil::ISO_le;
		ret = reader->sendToSAM(apdu,index, samres, &samres_len, GetSAMType(Card::DESFIRE_EV1_CARD));
		//ret = Mifare_TransmitDataToSAM(apdu, payLoadLength + index, samres, &samres_len, g_CMD_SAM_APDU);
		PrintHexArray("SAM RES: ", samres_len, samres);
		
		memcpy(&encData[enc_data_len], samres, samres_len);
		enc_data_len += samres_len;
		memset(payLoad,0,payLoadLength);
		payLoadLength = 0;
	} while (ind < (int)data_len);

	PrintHexArray("Enc Data: ", enc_data_len, encData);
	// printf("EncData Len: %d\n", enc_data_len);
	unsigned char  cmd1_res[350];
	unsigned long cmd1_res_len = 256;
	Write_to_card(reader,fileno,encData, enc_data_len, 0x3D, cmd1_res, cmd1_res_len,offset,data_len); // function
	index = 0;
	memcpy(apdu,MifareSAMCmdUtil::ISO_write_2,sizeof(MifareSAMCmdUtil::ISO_write_2));
	/*apdu[index++] = 0x80;
	apdu[index++] = 0x5C;
	apdu[index++] = 0x00;
	apdu[index++] = 0x00;
	apdu[index++] = 0x09;
	apdu[index++] = 0x00;*/
	index = sizeof(MifareSAMCmdUtil::ISO_write_2);
	memcpy(&apdu[index], cmd1_res, cmd1_res_len - 2);
	PrintHexArray("SEND BUF SAM: ", 14, apdu);
	unsigned char samres1[arraySize];
	unsigned long samres_len1;
	samres_len1 = 20;
	payLoadLength = 14;
	ret = reader->sendToSAM(apdu, payLoadLength , samres1, &samres_len1, GetSAMType(Card::DESFIRE_EV1_CARD));
	//ret = Mifare_TransmitDataToSAM1(apdu, 14, samres1, &samres_len1, g_CMD_SAM_APDU);
	PrintHexArray("SAM RES BUF: ", samres_len1, samres1);
	if(ret != APP_SUCCESS)
		return APP_ERROR;
	return APP_SUCCESS;
}

void DesfireEV1Card::Write_to_card(Reader *reader,unsigned char fileno,unsigned char encData[],unsigned long enc_data_len,unsigned char CMD_Write, unsigned char cmd_res[], unsigned long cmd_res_len,int offset, int data_len)
{// CMD_Write is either write data to card or write record.
	unsigned char dataSplit[arraySize],cmd1[arraySize], payLoad[arraySize],out[4];
	memset(dataSplit, 0, arraySize);
	int ind = 0, i,j;
	int min_len = 0, flag = 0;
	int maxim = 47;
	payLoad[0] = fileno;
	IntToCharArray(offset, out);
	for (i = 1, j = 3; j >= 1; i++, j--) {
		payLoad[i] = out[j];
	}
	IntToCharArray(data_len, out);
	for (i = 4, j = 3; j >= 1; i++, j--) {
		payLoad[i] = out[j];
	}
	memcpy(cmd1,DesfireCardCmdUtil::ISO_write,sizeof(DesfireCardCmdUtil::ISO_write));
	cmd1[1] =CMD_Write;
	
	int payLoadLength = 0;

	do {
		reader->hasAdditionalFrames = false;
		min_len = min(ind + maxim, enc_data_len);
		enc_data_len = enc_data_len - maxim;
		// printf("min Len: %d\n", min_len);
		if (min_len > 47)
			min_len = 47;
		memcpy(dataSplit, &encData[ind], min_len);
		PrintHexArray("Data split: ", min_len, dataSplit);
		//payLoadLength = 7 + min_len;
		ind = ind + maxim;
		// printf("min Len: %d\n", min_len);
		// printf("Maxim Len: %d\n", maxim);
		// printf("index Len: %d\n", ind);
		// printf("payLoad Len: %d\n", payLoadLength);
		if (flag == 0) {
			memcpy(&payLoad[7], dataSplit, min_len);
			payLoadLength = 7 + min_len;
		}
		else {
			memcpy(&payLoad[0], dataSplit, min_len);
			payLoadLength = 0 + min_len;
		}
		cmd1[4] = (unsigned char)payLoadLength;
		////////////////////
		printf("\n\npayLoadLength%d\n",payLoadLength );
		memcpy(&cmd1[5], payLoad, payLoadLength);
		unsigned long cmd_len = payLoadLength + 5;
		cmd1[cmd_len] = DesfireCardCmdUtil::ISO_le;
		cmd_len = cmd_len + 1;
		cmd_res_len = arraySize;
		// printf("CMD Len: %d", cmd_len);
		PrintHexArray("PC -> CARD: ", cmd_len, cmd1);
		unsigned long ret = reader->sendToCard(cmd1, cmd_len, cmd_res, &cmd_res_len, Card::DESFIRE_EV1_CARD);
		
		PrintHexArray("CARD->PC: ", cmd_res_len, cmd_res);
		memcpy(cmd1,DesfireCardCmdUtil::ISO_additinalFrame,sizeof(DesfireCardCmdUtil::ISO_additinalFrame));
		
		flag = 1;
		payLoadLength = 0;
		maxim = 47;
		
	} while (reader->hasAdditionalFrames);

	PrintHexArray("CARD->PC: ", cmd_res_len, cmd_res);
}

long DesfireEV1Card::creditAmount(Reader *reader,unsigned char fileno, long amount)
{
	unsigned char cmd[arraySize],cmdRes[arraySize];
	unsigned char amt[4], apdu[arraySize], apduRes[arraySize];
	int i, j, index = 0;
	unsigned int offset = 0;
	amount = amount * 100;
	IntToCharArray(amount, amt);
	memcpy(cmd,DesfireCardCmdUtil::ISO_credit,sizeof(DesfireCardCmdUtil::ISO_credit));
	memcpy(apdu,MifareSAMCmdUtil::ISO_credit,sizeof(MifareSAMCmdUtil::ISO_credit));
	apdu[6] = fileno;
	for (i = 7, j = 3; j >= 0; i++, j--) {
		apdu[i] = amt[j];
	}
	apdu[11] = DesfireCardCmdUtil::ISO_le;
	unsigned long apduLen = 12;
	unsigned long apduResLen = 256;
	unsigned long ret = reader->sendToSAM(apdu, apduLen, apduRes, &apduResLen, GetSAMType(Card::DESFIRE_EV1_CARD));
	if (ret != APP_SUCCESS) {
		PrintText("credit Failed");
		return APP_ERROR;
	}
	cmd[4] = static_cast<unsigned char>(apduResLen)+1;
	cmd[5] = fileno;
	memcpy(&cmd[6],apduRes,apduResLen);
	cmd[22] = DesfireCardCmdUtil::ISO_le;
	unsigned long cmdLen = 23;
	unsigned long cmdResLen = 256;
	ret = reader->sendToCard(cmd, cmdLen, cmdRes, &cmdResLen, Card::DESFIRE_EV1_CARD);
	memcpy(apdu,MifareSAMCmdUtil::ISO_write_2,sizeof(MifareSAMCmdUtil::ISO_write_2));
	memcpy(&apdu[6],cmdRes,cmdResLen);
	apduLen = 14;
	ret = reader->sendToSAM(apdu, apduLen, apduRes, &apduResLen, GetSAMType(Card::DESFIRE_EV1_CARD));
	if (ret != APP_SUCCESS) {
		PrintText("credit Failed");
		return APP_ERROR;
	}
	return APP_SUCCESS;
}
long DesfireEV1Card::debitAmount(Reader *reader,unsigned char fileno,long amount)
{
	unsigned char cmd[arraySize],cmdRes[arraySize];
	unsigned char amt[4], apdu[arraySize], apduRes[arraySize];
	int i, j, index = 0;
	unsigned int offset = 0;
	amount = amount * 100;
	IntToCharArray(amount, amt);
	memcpy(cmd,DesfireCardCmdUtil::ISO_debit,sizeof(DesfireCardCmdUtil::ISO_debit));
	memcpy(apdu,MifareSAMCmdUtil::ISO_debit,sizeof(MifareSAMCmdUtil::ISO_debit));
	apdu[6] = fileno;
	for (i = 7, j = 3; j >= 0; i++, j--) {
		apdu[i] = amt[j];
	}
	apdu[11] = DesfireCardCmdUtil::ISO_le;
	unsigned long apduLen = 12;
	unsigned long apduResLen = 256;
	unsigned long ret = reader->sendToSAM(apdu, apduLen, apduRes, &apduResLen, GetSAMType(Card::DESFIRE_EV1_CARD));
	if (ret != APP_SUCCESS) {
		PrintText("debit Failed");
		return APP_ERROR;
	}
	cmd[4] = static_cast<unsigned char>(apduResLen)+1;
	cmd[5] = fileno;
	memcpy(&cmd[6],apduRes,apduResLen);
	cmd[22] = DesfireCardCmdUtil::ISO_le;
	unsigned long cmdLen = 23;
	unsigned long cmdResLen = 256;
	ret = reader->sendToCard(cmd, cmdLen, cmdRes, &cmdResLen, Card::DESFIRE_EV1_CARD);
	memcpy(apdu,MifareSAMCmdUtil::ISO_write_2,sizeof(MifareSAMCmdUtil::ISO_write_2));
	memcpy(&apdu[6],cmdRes,cmdResLen);
	apduLen = 14;
	ret = reader->sendToSAM(apdu, apduLen, apduRes, &apduResLen, GetSAMType(Card::DESFIRE_EV1_CARD));
	if (ret != APP_SUCCESS) {
		PrintText("debit Failed");
		return APP_ERROR;
	}
	// ret = commit(reader);
	// if (ret != APP_SUCCESS) {
	// 	PrintText("commit Failed");
	// 	return APP_ERROR;
	// }
	return APP_SUCCESS;
}
long DesfireEV1Card::readRecord(Reader *reader,unsigned char fileno, int offset, int len,int recordSize, unsigned char output[], unsigned long *outputLen){
	reader->hasAdditionalFrames =false;
	unsigned char apdu[arraySize], apduRes[arraySize], payload[arraySize], byteArr[4], rotByteArr[4]; // , keyno = 0x00, samkey[2] = { 0x05,0x00 };
	int index = 0;
	memcpy(apdu,MifareSAMCmdUtil::ISO_read,sizeof(MifareSAMCmdUtil::ISO_read));
	index =sizeof(MifareSAMCmdUtil::ISO_read); 
	apdu[5] = DesfireCardCmdUtil::ISO_readRecordCmd;
	IntToCharArray(offset, byteArr);
	PrintHexArray("Byte Converted: ", 4, byteArr);
	payload[0] = fileno;
	for (int i = 1, j = 3; i < 4; i++, j--) {
		rotByteArr[i] = byteArr[j];
		payload[i] = byteArr[j];
	}
	PrintHexArray("byte rotated: ", 4, rotByteArr);
	IntToCharArray(len, byteArr);
	PrintHexArray("byte coonverted: ", 4, byteArr);
	for (int i = 4, j = 3; i < 7; i++, j--) {
		payload[i] = byteArr[j];
	}
	PrintHexArray("payload: ", 7, payload);
	memcpy(&apdu[index], payload, 7);
	index += 7;
	apdu[index++] = DesfireCardCmdUtil::ISO_le;
	unsigned long apduLen = index;
	PrintHexArray("apdu: ", 13, apdu);
	unsigned long apduResLen = 20;

	unsigned long ret = reader->sendToSAM(apdu, apduLen, apduRes, &apduResLen, GetSAMType(Card::DESFIRE_EV1_CARD));
	if(ret != APP_SUCCESS)
	{
		// printf("Read Record Failed\n");
		return APP_ERROR;
	}
	unsigned char cmd[256], cmdRes[256];
	memcpy(cmd,DesfireCardCmdUtil::ISO_read,sizeof(DesfireCardCmdUtil::ISO_read));
	memcpy(&cmd[5], payload, 7);
	cmd[1] = DesfireCardCmdUtil::ISO_readRecordCmd;
	cmd[12] = DesfireCardCmdUtil::ISO_le;
	unsigned long cmdLen = 13;
	unsigned long cmdResLen = 256;

	unsigned char encData[300];
	int encDataLen = 0;
	do {
		ret = reader->sendToCard(cmd, cmdLen, cmdRes, &cmdResLen, Card::DESFIRE_EV1_CARD);
		if (ret != APP_SUCCESS)
		{
			PrintText("Reading Failed\n");
			return APP_ERROR;
		}
		memcpy(&encData[encDataLen], cmdRes, cmdResLen);
		encDataLen += cmdResLen;
		index = 0;
		memcpy(cmd,DesfireCardCmdUtil::ISO_additinalFrame,sizeof(DesfireCardCmdUtil::ISO_additinalFrame));
		index = sizeof(DesfireCardCmdUtil::ISO_additinalFrame);
		cmd[index++] = DesfireCardCmdUtil::ISO_le;
		cmdLen = index;
		PrintHexArray("CMD ", cmdLen, cmd);
		
	} while (reader->hasAdditionalFrames);
	PrintHexArray("EncData", encDataLen, encData);
	
	index = 0;
	memcpy(apdu,MifareSAMCmdUtil::ISO_getValue_2,sizeof(MifareSAMCmdUtil::ISO_getValue_2));
	index = sizeof(MifareSAMCmdUtil::ISO_getValue_2)-1;
	apdu[index++] = static_cast<unsigned char>(encDataLen) + 4;
	recordSize *= len;
	IntToCharArray(recordSize, byteArr);//////////////////////////////////////////////////////////////
	for (int i = 5, j = 3; j >= 1; i++, j--) {
		apdu[i] = byteArr[j];
	}
	PrintHexArray("SAM APDU ", 7, apdu);
	memcpy(&apdu[8], encData, encDataLen);
	PrintHexArray("SAM APDU test ", encDataLen + 8, apdu);
	index = encDataLen + 8;
	apdu[index++] = DesfireCardCmdUtil::ISO_le;
	apdu[index++] = DesfireCardCmdUtil::ISO_le;
	apduLen =index;
	PrintHexArray("SAM APDU test1", apduLen, apdu);
	apduResLen = arraySize;
	ret = reader->sendToSAM(apdu, apduLen, apduRes, &apduResLen, GetSAMType(Card::DESFIRE_EV1_CARD));
	if (ret != APP_SUCCESS)
	{
		PrintText("Reading Failed\n");
		return APP_ERROR;
	}
	memcpy(output, apduRes, apduResLen);
	*outputLen = apduResLen;
	return APP_SUCCESS;
}
long DesfireEV1Card::commit(Reader *reader)
{
	unsigned char cmd[arraySize],cmdRes[arraySize];
	unsigned long cmdResLen;
	memcpy(cmd,DesfireCardCmdUtil::ISO_commit,sizeof(DesfireCardCmdUtil::ISO_commit));
	unsigned long cmdLen = sizeof(DesfireCardCmdUtil::ISO_commit);
	unsigned long ret = reader->sendToCard(cmd, cmdLen, cmdRes, &cmdResLen, Card::DESFIRE_EV1_CARD);
	if (ret != APP_SUCCESS) {
		PrintText("commit Failed");
		return APP_ERROR;
	}
	return APP_SUCCESS;
}

long DesfireEV1Card::writeRecord(Reader *reader,unsigned char fileno,unsigned long offset,unsigned char payLoad[], unsigned long payLoadLength)
{
	unsigned char apdu[256],  apduRes[256], cmdRes[256] ={0};
	unsigned long cmdResLen =0;
	int index = 0;
	memcpy(apdu,MifareSAMCmdUtil::ISO_write_1,4);
	apdu[2] = DesfireCardCmdUtil::ISO_zero;
	index =4;
	apdu[index++] = static_cast<unsigned char>(payLoadLength)+8;
	apdu[index++] = DesfireCardCmdUtil::ISO_writeRecordCmd;
	apdu[index++] = fileno;
	apdu[index++] = DesfireCardCmdUtil::ISO_zero;
	apdu[index++] = DesfireCardCmdUtil::ISO_zero;
	apdu[index++] = DesfireCardCmdUtil::ISO_zero;
	apdu[index++] = static_cast<unsigned char>(payLoadLength);
	apdu[index++] = DesfireCardCmdUtil::ISO_zero;
	apdu[index++] = DesfireCardCmdUtil::ISO_zero;
	memcpy(&apdu[index],payLoad,payLoadLength);
	unsigned long apduResLen = 256;
	unsigned long apduLen = index+payLoadLength;
	apdu[apduLen++] = DesfireCardCmdUtil::ISO_le;
	unsigned long ret = reader->sendToSAM(apdu, apduLen, apduRes, &apduResLen, GetSAMType(Card::DESFIRE_EV1_CARD));
	if (ret != APP_SUCCESS) {
		PrintText("Write Record Failed");
		return APP_ERROR;
	}
	Write_to_card(reader,fileno,apduRes,apduResLen,DesfireCardCmdUtil::ISO_writeRecordCmd,cmdRes,cmdResLen,0, payLoadLength);
	index =0;
	memcpy(apdu,MifareSAMCmdUtil::ISO_write_2,sizeof(MifareSAMCmdUtil::ISO_write_2));
	index = sizeof(MifareSAMCmdUtil::ISO_write_2);
	// printf("cmdResLen %ld\n", cmdResLen);
	memcpy(&apdu[index], cmdRes, 8);
	apduLen = index+8;
	unsigned char samres[256];
	unsigned long samresLen = 256;
	ret = reader->sendToSAM(apdu, apduLen, samres, &samresLen, GetSAMType(Card::DESFIRE_EV1_CARD));
	if (ret != APP_SUCCESS) {
		PrintText("Write Record Failed");
		return APP_ERROR;
	}
	// ret = commit(reader);
	return APP_SUCCESS;
}

long DesfireEV1Card::addRecordInTxnLog(Reader *reader,unsigned char fileno,unsigned char txnType,long amount,long runningBalance,string bizLevelID,string terminalId,ServiceType domainType,long long timeInMsec)
{
	unsigned char amtArr[4],currBalArr[4],bizLevelIDArr[4], terminalIdArr[4];

	IntToCharArray(amount,amtArr);
	IntToCharArray(runningBalance,currBalArr);
	unsigned char *bizLevelIDPtr = (unsigned char *)bizLevelID.c_str();
	unsigned char *terminalIdPtr = (unsigned char *)bizLevelID.c_str();
	for(int i=0;i<4;i++)
	{
		bizLevelIDArr[i] = bizLevelIDPtr[i];
		terminalIdArr[i] = terminalIdPtr[i];
	}
	unsigned char payLoad[256];
	payLoad[0] = txnType;
	unsigned char time[6];
	//	long now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	longtobytes6(timeInMsec,time);
	memcpy(&payLoad[1], time, 6);
	memcpy(&payLoad[7], amtArr, 4);
	memcpy(&payLoad[11], currBalArr, 4);
	memcpy(&payLoad[15], bizLevelIDArr, 4);
	memcpy(&payLoad[19], terminalIdArr, 4);
	payLoad[23] = (unsigned char)domainType;
	memset(&payLoad[24], 0, 8);
	int dataLen = 32;
	unsigned long ret = writeRecord(reader,fileno,0,payLoad,dataLen);
	if (ret != APP_SUCCESS) {
		PrintText("adding record in txn log Failed");
		return APP_ERROR;
	}
	return APP_SUCCESS;
}

long DesfireEV1Card::activateCard(Reader *reader,unsigned char posTerminalId[],long long issueDate)
{	
	unsigned char out[4],payLoad[256], fileno = 0x01,status = static_cast<int>(CardStatusEnum::activate);
	unsigned int offset = 97, len = 13;
	long long expiryDate=0;
	issueDate = trimMilliseconds(issueDate);
	// issueDate = issueDate/1000;
	IntToCharArray(issueDate,out);
	payLoad[0] = static_cast<unsigned char>(status);
	memcpy(&payLoad[5],out,4);
	expiryDate=issueDate+315532800; //Asuming 10 years from the date of issue
	IntToCharArray(expiryDate,out);
	memcpy(&payLoad[9],out,4);
	unsigned long ret = writeToDataFile(reader,fileno,offset,payLoad,len);
	if(ret != APP_SUCCESS)
	{
		printf("Activating card is Failed");
		return APP_ERROR;
	}
	return APP_SUCCESS;
}

long DesfireEV1Card::updateCardHoldersData(Reader *reader,unsigned char fileNo,unsigned char FName[],unsigned char MName[],unsigned char LName[],unsigned char Gender,long DOB, long long MobileNumber, long long Adhar)
{
	unsigned char payLoad[256],DateofBirth[4],MN[6],Aadhar[5];
	memset(payLoad,0,256);
	IntToCharArray(DOB, DateofBirth);
	PrintHexArray("DateofBirth ", 4, DateofBirth);

	longtobytes6(MobileNumber, MN);
	PrintHexArray("MObileNO HEX ", 6, MN);

	longtobytes6(Adhar, Aadhar);
	PrintHexArray("Aadhar ", 5, Aadhar);
	
	memcpy(payLoad, FName, 16);                //Setting FirstName
	memcpy(&payLoad[16], MName, 16);    	  //Setting MiddleName
	memcpy(&payLoad[32], LName, 16);           //Setting LastName
	payLoad[48] = Gender;                       //Setting Gender
	memcpy(&payLoad[49], DateofBirth, 4);   //Setting DOB
	memcpy(&payLoad[53], MN, 6);            //Setting MobileNumber
	memcpy(&payLoad[59], Aadhar, 5);             //Setting Aadhar
	
	unsigned int data_len = 64;
	unsigned int offset =0;
	long ret = writeToDataFile(reader, fileNo, offset, payLoad,data_len);
	if (ret != APP_SUCCESS) {
		PrintText("updatePersonalData Failed");
		return APP_ERROR;
	}
	return APP_SUCCESS;
}

long DesfireEV1Card::updateVehicleData(Reader *reader,unsigned char fileNo, int VehicleType, unsigned char vehicleNo[]) {
	unsigned char VN[16],payLoad[17];
	memset(VN, 0, 16);

	unsigned char VT = static_cast<unsigned char>(VehicleType);
	// printf("VehicleType: %02x\n", VT);

	payLoad[0] = VT;					     //Setting VehicleType
	memcpy(&payLoad[1], VN, 16);             //Setting VehicleType

	unsigned int data_len = 17;
	unsigned int offset =64;

	unsigned long ret = writeToDataFile(reader, fileNo, offset, payLoad,data_len);
	if (ret != APP_SUCCESS) {
		PrintText("updateVehicleData Failed");
		return APP_ERROR;
	}
	return APP_SUCCESS;
}

bool DesfireEV1Card::isCardActive(Reader *reader,long long timeInMsec)
{
	unsigned char output[256];
	unsigned int offset = 97, len = 13;
	unsigned long outputLen;
	int hour;
	unsigned char fileId =0x01;
	long ret = readFromDataFile(reader,fileId,offset,len,output,outputLen);
	if(ret != APP_SUCCESS)
	{
		PrintText("Read Failed Try again");
		return false;
	}
	timeInMsec = trimMilliseconds(timeInMsec);
	// timeInMsec = timeInMsec/1000;
	unsigned char expiryDateArr[4];
	unsigned char status = output[0];
	memcpy(expiryDateArr,&output[8],4);
	int currentDate,currentMonth, currentYear;
	int expiryDate = 0 , expiryMonth = 0 ,expiryYear =0;
	long long cardStoredtime = CharArrayToInt(expiryDateArr,4);
	// getdateFromEpochTime(cardStoredtime,expiryDate,expiryMonth,expiryYear,hour);
	// getdateFromEpochTime(timeInMsec,currentDate,currentMonth,currentYear,hour);
	if(status == static_cast<int>(CardStatusEnum::activate) && timeInMsec <= cardStoredtime)
		return true;
	return false;
}

long DesfireEV1Card::buyTollTicket(Reader *reader,unsigned char branchId[],long long txnTime,long long expiryTime){

	unsigned char payLoad[256],out[4];
	unsigned char fileno = 5;
	memcpy(payLoad, branchId, 4);
	txnTime = trimMilliseconds(txnTime);
	IntToCharArray(txnTime, out);
	PrintHexArray("TXN TIME: ",4,out);
	memcpy(&payLoad[4], out, 4);
	expiryTime = trimMilliseconds(expiryTime);
	cout<<"expiryTime "<<expiryTime<<endl;
	IntToCharArray(expiryTime, out);
	PrintHexArray("expiryTime: ",4,out);
	memcpy(&payLoad[8], out, 4);
	memset(&payLoad[12], 0, 4);//RFU
	unsigned long payLoadLength = 16;
	unsigned long ret = writeRecord(reader,fileno,0,payLoad,payLoadLength);
	if(ret != APP_SUCCESS)
	{
		PrintText("Buying toll Tkt Failed");
		return APP_ERROR;
	}

	return APP_SUCCESS;
}

long DesfireEV1Card::buyRailwayTkt(Reader *reader,unsigned char stationID[],long long pnrNo,long long bookedDate)
{
	unsigned char payLoad[256],out[5];
	unsigned char fileno = 7;
	memcpy(payLoad, stationID, 4);
	longtobytes6(pnrNo, out);
	memcpy(&payLoad[4], out, 5);
	bookedDate = trimMilliseconds(bookedDate);
	IntToCharArray(bookedDate, out);
	memcpy(&payLoad[9], out, 4);
	memset(&payLoad[13], 0, 3);//RFU
	unsigned long payLoadLength = 16;
	unsigned long ret = writeRecord(reader,fileno,0,payLoad,payLoadLength);
	if(ret != APP_SUCCESS)
	{
		PrintText("Buying Railway Tkt Failed");
		return APP_ERROR;
	}
	return APP_SUCCESS;
}
 
long DesfireEV1Card::buyMetroTkt(Reader *reader,unsigned char stationID[],long long startDatetime)
{
	unsigned char payLoad[256],out[4];
	unsigned char fileno = 0x09;
	memcpy(payLoad, stationID, 4);
	startDatetime = trimMilliseconds(startDatetime);
	// startDatetime /= 1000;
	IntToCharArray(startDatetime, out);
	memcpy(&payLoad[4], out, 4);
	memset(&payLoad[8], 0, 8);//RFU
	unsigned long payLoadLength = 16;	
	unsigned long ret = writeRecord(reader,fileno,0,payLoad,payLoadLength);
	if(ret != APP_SUCCESS)
	{
		PrintText("Buying Railway Tkt Failed");
		return APP_ERROR;
	}
	return APP_SUCCESS;
}
long DesfireEV1Card::buyBusTkt(Reader *reader,unsigned char stationID[],long long startDatetime)
{
	unsigned char payLoad[256],out[4];
	unsigned char fileno = 0x0B;
	memcpy(payLoad, stationID, 4);
	startDatetime = trimMilliseconds(startDatetime);
	// startDatetime /= 1000;
	IntToCharArray(startDatetime, out);
	memcpy(&payLoad[4], out, 4);
	memset(&payLoad[8], 0, 8);//RFU
	unsigned long payLoadLength = 16;	
	unsigned long ret = writeRecord(reader,fileno,0,payLoad,payLoadLength);
	if(ret != APP_SUCCESS)
	{
		PrintText("Buying Bus Tkt Failed");
		return APP_ERROR;
	}
	return APP_SUCCESS;
}

long DesfireEV1Card::buyParkingTkt(Reader *reader,long long startTime,long long endTime)
{
	unsigned char payLoad[256],out[4];
	unsigned char fileno = 0x0D;
	IntToCharArray(startTime, out);
	memcpy(payLoad, out, 4);
	longtobytes6(endTime, out);
	memcpy(&payLoad[4], out, 4);
	memset(&payLoad[8], 0, 8);//RFU
	unsigned long payLoadLength = 16;	
	unsigned long ret = writeRecord(reader,fileno,0,payLoad,payLoadLength);
	if(ret != APP_SUCCESS)
	{
		PrintText("Buying Parking Tkt Failed");
		return APP_ERROR;
	}
	return APP_SUCCESS;	
}

long DesfireEV1Card::createPass(Reader *reader,ServiceType serviceType,unsigned char FromBranchID[], unsigned char ToBranchId[],
					unsigned char NoofTrips[], unsigned char ExpiryDate[], unsigned char Passtype,
					unsigned char limitPeriodicity, int limitMaxCount[],  
					unsigned char limitStartTime[], unsigned char limitStartTime1[],bool isRenewal)
{
	// only Metro Toll Bus Passes
	unsigned char fileno;
	unsigned long ret;
	unsigned char LimitsRemainingCount[2] = {0},LimitRemainingCount1[2] = {0};
	unsigned char byteLimitsMaxCount[2] = { 0 }, byteLimitsMaxCount1[2] = { 0 };

	IntToCharArrayToBytes(limitMaxCount[0], byteLimitsMaxCount);
	IntToCharArrayToBytes(limitMaxCount[1], byteLimitsMaxCount1);
	printf("limitMaxCount-- %d\n", limitMaxCount[0]);
	printf("limitMaxCount1-- %d\n", limitMaxCount[1]);
	limitMaxCount[0] -= 1;
	limitMaxCount[1] -= 1;
	IntToCharArrayToBytes(limitMaxCount[0], LimitsRemainingCount);
	IntToCharArrayToBytes(limitMaxCount[1], LimitRemainingCount1);


	PrintHexArray("LimitsRemainingCount ", 2, LimitsRemainingCount);

	PrintHexArray("LimitsRemainingCount1 ", 2, LimitRemainingCount1);

	int i = 0;
	int passId = 1;
	int offset = 0,is_equlal =0;
	unsigned char output[4],zeros[4];
	bool flag = false;
	memset(zeros, 0, 4);
	unsigned char payload[256];
	memset(payload,0,256);
	memcpy(payload, FromBranchID, 4);
	memcpy(&payload[4], ToBranchId, 4);
	memcpy(&payload[8], NoofTrips, 2);
	memcpy(&payload[10], ExpiryDate, 4);
	payload[14] = Passtype;
	payload[15] = limitPeriodicity;
	memcpy(&payload[16], byteLimitsMaxCount, 2);
	memcpy(&payload[18], LimitsRemainingCount, 2);
	memcpy(&payload[20], limitStartTime, 4);
	// cout<<"limitPeriodicity "<<(limitPeriodicity & 0x0f)<<endl;; //Taking first 4 bits
	if((limitPeriodicity & 0x0f) != 0){
	memcpy(&payload[24], byteLimitsMaxCount1, 2);
	memcpy(&payload[26], LimitRemainingCount1, 2);
	memcpy(&payload[28], limitStartTime1, 4);
	}
	PrintHexArray("Payload ", 32, payload);
	if(serviceType == ServiceType::TOLL)
	{
		fileno = 0x04;
	}else if(serviceType == ServiceType::BUS)
	{
		fileno = 0x06;
	}else if(serviceType == ServiceType::METRO)
	{
		fileno =0x08;
	}else
	{
		PrintText("Invalid serviceType code");
		return APP_ERROR;
	}
	do {
		ret = readBranchIDofPass(reader,serviceType,fileno, passId, output);
		if(ret != APP_SUCCESS)
		{
			return APP_ERROR;
		}
		PrintHexArray("Output ", 4, output);
		PrintHexArray("Branch ID ", 4, FromBranchID);
		if (Equals(output, 4, zeros, 4))
		{
			printf("PassID %d\n",passId );
			PrintText("IF LOOP -2 \n\n\n\n");
			printf("TOLLPASS ID : %d\n", passId); 
			break;
			
		}
		else if (Equals(output, 4, FromBranchID, 4))
		{
			PrintText("IF LOOP -1 \n\n\n\n");
			PrintText("PASS Already Exists");
			printf("TOLLPASS ID : %d\n", passId); 
			flag = true;
			break;
		}

		passId += 1;
	} while (passId <= 3);
	printf("isRenewal  %d\n",isRenewal );
	 if (flag && !isRenewal) {
		printf("isRenewal  %d\n",isRenewal );
		cout<<"flag "<<flag<<endl;
		PrintText("PASS Already Exists Try Creating from new Branch ID");
		return APP_ERROR;
	}
	if (passId == 1)
	{
		offset = 0;
	}
	else if (passId == 2) {
		offset = 32;
	}
	else if (passId == 3)
	{
		offset = 64;
	}	
	else
	{
		printf("PassID %d\n",passId );
		PrintText("Your Pass only Supports only 3 Passes");
		return APP_ERROR;
	}
	printf("PassID %d\n",passId );
	if (passId == 1 || passId == 2|| passId == 3)
		ret = writeToDataFile(reader,fileno, offset, payload, 32);
	if(ret!= APP_SUCCESS)
	{
		return APP_ERROR;
	}
	return APP_SUCCESS;
}

long DesfireEV1Card::readBranchIDofPass(Reader *reader,ServiceType serviceType,unsigned char fileno,int passId, unsigned char output[]) {
	PrintText("-----------------------");
	PrintText("  Read Pass branchId - ");
	printf("%d\n", passId);
	long ret;
	int offset, readfrom;
	unsigned long outputLen;
	if(serviceType == ServiceType::TOLL || serviceType == ServiceType::BUS || serviceType == ServiceType::METRO ||serviceType == ServiceType::PARKING)
	{	
		if (passId == 1) {
		readfrom = 0;
		offset = 4;
		}
		if (passId == 2) {
			readfrom = 32;
			offset = 4;
		}
		if (passId == 3) {
			readfrom = 64;
			offset = 4;
		}
		if(passId == 4) //only metro
		{
			readfrom = 96;
			offset =4;
		}
		ret = readFromDataFile(reader, fileno, readfrom, offset, output, outputLen);
		if(ret != APP_SUCCESS)
		{
			return APP_ERROR;
		}
		return APP_SUCCESS;;
	}
}

long DesfireEV1Card::readPass(Reader *reader,ServiceType serviceType,unsigned char srcBranchID[],unsigned char passOutPut[],int *passOutPutLen)
{
	unsigned char  payload[256], fileno;
	int flag = 0, PassID = 1, offset =0, length = 96;
	unsigned char pass1[32], pass2[32], pass3[32];
	unsigned long outputLen=96;
	if(serviceType == ServiceType::TOLL)
	{
		fileno = 0x04;
	}else if(serviceType == ServiceType::BUS)
	{
		fileno = 0x0A;
		length = 128;
	}else if(serviceType == ServiceType::METRO)
	{
		fileno =0x08;
		 length = 128;
	}else if(serviceType == ServiceType::RAIL)
	{
		fileno = 0x06;
	}else if(serviceType == ServiceType::PARKING)
	{
		fileno =0x0C;
		length = 128;
	}else
	{
		PrintText("Invalid serviceType code");
		return APP_ERROR;
	}
	unsigned long ret = readFromDataFile(reader, fileno, offset, length, payload,outputLen);
	if(ret != APP_SUCCESS)
	{
		PrintText("reading Passes Failed");
		return APP_ERROR;
	}
	memcpy(pass1, payload, 32);
	memcpy(pass2, &payload[32], 32);
	memcpy(pass3, &payload[64], 32);
	if(Equals(srcBranchID,4,pass1,4))
	{
		PrintText("Yours pass");
		PrintHexArray("Pass 1 : ", 32, pass1);
		memcpy(passOutPut,pass1,32);
		*passOutPutLen = 32;
	}
	else if(Equals(srcBranchID,4,pass2,4))
	{
		PrintText("Yours pass");
		PrintHexArray("Pass 2 : ", 32, pass2);
		memcpy(passOutPut,pass2,32);
		*passOutPutLen = 32;
	}
	else if(Equals(srcBranchID,4,pass2,4))
	{
		PrintText("Yours pass");
		PrintHexArray("Pass 3 : ", 32, pass3);
		memcpy(passOutPut,pass3,32);
		*passOutPutLen = 32;
	}else{
		PrintText("The Pass You asked is not found \nThese are the passes available in the card");
		PrintHexArray("Pass 1 : ", 32, pass1);
		PrintHexArray("Pass 2 : ", 32, pass2);
		PrintHexArray("Pass 3 : ", 32, pass3);
		// memcpy(passOutPut,0,32);
		 passOutPut = {0};
		*passOutPutLen = 0;

	}
	
	return APP_SUCCESS;
}
long DesfireEV1Card::decrementPassTrips(Reader *reader,ServiceType serviceType,unsigned char branchId[],long long timeInMsec,int passType)
{
	int passId;
	int index;
	unsigned char passData[32] = {0},fileno;
	if(serviceType == ServiceType::TOLL)
	{
		fileno = 0x04;
	}else if(serviceType ==ServiceType::BUS)
	{
		fileno = 0x0A;
	}else if(serviceType == ServiceType::METRO)
	{
		fileno =0x08;
	}else
	{
		PrintText("Invalid serviceType code");
		return APP_ERROR;
	}	

	long ret = readPassType(reader,fileno,branchId,passType,&passId,passData);
	long partialPassDatalen =32;
	if(ret !=APP_SUCCESS)
	{
		PrintText("ERROR in passType");
		return APP_ERROR; 
	}
	if (serviceType == ServiceType::TOLL || serviceType ==ServiceType::BUS || serviceType == ServiceType::METRO)
	{
		index = 8;
		PrintText("TOLL||METRO||BUS ");
		partialPassDatalen -= index;
	}
	if(serviceType == ServiceType::RAIL)
	{
		index = 6;
		partialPassDatalen -= index;
	}
	if (serviceType == ServiceType::PARKING)
	{
		index = 4;
		partialPassDatalen -= index;
	}
	// printf("timeInMsec :: %lld\n",timeInMsec );
	unsigned char partialPassData[28] ={0};
	PrintHexArray("passData ",32,passData);
	memcpy(partialPassData,&passData[index],partialPassDatalen);
	PrintHexArray("partialPassData: ",partialPassDatalen,partialPassData);
	ret = passActions(reader,partialPassData,timeInMsec);
	if(ret !=APP_SUCCESS)
	{
		PrintText("ERROR  in passActions");
		return APP_ERROR;
	}
	PrintHexArray("partialPassData: ",partialPassDatalen,partialPassData);
	memcpy(&passData[index],partialPassData,partialPassDatalen);
	PrintHexArray("passData: ",32,passData);
	// printf("passId : %d\n", passId);
	
	unsigned int offset;
	if(passId == 1)
	{
		offset = 0;
	}if(passId == 2)
	{
		offset = 32;
	}if(passId == 3)
	{
		offset = 64;
	}
	ret = writeToDataFile(reader,fileno,offset,passData,32);
	if(ret !=APP_SUCCESS)
	{
		PrintText("Error in writing back to card");
		return APP_ERROR;
	}
	return APP_SUCCESS;
}

long DesfireEV1Card::readPassType(Reader *reader,unsigned char fileno,unsigned char branchId[],int passType,int *passId,unsigned char passData[]){
	unsigned char payLoad[256],pass1[32],pass2[32],pass3[32];
	unsigned char pass1BranchID[4], pass2BranchID[4], pass3BranchID[4];
	unsigned long outputLen;
	int offset = 0,length=96;
	long ret = readFromDataFile(reader, fileno, offset, length, payLoad,outputLen);
	memcpy(pass1,payLoad,32);
	memcpy(pass2,&payLoad[32],32);
	memcpy(pass3,&payLoad[32],32);
	PrintHexArray("Pass1: ",32,pass1);
	PrintHexArray("Pass2: ",32,pass2);
	PrintHexArray("Pass3: ",32,pass3);
	memcpy(pass1BranchID,pass1,4);
	memcpy(pass2BranchID,pass2,4);
	memcpy(pass3BranchID,pass3,4);
	if(Equals(pass1BranchID,4,branchId,4))
	{
		// PrintText("Test Reached ");
		// if(pass1[14] == passType){
			*passId = 1;
			PrintText("Test1 Reached ");
			memcpy(passData,pass1,32);
		// }
	}else if(Equals(pass2BranchID,4,branchId,4))
	{
		// if(pass2[14] == passType){
			*passId = 2;
			memcpy(passData,pass2,32);
		// }
	}else if(Equals(pass3BranchID,4,branchId,4))
	{
		// if(pass3[14] == passType){
			*passId = 3;
			memcpy(passData,pass3,32);
		// }
	}else{
		PrintText("required pass type is not found in the card");
		return APP_ERROR;
	}
	return APP_SUCCESS;
}

long DesfireEV1Card::passActions(Reader *reader,unsigned char payLoad[],long long timeInMsec)
{	
	PrintHexArray("payLoad ",24,payLoad);
	unsigned char maxNoOfTrips[2],expiryDate[4],limitMaxCount[2],limitRCount[2],limitsStartDate[4],limitsStartDate1[4],limitMaxCount1[2],limitRCount1[2];
	long noOfTrips;
	unsigned char currentTime[4];
	int expiryDateNo=0,expiryMonthNo=0,expiryYearNo=0,expiryHour=0;
	int currentDateNo=0,currentMonthNo=0,currentYearNo=0,currentHour=0;
	int limitsStartDateNo[2]={0},limitsStartMonthNo[2]={0},limitsStartYearNo[2]={0};
	int limitsHours[2];
		
	long limitsStartDateInLong[2];
	long limitRCountInLong[2] ={0};
	long maxCount[2] = {0};

	memcpy(maxNoOfTrips,&payLoad[0],2);
	memcpy(expiryDate,&payLoad[2],4);
	unsigned char passType = payLoad[6];
	unsigned char limitPeriodicity = payLoad[7];
	memcpy(limitMaxCount,&payLoad[8],2);
	memcpy(limitRCount,&payLoad[10],2);
	memcpy(limitsStartDate,&payLoad[12],4);
	memcpy(limitMaxCount1,&payLoad[16],2);
	memcpy(limitRCount1,&payLoad[18],2);
	memcpy(limitsStartDate1,&payLoad[20],4);
	int periodicityLimit[2] = {0};

	periodicityLimit[0] = limitPeriodicity>>4; //Taking last 4 bits
	periodicityLimit[1] = limitPeriodicity & 0x0f; //Taking first four bits

	long expiryDateInLong = CharArrayToInt(expiryDate,4); //Expiry Date in the card to epoch time
	PrintHexArray("expiryDate: ",4,expiryDate);
	timeInMsec = trimMilliseconds(timeInMsec);
	PrintText("Expiry Date: ");
	getdateFromEpochTime(expiryDateInLong,&expiryDateNo,&expiryMonthNo,&expiryYearNo,&expiryHour); //Separate date Month and year from the expiryDate from the card

	PrintText("Current Time: ");
	getdateFromEpochTime(timeInMsec,&currentDateNo,&currentMonthNo,&currentYearNo,&currentHour); //Seperate Date month and time from current time
	IntToCharArray(timeInMsec,currentTime);
	limitsStartDateInLong[0] = CharArrayToInt(limitsStartDate,4);  //first Limit to Epoch Time
	PrintText("First limit Time: ");
	getdateFromEpochTime(limitsStartDateInLong[0],&limitsStartDateNo[0],&limitsStartMonthNo[0],&limitsStartYearNo[0],&limitsHours[0]); 

	limitsStartDateInLong[1] = CharArrayToInt(limitsStartDate1,4);  //second Limit to Epoch Time
	PrintText("second limit Time: ");
	getdateFromEpochTime(limitsStartDateInLong[1],&limitsStartDateNo[1],&limitsStartMonthNo[1],&limitsStartYearNo[1],&limitsHours[1]);

	limitRCountInLong[1] = CharArrayToInt(limitRCount1,2);
	limitRCountInLong[0] = CharArrayToInt(limitRCount,2);
	unsigned int maxCountInt = CharArrayToInt(maxNoOfTrips,2);
	
	maxCount[0] = CharArrayToInt(limitMaxCount,2);
	maxCount[1] = CharArrayToInt(limitMaxCount1,2);
	cout<<"expiryDateInLong "<<expiryDateInLong<<endl;
	cout<<"timeInMsec "<<timeInMsec<<endl;
	unsigned char resettedTime[8]={0};
	if(expiryDateInLong>=timeInMsec && maxCountInt != 0){
		for(int i=0;i<2;i++)
		{
			switch(static_cast<LimitPeriodicity>(periodicityLimit[i]))
			{
				case LimitPeriodicity::UNKNOWN_PERIODICITY:
					PrintText("case 0");
					continue;
				case LimitPeriodicity::DAILY:
					if(currentDateNo == limitsStartDateNo[i])
					{
						PrintText("DAILY TRIPS");
						if(limitRCountInLong[i]>0)
						{
								limitRCountInLong[i] -= 1;
								memcpy(&resettedTime[i*4],limitsStartDate,4);
						}
						else{
							PrintText("DAILY Trips are used Trip count =0");
							return APP_ERROR;
						}
					}else if(currentDateNo != limitsStartDateNo[i] && currentMonthNo != limitsStartMonthNo[i]){
						memcpy(&resettedTime[i*4],currentTime,4);
						limitRCountInLong[i] = maxCount[i]-1;
					}	
					break;
				case LimitPeriodicity::MONTHLY:
					if(currentMonthNo == limitsStartMonthNo[i])
					{
						PrintText("MONTHLY TRIPS");
						if(limitRCountInLong[i]>0)
						{
								limitRCountInLong[i] -= 1;
								memcpy(&resettedTime[i*4],limitsStartDate,4);
						}
						else{
							PrintText("ALL Trips are used Trip count =0");
							return APP_ERROR;
						}
					}if(currentMonthNo != limitsStartMonthNo[i] && (currentYearNo != limitsStartYearNo[i])){
						memcpy(&resettedTime[i*4],currentTime,4);
						limitRCountInLong[i] = maxCount[i]-1;
					}
					break;
				case LimitPeriodicity::YEARLY:
					if(currentYearNo == limitsStartYearNo[i])
					{
						PrintText("YEARLY TRIPS");
						if(limitRCountInLong[i]>0)
						{
								limitRCountInLong[i] -= 1;
								memcpy(&resettedTime[i*4],limitsStartDate,4);
						}
						else{
							PrintText("ALL Trips are used Trip count =0");
							return APP_ERROR;
						}
					}if(currentYearNo == limitsStartYearNo[i]){
						memcpy(&resettedTime[i*4],currentTime,4);
						limitRCountInLong[i] = maxCount[i]-1;
					}
					break;
				case LimitPeriodicity::HOURLY:
					if(currentYearNo == limitsStartYearNo[i] && currentMonthNo == limitsStartMonthNo[i] &&currentDateNo == limitsStartDateNo[i] && currentHour == limitsHours[i])
					{
						PrintText("HOURLY TRIPS");
						if(limitRCountInLong[i]>0)
						{
								limitRCountInLong[i] -= 1;
								memcpy(&resettedTime[i*4],limitsStartDate,4);
								//maxCountInt -= 1;
						}
						else{
							PrintText("ALL Trips are used Trip count =0");
							return APP_ERROR;
						}
					}
					// if(currentYearNo == limitsStartYearNo[i]){
					// 	memcpy(limitsStartDate,currentTime,4);
					// 	limitRCountInLong[i] = maxCount[i]-1;
					// }
					break;
				case LimitPeriodicity::WEEKLY:
					if(currentYearNo == limitsStartYearNo[i] && currentMonthNo == limitsStartMonthNo[i] &&currentDateNo <= limitsStartDateNo[i] )
					{
						PrintText("WEEKLY TRIPS");
						// printf("limitRCountInLong  %d\n", limitRCountInLong[i]);
						if(limitRCountInLong[i]>0)
						{
								limitRCountInLong[i] -= 1;
								memcpy(&resettedTime[i*4],limitsStartDate,4);
								//maxCountInt -= 1;
						}
						else{
							PrintText("ALL Trips are used Trip count =0");
							return APP_ERROR;
						}
					}
					break;
				case LimitPeriodicity::QUARTLY:
				case LimitPeriodicity::HALFYEARLY:
					if(currentYearNo == limitsStartYearNo[i] && currentMonthNo == limitsStartMonthNo[i])
					{
						PrintText("QUARTLY/HALFYEARLY TRIPS");
						if(limitRCountInLong[i]>0)
						{
								limitRCountInLong[i] -= 1;
								memcpy(&resettedTime[i*4],limitsStartDate,4);
								//maxCountInt -= 1;
						}
						else{
							PrintText("ALL Trips are used Trip count =0");
							return APP_ERROR;
						}
					}
					break;
			}
		}
		maxCountInt -= 1;
	}else{
			PrintText("PASS expired ");
			return APP_ERROR;
		}
	PrintHexArray("payLoad: ",24,payLoad);
	// printf("maxCountInt: %d\n", maxCountInt);
	limitRCount[1] = (unsigned char)limitRCountInLong[0];
	limitRCountInLong[0] = limitRCountInLong[0]>> 8;
	limitRCount[0] = (unsigned char)limitRCountInLong[0];

	limitRCount1[1] = (unsigned char)limitRCountInLong[1];
	limitRCountInLong[1] = limitRCountInLong[1]>> 8;
	limitRCount1[0] = (unsigned char)limitRCountInLong[1];

	maxNoOfTrips[1] = (unsigned char)maxCountInt;
	maxCountInt = maxCountInt>> 8;
	maxNoOfTrips[0] = (unsigned char)maxCountInt;

	memcpy(&payLoad[10],limitRCount,2);
	memcpy(&payLoad[18],limitRCount1,2);
	memcpy(payLoad,maxNoOfTrips,2);
	PrintHexArray("limitsStartDate ",4,limitsStartDate);
	for(int i=0;i<2;i++){
		memcpy(&payLoad[((i*8)+12)],&resettedTime[i*4],4);
	}
		//TODO: copy limitsStartDate
		PrintHexArray("payLoad Final: ",24,payLoad);
	return APP_SUCCESS;
}

long DesfireEV1Card::readTxnLog(Reader *reader,unsigned char fileNo,int offset,int noOfTxns,unsigned char output[],unsigned long *outputLen)
{
	int recordSize =32;
	long ret =readRecord(reader,fileNo,offset,noOfTxns,recordSize,output,outputLen);
	if(ret!= APP_SUCCESS)
	{
		PrintText("Reading TXN log Failed");
		return APP_ERROR;
	}
	return APP_SUCCESS;
}

long DesfireEV1Card::getFileSettings(Reader *reader,unsigned char fileNo,unsigned char response[],unsigned long *responselen)
{
	unsigned char cmd[256];
	memcpy(cmd,DesfireCardCmdUtil::ISO_threePass,sizeof(DesfireCardCmdUtil::ISO_threePass));
	cmd[1] = DesfireCardCmdUtil::ISO_getFileSettings;
	cmd[5] = fileNo;
	unsigned long cmdLen = sizeof(DesfireCardCmdUtil::ISO_threePass);
	*responselen =256;
	unsigned long ret = reader->sendToCard(cmd, cmdLen, response, responselen, Card::DESFIRE_EV1_CARD);
	if(ret!= APP_SUCCESS)
	{
		PrintText("getFileSettings Failed");
		return APP_ERROR;
	}
	return APP_SUCCESS;
}

long DesfireEV1Card::createRailwayPass(Reader *reader,unsigned char CRISID[],unsigned char noOfTrips[], unsigned char ExpiryDate[], unsigned char passType,
					unsigned char limitPeriodicity, unsigned char limitMaxCount[],  
					unsigned char limitStartTime[],unsigned char limitsMaxCount1[], unsigned char limitStartTime1[],long long currentTime)
{
	unsigned char fileNo = 0x06;
	bool isExpired = false;
	unsigned char limitsRemainingCount[2] = {0},LimitRemainingCount1[2] = {0};
	int i = 0;
	int passId =1;
	int offset = 0,is_equlal =0;
	unsigned char output[4],zeros[4];
	bool flag = false;
	memset(zeros, 0, 4);
	unsigned char payload[256];
	memcpy(payload, CRISID, 6);
	memcpy(&payload[6], noOfTrips, 2);
	memcpy(&payload[8], ExpiryDate, 4);
	payload[12] = passType;
	payload[13] = limitPeriodicity;
	memcpy(&payload[14], limitMaxCount, 2);
	memcpy(&payload[16], limitsRemainingCount, 2);
	memcpy(&payload[18], limitStartTime, 4);
	memcpy(&payload[22], limitsMaxCount1, 2);
	memcpy(&payload[24], LimitRemainingCount1, 2);
	memcpy(&payload[26], limitStartTime1, 4);
	do{
		long ret = checkRailWayPassExpired(reader,fileNo,isExpired, currentTime,passId);
		if(isExpired)
		{
			ret = writeToDataFile(reader,fileNo,offset,payload,32);
			if(ret != APP_SUCCESS)
			{
				PrintText("Railway PASS Create Failed");
				return APP_ERROR;
			}
			break;
		}
		passId +=1;
		offset +=32;

	}while(passId<3);
	return APP_SUCCESS;
}

long DesfireEV1Card::checkRailWayPassExpired(Reader *reader,unsigned char fileNo,bool isExpired, long long timeInsec,int passId)
{
	unsigned char pass1[32],pass2[32];
	unsigned char dateInCard[4];
	unsigned char outPut[256];
	long long date;
	unsigned long outputLen;
	int offset =0,len =64;
	long ret = readFromDataFile(reader, fileNo, offset, len, outPut, outputLen);
	if(ret != APP_SUCCESS)
	{
		PrintText("GET CRIS ID FAILED");
		return APP_ERROR;
	}
	memcpy(pass1,outPut,32);
	memcpy(pass2,&outPut[32],32);
	if(passId ==1)
	{
		memcpy(dateInCard,&pass1[7],4);
		date = CharArrayToInt(dateInCard,4);
		if(date < timeInsec)
		{
			isExpired = false;
		}
		else { // this also works if no passes are there in card if no passes are there it be zeros in the card
			isExpired = true;
		}
	}
	if (passId ==2)
	{
		memcpy(dateInCard,&pass2[7],4);
		date = CharArrayToInt(dateInCard,4);
		isExpired = date > timeInsec;
	}
	else{
		PrintText("Your PASS Supports only one 2 Passes");
		return APP_ERROR;
	}
	return APP_SUCCESS;
}

long DesfireEV1Card::createParkingPass(Reader *reader,unsigned char BranchId[],unsigned char noOfTrips[],long long ExpiryDate,unsigned char passType,
							unsigned char limitPeriodicity, unsigned char limitMaxCount[],  
					unsigned char limitStartTime[],unsigned char limitsMaxCount1[], unsigned char limitStartTime1[],long long currentTime)
{
	unsigned char fileNo = 0x0C,expiryDateArr[4];
	bool isExpired = false;
	unsigned char limitsRemainingCount[2] = {0},LimitRemainingCount1[2] = {0};
	int i = 0;
	int passId =1;
	int offset = 0,is_equlal =0;
	unsigned char output[4],zeros[4];
	bool flag = false;
	memset(zeros, 0, 4);
	IntToCharArrayLE(ExpiryDate,expiryDateArr);
	unsigned char payload[256];
	memcpy(payload, BranchId, 4);
	memcpy(&payload[4], noOfTrips, 2);
	memcpy(&payload[6], expiryDateArr, 4);
	payload[10] = passType;
	payload[11] = limitPeriodicity;
	memcpy(&payload[12], limitMaxCount, 2);
	memcpy(&payload[14], limitsRemainingCount, 2);
	memcpy(&payload[16], limitStartTime, 4);
	memcpy(&payload[20], limitsMaxCount1, 2);
	memcpy(&payload[22], LimitRemainingCount1, 2);
	memcpy(&payload[24], limitStartTime1, 4);
	memset(&payload[28],0,4);
	do{
		long ret = checkifParkingPassExpired(reader,fileNo,isExpired, currentTime,passId);
		if(isExpired)
		{
			ret = writeToDataFile(reader,fileNo,offset,payload,32);
			if(ret != APP_SUCCESS)
			{
				PrintText("Parking PASS Create Failed");
				return APP_ERROR;
			}
			break;
		}
		passId +=1;
		offset +=32;

	}while(passId<5);
	return APP_SUCCESS;

}

long DesfireEV1Card::checkifParkingPassExpired(Reader *reader,unsigned char fileNo,bool isExpired, long long timeInsec,int passId)
{
	unsigned char dateInCard[4];
	unsigned char outPut[256];
	unsigned long outputLen;
	int offset =0,len =32;
	switch(passId)
	{
		case 1:
			offset = 0;
			break;
		case 2:
			offset = 32;
			break;
		case 3:
			offset =64;
			break;
		case 4:
			offset = 96;
			break;
	}
	long ret = readFromDataFile(reader, fileNo, offset, len, outPut, outputLen);
	if(ret != APP_SUCCESS)
	{
		PrintText("GET CRIS ID FAILED");
		return APP_ERROR;
	}
	int expiryDateIndex = 6;
	memcpy(dateInCard,&outPut[expiryDateIndex],4);
	long long date = CharArrayToInt(dateInCard,4);
	if(date < timeInsec)
	{
		isExpired = false;
	}
	else { // this also works if no passes are there in card if no passes are there it be zeros in the card
		isExpired = true;
	}
	return APP_SUCCESS;
}

ErrorConstants DesfireEV1Card::payForActivityAndAddRecordInTxnLog(Reader *reader, Activity *activity,unsigned char samKey[]){
	PrintText("REACHED");
	unsigned char fileno = 0x01;
	long cardBalance=0, amount = activity->getAmount();
	long runningBalance =0;
	// printf("amount %ld\n",amount );
	long ret = selectApplication(reader,(unsigned char *) DesfireEV1Card::app0_ID);
	ret = selectApplication(reader,(unsigned char *) DesfireEV1Card::app2_ID);
	ret = authenticateCard(reader, App_2_File_1.writeKey, samKey);
	if (ret != APP_SUCCESS)
	{
		PrintText("card Auth Failed");
		return ErrorConstants::CARD_AUTH_FAILED;
	}
	ret = debitAmount(reader,fileno,amount);
	if(ret!=APP_SUCCESS)
	{
		PrintText("Debit Amount Failed or not enough amount in the card");
		return ErrorConstants::PAY_FAILED;
	}
		
	ret = authenticateCard(reader, App_2_File_1.writeKey, samKey);
	if (ret != APP_SUCCESS)
	{
		PrintText("card Auth Failed");
		return ErrorConstants::CARD_AUTH_FAILED;
	}
	 ret = getValue(reader,fileno,&runningBalance);
	runningBalance -= amount;
	fileno = 0x02;
	unsigned char txnType = (unsigned char)TransactionType::PAY;
	string bizLevelID = activity->getSrcBranchId();
	// printf("bizLevelID ----------------------------------------------------------------%s\n",bizLevelID.c_str() );
	string terminalId = activity->getPosTerminalId();
	// printf("terminalId ----------------------------------------------------------------%s\n",terminalId.c_str() );
	ServiceType domainType = activity->getServiceType();
	long long timeInMsec =  activity->getTimeInMilliSeconds();
	// printf("time in mSec ----------------------------------------------------------------%lld\n",timeInMsec );
	ret = authenticateCard(reader, App_2_File_2.writeKey, samKey);
	if (ret != APP_SUCCESS)
	{
		PrintText("card Auth Failed");
		return ErrorConstants::CARD_AUTH_FAILED;
	}
	unsigned char response[10];
	 unsigned long responseslen;
	ret = addRecordInTxnLog(reader,fileno,txnType,amount, runningBalance,bizLevelID,terminalId,domainType,timeInMsec);
	// getFileSettings(reader,fileno,response,responseslen);
	if(ret !=APP_SUCCESS)
	{
		PrintText("Add record in Txn Log Failed");
		return ErrorConstants::ADD_RECORD_IN_TXN_LOG_FAILED;
	}
	activity->setAmount(runningBalance);
	return ErrorConstants::NO__ERROR;
}


ErrorConstants DesfireEV1Card::GetGenericData(Reader *reader, GenericData *genericData){return ErrorConstants::GET_VALUE_FAILED;}