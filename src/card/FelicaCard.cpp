#include<stdio.h>
#include<iostream>
#include <string>
#include<stdbool.h>
#include "reader/Reader.h"
#include "enum/ServiceType.h"
#include "enum/TollSpecific.h"
#include "card/Card.h"
#include "Parameter.h"
#include "model/Pass.h"
#include "constants/ErrorConstants.h"
#include "sam/FelicaSAM.h"
#include "card/FelicaCard.h"
#include "../include/Util.h"
#include "../include/Util.h"
#include "model/CardDetail.h"
#include "model/GenericData.h"
#include "enum/VehicleType.h"
#include "enum/CardStatusEnum.h"
#include <TransactionsList.h>
using namespace std;

unsigned char g_IDm[8];
unsigned char g_IDt[2];

struct LOG{
		int transactionType;
		int amount;
		long long timeinMills;
		long long terminalId;
	};
FelicaCard::FelicaCard()
{
    type = Card::FELICA_RCSA01_CARD;
}


ErrorConstants FelicaCard::GetBalance(Reader *reader, long *amount)
{
	long			_ret =0;
	unsigned char	_service_num, _block_num;
	unsigned char	_service_list[64], _block_list[128], _read_data[128];
	unsigned long	_read_len;


	_ret = PollingFeliCaCard(reader);
	if (_ret != CARD_COMMAND_SUCCEEDED) {
		printf("Polling Error\n");
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CARD_AUTH_FAILED;
	}

	_service_num = 1;			// Number of service
	_service_list[0]	= 0x17; // Service code
	_service_list[1]	= 0x20; // Service code

	_block_num = 1;				// Number of Blocks
	_block_list[0] = 0x80;		// Block list
	_block_list[1] = 0x00;		// Block list

	_ret = ReadDataWOEnc(_service_num, _service_list, _block_num, _block_list, &_read_len, _read_data, reader);
	if(_ret != CARD_COMMAND_SUCCEEDED){
		return ErrorConstants::CARD_AUTH_FAILED;
	}


	//Check Status FLAG
	if((_read_data[0] != 0x00) || (_read_data[0] != 0x00)){
		return ErrorConstants::CARD_AUTH_FAILED;
	}
	

	//Get Balance(little endian)
	*amount = CharArrayToIntLE(&_read_data[3],4);
	//reader-> DisconnectFeliCaCard();
	return ErrorConstants::NO__ERROR;
}


ErrorConstants FelicaCard::GetCardDetails(Reader *reader, CardDetail *cardDetail)
{
	long			_ret = 0, result = 0;
	unsigned char	_service_num, _block_num, workBuf[5];
	unsigned char	_service_list[64], _block_list[128], _read_data[128];
	unsigned long	_read_len;
	int    balance, vehicleType;
	char name[48], vehicleNo[11];
	long long phoneNo, adhaarNo, cardNo;

	// Polling FeliCa Card
	result = PollingFeliCaCard(reader);
	if (result != APP_SUCCESS) {
		printf("Polling Error\n");
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CARD_AUTH_FAILED;
	}

	_service_num = 3;			// Number of service
	_service_list[0] = 0x0B; // Service code
	_service_list[1] = 0x10; // Service code
	_service_list[2] = 0x8B; // Service code
	_service_list[3] = 0x10; // Service code
	_service_list[4] = 0x17; // Service code
	_service_list[5] = 0x20; // Service code


	_block_num = 6;				// Number of Blocks
	_block_list[0] = 0x80;		// Block list
	_block_list[1] = 0x00;		// Block list
	_block_list[2] = 0x80;		// Block list
	_block_list[3] = 0x01;		// Block list
	_block_list[4] = 0x80;		// Block list
	_block_list[5] = 0x02;		// Block list
	_block_list[6] = 0x80;		// Block list
	_block_list[7] = 0x03;		// Block list
	_block_list[8] = 0x81;		// Block list
	_block_list[9] = 0x00;		// Block list
	_block_list[10] = 0x82;		// Block list
	_block_list[11] = 0x00;		// Block list

	_ret = ReadDataWOEnc(_service_num, _service_list, _block_num, _block_list, &_read_len, _read_data, reader);
	if (_ret != APP_SUCCESS) {
		return ErrorConstants::CARD_AUTH_FAILED;
	}
	PrintHexArray("\n\n response from card\n\n", _read_len, _read_data);

	memcpy(name, &_read_data[3], 48);
	printf("\n \n name isss  %s", name);

	memcpy(workBuf, &_read_data[3 + 48], 1);
	vehicleType = _read_data[3 + 48];
	printf("\n \n vehicle type isss  %d", vehicleType);

	memcpy(vehicleNo, &_read_data[3 + 48 + 1], 11);
	printf("\n \n vehicle no isss  %s", vehicleNo);

	memcpy(workBuf, &_read_data[3 + 48 + 1 + 11 + 4], 5);
	cardNo = CharArrayTolonglong(workBuf, 5);
	printf("\n\n card number is iss %lld \n\n", cardNo);

	cardDetail->setCardNumber(std::to_string(cardNo));


	memcpy(workBuf, &_read_data[3 + 80], 4);
	PrintHexArray("amount iss ", 4, workBuf);
	balance = CharArrayToIntLE(workBuf, 4);
	printf("\n\n balance iss %d \n\n", balance);

	_service_num = 1;			// Number of service
	_service_list[0] = 0x48; // Service code
	_service_list[1] = 0x10; // Service code
	_service_list[2] = 0x02; // Service code
	_service_list[3] = 0x01; // Service code

	_block_num = 1;				// Number of Blocks
	_block_list[0] = 0x80;		// Block list
	_block_list[1] = 0x00;		// Block list


	_ret = MutualAuthV2WithFeliCa(SYSTEM_CODE, _service_num, _service_list, reader);
	if (_ret != SAM_COMMAND_SUCCEEDED) {
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CARD_AUTH_FAILED;
	}

	ReadDataBlock(_block_num, _block_list, &_read_len, _read_data, reader);
	// Check Status FLAG
	if ((_read_data[0] != 0x00) || (_read_data[1] != 0x00)) {
		return ErrorConstants::CARD_AUTH_FAILED;
	}

	memcpy(workBuf, &_read_data[3 + 3], 5);
	phoneNo = CharArrayTolonglong(workBuf, 5);
	printf("\n\n phone number is iss %lld \n\n", phoneNo);

	memcpy(workBuf, &_read_data[3 + 3 + 5], 5);
	adhaarNo = CharArrayTolonglong(workBuf, 5);
	printf("\n\n adhaar number is iss %lld \n\n", adhaarNo);

	cardDetail->setCardUID("123456");
	cardDetail->setVersion("5.1");
	//cardDetail->setCardNumber(stdCardNo.c_str());
	cardDetail->setBalance(balance);
	cardDetail->setStatus(1);

	reader-> DisconnectFeliCaCard();
	return ErrorConstants::NO__ERROR;
}

int getSpecificServiceNumber(GenericData *genericData){
	ServiceType serviceType = genericData->getServiceType();
	switch(serviceType)
	{
		case ServiceType::TOLL:
		return (int) TollSpecific::TOTAL_SERVICES;
		break;
		default:
		printf("%s\n","Something went wrong" );
		break;
	}
}

void getLogs(unsigned char hexData[], struct LOG historyData[5]){
	LOG temp;
	unsigned char buffer[20],workBuffer[16];
	for(int i=0;i<5;i++){
		memcpy(workBuffer,&hexData[16*i],16);
		memcpy(buffer,&workBuffer[0],1);
		temp.transactionType= (int) CharArrayToInt(buffer,1);
		memcpy(buffer,&workBuffer[1],6);
		temp.timeinMills=(long long)CharArrayTolonglong(buffer,6);
		memcpy(buffer,&workBuffer[7],4);
		temp.amount=(int)CharArrayToInt(buffer,4);
		memcpy(buffer,&workBuffer[11],4);
		temp.terminalId=(long long)CharArrayTolonglong(buffer,4);
		historyData[i]=temp;
	}
}

void getPasses(unsigned char passes[],GenericData *genericData,vector<Pass> * matchedPassesData, int* passesDataLength){
	*passesDataLength=0;
	for(int i=0;i<96;i+=32){
		long long srcBranchId=CharArrayTolonglong(&passes[i],4);
		long long destBranchId=CharArrayTolonglong(&passes[i+4],4);

		printf("\n %s \n",std::to_string(srcBranchId).c_str());	
		printf("\n %llu \n",destBranchId);	

		//std::string abc= std::to_string(branchId);
		//printf("%s This is the branch id",abc.c_str());
		char* endptr=NULL;
		if(genericData == NULL||srcBranchId ==  strtoll(genericData->getBranchId().c_str(),&endptr,10) || destBranchId== strtoll(genericData->getBranchId().c_str(),&endptr,10)){
			*passesDataLength+=1;
			Pass* pass = new Pass();
			unsigned char passLimitsPeriodicity,passLimitsMAxCount1[2],passLimitsMAxCount[2],passLimitsRemainingCount[2],passLimitsRemainingCount1[2],passLimitsStartDate[4],passLimitsStartDate1[4];
			int periodicityLimit[2];

			pass->setSrcBranchId(std::to_string(srcBranchId).c_str());
			pass->setDestBranchId(std::to_string(destBranchId).c_str());
			pass->setMaxTrips(CharArrayToInt(&passes[i+4+4],2));
			pass->setExpiryDate((long long)CharArrayToInt(&passes[i+4+4+2],4));
			printf("\n%02X pass type issss\n",passes[i+4+4+2+4] );
			pass->setPassType((PassType) passes[i+4+4+2+4]);
			
			passLimitsPeriodicity=passes[i+4+4+2+4+1];
			periodicityLimit[0] = passLimitsPeriodicity>>4; //Taking last 4 bits
			periodicityLimit[1] = passLimitsPeriodicity & 0x0f; //Taking first four bits
			memcpy(passLimitsMAxCount,&passes[i+4+4+2+4+1+1],2);
			memcpy(passLimitsRemainingCount,&passes[i+4+4+2+4+1+1+2],2);
			memcpy(passLimitsStartDate,&passes[i+4+4+2+4+1+1+2+2],4);
			int limitsMaxCount[2],limitsRemainingCount[2];
			long long limitsStartDate[2];
			limitsMaxCount[0] = CharArrayToInt(passLimitsMAxCount,2);
			limitsRemainingCount[0] = CharArrayToInt(passLimitsRemainingCount,2);
			limitsStartDate[0] = (long long)CharArrayToInt(passLimitsStartDate,4);
			vector<Periodicity> periodicitiyValue;
			int count = 2;
			if(periodicityLimit[1] == 0)
				count = 1;
			if(count > 1)
			{
				memcpy(passLimitsMAxCount1,&passes[i+4+4+2+4+1+1+2+2+4],2);
				memcpy(passLimitsRemainingCount1,&passes[i+4+4+2+4+1+1+2+2+2],2);
				memcpy(passLimitsStartDate1,&passes[i+4+4+2+4+1+1+2+2+2],4);
				limitsMaxCount[1] = CharArrayToInt(passLimitsMAxCount1,2);
				limitsRemainingCount[1] = CharArrayToInt(passLimitsRemainingCount1,2);
				limitsStartDate[1] = (long long)CharArrayToInt(passLimitsStartDate1,4);
			}
			for(int i=0;i<count;i++){
				Periodicity *periodicityOfPass = new Periodicity();
				periodicityOfPass->setLimitPeriodicity((LimitPeriodicity)periodicityLimit[i]);
				periodicityOfPass->setLimitsMaxCount(limitsMaxCount[i]);
				periodicityOfPass->setLimitsRemainingCount(limitsRemainingCount[i]);
				periodicityOfPass->setLimitsStartTime(limitsStartDate[i]);
				periodicitiyValue.push_back(*periodicityOfPass);
			}
			pass->setPeriodicity(periodicitiyValue);
			(*matchedPassesData).push_back(*pass);
		}
	}
}

ErrorConstants FelicaCard::GetGenericData(Reader *reader, GenericData *genericData)
{
	long			_ret = 0, result = 0;
	unsigned char	_service_num, _block_num, workBuf[16];
	unsigned char	_service_list[64], _block_list[128], _read_data[512];
	unsigned long	_read_len;
	int    balance, vehicleType,cardStatus;
	char name[48], vehicleNo[16];
	long long phoneNo, adhaarNo, cardNo;
	unsigned  char logs[256],passes[256];
	//Pass related Variables
	unsigned char passSrcBranchID[4], passDestBranchID[4],passMaxTrips[2],passExpiryDate[4],Passtype,passLimitsPeriodicity;
	unsigned char passLimitsMAxCount[2],passLimitsMAxCount1[2],passLimitsRemainingCount[2],passLimitsRemainingCount1[2],passLimitsStartDate[4];
	unsigned char passLimitsStartDate1[4];
	int periodicityLimit[2];
	int maxNoOfTrips;
	// Polling FeliCa Card
	result = PollingFeliCaCard(reader);
	if (result != CARD_COMMAND_SUCCEEDED) {
		printf("Polling Error\n");
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::POLLING_FAILED;
	}

	_service_num = 4;			// Number of service
	_service_list[0] = 0x0B; // Service code
	_service_list[1] = 0x10; // Service code
	_service_list[2] = 0x8B; // Service code
	_service_list[3] = 0x10; // Service code
	_service_list[4] = 0x17; // Service code
	_service_list[5] = 0x20; // Service code
	_service_list[6] = 0x4F; // Service code
	_service_list[7] = 0x20; // Service code


	_block_num = 12;				// Number of Blocks
	_block_list[0] = 0x80;		// Block list
	_block_list[1] = 0x00;		// Block list
	_block_list[2] = 0x80;		// Block list
	_block_list[3] = 0x01;		// Block list
	_block_list[4] = 0x80;		// Block list
	_block_list[5] = 0x02;		// Block list
	_block_list[6] = 0x80;		// Block list
	_block_list[7] = 0x03;		// Block list
	_block_list[8] = 0x80;		// Block list
	_block_list[9] = 0x04;		// Block list
	_block_list[10] = 0x81;		// Block list
	_block_list[11] = 0x00;		// Block list
	_block_list[12] = 0x82;		// Block list
	_block_list[13] = 0x00;		// Block list
	_block_list[14] = 0x83;		// Block list
	_block_list[15] = 0x00;		// Block list
	_block_list[16] = 0x83;		// Block list
	_block_list[17] = 0x01;		// Block list
	_block_list[18] = 0x83;		// Block list
	_block_list[19] = 0x02;		// Block list
	_block_list[20] = 0x83;		// Block list
	_block_list[21] = 0x03;		// Block list
	_block_list[22] = 0x83;		// Block list
	_block_list[23] = 0x04;		// Block list

	_ret = ReadDataWOEnc(_service_num, _service_list, _block_num, _block_list, &_read_len, _read_data, reader);
	if (_ret != CARD_COMMAND_SUCCEEDED) {
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CARD_AUTH_FAILED;
	}
	PrintHexArray("\n\n response from card\n\n", _read_len, _read_data);

	memcpy(name, &_read_data[3], 48);
	printf("\n \n name isss  %s", name);

	memcpy(workBuf, &_read_data[3 + 48], 1);
	vehicleType = _read_data[3 + 48];
	printf("\n \n vehicle type isss  %d", vehicleType);

	memcpy(workBuf, &_read_data[3 + 48 + 1], 1);
	cardStatus = _read_data[3 + 48 + 1];
	printf("\n \n card status isss  %d", cardStatus);

	memcpy(vehicleNo, &_read_data[3 + 48 + 16], 16);
	printf("\n \n vehicle no isss  %s", vehicleNo);

	memcpy(workBuf, &_read_data[3 + 48 + 16 + 16], 16);
	cardNo = CharArrayTolonglong(workBuf, 16);
	printf("\n\n card number is iss %lld \n\n", cardNo);

	memcpy(workBuf, &_read_data[3 + 96], 4);
	balance = CharArrayToIntLE(workBuf, 4);
	printf("\n\n balance iss %d \n\n", balance);

	memcpy(logs,&_read_data[103],80);
	struct LOG historyData[5];
	getLogs(logs,historyData);
	printf("history data isss %d\n", historyData[0].transactionType);

	_service_num = getSpecificServiceNumber(genericData)+ 1;			// Number of service
	printf("service number isss %d",_service_num);
	_service_list[0] = 0x48; // Service code
	_service_list[1] = 0x10; // Service code
	_service_list[2] = 0x02; // Service code
	_service_list[3] = 0x01; // Service code
	_service_list[4] = 0x08; // Service code
	_service_list[5] = 0x40; // Service code
	_service_list[6] = 0x02; // Service code
	_service_list[7] = 0x01; // Service code
	_service_list[8] = 0x48; // Service code
	_service_list[9] = 0x40; // Service code
	_service_list[10] = 0x02; // Service code
	_service_list[11] = 0x01; // Service code

	_block_num = 14;				// Number of Blocks
	_block_list[0] = 0x80;		// Block list
	_block_list[1] = 0x00;		// Block list
	_block_list[2] = 0x81;		// Block list
	_block_list[3] = 0x00;	
	_block_list[4] = 0x81;		// Block list
	_block_list[5] = 0x01;	
	_block_list[6] = 0x81;		// Block list
	_block_list[7] = 0x02;	
	_block_list[8] = 0x81;		// Block list
	_block_list[9] = 0x03;	
	_block_list[10] = 0x81;		// Block list
	_block_list[11] = 0x04;	
	_block_list[12] = 0x81;		// Block list
	_block_list[13] = 0x05;
	_block_list[14] = 0x82;		// Block list
	_block_list[15] = 0x00;	
	_block_list[16] = 0x82;		// Block list
	_block_list[17] = 0x01;	
	_block_list[18] = 0x82;		// Block list
	_block_list[19] = 0x02;	
	_block_list[20] = 0x82;		// Block list
	_block_list[21] = 0x03;	
	_block_list[22] = 0x82;		// Block list
	_block_list[23] = 0x04;	
	_block_list[24] = 0x82;		// Block list
	_block_list[25] = 0x05;	
	_block_list[26] = 0x82;		// Block list
	_block_list[27] = 0x06;
	


	_ret = MutualAuthV2WithFeliCa(SYSTEM_CODE, _service_num, _service_list, reader);
	if (_ret != SAM_COMMAND_SUCCEEDED) {
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CARD_AUTH_FAILED;
	}

	ReadDataBlock(_block_num, _block_list, &_read_len, _read_data, reader);
	// Check Status FLAG
	if ((_read_data[0] != 0x00) || (_read_data[1] != 0x00)) {
		return ErrorConstants::CARD_AUTH_FAILED;
	}

	memcpy(workBuf, &_read_data[3 + 4], 6);
	phoneNo = CharArrayTolonglong(workBuf, 6);
	printf("\n\n phone number is iss %lld \n\n", phoneNo);

	memcpy(workBuf, &_read_data[3 + 4 + 6], 5);
	adhaarNo = CharArrayTolonglong(workBuf, 5);
	printf("\n\n adhaar number is iss %lld \n\n", adhaarNo);

	memcpy(passes,&_read_data[3+4+6+5+1],96);

	vector<Pass>  matchedPassesData;
	int  passesDataLength=0;
	getPasses(passes,genericData,&matchedPassesData,&passesDataLength);
	printf("/n Passes data length is %d",passesDataLength);
	reader-> DisconnectFeliCaCard();
	genericData->setpassCount(passesDataLength);
	genericData->setName(name);
	genericData->setVehicleType((VehicleType) vehicleType);
	genericData->setCardStatus((CardStatusEnum)cardStatus);
	genericData->setCardNumber(std::to_string(cardNo));
	genericData->setVehicleNumber(vehicleNo);
	genericData->setMobile(phoneNo);
	genericData->setBalance(balance);
	genericData->setAadhaar(adhaarNo);
	genericData->setPass(matchedPassesData);
	
	printf("going to serialize\n");
	return ErrorConstants::NO__ERROR;
}

ErrorConstants FelicaCard::Pay(Reader *reader, Activity *activity)
{
	long result;
	unsigned int balance, amount, newBalance;
	unsigned char history[6][16];

	amount = activity->getAmount();


	// Polling FeliCa Card
	result = PollingFeliCaCard(reader);
	if (result != CARD_COMMAND_SUCCEEDED) {
		printf("Polling Error\n");
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::POLLING_FAILED;
	}

	//Read E-money Balance
	result = ReadBalance(&balance, reader);
	if(result != CARD_COMMAND_SUCCEEDED){
		printf("Read Balance Error\n");
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CARD_AUTH_FAILED;
	}

	//Check balance and amount
	if(amount > balance){
		printf("Insufficient Balance\n");
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CARD_AUTH_FAILED;
	}

	// Write new balance data
	newBalance = balance - amount;
	result = UpdateBalance(newBalance, amount, METHOD_PAYMENT, reader);
	if( result != CARD_COMMAND_SUCCEEDED){
		printf("Write Error\n");
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CREDIT_FAILED;
	}
	activity->setAmount(newBalance);
	reader-> DisconnectFeliCaCard();
	return ErrorConstants::NO__ERROR;
}

ErrorConstants FelicaCard::Recharge(Reader *reader, Activity *activity)
{
	long result;
	unsigned int balance, amount, newBalance;
	unsigned char history[6][16];

	amount = activity->getAmount();


	// Polling FeliCa Card
	result = PollingFeliCaCard(reader);
	if(result != CARD_COMMAND_SUCCEEDED){
		printf("Polling Error\n");
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CARD_AUTH_FAILED;
	}

	//Read E-money Balance
	result = ReadBalance(&balance, reader);
	if(result != CARD_COMMAND_SUCCEEDED){
		printf("Read Balance Error\n");
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CARD_AUTH_FAILED;
	}

	// Write new balance data
	newBalance = balance + amount;
	result = UpdateBalance(newBalance, amount, METHOD_CHARGE, reader);
	if( result != CARD_COMMAND_SUCCEEDED){
		printf("Write Error\n");
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CREDIT_FAILED;
	}
	activity->setAmount(newBalance);
	reader-> DisconnectFeliCaCard();
	return ErrorConstants::NO__ERROR;
}

ErrorConstants FelicaCard::ActivateCard(Reader *reader, CardActivation *cardActivation){
	
	long result;
	unsigned char amount[4], phoneNo[6], adhaarNo[5], name[48], vehicleNo[16], VehicleType;
	long amountInt;
	long long adhaarInt, mobileInt;

	string vNumber = cardActivation->getVehicleNumber();
	unsigned char *vehicleNumberPtr = (unsigned char *)vNumber.c_str();

	memset(vehicleNo,0x00,16);
	for(int i=0;i<vNumber.length();i++)
	{
		vehicleNo[i] = vehicleNumberPtr[i];
	}

	unsigned char *namePtr = 0;
	memset(name,0x00,48);
	string namestr = cardActivation->getName();
	namePtr = (unsigned char *)namestr.c_str();
	for(int i=0;i<namestr.length();i++)
	{
		name[i] = namePtr[i];
	}


	// Polling FeliCa Card
	result = PollingFeliCaCard(reader);
	if(result != CARD_COMMAND_SUCCEEDED){
		printf("Polling Error\n");
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CARD_AUTH_FAILED;
	}
	
	
	mobileInt = cardActivation->getMobile();
	IntToCharArrayDigit(mobileInt, phoneNo);

	adhaarInt = cardActivation->getAadhaar();
	IntToCharArrayTwelveDigit(adhaarInt, adhaarNo);

	amountInt = cardActivation->getAmount();
	IntToCharArrayLE(amountInt, amount);

	VehicleType =  static_cast<unsigned char> (cardActivation->getVehicleType());

	PrintHexArray("amount is", 4, amount);
	PrintHexArray("adhaarNo is", 5, adhaarNo);
	PrintHexArray("phoneno is", 6, phoneNo);
	PrintHexArray("Name is", 48, name);
	PrintHexArray("vehicleno is", 16, vehicleNo);
	
	result = WriteGenericaDataToCard(name, vehicleNo, phoneNo, adhaarNo, amount,VehicleType, reader);
	if (result != CARD_COMMAND_SUCCEEDED) {
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CARD_AUTH_FAILED;
	}
	reader-> DisconnectFeliCaCard();
	return ErrorConstants::NO__ERROR;

}

ErrorConstants FelicaCard::UpdatePersonalData(Reader *reader, PersonalData *personalData){
	
	// updating name mobile adhaar for now
	long result;
	unsigned char phoneNo[6], adhaarNo[5], name[48],vehicleNo[16];
	long long adhaarInt, mobileInt;


	string vNumber = personalData->getVehicleNumber();
	unsigned char *vehicleNumberPtr = (unsigned char *)vNumber.c_str();

	memset(vehicleNo,0x00,16);
	for(int i=0;i<vNumber.length();i++)
	{
		vehicleNo[i] = vehicleNumberPtr[i];
	}

	unsigned char *namePtr = 0;

	string namestr = personalData->getFirstName();
	namePtr = (unsigned char *)namestr.c_str();
	memset(name,0x00,48);
	for(int i=0;i<namestr.length();i++)
	{
		name[i] = namePtr[i];
	}


	// Polling FeliCa Card
	result = PollingFeliCaCard(reader);
	if(result != CARD_COMMAND_SUCCEEDED){
		printf("Polling Error\n");
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CARD_AUTH_FAILED;
	}

	mobileInt = personalData->getMobile();
	IntToCharArrayDigit(mobileInt, phoneNo);

	adhaarInt = personalData->getAadhaar();
	IntToCharArrayTwelveDigit(adhaarInt, adhaarNo);

	PrintHexArray("adhaarNo is", 5, adhaarNo);
	PrintHexArray("phoneno is", 6, phoneNo);
	PrintHexArray("Name is", 48, name);
	PrintHexArray("vehicleno is", 16, vehicleNo);
	result = UpdataCardHoldersData(name, phoneNo, adhaarNo, vehicleNo, reader);
	if (result != CARD_COMMAND_SUCCEEDED) {
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CARD_AUTH_FAILED;
	}
	reader-> DisconnectFeliCaCard();
	return ErrorConstants::NO__ERROR;
}

long FelicaCard::ReadBalance(unsigned int* balance, Reader* reader)
{
	long			_ret =0;
	unsigned char	_service_num, _block_num;
	unsigned char	_service_list[64], _block_list[128], _read_data[128];
	unsigned long	_read_len;

	_service_num = 1;			// Number of service
	_service_list[0]	= 0x17; // Service code
	_service_list[1]	= 0x20; // Service code

	_block_num = 1;				// Number of Blocks
	_block_list[0] = 0x80;		// Block list
	_block_list[1] = 0x00;		// Block list

	_ret = ReadDataWOEnc(_service_num, _service_list, _block_num, _block_list, &_read_len, _read_data, reader);
	if(_ret != CARD_COMMAND_SUCCEEDED){
		reader-> DisconnectFeliCaCard();
		return _ret;
	}

	//Check Status FLAG
	if((_read_data[0] != 0x00) || (_read_data[0] != 0x00)){
		return CARD_READ_DATA_ERROR;
	}

	//Get Balance(little endian)
	*balance = CharArrayToIntLE(&_read_data[3],4);

	return CARD_COMMAND_SUCCEEDED;
}

long FelicaCard::UpdateBalance(unsigned int newBalance, int amount, unsigned char method, Reader *reader)
{
	long			_ret =0;
	unsigned char	_service_code_list[128], _block_list[128], _block_data[128], _read_data[128];
	unsigned char	_block_num;
	unsigned char	_exec_id[2], _current_time[7], _work_buf[4];
	unsigned long	_read_len;
	unsigned int	_balance;

	
	_service_code_list[0]	= 0x10; //Service code(Direct Access)
	_service_code_list[1]	= 0x20; //Service code(Direct Access)
	_service_code_list[2]	= 0x02; //Service key ver
	_service_code_list[3]	= 0x01; //Service key ver
	
	_ret = MutualAuthV2WithFeliCa(SYSTEM_CODE, 1, _service_code_list, reader);
	if (_ret != SAM_COMMAND_SUCCEEDED) {
		reader-> DisconnectFeliCaCard();
		return _ret;
	}

	// Read Current Data
	_block_num = 1;			//Number of Blocks
	_block_list[0] = 0x80;	//Block list
	_block_list[1] = 0x00;	//Block list

	_ret = ReadDataBlock(_block_num, _block_list, &_read_len, _read_data, reader);
	if(_ret != SAM_COMMAND_SUCCEEDED){
		return _ret;
	}

	// Check Status FLAG
	if((_read_data[0] != 0x00) || (_read_data[1] != 0x00)){
		return CARD_READ_DATA_ERROR;
	}

	// Get Balance(convert little endian to big endian)
	_balance = CharArrayToIntLE(&_read_data[3],4);

	// Get current exec id
	_exec_id[0] = _read_data[3+14];
	_exec_id[1] = _read_data[3+15];

	// Write new data
	_block_num = 0x01;		// Number of Block
	_block_list[0] = 0x80;	// Block list
	_block_list[1] = 0x00;	// Block list

	IntToCharArrayLE(newBalance, _work_buf);
	
	_update_exec_id (_exec_id);

	memcpy(&_block_data[0], _work_buf, 4);	// amount of payment/ new balance data
	memset(&_block_data[4], 0x00, 10);		// Zero
	_block_data[14] = _exec_id[0];			// Exec ID
	_block_data[15] = _exec_id[1];			// Exec ID

	IntToCharArray(amount, _work_buf);

	_ret = WriteDataBlock(_block_num, _block_num*2, _block_list, _block_data, reader);
	if(_ret != CARD_COMMAND_SUCCEEDED){
		return _ret;
	}
	
	return CARD_COMMAND_SUCCEEDED;
}

long FelicaCard::WriteGenericaDataToCard(unsigned char name [48], unsigned char vehicleNo[16],
	unsigned char mobileNo[6],unsigned char adhaarNo[5],unsigned char amount[4],unsigned char VehicleType, Reader* reader)
{
	long			_ret = 0;
	unsigned char	_service_code_list[128], _block_list[128], _block_data[128], _block_num;

	_service_code_list[0] = 0x08; //Service code
	_service_code_list[1] = 0x10; //Service code
	_service_code_list[2] = 0x02; //Service key ver
	_service_code_list[3] = 0x01; //Service key ver
	_service_code_list[4] = 0x48; //Service code
	_service_code_list[5] = 0x10; //Service code
	_service_code_list[6] = 0x02; //Service key ver
	_service_code_list[7] = 0x01; //Service key ver
	// _service_code_list[8] = 0x88; //Service code
	// _service_code_list[9] = 0x10; //Service code
	// _service_code_list[10] = 0x02; //Service key ver
	// _service_code_list[11] = 0x01; //Service key ver
	_service_code_list[8] = 0x10; //Service code(Direct Access)
	_service_code_list[9] = 0x20; //Service code(Direct Access)
	_service_code_list[10] = 0x02; //Service key ver
	_service_code_list[11] = 0x01; //Service key ver
	_ret = MutualAuthV2WithFeliCa(SYSTEM_CODE, 3, _service_code_list, reader);
	if (_ret != SAM_COMMAND_SUCCEEDED) {
		reader-> DisconnectFeliCaCard();
		return _ret;
	}

	// Write new data
	_block_num = 0x07;		// Number of Block
	_block_list[0] = 0x80;	// Block list
	_block_list[1] = 0x00;	// Block list
	_block_list[2] = 0x80;	// Block list
	_block_list[3] = 0x01;	// Block list
	_block_list[4] = 0x80;	// Block list
	_block_list[5] = 0x02;	// Block list
	_block_list[6] = 0x80;	// Block list
	_block_list[7] = 0x03;	// Block list
	_block_list[8] = 0x80;	// Block list
	_block_list[9] = 0x04;	// Block list
	_block_list[10] = 0x81;	// Block list
	_block_list[11] = 0x00;	// Block list
	_block_list[12] = 0x82;	// Block list
	_block_list[13] = 0x00;	// Block list
	// _block_list[12] = 0x83;	// Block list
	// _block_list[13] = 0x00;	// Block list
	
	memcpy(&_block_data[0], name, 48); //Write name to block Data
	_block_data[48] = VehicleType;//vehicle type
	//_block_data[49] = 0x00;//status
	_block_data[49] = (unsigned char)static_cast<int>(CardStatusEnum::activate);
	_block_data[50] = 0x04;//version
	_block_data[51] = 0x00;//gender
	memset(&_block_data[52], 0x00, 12);//blank bits
	memcpy(&_block_data[64], vehicleNo, 16); //Write vehicle no to block Data
	_block_data[80] = 0x00;//--------|
	_block_data[81] = 0x00;//--------|--Issuer Date
	_block_data[82] = 0x00;//--------|
	_block_data[83] = 0x00;//--------|
	// _block_data[64] = 0x00;//----|
	// _block_data[65] = 0x00;//----|--Issue Date
	// _block_data[66] = 0x00;//----|
	memcpy(&_block_data[84], mobileNo, 6); //Write mobile number to block Data
	memcpy(&_block_data[90], adhaarNo, 5);//Write adhaar number to block Data
	// _block_data[77] = 0x00;//is blocked byte
	// _block_data[78] = 0x04;//card structure version
	_block_data[95] = 0x00;//Unused byte
	//memcpy(&_block_data[80], cardNo, 16);
	memcpy(&_block_data[96], amount, 4);//Amount
	memset(&_block_data[100], 0x00, 12);//blank bits
	PrintHexArray("\n\n Writing blockData ",_block_num*16,_block_data);
	printf("\n\n the card status is %02X",_block_data[49]);
	_ret = WriteDataBlock(_block_num, _block_num * 2, _block_list, _block_data, reader);
	if (_ret != CARD_COMMAND_SUCCEEDED) {
		printf("\n\n Writing faileddddd");
		return _ret;
	}
	//DisconnectFeliCaCard();
	return CARD_COMMAND_SUCCEEDED;
}

long FelicaCard::UpdataCardHoldersData(unsigned char name [48], unsigned char mobileNo[6],unsigned char adhaarNo[5],unsigned char vehicleNo[16], Reader* reader)
{
	long			_ret = 0;
	unsigned char	_service_code_list[128], _block_list[128], _block_data[128], _block_num;

	_service_code_list[0] = 0x08; //Service code
	_service_code_list[1] = 0x10; //Service code
	_service_code_list[2] = 0x02; //Service key ver
	_service_code_list[3] = 0x01; //Service key ver
	_service_code_list[4] = 0x48; //Service code
	_service_code_list[5] = 0x10; //Service code
	_service_code_list[6] = 0x02; //Service key ver
	_service_code_list[7] = 0x01; //Service key ver
	// _service_code_list[8] = 0x88; //Service code
	// _service_code_list[9] = 0x10; //Service code
	// _service_code_list[10] = 0x02; //Service key ver
	// _service_code_list[11] = 0x01; //Service key ver
	// _service_code_list[8] = 0x10; //Service code(Direct Access)
	// _service_code_list[9] = 0x20; //Service code(Direct Access)
	// _service_code_list[10] = 0x02; //Service key ver
	// _service_code_list[11] = 0x01; //Service key ver
	_ret = MutualAuthV2WithFeliCa(SYSTEM_CODE, 2, _service_code_list, reader);
	if (_ret != SAM_COMMAND_SUCCEEDED) {
		reader-> DisconnectFeliCaCard();
		return _ret;
	}

	printf("\n\n\n\n auth success");

	// Write new data
	_block_num = 0x05;		// Number of Block
	_block_list[0] = 0x80;	// Block list
	_block_list[1] = 0x00;	// Block list
	_block_list[2] = 0x80;	// Block list
	_block_list[3] = 0x01;	// Block list
	_block_list[4] = 0x80;	// Block list
	_block_list[5] = 0x02;	// Block list
	_block_list[6] = 0x80;	// Block list
	_block_list[7] = 0x04;	// Block list
	_block_list[8] = 0x81;	// Block list
	_block_list[9] = 0x00;	// Block list
	// _block_list[12] = 0x83;	// Block list
	// _block_list[13] = 0x00;	// Block list
	
	memcpy(&_block_data[0], name, 48); //Write name to block Data
	//_block_data[48] = 0x00;//vehicle type
	// memcpy(&_block_data[49], vehicleNo, 11); //Write vehicle no to block Data
	// _block_data[60] = 0x00;//--------|
	// _block_data[61] = 0x00;//--------|--Issuer Id
	// _block_data[62] = 0x00;//--------|
	// _block_data[63] = 0x00;//--------|
	memcpy(&_block_data[48], vehicleNo, 16);
	_block_data[64] = 0x00;//----|
	_block_data[65] = 0x00;//----|--Issue Date
	_block_data[66] = 0x00;//----|
	_block_data[67] = 0x00;//----|
	memcpy(&_block_data[68], mobileNo, 6); //Write mobile number to block Data
	memcpy(&_block_data[74], adhaarNo, 5);//Write adhaar number to block Data
	// _block_data[61] = 0x00;//is blocked byte
	// _block_data[62] = 0x04;//card structure version
	_block_data[79] = 0x00;//Unused byte
	//memcpy(&_block_data[80], cardNo, 16);
	// memcpy(&_block_data[80], amount, 4);//Amount
	// memset(&_block_data[84], 0x00, 12);//blank bits
	//printf("\n\n Writing st'art");
	_ret = WriteDataBlock(_block_num, _block_num * 2, _block_list, _block_data, reader);
	if (_ret != CARD_COMMAND_SUCCEEDED) {
		printf("\n\n Writing faileddddd");
		return _ret;
	}
	//DisconnectFeliCaCard();
	return CARD_COMMAND_SUCCEEDED;
}

long FelicaCard::ReadDataWOEnc(const unsigned char serviceNum, 
				   const unsigned char serviceList[], 
				   const unsigned char blockNum, 
				   const unsigned char blockList[], 
				   unsigned long* readLen,
				   unsigned char readData[],
				   Reader *reader)
{
	long			ret;

	unsigned char	felica_cmd_params[256];

	unsigned char	felica_cmd[262], felica_res[262], work_buf[262];
	unsigned long	felica_cmd_len, _felica_res_len, _work_buf_len;
	
	unsigned int nIdx = 0;
	
	//-------------------------------
	// Generate params sent to SAM
	//-------------------------------
	//IDm
	memcpy(&felica_cmd_params[nIdx], g_IDm, 8);
	nIdx += 8;
	//Number of Service
	felica_cmd_params[nIdx++] = serviceNum;
	//Service List
	memcpy(&felica_cmd_params[nIdx], serviceList, (serviceNum * 2));
	nIdx += (serviceNum * 2);
	//Number of Read Block
	felica_cmd_params[nIdx++] =	blockNum;
	//Block List
	memcpy(&felica_cmd_params[nIdx], blockList, (blockNum * 2));
	nIdx += (blockNum * 2);
	
#ifdef DEBUG
	PrintText("\n   (1).AskFeliCaCmdToSAM \n");
	PrintHexArray("Snr= ", 4, Snr);
#endif
	// Ask to SAM to generate FeliCa Command
	ret = AskFeliCaCmdToSAM(SAM_COMMAND_CODE_READ_WO_ENC, nIdx,  felica_cmd_params, &felica_cmd_len, felica_cmd, reader);
	if (ret != SAM_COMMAND_SUCCEEDED)
	{
		return ret;
	}
	
#ifdef DEBUG
	PrintText("\n   (2).TransmitDataToFeliCaCard \n");
	PrintHexArray("Snr= ", 4, Snr);
#endif
	//Send Command to FeliCa Card
	ret = reader-> sendToCard(felica_cmd, felica_cmd_len, felica_res, &_felica_res_len, Card::FELICA_RCSA01_CARD);
	if (ret != CARD_DATA_TRANSMIT_SUCCEEDED)
	{
		// Report FeliCa Error to SAM
		//ret = SendCardErrorToSAM(&_work_buf_len, work_buf, reader);
		update_SNR_For_NextCommand(reader);
		return ret;
	}
	
#ifdef DEBUG
	PrintText("\n   (3).SendCardResultToSAM \n");
	PrintHexArray("Snr= ", 4, Snr);
#endif
	//Send FeliCa Response to SAM
	ret = SendCardResultToSAM(_felica_res_len, felica_res, readLen, readData, reader);
	if (ret != SAM_COMMAND_SUCCEEDED)
	{
		return ret;
	}
	return CARD_COMMAND_SUCCEEDED;
}

long FelicaCard::PollingFeliCaCard(Reader *reader)
{
	long			ret;

	unsigned char	felica_cmd_params[256];

	unsigned char	felica_cmd[5], felica_res[262], work_buf[262];
	unsigned long	felica_cmd_len, _felica_res_len, _work_buf_len;

	unsigned int nIdx = 0;
	
	//-------------------------------
	// Generate params sent to SAM
	//-------------------------------
	//SystemCode
	memcpy(&felica_cmd_params[nIdx], SYSTEM_CODE, 2);
	nIdx += 2;
	//TimeSlot
	felica_cmd_params[nIdx++] = 0x00;
	
#ifdef DEBUG
	PrintText("\n 1.AskFeliCaCmdToSAM \n");
	PrintHexArray("Snr= ", 4, Snr);
#endif
	// Ask to SAM to generate FeliCa Command
	ret = AskFeliCaCmdToSAM(SAM_COMMAND_CODE_POLLING, nIdx, felica_cmd_params, &felica_cmd_len, felica_cmd, reader);
	if(ret != SAM_COMMAND_SUCCEEDED){
		//PrintText("File:%s Line:%d Function:%s AskFeliCaCmdToSAM() failed with error Code:%ld",__FILENAME, LINE__,__func__,ret);
		return ret;
	}
	
#ifdef DEBUG
	PrintText("\n 2.TransmitDataToFeliCaCard \n");
	PrintHexArray("Snr= ", 4, Snr);
	PrintHexArray("felica_res= ", 8, felica_res + 1);
#endif
	// Send Command to FeliCa Card
	ret = reader-> sendToCard(felica_cmd, felica_cmd_len, felica_res, &_felica_res_len, Card::FELICA_RCSA01_CARD);
	if ((ret != CARD_DATA_TRANSMIT_SUCCEEDED) || (_felica_res_len == 0))
	{
		printf("card error code %d \n",ret);
		// Report FeliCa Error to SAM
		//ret = SendCardErrorToSAM(&_work_buf_len, work_buf, reader);
		update_SNR_For_NextCommand(reader);
		return ret;
	}
	
#ifdef DEBUG
	PrintText("\n 3.SendPollingResToSAM \n");
	PrintHexArray("Snr= ", 4, Snr);
	PrintHexArray("felica_res= ", 8, felica_res + 1);
#endif
	//Send FeliCa Response to SAM
	ret = SendPollingResToSAM(_felica_res_len, felica_res, reader);
	if (ret != SAM_COMMAND_SUCCEEDED)
	{
		return ret;
	}

	memcpy(g_IDm, &felica_res[1], 8);

#ifdef DEBUG
	PrintHexArray("\n IDm:", 8, g_IDm);
	PrintHexArray("Snr= ", 4, Snr);
#endif

	return CARD_COMMAND_SUCCEEDED;
}

long FelicaCard::MutualAuthV2WithFeliCa(const unsigned char systemCode[2],
							const unsigned char serviceCodeNum, 
							const unsigned char serviceCodeKeyVerList[],
							Reader* reader)
{
	long			ret;
	
	unsigned char	felica_cmd_params[256];
	unsigned int	felica_cmd_params_len=0;

	unsigned char	_sam_res_buf[262], felica_cmd[262], felica_res[262];
	unsigned long	_sam_res_len, felica_cmd_len, _felica_res_len;

	unsigned int nIdx = 0;
	
	//-------------------------------
	// Generate params sent to SAM
	//-------------------------------
	//IDm
	memcpy(&felica_cmd_params[nIdx], g_IDm, 8);
	nIdx += 8;
	//Reserved
	felica_cmd_params[nIdx++] = 0x00;
	// Key Type(Node key, Diversification Code specified)
	felica_cmd_params[nIdx++] =	0x03;
	// SystemCode
	memcpy(&felica_cmd_params[nIdx], systemCode, 2);
	nIdx += 2;
	// Operation Parameter(No Diversification, AES128)
	felica_cmd_params[nIdx++] = 0x00;
	// Diversification code(All Zero)
	memset(&felica_cmd_params[nIdx], 0x00, 16);
	nIdx += 16;
	// Number of Service 
	felica_cmd_params[nIdx++] = serviceCodeNum;
	// Service Code List
	memcpy(&felica_cmd_params[nIdx], serviceCodeKeyVerList, serviceCodeNum * 4);
	nIdx += serviceCodeNum * 4;

	felica_cmd_params_len = nIdx;
	
#ifdef DEBUG
	PrintText("\n   (1).AskFeliCaCmdToSAMSC \n");
#endif
	// Ask to SAM to generate FeliCa Command
	ret = AskFeliCaCmdToSAMSC(SAM_COMMAND_CODE_MUTUAL_AUTH_V2_RWSAM, SAM_SUB_COMMAND_CODE_MUTUAL_AUTH_V2_RWSAM, felica_cmd_params_len, felica_cmd_params, &felica_cmd_len, felica_cmd, reader);
	if (ret != SAM_COMMAND_SUCCEEDED)
	{
		return ret;
	}
	
#ifdef DEBUG
	PrintText("\n   (2).TransmitDataToFeliCaCard \n");
#endif
	//Send authentication1V2 command to FeliCa Card
	ret = reader-> sendToCard(felica_cmd, felica_cmd_len, felica_res, &_felica_res_len, Card::FELICA_RCSA01_CARD);
	if (ret != CARD_DATA_TRANSMIT_SUCCEEDED)
	{
		update_SNR_For_NextCommand(reader);
		return ret;
	}
	
#ifdef DEBUG
	PrintText("\n   (3).SendAuth1V2ResultToSAM \n");
#endif
	//Send authentication1V2 result to SAM
	ret = SendAuth1V2ResultToSAM(_felica_res_len, felica_res, &felica_cmd_len, felica_cmd, reader);
	if (ret != SAM_DATA_TRANSMIT_SUCCEEDED)
	{
		return ret;
	}
	
#ifdef DEBUG
	PrintText("\n   (4).TransmitDataToFeliCaCard \n");
#endif
	//Send authentication2V2 to FeliCa
	ret = reader-> sendToCard(felica_cmd, felica_cmd_len, felica_res, &_felica_res_len, Card::FELICA_RCSA01_CARD);
	if (ret != CARD_DATA_TRANSMIT_SUCCEEDED)
	{
		update_SNR_For_NextCommand(reader);
		return ret;	}
	
#ifdef DEBUG
	PrintText("\n   (5).SendCardResultToSAM \n");
#endif
	//Send authentication2V2 result to SAM
	ret = SendCardResultToSAM(_felica_res_len, felica_res, &_sam_res_len, _sam_res_buf, reader);
	if (ret != SAM_COMMAND_SUCCEEDED)
	{
		return ret;
	}

	if (_sam_res_buf[0] != 0)
	{
		return SAM_RESPONSE_BUFFER_ERROR;
	}

	//Get IDt
	g_IDt[0] = _sam_res_buf[1+8+8];
	g_IDt[1] = _sam_res_buf[1+8+8+1];

	return SAM_COMMAND_SUCCEEDED;
}

long FelicaCard::ReadDataBlock(const unsigned char blockNum,
				   const unsigned char blockList[], 
				   unsigned long* readLen,
				   unsigned char readData[],
				   Reader* reader)
{
	long			ret;

	unsigned char	felica_cmd_params[256];

	unsigned char	felica_cmd[262], felica_res[262];
	unsigned long	felica_cmd_len, _felica_res_len;

	unsigned int nIdx = 0;
	
	//-------------------------------
	// Generate params sent to SAM
	//-------------------------------
	//IDt
	felica_cmd_params[nIdx++] = g_IDt[0];
	felica_cmd_params[nIdx++] = g_IDt[1];
	//Number of Blocks
	felica_cmd_params[nIdx++] =	blockNum;
	//Block List
	memcpy(&felica_cmd_params[nIdx], blockList, (blockNum * 2));
	nIdx += (blockNum * 2);

	// Ask to SAM to generate FeliCa Command
	ret = AskFeliCaCmdToSAM(SAM_COMMAND_CODE_READ, nIdx, felica_cmd_params, &felica_cmd_len, felica_cmd, reader);
	if (ret != SAM_COMMAND_SUCCEEDED)
	{
		return ret;
	}

	//Send Command to FeliCa Card
	ret = reader-> sendToCard(felica_cmd, felica_cmd_len, felica_res, &_felica_res_len, Card::FELICA_RCSA01_CARD);
	if (ret != CARD_DATA_TRANSMIT_SUCCEEDED)
	{
		update_SNR_For_NextCommand(reader);
		return ret;
	}

	//Send FeliCa Response to SAM
	ret = SendCardResultToSAM(_felica_res_len, felica_res, readLen, readData, reader);
	if (ret != SAM_COMMAND_SUCCEEDED)
	{
		return ret;
	}

	return SAM_COMMAND_SUCCEEDED;
}

long FelicaCard::WriteDataBlock(const unsigned char	blockNum,
					const int             blockLen,
					const unsigned char	blockList[], 
					const unsigned char	blockData[],
					Reader* reader)
{
	long			ret;

	unsigned char	felica_cmd_params[256];
	unsigned int	felica_cmd_params_len=0;

	unsigned char	felica_cmd[262], felica_res[262], work_buf[262];
	unsigned long	felica_cmd_len, _felica_res_len, work_len;

	unsigned int nIdx = 0;
	
	//-------------------------------
	// Generate params sent to SAM
	//-------------------------------
	//IDt
	felica_cmd_params[nIdx++] = g_IDt[0];
	felica_cmd_params[nIdx++] = g_IDt[1];
	//Number of Blocks
	felica_cmd_params[nIdx++] =	blockNum;
	//Block List
	memcpy(&felica_cmd_params[nIdx], blockList, blockLen);
	nIdx += blockLen;
	//Block Data
	memcpy(&felica_cmd_params[nIdx], blockData, (blockNum * 16));
	nIdx += (blockNum * 16);
	// Ask to SAM to generate FeliCa Command
	ret = AskFeliCaCmdToSAM(SAM_COMMAND_CODE_WRITE, nIdx, felica_cmd_params, &felica_cmd_len, felica_cmd, reader);
	if (ret != SAM_COMMAND_SUCCEEDED)
	{
		return ret;
	}

	//Send Command to FeliCa Card
	ret = reader-> sendToCard(felica_cmd, felica_cmd_len, felica_res, &_felica_res_len, Card::FELICA_RCSA01_CARD);
	if (ret != CARD_DATA_TRANSMIT_SUCCEEDED)
	{
		return ret;
	}

	//Send FeliCa Response to SAM
	ret = SendCardResultToSAM(_felica_res_len, felica_res, &work_len, work_buf, reader);
	if (ret != SAM_COMMAND_SUCCEEDED)
	{
		return ret;
	}
	// XX AA means that failed to change key
	return CARD_COMMAND_SUCCEEDED;
}

void FelicaCard::_update_exec_id( unsigned char execID[] ) 
{
	unsigned int _id_val, _new_id_val;
	unsigned char _tmp[4];

	_id_val = CharArrayToInt(execID, 2);
	if ( _id_val == 0xFFFF ) {
		_new_id_val = 0;
	} else {
		_new_id_val = _id_val + 1;
	}

	IntToCharArray(_new_id_val, _tmp);
	execID[0] = _tmp[2];
	execID[1] = _tmp[3];
}

ErrorConstants FelicaCard::ActivatePass(Reader *reader, Pass *pass){
	cout<<"\n\n Starting to work on activate pass \n";
	long			_ret = 0;
	unsigned char	serviceNumber,blockNo;
	unsigned long _read_len;
	unsigned char payload[256],serviceList[128],blockList[256], _read_data[512],workBuf[16],passes[256],_work_buf[256],_exec_id[2],_block_data[16];
	char* endptr=NULL;
	unsigned int balance;

	_ret = PollingFeliCaCard(reader);
	if (_ret != CARD_COMMAND_SUCCEEDED) {
		printf("Polling Error\n");
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::POLLING_FAILED;
	}

	ServiceType serviceType = pass->getServiceType();
	if(ServiceType::TOLL == serviceType){
		serviceNumber= (int) TollSpecific::PASS_SERVICE+1;//For payment
		serviceList[0] = 0x08; 		// Service code
		serviceList[1] = 0x40; 		// Service code
		serviceList[2] = 0x02; 		// Service code
		serviceList[3] = 0x01; 		// Service code
		serviceList[4] = 0x14; 		// Service code
		serviceList[5] = 0x20; 		// Service code
		serviceList[6] = 0x02; 		// Service code
		serviceList[7] = 0x01; 		// Service code
		blockNo= (unsigned char)TollSpecific::BLOCK_NUM_PASSES+1;//For payment
		blockList[0] = 0x80;		// Block list
		blockList[1] = 0x00;		// Block list
		blockList[2] = 0x80;		// Block list
		blockList[3] = 0x01;	    // Block list
		blockList[4] = 0x80;		// Block list
		blockList[5] = 0x02;		// Block list
		blockList[6] = 0x80;		// Block list
		blockList[7] = 0x03;		// Block list
		blockList[8] = 0x80;		// Block list
		blockList[9] = 0x04;		// Block list
		blockList[10] = 0x80;		// Block list
		blockList[11] = 0x05;		// Block list
		blockList[12] = 0x81;		// Block list
		blockList[13] = 0x00;		// Block list
	}else if(ServiceType::BUS == serviceType){
		cout<<"Service not supported yet";
	}else if (ServiceType::METRO==serviceType){
		cout<<"Service not supported yet";
	}else if(ServiceType::RAIL == serviceType){
		cout<<"Service not supported yet";
	}else if(ServiceType::PARKING == serviceType){
		cout<<"Service not supported yet";
	}else{
		return ErrorConstants::INVALID_SERVICE_TYPE;
	}
		
	_ret = MutualAuthV2WithFeliCa(SYSTEM_CODE, serviceNumber, serviceList, reader);
	if (_ret != SAM_COMMAND_SUCCEEDED) {
		reader-> DisconnectFeliCaCard();
		return ErrorConstants::CARD_AUTH_FAILED;
	}

	ReadDataBlock(blockNo, blockList, &_read_len, _read_data, reader);
	// Check Status FLAG
	if ((_read_data[0] != 0x00) || (_read_data[1] != 0x00)) {
		return ErrorConstants::READ_FROM_CARD_FAILED;
	}

	memcpy(passes,&_read_data[3],96);
	
	balance = CharArrayToIntLE(&_read_data[3+96],4);
	if(balance < pass->getAmount()){
		return ErrorConstants::LOW_BALANCE;
	}

	// Get current exec id
	_exec_id[0] = _read_data[3+96+14];
	_exec_id[1] = _read_data[3+96+15];

	IntToCharArrayLE(pass->getAmount(), _work_buf);
	
	_update_exec_id (_exec_id);

	memcpy(&_block_data[0], _work_buf, 4);	// amount of payment/ new balance data
	memset(&_block_data[4], 0x00, 10);		// Zero
	_block_data[14] = _exec_id[0];			// Exec ID
	_block_data[15] = _exec_id[1];			// Exec ID
	
	vector<Pass>  matchedPassesData;
	int  passesDataLength=0;
	getPasses(passes,NULL,&matchedPassesData,&passesDataLength);
	printf("/n Passes data length is %d",passesDataLength);
	
	for(int i=0;i<passesDataLength;i++){
		printf(" Matched passes branch ID %s\n",matchedPassesData[i].getSrcBranchId().c_str());
		printf("sentPass branchId %s \n",pass->getSrcBranchId().c_str());
		if(matchedPassesData[i].getSrcBranchId()==pass->getSrcBranchId() || matchedPassesData[i].getDestBranchId()==pass->getSrcBranchId() || matchedPassesData[i].getSrcBranchId()==pass->getDestBranchId() ||  matchedPassesData[i].getDestBranchId()==pass->getDestBranchId()){
			if(pass->getIsRenewal()){
				printf("Renewal Process");
				break;
			}else{
				printf("\n Expiry time for matched pass isss %lld", (matchedPassesData[i].getExpiryDate()*1000));
				printf("\n current time for provided pass isss %lld", pass->getTimeInMilliSeconds());
				if((matchedPassesData[i].getExpiryDate()*1000) > pass->getTimeInMilliSeconds()){
					return ErrorConstants::PASS_ALREADY_EXIST;
				}
			}
		}
	}

	for(int i=0;i<passesDataLength;i++){

			if(strtoll(matchedPassesData[i].getSrcBranchId().c_str(),&endptr,10)==0 || (matchedPassesData[i].getExpiryDate()*1000) < pass->getTimeInMilliSeconds() || (matchedPassesData[i].getSrcBranchId()==pass->getSrcBranchId() && matchedPassesData[i].getDestBranchId()==pass->getDestBranchId() && pass->getIsRenewal())){
				unsigned char out[4],byteExpiryDate[4];
				char* endptr;
				unsigned int FromBranchID = strtoll(pass->getSrcBranchId().c_str(),&endptr,10);
				long amount = pass->getAmount();
				unsigned int ToBranchId = strtoll(pass->getDestBranchId().c_str(),&endptr,10);
				int maxNoOfTrips = pass->getMaxTrips();
				PassType passType = pass->getPassType();
				cout << "passType " << (int)passType << endl;
				long long expiryDate = pass->getExpiryDate();
				cout<<"expiryDate "<<expiryDate<<endl;
				long long currentTime = pass->getTimeInMilliSeconds();
				currentTime = trimMilliseconds(currentTime);
				unsigned char byteConvertedNoOfTrips[2];
				expiryDate = trimMilliseconds(expiryDate);
				IntToCharArray(expiryDate,out);
				memcpy(byteExpiryDate,out,4);
				PrintHexArray("byteExpiryDate: ",4,byteExpiryDate);
				LimitPeriodicity limitPeriodicity; 
				unsigned char lPeriodicity = 0 ;
				int lmaxCount;
				int limitMaxCount[2] = { 0 };//, limitMaxCount1[2] = { 0};
				vector<Periodicity> periodicities = pass->getPeriodicity();
				auto j = 0;
				for (auto & periodicity : periodicities) {
					
					limitPeriodicity = periodicity.getLimitPeriodicity();
					cout<<"limitPeriodicity " <<static_cast<int>(limitPeriodicity)<<endl;
					lmaxCount = periodicity.getLimitsMaxCount();
					lPeriodicity = lPeriodicity + static_cast<int>(limitPeriodicity)*(16 - (j * 16) + j);
					cout<<"lPeriodicity inside "<<lPeriodicity<<endl;
					limitMaxCount[j] = lmaxCount;
					j++;
				}
				printf("lPeriodicity %02x \n",lPeriodicity);
				cout<<"limitPeriodicity out side " <<lPeriodicity<< endl;
				unsigned char limitStartTime[4];
				unsigned char CRISID[6] = {0};
				IntToCharArray(currentTime,out);
				memcpy(limitStartTime,out,4);
				unsigned char byteConvertedFromBranchID[4] = {0};
				unsigned char byteConvertedToBranchID[4] = {0};
				IntToCharArray(FromBranchID,byteConvertedFromBranchID);
				IntToCharArray(ToBranchId,byteConvertedToBranchID);
				PrintHexArray("byteConvertedToBranchID: ",4,byteConvertedToBranchID);
				PrintHexArray("byteConvertedFromBranchID: ",4,byteConvertedFromBranchID);
				int noOfTrips = maxNoOfTrips;
				IntToCharArrayToBytes(noOfTrips, byteConvertedNoOfTrips);
				memset(payload,0,256);
				if (serviceType ==ServiceType::BUS || serviceType == ServiceType::TOLL || serviceType ==ServiceType::METRO)
				{
					_ret = createPass(reader,serviceType,byteConvertedFromBranchID, byteConvertedToBranchID,
								 byteConvertedNoOfTrips, byteExpiryDate, (unsigned char)passType, lPeriodicity, limitMaxCount,
								 limitStartTime, limitStartTime,payload);
					if(_ret != APP_SUCCESS)
						return ErrorConstants::CREATE_PASS_FAILED;
					blockNo=2+1;//For balance
					blockList[0]=0x80;
					blockList[1]=i*2;
					blockList[2]=0x80;
					blockList[3]=(i*2)+1;
					blockList[4]=0x81;
					blockList[5]=0x00;
					PrintHexArray("\n BlockList \n ", 4, blockList);
					//Add the data to deduct the amount
					memcpy(&payload[32],_block_data,16);
					_ret=WriteDataBlock(blockNo,2*blockNo,blockList,payload,reader);
					if(_ret != CARD_COMMAND_SUCCEEDED){
						return ErrorConstants::CREATE_PASS_FAILED;
					}
					return ErrorConstants::NO__ERROR;
				}else{
					//HANDLE THE CASES FOR DIFF SERVICES
				}
				break;
			}
	}

	return ErrorConstants::PASS_DATA_FULL;
}

long FelicaCard::createPass(Reader *reader,ServiceType serviceType,unsigned char FromBranchID[], unsigned char ToBranchId[],
					unsigned char NoofTrips[], unsigned char ExpiryDate[], unsigned char Passtype,
					unsigned char limitPeriodicity, int limitMaxCount[],  
					unsigned char limitStartTime[], unsigned char limitStartTime1[],unsigned char payload[])
{
	unsigned long ret;
	unsigned char LimitsRemainingCount[2] = {0},LimitRemainingCount1[2] = {0};
	unsigned char byteLimitsMaxCount[2] = { 0 }, byteLimitsMaxCount1[2] = { 0 };

	IntToCharArrayToBytes(limitMaxCount[0], byteLimitsMaxCount);
	IntToCharArrayToBytes(limitMaxCount[1], byteLimitsMaxCount1);
	printf("limitMaxCount-- %d\n", limitMaxCount[0]);
	printf("limitMaxCount1-- %d\n", limitMaxCount[1]);
	//limitMaxCount[0] -= 1;  // commented for now
	//limitMaxCount[1] -= 1;
	IntToCharArrayToBytes(limitMaxCount[0], LimitsRemainingCount);
	IntToCharArrayToBytes(limitMaxCount[1], LimitRemainingCount1);


	PrintHexArray("LimitsRemainingCount ", 2, LimitsRemainingCount);

	PrintHexArray("LimitsRemainingCount1 ", 2, LimitRemainingCount1);
	
	memcpy(payload, FromBranchID, 4);
	memcpy(&payload[4], ToBranchId, 4);
	memcpy(&payload[8], NoofTrips, 2);
	memcpy(&payload[10], ExpiryDate, 4);
	//memcpy(payload[14], Passtype, 1);
	payload[14] = Passtype;
	//memcpy(payload[15], limitPeriodicity, 1);
	payload[15] = limitPeriodicity;
	memcpy(&payload[16], byteLimitsMaxCount, 2);
	memcpy(&payload[18], LimitsRemainingCount, 2);
	memcpy(&payload[20], limitStartTime, 4);
	// cout<<"limitPeriodicity "<<(limitPeriodicity & 0x0f)<<endl;; //Taking first 4 bits
	if((limitPeriodicity & 0x0f) != 0){
	memcpy(&payload[24], byteLimitsMaxCount1, 2);
	memcpy(&payload[26], LimitRemainingCount1, 2);
	memcpy(&payload[28], limitStartTime1, 4);
	}
	PrintHexArray("Payload ", 32, payload);
	
	return APP_SUCCESS;
}

ErrorConstants FelicaCard::UpdateVehicleData(Reader *reader, VehicleData *vehicleData){return ErrorConstants::GET_VALUE_FAILED;}
ErrorConstants FelicaCard::UpdateCardStatus(Reader *reader, CardStatus *cardStatus){return ErrorConstants::GET_VALUE_FAILED;}
ErrorConstants FelicaCard::PurchaseTicket(Reader *reader, Ticket *ticket){return ErrorConstants::GET_VALUE_FAILED;}
ErrorConstants FelicaCard::GetTicket(Reader *reader, Ticket *ticket){return ErrorConstants::GET_VALUE_FAILED;}
ErrorConstants FelicaCard::UsePass(Reader *reader, Pass *pass){return ErrorConstants::GET_VALUE_FAILED;}
ErrorConstants FelicaCard::GetPass(Reader *reader, Pass *pass, PassCriteria *passCriteria){return ErrorConstants::GET_VALUE_FAILED;}
ErrorConstants FelicaCard::GetTransactions(Reader *reader,TransactionsList *transactionsList,TransactionsCriteria *transactionsCriteria){return ErrorConstants::GET_VALUE_FAILED;}
