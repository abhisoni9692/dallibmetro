#include <iostream>
#include <string>
#include <utility>
#include "model/Activity.h"
#include "model/utils/ConversionUtil.h"
#include "model/utils/EnumUtil.h"
#include "Validator.h"

using v8::Isolate;
using v8::String;
using v8::Value;
using v8::Integer;

ServiceType Activity::getServiceType(){
	return Activity::serviceType;
}

void Activity::setServiceType(ServiceType serviceType){
	 Activity::serviceType =serviceType;
}

Action Activity::getAction(){
	return Activity::action;
}

void Activity::setAction(Action action){
	 Activity::action =action;
}

string Activity::getPosTerminalId(){
	return Activity::posTerminalId;
}

void Activity::setPosTerminalId(string posTerminalId){
	 Activity::posTerminalId.assign(posTerminalId);
}

long Activity::getAmount(){
	return amount;
}

void Activity::setAmount(long amount) {
	Activity::amount = amount;
}

string Activity::getSrcBranchId(){
	return Activity::srcBranchId;
}

void Activity::setSrcBranchId(string srcBranchId) {
	 Activity::srcBranchId.assign(srcBranchId);
}

string Activity::getDestBranchId() {
	return Activity::destBranchId;

}

void Activity::setDestBranchId(string destBranchId){
	this->destBranchId = std::move(destBranchId);
}

long long Activity::getTimeInMilliSeconds(){
	return Activity::timeInMilliSeconds;
}

void Activity::setTimeInMilliSeconds(long long timeInMilliSeconds){
	Activity::timeInMilliSeconds = timeInMilliSeconds;
}

ErrorList Activity::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList;

	Local<Value> value = object->Get(String::NewFromUtf8(isolate, "serviceType"));
	if (Validator::NotNullString(value, "serviceType", "Invalid service type", &errorList)) {
		this->setServiceType(EnumUtil::getServiceType(ConversionUtil::toCharArray(object->Get(String::NewFromUtf8(isolate, "serviceType"))->ToString())));
	}

	value = object->Get(String::NewFromUtf8(isolate, "action"));
	if (Validator::NotNullString(value, "action", "Invalid action", &errorList)) {
		this->setAction(EnumUtil::getAction(ConversionUtil::toCharArray(object->Get(String::NewFromUtf8(isolate, "action"))->ToString())));
	}

	value = object->Get(String::NewFromUtf8(isolate, "posTerminalId"));
	if (Validator::NotNullString(value, "posTerminalId", "Invalid pos terminal id", &errorList)) {
		this->setPosTerminalId(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "posTerminalId"))->ToString()));
	}

	value = object->Get(String::NewFromUtf8(isolate, "amount"));
	if (Validator::NotNullNumber(value, "amount", "Invalid amount", &errorList)) {
		this->setAmount((long)object->Get(String::NewFromUtf8(isolate, "amount"))->IntegerValue());
	}

	value = object->Get(String::NewFromUtf8(isolate, "sourceBranchId"));
	if (Validator::NotNullString(value, "sourceBranchId", "Invalid source branch id", &errorList)) {
		std::cout<<"reached Activity"<<endl;
		this->setSrcBranchId(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "sourceBranchId"))->ToString()));
	}

	value = object->Get(String::NewFromUtf8(isolate, "destinationBranchId"));
	if (Validator::NotNullString(value, "destinationBranchId", "Invalid destination branch id", &errorList)) {
		this->setDestBranchId(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "destinationBranchId"))->ToString()));
	}

	value = object->Get(String::NewFromUtf8(isolate, "timeInMilliSeconds"));
	if (Validator::NotNullNumber(value, "timeInMilliSeconds", "Invalid time in milli seconds", &errorList)) {
		this->setTimeInMilliSeconds(object->Get(String::NewFromUtf8(isolate, "timeInMilliSeconds"))->IntegerValue());
	}
	return errorList;
}

Local<Object> Activity::serialize() {
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> object = Object::New(isolate);
	// object->Set(String::NewFromUtf8(isolate, "posTerminalId"), String::NewFromUtf8(isolate, this->getPosTerminalId().c_str()));
	object->Set(String::NewFromUtf8(isolate, "amount"), Integer::New(isolate, this->getAmount()));
	object->Set(String::NewFromUtf8(isolate, "sourceBranchId"), String::NewFromUtf8(isolate, this->getSrcBranchId().c_str()));
	object->Set(String::NewFromUtf8(isolate, "destinationBranchId"), String::NewFromUtf8(isolate, this->getDestBranchId().c_str()));
	// object->Set(String::NewFromUtf8(isolate, "timeInMilliSeconds"), Number::New(isolate, this->getTimeInMilliSeconds()));
	return object;
 }