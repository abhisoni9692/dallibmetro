#include <iostream>
#include <string>
#include <utility>
#include "model/Balance.h"
#include "model/utils/ConversionUtil.h"
#include "model/utils/EnumUtil.h"
#include "Validator.h"

using v8::Isolate;
using v8::String;
using v8::Value;
using v8::Integer;

Balance::Balance() {

}

long Balance::getAmount() {
	return amount;
}

void Balance::setAmount(long amount) {
	Balance::amount = amount;
}

ErrorList Balance::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList;

	Local<Value> value = object->Get(String::NewFromUtf8(isolate, "amount"));
	if (Validator::NotNullNumber(value, "amount", "Invalid amount", &errorList)) {
		this->setAmount((long)object->Get(String::NewFromUtf8(isolate, "amount"))->IntegerValue());
	}
	return errorList;
}

Local<Object> Balance::serialize() {
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> object = Object::New(isolate);
	object->Set(String::NewFromUtf8(isolate, "amount"), Integer::New(isolate, this->getAmount()));
	return object;
 }