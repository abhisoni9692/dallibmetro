#include <utility>
#include "model/CardActivation.h"
#include "model/utils/ConversionUtil.h"
#include "model/utils/EnumUtil.h"
#include "model/enum/Comparison.h"
#include "model/enum/VehicleType.h"
#include "Validator.h"

using v8::Isolate;
using v8::String;
using v8::Value;
using v8::Integer;
using v8::Number;

string CardActivation::getPosTerminalId() 
{
	return CardActivation::posTerminalId;
}

void CardActivation::setPosTerminalId(string posTerminalId)
{
	CardActivation::posTerminalId = std::move(posTerminalId);
}

long long CardActivation::getTimeInMilliSeconds()
{
	return CardActivation::timeInMilliSeconds;
}

void CardActivation::setTimeInMilliSeconds(long long timeInMilliSeconds)
{
	CardActivation::timeInMilliSeconds = timeInMilliSeconds;
}


long CardActivation::getAmount(){
	return amount;
}

void CardActivation::setAmount(long amount) {
	CardActivation::amount = amount;
}


string CardActivation::getName(){
	return CardActivation::name;
}

void CardActivation::setName(string name){
	//this->name = std::move(name);
	CardActivation::name.assign(name);
	//cout << "firstName:" << this->firstName << "\n";
}


VehicleType CardActivation::getVehicleType(){
	return CardActivation::vehicleType;
}

void CardActivation::setVehicleType(VehicleType vehicleType){
	CardActivation::vehicleType=vehicleType;
}

string CardActivation::getVehicleNumber(){
	return CardActivation::vehicleNumber;
}

void CardActivation::setVehicleNumber(string vehicleNumber){
	CardActivation::vehicleNumber.assign(vehicleNumber);
}

long long CardActivation::getMobile(){
	return CardActivation::mobile;
}

void CardActivation::setMobile(long long mobile){
	CardActivation::mobile=mobile;
}

long long CardActivation::getAadhaar(){
	return CardActivation::aadhaar;
}

void CardActivation::setAadhaar(long long aadhaar){
	CardActivation::aadhaar = aadhaar;
}

ErrorList CardActivation::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList;
	Local<Value>value = object->Get(String::NewFromUtf8(isolate, "posTerminalId"));
	if (Validator::NotNullString(value, "posTerminalId", "Invalid pos terminal id", &errorList)) {
		this->setPosTerminalId(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "posTerminalId"))->ToString()));
	}

	value = object->Get(String::NewFromUtf8(isolate, "timeInMilliSeconds"));
	if (Validator::NotNullNumber(value, "timeInMilliSeconds", "Invalid time in milli seconds", &errorList)) {
		this->setTimeInMilliSeconds(object->Get(String::NewFromUtf8(isolate, "timeInMilliSeconds"))->IntegerValue());
	}

	value = object->Get(String::NewFromUtf8(isolate, "amount"));
	if (Validator::NotNullNumber(value, "amount", "Invalid amount", &errorList)) {
		this->setAmount((long)object->Get(String::NewFromUtf8(isolate, "amount"))->IntegerValue());
	}
	
	value = object->Get(String::NewFromUtf8(isolate, "name"));
	if(Validator::NotNullString(value, "name", "Invalid name", &errorList) && Validator::LengthOfString(value,"name","Invalid name length",48,Comparison::LESS_THAN_EQUAL_TO,&errorList)) {
		this->setName(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "name"))->ToString()));
	}

	value = object->Get(String::NewFromUtf8(isolate, "mobile"));
	if(Validator::NotNullNumber(value, "mobile", "Invalid mobile number", &errorList) && Validator::NumberOfDigits(value,"mobile","Invalid mobile length",10, Comparison::EQUAL_TO,&errorList)) {
		this->setMobile(object->Get(String::NewFromUtf8(isolate, "mobile"))->NumberValue());
	}
	
	value = object->Get(String::NewFromUtf8(isolate, "aadhaar"));
	if(Validator::NotNullNumber(value, "aadhaar", "Invalid aadhaar number", &errorList)  &&  Validator::NumberOfDigits(value,"adhaar","Invalid adhaar length",12,  Comparison::EQUAL_TO, &errorList)) {
		this->setAadhaar(object->Get(String::NewFromUtf8(isolate, "aadhaar"))->NumberValue());
	}

	value = object->Get(String::NewFromUtf8(isolate, "vehicleType"));
	if(Validator::NotNullString(value, "vehicleType", "Invalid vehicleType", &errorList)) {
		this->setVehicleType(EnumUtil::getVehicleType(ConversionUtil::toCharArray(object->Get(String::NewFromUtf8(isolate, "vehicleType"))->ToString())));
	}

	value = object->Get(String::NewFromUtf8(isolate, "vehicleNumber"));
	if(Validator::NotNullString(value, "vehicleNumber", "Invalid vehicleNumber", &errorList) && Validator::LengthOfString(value,"vehicle number","Invalid vehicle number length",16,Comparison::LESS_THAN_EQUAL_TO,&errorList)) {
		this->setVehicleNumber(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "vehicleNumber"))->ToString()));
	}
	
	return errorList;
}

Local<Object> CardActivation::serialize() {
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> object = Object::New(isolate);
	object->Set(String::NewFromUtf8(isolate, "posTerminalId"), String::NewFromUtf8(isolate, this->getPosTerminalId().c_str()));
	object->Set(String::NewFromUtf8(isolate, "timeInMilliSeconds"), Integer::New(isolate, this->getTimeInMilliSeconds()));
	return object;
}

 //amount[4], cardNo[16], phoneNo[5], adhaarNo[5], name[48], vehicleNo[11];