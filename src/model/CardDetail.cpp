#include <iostream>
#include <string>
#include "model/CardDetail.h"
#include "model/utils/ConversionUtil.h"
#include "model/utils/EnumUtil.h"
#include "Validator.h"

using v8::Isolate;
using v8::String;
using v8::Value;
using v8::Integer;

CardDetail::CardDetail()
{

}

string CardDetail::getCardUID()
{
	return CardDetail::cardUID;
}

void CardDetail::setCardUID(string cardUID)
{
	this->cardUID.assign(cardUID);
}

string CardDetail::getCardNumber()
{
	return CardDetail::cardNumber;
}

void CardDetail::setCardNumber(string cardNumber)
{
	CardDetail::cardNumber.assign(cardNumber);
}

int CardDetail::getBalance()
{
	return CardDetail::balance;
}

void CardDetail::setBalance(int balance)
{
	CardDetail::balance = balance;
}

int CardDetail::getStatus()
{
	return CardDetail::status;

}

void CardDetail::setStatus(int status)
{
	CardDetail::status = status;
}

string CardDetail::getVersion()
{
	return CardDetail::version;
}

void CardDetail::setVersion(string version)
{
	CardDetail::version.assign(version);
}

ErrorList CardDetail::deserialize(Local<Object> object) {
	ErrorList errorList ;
	return errorList;
}

Local<Object> CardDetail::serialize() {
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> object = Object::New(isolate);

	object->Set(String::NewFromUtf8(isolate, "cardUID"), String::NewFromUtf8(isolate, this->getCardUID().c_str()));
	object->Set(String::NewFromUtf8(isolate, "cardNumber"), String::NewFromUtf8(isolate, this->getCardNumber().c_str()));
	object->Set(String::NewFromUtf8(isolate, "balance"), Integer::New(isolate, this->getBalance()));
	object->Set(String::NewFromUtf8(isolate, "status"), String::NewFromUtf8(isolate, (const char *)EnumUtil::ToString((CardStatusEnum)this->getStatus())));
	object->Set(String::NewFromUtf8(isolate, "version"), String::NewFromUtf8(isolate, this->getVersion().c_str()));

	return object;
}