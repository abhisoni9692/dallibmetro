#include <iostream>
#include <string>
#include <utility>
#include "model/CardRemove.h"
#include "model/utils/ConversionUtil.h"
#include "model/utils/EnumUtil.h"
#include "Validator.h"

using v8::Isolate;
using v8::String;
using v8::Value;
using v8::Boolean;

CardRemove::CardRemove() {
	
}

bool CardRemove::isCardRemoved(){
	return cardRemoved;
}

void CardRemove::setCardRemoved(bool cardRemoved) {
	CardRemove::cardRemoved = cardRemoved;
}

ErrorList CardRemove::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList;
	return errorList;
}

Local<Object> CardRemove::serialize() {
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> object = Object::New(isolate);
	object->Set(String::NewFromUtf8(isolate, "cardRemoved"), Boolean::New(isolate, this->isCardRemoved()));
	return object;
 }