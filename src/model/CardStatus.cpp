#include <string>
#include "model/CardStatus.h"
#include "model/utils/ConversionUtil.h"
#include "model/utils/EnumUtil.h"
#include "Validator.h"
#include "model/utils/EnumUtil.h"

using v8::Isolate;
using v8::String;
using v8::Value;

CardStatusEnum CardStatus::getStatus(){
 return CardStatus::status;
}

void CardStatus::setStatus(CardStatusEnum status){
	CardStatus::status =status;
}

ErrorList CardStatus::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList;
	Local<Value> value = object->Get(String::NewFromUtf8(isolate, "status"));
	if (Validator::NotNullNumber(value, "status", "Invalid status", &errorList)) {
		this->setStatus(EnumUtil::getCardStatusEnum(ConversionUtil::toCharArray(object->Get(String::NewFromUtf8(isolate, "status"))->ToString())));
	}
	
	return errorList;
}

Local<Object> CardStatus::serialize() {
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> object = Object::New(isolate);
	object->Set(String::NewFromUtf8(isolate, "status"), String::NewFromUtf8(isolate, (const char *)EnumUtil::ToString(this->getStatus())));
	return object;
}