#include <iostream>
#include "model/constants/ErrorConstants.h"
#include "model/ErrorMessage.h"
using namespace std;

ErrorMessage::ErrorMessage(){
};
ErrorMessage::ErrorMessage(std::string errorCode,std::string errorMsg)
{
	this->errorCode.assign(errorCode);
	this->errorMsg.assign(errorMsg);
}

ErrorMessage *getErrorMessage(ErrorConstants errorConstants)
{
	switch(errorConstants)
	{
		case ErrorConstants::NO__ERROR: return new ErrorMessage("SUCCESS","No error");
		case ErrorConstants::APP__ERROR: return new ErrorMessage("APP_ERROR", "Internal error");
		case ErrorConstants::CARD_NOT_DETECTED: return new ErrorMessage("APP_CANCELED_OR_NO_READER", "Card detection canceled or no reader connected");
		case ErrorConstants::READ_NOT_FOUND: return new ErrorMessage("READ_NOT_FOUND", "Cannot find reader");
		case ErrorConstants::CARD_AUTH_FAILED: return new ErrorMessage("CARD_AUTH_FAILED","Card Authentication failed");
		case ErrorConstants::GET_CARD_UID_FAILED: return new ErrorMessage("GET_CARD_UID_FAILED","Getting Card UID is failed");
		case ErrorConstants::CARD_ACTICVATION_FAILED: return new ErrorMessage("CARD_ACTICVATION_FAILED","Activation of Card failed");
		case ErrorConstants::CARD_FORMATTING_FAILED: return new ErrorMessage("CARD_FORMATTING_FAILED","Card foramtting Failed");
		case ErrorConstants::CREATE_APPLICATION_FAILED: return new ErrorMessage("CREATE_APPLICATION_FAILED","creating Application failed");
		case ErrorConstants::SELECT_APPLICATION_FAILED: return new ErrorMessage("SELECT_APPLICATION_FAILED","Selecting the Application in the card failed");;
		case ErrorConstants::CREATE_BACKUP_DATA_FILE_FAILED: return new ErrorMessage("CREATE_BACKUP_DATA_FILE_FAILED","creating backup data failed");
		case ErrorConstants::CREATE_DATA_FILE_FAILED: return new ErrorMessage("CREATE_DATA_FILE_FAILED","creating data failed");
		case ErrorConstants::CREATE_VALUE_FILE_FAILED: return new ErrorMessage("CREATE_VALUE_FILE_FAILED","creating value data file failed");
		case ErrorConstants::CREATE_CYCLIC_RECORD_FAILE: return new ErrorMessage("CREATE_CYCLIC_RECORD_FAILE","creating cyclic record file failed");
		case ErrorConstants::CREATE_RECORD_FILE_FAILED: return new ErrorMessage("CREATE_RECORD_FILE_FAILED","create record file failed");
		case ErrorConstants::PERSONALIZATION_FAILED: return new ErrorMessage("PERSONALIZATION_FAILED","personalizing the card failed try again");
		case ErrorConstants::CARD_STATUS_UPDATE_FAILED: return new ErrorMessage("CARD_STATUS_UPDATE_FAILED","card status updation failed");
		case ErrorConstants::GET_VALUE_FAILED: return new ErrorMessage("GET_VALUE_FAILED","Getting balance from the card is failed");
		case ErrorConstants::UPDATE_PERSONAL_DATA_FAILAED: return new ErrorMessage("UPDATE_PERSONAL_DATA_FAILAED","updation of user personal data failed");
		case ErrorConstants::UPDATE_VEHICLE_DATA_FAILED: return new ErrorMessage("UPDATE_VEHICLE_DATA_FAILED","updation of user vehicle data failed");
		case ErrorConstants::COMMIT_FAILED: return new ErrorMessage("COMMIT_FAILED","commiting the transaction failed try again");
		case ErrorConstants::READ_FROM_CARD_FAILED: return new ErrorMessage("READ_FROM_CARD_FAILED","reading from the card failed");
		case ErrorConstants::PAY_FAILED: return new ErrorMessage("PAY_FAILED","payment failed, Please try again");
		case ErrorConstants::CREDIT_FAILED: return new ErrorMessage("CREDIT_FAILED","your recharge failed try again");
		case ErrorConstants::INVALID_AMOUNT: return new ErrorMessage("INVALID_AMOUNT","invalid amount try with valid recharge amount");
		case ErrorConstants::INVALID_SERVICE_TYPE: return new ErrorMessage("INVALID_SERVICE_TYPE","invalid service type");
		case ErrorConstants::READ_RECORD_FAILED: return new ErrorMessage("READ_RECORD_FAILED","read record failed try agian");
		case ErrorConstants::GET_FILE_SETTINGS_FAILED: return new ErrorMessage("GET_FILE_SETTINGS_FAILED","getting file settings failed");
		case ErrorConstants::ADD_RECORD_IN_TXN_LOG_FAILED: return new ErrorMessage("ADD_RECORD_IN_TXN_LOG_FAILED","adding record in transaction log failed");
		case ErrorConstants::BUYING_TICKET_FAILED: return new ErrorMessage("BUYING_TICKET_FAILED","buying ticket failed");
		case ErrorConstants::NO_TICKETS_FOUND: return new ErrorMessage("NO_TICKETS_FOUND", "No tickets found in the card try buying a ticket");
		case ErrorConstants::CARD_INACTIVE: return new ErrorMessage("CARD_INACTIVE","card is inactive");
		case ErrorConstants::READER_INIT_FAILED: return new ErrorMessage("READER_INIT_FAILED","reader intialization failed");
		case ErrorConstants::CREATE_PASS_FAILED: return new ErrorMessage("CREATE_PASS_FAILED","create pass Faled try again or Pass Already exists");
		case ErrorConstants::PASS_USE_FAILED: return new ErrorMessage("PASS_USE_FAILED","pass use failed");
		case ErrorConstants::READ_TXNLOG_FAILED: return new ErrorMessage("READ_TXNLOG_FAILED","reading transaction log failed");
		case ErrorConstants::TICKET_EXPIRED: return new ErrorMessage("TICKET_EXPIRED","Ticket Expired");
		case ErrorConstants::FUNCTION_NOT_SUPPORTED: return new ErrorMessage("FUNCTION_NOT_SUPPORTED","Function not supported in this reader");
		case ErrorConstants::PASS_NOT_FOUND: return new ErrorMessage("PASS_NOT_FOUND","Pass not Found with the given source branchId");
		case ErrorConstants::POLLING_FAILED: return new ErrorMessage("POLLING_FAILED","Polling Command Failed");
		case ErrorConstants::LOW_BALANCE: return new ErrorMessage("LOW_BALANCE","Card balance is low, Please recharge first");
		case ErrorConstants::PASS_ALREADY_EXIST: return new ErrorMessage("PASS_ALREADY_EXIST","Pass for same Toll already exists");
		case ErrorConstants::PASS_DATA_FULL: return new ErrorMessage("PASS_DATA_FULL","Passes for this service are already present and have not expired yet");

	}
}