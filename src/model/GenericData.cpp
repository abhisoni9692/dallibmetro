#include <iostream>
#include <string>
#include "model/GenericData.h"
#include "model/utils/ConversionUtil.h"
#include "model/utils/EnumUtil.h"
#include "model/enum/VehicleType.h"
#include "model/enum/CardStatusEnum.h"
#include "Validator.h"
#include "model/Pass.h"
#include "model/ParkingService.h"
#include "model/Transaction.h"

using v8::Isolate;
using v8::String;
using v8::Value;
using v8::Integer;
using v8::Number;

GenericData::GenericData()
{

}

string GenericData::getName(){
	return GenericData::name;
}

void GenericData::setName(string name){
	//this->name = std::move(name);
	GenericData::name.assign(name);
	//cout << "firstName:" << this->firstName << "\n";
}

string GenericData::getCardNumber()
{
	return GenericData::cardNumber;
}

void GenericData::setCardNumber(string cardNumber)
{
	GenericData::cardNumber.assign(cardNumber);
}

int GenericData::getBalance()
{
	return GenericData::balance;
}

void GenericData::setBalance(int balance)
{
	GenericData::balance = balance;
}

VehicleType GenericData::getVehicleType(){
	return GenericData::vehicleType;
}

void GenericData::setVehicleType(VehicleType vehicleType){
	GenericData::vehicleType=vehicleType;
}

CardStatusEnum GenericData::getCardStatus(){
	return GenericData::cardStatus;
}

void GenericData::setCardStatus(CardStatusEnum cardStatus){
	GenericData::cardStatus=cardStatus;
}

string GenericData::getVehicleNumber(){
	return GenericData::vehicleNumber;
}

void GenericData::setVehicleNumber(string vehicleNumber){
	GenericData::vehicleNumber.assign(vehicleNumber);
}

long long GenericData::getMobile(){
	return GenericData::mobile;
}

void GenericData::setMobile(long long mobile){
	GenericData::mobile=mobile;
}

long long GenericData::getAadhaar(){
	return GenericData::aadhaar;
}

void GenericData::setAadhaar(long long aadhaar){
	GenericData::aadhaar = aadhaar;
}
string GenericData::getReaderType(){
	return GenericData::readerType;
}
void GenericData::setReaderType(string readerType){
	GenericData::readerType=readerType;
}
int GenericData::getReaderIndex(){
	return GenericData::readerIndex;
}
void GenericData::setReaderIndex(int readerIndex){
	GenericData::readerIndex = readerIndex;
}

ServiceType GenericData::getServiceType(){
	return GenericData::serviceType;
}
void GenericData::setServiceType(ServiceType serviceType){
	GenericData::serviceType = serviceType;
}
string GenericData::getBranchId(){
	return GenericData::branchId;
}
void GenericData::setBranchId(string branchId){
	GenericData::branchId = branchId;
}
int GenericData::getpassCount(){
	return GenericData::passesCount;
}
void GenericData::setpassCount(int passesCount){
	GenericData::passesCount = passesCount;
}
vector<Pass> GenericData::getPass(){
	return GenericData::passes;
}
void GenericData::setPass(vector<Pass> passes){
	GenericData::passes = passes;
}
vector<Transaction> GenericData::getTransactionLog(){
	return GenericData::transactions;
}
void GenericData::setTransactionLog(vector<Transaction> transactions){
	GenericData::transactions = transactions;
}

vector<TollLog> GenericData::getTollLog(){
	return GenericData::tollLogs;
}
void GenericData::setTollLog(vector<TollLog> tollLogs){
	GenericData::tollLogs = tollLogs;
}

void GenericData::setParkingData(ParkingService *parkingData){
	GenericData::parkingData=*parkingData;
}

ParkingService GenericData::getParkingData(){
	return GenericData::parkingData;
}


ErrorList GenericData::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList ;

	printf("\n ------------------------started----------------------------");
	Local<Value>value = object->Get(String::NewFromUtf8(isolate, "readerType"));
	if(Validator::NotNullString(value, "readerType", "Invalid readerType", &errorList)) {
		this->setReaderType(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "readerType"))->ToString()));
		cout<<this->getReaderType();
	}
	printf("1 ------------------------started----------------------------");
	value = object->Get(String::NewFromUtf8(isolate, "readerIndex"));
	if(Validator::NotNullNumber(value, "readerIndex", "Invalid reader Index", &errorList)) {
		this->setReaderIndex(object->Get(String::NewFromUtf8(isolate, "readerIndex"))->NumberValue());
		printf("Reader indes is %d ",this->getReaderIndex());
	}
	value = object->Get(String::NewFromUtf8(isolate, "serviceType"));
	if (Validator::NotNullString(value, "serviceType", "Invalid service Type", &errorList))
	{
		this->setServiceType(EnumUtil::getServiceType(ConversionUtil::toCharArray(object->Get(String::NewFromUtf8(isolate, "serviceType"))->ToString())));
		printf("service type is %d ",this->getServiceType());
		}
	value = object->Get(String::NewFromUtf8(isolate, "branchId"));
	if (Validator::NotNullString(value, "branchId", "Invalid branchId", &errorList))
	{
		this->setBranchId(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "branchId"))->ToString()));
		printf("branchId is %d ",this->getBranchId());
		}
	return errorList;
}

Local<Object> GenericData::serialize() {

	Isolate* isolate = Isolate::GetCurrent();

	Local<Object> object1 = Object::New(isolate);
	Local<Array>passes = Array::New(isolate);
	Local<Array>transactions = Array::New(isolate);
	Local<Array>tollLog = Array::New(isolate);

	vector<Pass> finalPasses=getPass();
	vector<Transaction> finalTransactions=getTransactionLog();
	vector<TollLog> finalTollLogs=getTollLog();

	for(int i=0;i<getpassCount();i++){
		printf("the passes is %x\n",finalPasses[i].serialize());
		passes->Set(i,(Local<Object>) finalPasses[i].serialize());
	}
	for(int j=0;j<getTransactionLog().size();j++){
		printf("the Transaction	 is %x\n",finalTransactions[j].serialize());
		transactions->Set(j,(Local<Object>) finalTransactions[j].serialize());
	}
	for(int k=0;k<getTollLog().size();k++){
		printf("the toll log is %x\n",finalTollLogs[k].serialize());
		tollLog->Set(k,(Local<Object>) finalTollLogs[k].serialize());
	}

	printf("Serializing");
	object1->Set(String::NewFromUtf8(isolate, "name"), String::NewFromUtf8(isolate, ConversionUtil::toCharArray(getName())));	
	object1->Set(String::NewFromUtf8(isolate, "cardNumber"), String::NewFromUtf8(isolate, ConversionUtil::toCharArray(getCardNumber())));
	object1->Set(String::NewFromUtf8(isolate, "balance"), Integer::New(isolate, getBalance()));
	object1->Set(String::NewFromUtf8(isolate, "vehicleNumber"), String::NewFromUtf8(isolate, ConversionUtil::toCharArray(getVehicleNumber())));
	object1->Set(String::NewFromUtf8(isolate, "vehicleType"), String::NewFromUtf8(isolate, (const char *)EnumUtil::ToString(getVehicleType())));
	object1->Set(String::NewFromUtf8(isolate, "cardStatus"), String::NewFromUtf8(isolate, (const char *)EnumUtil::ToString(getCardStatus())));
	printf("\n card status %d",getCardStatus());
	object1->Set(String::NewFromUtf8(isolate, "mobile"), Number::New(isolate, getMobile()));
	object1->Set(String::NewFromUtf8(isolate, "aadhaar"), Number::New(isolate, getAadhaar()));
	object1->Set(String::NewFromUtf8(isolate, "readerIndex"), Number::New(isolate, getReaderIndex()));
	object1->Set(String::NewFromUtf8(isolate, "readerType"),String::NewFromUtf8(isolate, ConversionUtil::toCharArray(getReaderType())));
	object1->Set(String::NewFromUtf8(isolate, "passes"),passes);
	object1->Set(String::NewFromUtf8(isolate, "transactions"),transactions);
	object1->Set(String::NewFromUtf8(isolate, "tollLogs"),tollLog);
	object1->Set(String::NewFromUtf8(isolate, "parkingData"),getParkingData().serialize());
	return object1;
}