#include "../include/model/List.h"
#include <v8.h>

using v8::Local;
using v8::Array;

template <class T>
List<T>::List() {
	this->size = 0;
}

template <class T>
void List<T>::add(T item) {
	this->items = this->items + this->size;
	this->items = item;
	this->items = 0;
	this->size++;
}

template <class T>
void List<T>::addAll(List<T> items) {
	this->items = this->items + this->size;
	for(int i=0; i<items.Size(); i++, this->items++, this->size++) {
		this->items = items + i;
	}
	this->items = 0;
}

template <class T>
int List<T>::Size() {
	return this->size;
}