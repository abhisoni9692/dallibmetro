#include "model/ParkingService.h"
#include "model/utils/ConversionUtil.h"
#include "model/utils/EnumUtil.h"
#include "Validator.h"

using v8::Isolate;
using v8::String;
using v8::Value;
using v8::Integer;
using v8::Number;

ParkingService::ParkingService(){

}


long long ParkingService::getExitDateTime(){
	return ParkingService::exitDateTime;
}

void ParkingService::setExitDateTime(long long exitDateTime){
	ParkingService::exitDateTime = exitDateTime;
}

string ParkingService::getBranchId(){
	return ParkingService::branchId;
}

void ParkingService::setBranchId(string branchId){
	ParkingService::branchId = std::move(branchId);
}

long long ParkingService::getInDateTime(){
	return ParkingService::inDateTime;
}

void ParkingService::setInDateTime(long long inDateTime){
	ParkingService::inDateTime = inDateTime;
}


ErrorList ParkingService::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList;

	return errorList;
}

Local<Object> ParkingService::serialize() {
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> object = Object::New(isolate);
	object->Set(String::NewFromUtf8(isolate, "exitTime"), Number::New(isolate, getExitDateTime()));
	object->Set(String::NewFromUtf8(isolate, "branchId"), String::NewFromUtf8(isolate, ConversionUtil::toCharArray(getBranchId())));
	object->Set(String::NewFromUtf8(isolate, "inDateTime"), Number::New(isolate, getInDateTime()));
	return object;
}