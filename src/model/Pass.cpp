#include <iostream>
#include <vector>
#include "model/Pass.h"
#include "model/enum/PassType.h"
#include "model/utils/ConversionUtil.h"
#include "model/utils/EnumUtil.h"
#include "Validator.h"
#include "model/Activity.h"
#include "model/Periodicity.h"
#include "node.h"

using v8::Isolate;
using v8::String;
using v8::Value;
using v8::Integer;
using v8::Boolean;

using namespace std;
Pass::Pass()
{

}

PassType Pass::getPassType(){
	return passType;
}

void Pass::setPassType(PassType passType){
	this->passType = passType;
}

int Pass::getMaxTrips(){
	return maxTrips;
}

void Pass::setMaxTrips(int maxTrips){
	this->maxTrips = maxTrips;
}

long long Pass::getExpiryDate(){
	return expiryDate;
}

void Pass::setExpiryDate(long long expiryDate){
	this->expiryDate = expiryDate;
}

bool Pass::getIsRenewal(){
	return isRenewal;
}

void Pass::setIsRenewal(bool isRenewal){
	this->isRenewal = isRenewal;
}

vector<Periodicity> Pass::getPeriodicity() {
	return periodicities;
}

void Pass::setPeriodicity(vector<Periodicity> periodicities)
{
	this->periodicities = periodicities;
	// std::copy(std::begin(periodicity),std::end(periodicity),std::begin(this->periodicity));
	//this->periodicity.emplace_front(periodicity);
}

ErrorList Pass::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList = Activity::deserialize(object);
	Local<Value> value;
	vector<Periodicity> periodicitiyValue;
	Local<Array> array =  Local<Array>::Cast(object->Get(String::NewFromUtf8(isolate,"periodicity"))); 	
	int count = array->Length();
	for(int i = 0;i<count;i++)
	{
		Periodicity *periodicityOfPass = new Periodicity();
		errorList = periodicityOfPass->deserialize(Local<Object>::Cast(array->Get(i)));
		errorList.addAll(periodicityOfPass->deserialize(Local<Object>::Cast(array->Get(i))).getErrors());
		// this->getPeriodicity().push_back(*periodicityOfPass);
		 periodicitiyValue.push_back(*periodicityOfPass);
		 cout <<"getLimitsMaxCount "<<periodicityOfPass->getLimitsMaxCount() << endl;
		 // cout << "getLimitPeriodicity " << periodicityOfPass->getLimitPeriodicity() << endl;
	}
	this->setPeriodicity(periodicitiyValue);
	value = object->Get(String::NewFromUtf8(isolate, "passType"));
	if (Validator::NotNullString(value, "passType", "Invalid passType", &errorList)) {
		 this->setPassType(EnumUtil::getPassType(ConversionUtil::toCharArray(object->Get(String::NewFromUtf8(isolate, "passType"))->ToString())));
	}
	value = object->Get(String::NewFromUtf8(isolate, "maxTrips"));
	if (Validator::NotNullNumber(value, "maxTrips", "Invalid maxTrips", &errorList)) {
		this->setMaxTrips(object->Get(String::NewFromUtf8(isolate, "maxTrips"))->IntegerValue());
	}
	value = object->Get(String::NewFromUtf8(isolate, "expiryDate"));
	if (Validator::NotNullNumber(value, "expiryDate", "Invalid expiryDate", &errorList)) {
		this->setExpiryDate(object->Get(String::NewFromUtf8(isolate, "expiryDate"))->IntegerValue());
	}
	value = object->Get(String::NewFromUtf8(isolate, "isRenewal"));
	if (Validator::Bool(value, "isRenewal", "Invalid RenewType", &errorList)) {
		this->setIsRenewal(object->Get(String::NewFromUtf8(isolate, "isRenewal"))->BooleanValue());
	}
	return errorList;
}

Local<Object> Pass::serialize() {
	printf("In serializeeee");
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> object = Activity::serialize();
	cout<<"pass  serialize"<<endl;
	object->Set(String::NewFromUtf8(isolate, "passType"), String::NewFromUtf8(isolate, (const char *)EnumUtil::ToString(getPassType())));
	printf("In serializeeee 2");
	object->Set(String::NewFromUtf8(isolate, "maxTrips"), Integer::New(isolate, getMaxTrips()));
	printf("In serializeeee 3");
	object->Set(String::NewFromUtf8(isolate, "expiryDate"), Integer::New(isolate, getExpiryDate()));
	printf("In serializeeee 4");
	Local<Object> periodicitiesObject = Object::New(isolate);
	for(int i=0; i<periodicities.size(); i++) {
		printf("In serializeeee for loop");
		periodicitiesObject->Set(i, periodicities[i].serialize());
	}
	object->Set(String::NewFromUtf8(isolate, "periodicities"), periodicitiesObject);
		printf("In serializeeee 5");

	// object->Set(String::NewFromUtf8(isolate, "isRenewal"), Boolean::New(isolate, getIsRenewal()));

	return object;
}