#include <iostream>
#include <string>
#include "model/PassCriteria.h"
#include "enum/ServiceType.h"
#include "enum/PassType.h"
#include "model/utils/ConversionUtil.h"
#include "model/utils/EnumUtil.h"
#include "Validator.h"
#include "node.h"

using v8::Isolate;
using v8::String;
using v8::Value;

PassCriteria::PassCriteria(){

}

ServiceType PassCriteria::getServiceType()
{
	return PassCriteria::serviceType;
}
void PassCriteria::setServiceType(ServiceType serviceType)
{
	PassCriteria::serviceType = serviceType;
}
string PassCriteria::getSrcBranchId()
{
	return PassCriteria::srcBranchId;
}
void PassCriteria::setSrcBranchId(string srcBranchId)
{
	PassCriteria::srcBranchId.assign(srcBranchId);
}
PassType PassCriteria::getPassType()
{
	return PassCriteria::passType;
}
void PassCriteria::setPassType(PassType passType)
{
	PassCriteria::passType = passType;
}
long long PassCriteria::getTimeInMilliSeconds(){
	return PassCriteria::timeInMilliSeconds;
}

void PassCriteria::setTimeInMilliSeconds(long long timeInMilliSeconds){
	PassCriteria::timeInMilliSeconds = timeInMilliSeconds;
}
ErrorList PassCriteria::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList;

	Local<Value> value = object->Get(String::NewFromUtf8(isolate, "serviceType"));
	if (Validator::NotNullString(value, "serviceType", "Invalid service type", &errorList)) {
		this->setServiceType(EnumUtil::getServiceType(ConversionUtil::toCharArray(object->Get(String::NewFromUtf8(isolate, "serviceType"))->ToString())));
	}

	value = object->Get(String::NewFromUtf8(isolate, "sourceBranchId"));
	if (Validator::NotNullString(value, "sourceBranchId", "Invalid src branch id", &errorList)) {
		this->setSrcBranchId(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "sourceBranchId"))->ToString()));
	}

	value = object->Get(String::NewFromUtf8(isolate, "passType"));
	if (Validator::NotNullString(value, "passType", "Invalid passType", &errorList)) {
		 this->setPassType(EnumUtil::getPassType(ConversionUtil::toCharArray(object->Get(String::NewFromUtf8(isolate, "passType"))->ToString())));
	}

	value = object->Get(String::NewFromUtf8(isolate, "timeInMilliSeconds"));
	if (Validator::NotNullNumber(value, "timeInMilliSeconds", "Invalid time in milli seconds", &errorList)) {
		this->setTimeInMilliSeconds(object->Get(String::NewFromUtf8(isolate, "timeInMilliSeconds"))->IntegerValue());
	}

	return errorList;

}

Local<Object> PassCriteria::serialize() {
	Local<Object> object;
	return object;
}