#include<iostream>
#include "model/enum/LimitPeriodicity.h"
#include "model/Periodicity.h"
#include "model/utils/ConversionUtil.h"
#include "model/utils/EnumUtil.h"
#include "Validator.h"
#include "model/Activity.h"
#include "node.h"

using v8::Local;
using v8::Array;
using v8::Object;
using v8::String;
using v8::Isolate;
using v8::Handle;
using v8::Value;
using v8::Integer;

using namespace std;

Periodicity::Periodicity()
{

}

LimitPeriodicity Periodicity::getLimitPeriodicity(){
	return Periodicity::limitPeriodicity;
}
void Periodicity::setLimitPeriodicity(LimitPeriodicity limitPeriodicity){
	Periodicity::limitPeriodicity = limitPeriodicity;
}
int Periodicity::getLimitsMaxCount(){
	return Periodicity::limitsMaxCount;
}
void Periodicity::setLimitsMaxCount(int limitsMaxCount){
	Periodicity::limitsMaxCount = limitsMaxCount;
}
int Periodicity::getLimitsRemainingCount(){
	return Periodicity::limitsRemainingCount;
}
void Periodicity::setLimitsRemainingCount(int limitsRemainingCount){
	Periodicity::limitsRemainingCount =limitsRemainingCount;
}
long long Periodicity::getLimitsStartTime(){
	return Periodicity::limitsStartTime;
}
void Periodicity::setLimitsStartTime(int limitsStartTime){
	Periodicity::limitsStartTime = limitsStartTime;
}

ErrorList Periodicity::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList;
	Local<Value> value = object->Get(String::NewFromUtf8(isolate, "limitPeriodicity"));
	if (Validator::NotNullString(value, "limitPeriodicity", "Invalid limitPeriodicity type", &errorList)) {
		this->setLimitPeriodicity( EnumUtil::getLimitPeriodicity(ConversionUtil::toCharArray(object->Get(String::NewFromUtf8(isolate, "limitPeriodicity"))->ToString())));
	}
	value = object->Get(String::NewFromUtf8(isolate, "limitMaxCount"));
	if (Validator::NotNullNumber(value, "limitMaxCount", "Invalid limit max count", &errorList)) {
		this->setLimitsMaxCount(object->Get(String::NewFromUtf8(isolate, "limitMaxCount"))->IntegerValue());
	}
	return errorList;
}

Local<Object> Periodicity::serialize() {
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> object = Object::New(isolate);
	object->Set(String::NewFromUtf8(isolate, "limitPeriodicity"), String::NewFromUtf8(isolate, (const char *)EnumUtil::ToString(this->getLimitPeriodicity())));
	object->Set(String::NewFromUtf8(isolate, "limitMaxCount"), Integer::New(isolate, this->getLimitsMaxCount()));
	object->Set(String::NewFromUtf8(isolate, "limitRemainingCount"), Integer::New(isolate, this->getLimitsRemainingCount()));
	object->Set(String::NewFromUtf8(isolate, "limitStartTime"), Integer::New(isolate, this->getLimitsStartTime()));
	return object;
}