#include "model/PersonalData.h"
#include "model/utils/ConversionUtil.h"
#include "model/enum/Gender.h"
#include "model/enum/Comparison.h"
#include "model/utils/EnumUtil.h"
#include "Validator.h"
#include <stdio.h>
#include "node.h"
#include <iostream>
#include <string>
#include <utility>

using v8::Local;
using v8::Object;
using v8::Isolate;
using v8::String;
using v8::Value;
using v8::Integer;
using v8::Number;

using namespace std;

string PersonalData::getFirstName(){
	return PersonalData::firstName;
}

void PersonalData::setFirstName(string firstName){
	this->firstName = std::move(firstName);
	//cout << "firstName:" << this->firstName << "\n";
}

string PersonalData::getMiddleName(){
	return PersonalData::middleName;
}

void PersonalData::setMiddleName(string middleName){
	this->middleName = std::move(middleName);
}

string PersonalData::getLastName(){
	return PersonalData::lastName;
}

void PersonalData::setLastName(string lastName){
	this->lastName = lastName;
}

Gender PersonalData::getGender(){
	return PersonalData::gender;
}

void PersonalData::setGender(Gender gender){
	this->gender = gender;
	cout << "gender:" << &this->gender << "\n";
}

long PersonalData::getDateOfBirth(){
	return PersonalData::dateOfBirth;
}

void PersonalData::setDateOfBirth(long dateOfBirth){
	this->dateOfBirth = dateOfBirth;
	cout << "dateOfBirth:" << this->dateOfBirth << "\n";
}

long long PersonalData::getMobile(){
	return PersonalData::mobile;
}

void PersonalData::setMobile(long long mobile){
	this->mobile = mobile;
}

long long PersonalData::getAadhaar(){
	return PersonalData::aadhaar;
}

void PersonalData::setAadhaar(long long aadhaar){
	this->aadhaar = aadhaar;
}

long long PersonalData::getTimeInMilliSeconds(){
	return PersonalData::timeInMilliSeconds;
}

void PersonalData::setTimeInMilliSeconds(long long timeInMilliSeconds){
	this->timeInMilliSeconds = timeInMilliSeconds;
}

string PersonalData::getVehicleNumber(){
	return PersonalData::vehicleNumber;
}

void PersonalData::setVehicleNumber(string vehicleNumber){
	PersonalData::vehicleNumber.assign(vehicleNumber);
}

ErrorList PersonalData::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList;

	Local<Value> value = object->Get(String::NewFromUtf8(isolate, "firstName"));
	if(Validator::NotNullString(value, "firstName", "Invalid name", &errorList) && Validator::LengthOfString(value,"name","Invalid firstname length",48,Comparison::LESS_THAN_EQUAL_TO,&errorList)) {
		this->setFirstName(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "firstName"))->ToString()));
	}
	
	value = object->Get(String::NewFromUtf8(isolate, "middleName"));
	if(Validator::NotNullString(value, "middleName", "Invalid name", &errorList)) {
		this->setMiddleName(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "middleName"))->ToString()));
	}
	
	value = object->Get(String::NewFromUtf8(isolate, "lastName"));
	if(Validator::NotNullString(value, "lastName", "Invalid name", &errorList)) {
		this->setLastName(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "lastName"))->ToString()));
	}
	
	value = object->Get(String::NewFromUtf8(isolate, "gender"));
	if(Validator::NotNullString(value, "gender", "Invalid gender", &errorList)) {
		this->setGender(EnumUtil::getGender(ConversionUtil::toCharArray(object->Get(String::NewFromUtf8(isolate, "gender"))->ToString())));
	}
	
	value = object->Get(String::NewFromUtf8(isolate, "dateOfBirth"));
	if(Validator::NotNullNumber(value, "dateOfBirth", "Invalid date", &errorList)) {
		this->setDateOfBirth(object->Get(String::NewFromUtf8(isolate, "dateOfBirth"))->IntegerValue());
	}
	
	value = object->Get(String::NewFromUtf8(isolate, "mobile"));
	if(Validator::NotNullNumber(value, "mobile", "Invalid mobile number", &errorList)) {
		this->setMobile(object->Get(String::NewFromUtf8(isolate, "mobile"))->NumberValue());
	}
	
	value = object->Get(String::NewFromUtf8(isolate, "aadhaar"));
	if(Validator::NotNullNumber(value, "aadhaar", "Invalid aadhaar number", &errorList)) {
		this->setAadhaar(object->Get(String::NewFromUtf8(isolate, "aadhaar"))->NumberValue());
	}

	value = object->Get(String::NewFromUtf8(isolate, "timeInMilliSeconds"));
	if (Validator::NotNullNumber(value, "timeInMilliSeconds", "Invalid time in milli seconds", &errorList)) {
		this->setTimeInMilliSeconds(object->Get(String::NewFromUtf8(isolate, "timeInMilliSeconds"))->IntegerValue());
	}

	value = object->Get(String::NewFromUtf8(isolate, "vehicleNumber"));
	if(Validator::NotNullString(value, "vehicleNumber", "Invalid vehicleNumber", &errorList) && Validator::LengthOfString(value,"vehicle number","Invalid vehicle number length",16,Comparison::LESS_THAN_EQUAL_TO,&errorList)) {
		this->setVehicleNumber(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "vehicleNumber"))->ToString()));
	}
	return errorList;
}

Local<Object> PersonalData::serialize() {
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> object = Object::New(isolate);
	object->Set(String::NewFromUtf8(isolate, "firstName"), String::NewFromUtf8(isolate, ConversionUtil::toCharArray(getFirstName())));
	object->Set(String::NewFromUtf8(isolate, "middleName"), String::NewFromUtf8(isolate, ConversionUtil::toCharArray(getMiddleName())));
	object->Set(String::NewFromUtf8(isolate, "lastName"), String::NewFromUtf8(isolate, ConversionUtil::toCharArray(getLastName())));
	object->Set(String::NewFromUtf8(isolate, "gender"), String::NewFromUtf8(isolate, EnumUtil::ToString(getGender())));
	object->Set(String::NewFromUtf8(isolate, "dateOfBirth"), Integer::New(isolate, getDateOfBirth()));
	object->Set(String::NewFromUtf8(isolate, "mobile"), Number::New(isolate, getMobile()));
	object->Set(String::NewFromUtf8(isolate, "aadhaar"), Number::New(isolate, getAadhaar()));
	object->Set(String::NewFromUtf8(isolate, "timeInMilliSeconds"), Integer::New(isolate, getTimeInMilliSeconds()));
	object->Set(String::NewFromUtf8(isolate, "vehicleNumber"), String::NewFromUtf8(isolate,ConversionUtil::toCharArray(getVehicleNumber())));

	return object;
}