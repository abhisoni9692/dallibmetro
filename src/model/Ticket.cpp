#include<iostream>
#include<string>
#include "model/Ticket.h"
#include "model/utils/ConversionUtil.h"
#include "Validator.h"

using v8::Isolate;
using v8::String;
using v8::Value;
using v8::Integer;

long long Ticket::getExpiryTimeInMilliSeconds(){
	return Ticket::expiryTimeInMilliSeconds;
}

void Ticket::setExpiryTimeInMilliSeconds(long long expiryTimeInMilliSeconds){
	Ticket::expiryTimeInMilliSeconds = expiryTimeInMilliSeconds;
}

long long Ticket::getPartnerTxnId(){
	return Ticket::partnerTxnId;
}

void Ticket::setPartnerTxnId(long long partnerTxnId){
	Ticket::partnerTxnId= partnerTxnId;
}

ErrorList Ticket::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList = Activity::deserialize(object);
	Local<Value> value = object->Get(String::NewFromUtf8(isolate, "expiryTimeInMilliSeconds"));
	if (Validator::NotNullNumber(value, "expiryTimeInMilliSeconds", "Invalid time in milli seconds", &errorList)) {
		this->setExpiryTimeInMilliSeconds(object->Get(String::NewFromUtf8(isolate, "expiryTimeInMilliSeconds"))->IntegerValue());
	}

	value = object->Get(String::NewFromUtf8(isolate, "partnerTransactionId"));
	if (Validator::NotNullNumber(value, "partnerTransactionId", "Invalid partner transaction id", &errorList)) {
		this->setPartnerTxnId(object->Get(String::NewFromUtf8(isolate, "partnerTransactionId"))->IntegerValue());
	}
	return errorList;
}

Local<Object> Ticket::serialize() {
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> object = Activity::serialize();
	object->Set(String::NewFromUtf8(isolate, "expiryTimeInMilliSeconds"), Integer::New(isolate, getExpiryTimeInMilliSeconds()));
	object->Set(String::NewFromUtf8(isolate, "partnerTransactionId"), Integer::New(isolate, getPartnerTxnId()));
	return object;
}