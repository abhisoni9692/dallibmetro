#include "model/TollLog.h"
#include "model/utils/ConversionUtil.h"
#include "model/utils/EnumUtil.h"
#include "Validator.h"

using v8::Isolate;
using v8::String;
using v8::Value;
using v8::Integer;
using v8::Number;

TollLog::TollLog(){

}


long long TollLog::getExpiryDateTime(){
	return TollLog::expiryDateTime;
}

void TollLog::setExpiryDateTime(long long expiryDateTime){
	TollLog::expiryDateTime = expiryDateTime;
}

string TollLog::getBranchId(){
	return TollLog::branchId;
}

void TollLog::setBranchId(string branchId){
	TollLog::branchId = std::move(branchId);
}

long long TollLog::getInDateTime(){
	return TollLog::inDateTime;
}

void TollLog::setInDateTime(long long inDateTime){
	TollLog::inDateTime = inDateTime;
}


ErrorList TollLog::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList;

	return errorList;
}

Local<Object> TollLog::serialize() {
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> object = Object::New(isolate);
	object->Set(String::NewFromUtf8(isolate, "expiryDateTime"), Number::New(isolate, getExpiryDateTime()));
	object->Set(String::NewFromUtf8(isolate, "branchId"), String::NewFromUtf8(isolate, ConversionUtil::toCharArray(getBranchId())));
	object->Set(String::NewFromUtf8(isolate, "inDateTime"), Number::New(isolate, getInDateTime()));
	return object;
}