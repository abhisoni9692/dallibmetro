#include "model/Transaction.h"
#include "model/utils/ConversionUtil.h"
#include "model/utils/EnumUtil.h"
#include "Validator.h"

using v8::Isolate;
using v8::String;
using v8::Value;
using v8::Integer;
using v8::Number;

Transaction::Transaction(){

}

ServiceType Transaction::getServiceType(){
	return Transaction::serviceType;
}

void Transaction::setServiceType(ServiceType serviceType){
	 Transaction::serviceType =serviceType;
}

TransactionType Transaction::getType(){
	return Transaction::type;
}

void Transaction::setType(TransactionType type){
	Transaction::type = type;
}

long long Transaction::getTimeInMilliSeconds(){
	return Transaction::timeInMilliSeconds;
}

void Transaction::setTimeInMilliSeconds(long long timeInMilliSeconds){
	Transaction::timeInMilliSeconds = timeInMilliSeconds;
}

long Transaction::getAmount(){
	return Transaction::amount;
}

void Transaction::setAmount(long amount){
	Transaction::amount = amount;
}

long Transaction::getRunningBalance(){
	return Transaction::runningBalance;
}

void Transaction::setRunningBalance(long runningBalance){
	Transaction::runningBalance = runningBalance;
}

string Transaction::getPosTerminalId(){
	return Transaction::posTerminalId;
}

void Transaction::setPosTerminalId(string posTerminalId){
	 Transaction::posTerminalId=std::move(posTerminalId);
}

ErrorList Transaction::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList;
	Local<Value>value = object->Get(String::NewFromUtf8(isolate, "posTerminalId"));
	if (Validator::NotNullString(value, "posTerminalId", "Invalid pos terminal id", &errorList)) {
		this->setPosTerminalId(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "posTerminalId"))->ToString()));
	}

	return errorList;
}

Local<Object> Transaction::serialize() {
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> object = Object::New(isolate);
//	object->Set(String::NewFromUtf8(isolate, "serviceType"), String::NewFromUtf8(isolate, (const char *)EnumUtil::ToString(getServiceType())));
	object->Set(String::NewFromUtf8(isolate, "transactionType"), String::NewFromUtf8(isolate, (const char *)EnumUtil::ToString(getType())));
	object->Set(String::NewFromUtf8(isolate, "timeInMilliSeconds"), Number::New(isolate, getTimeInMilliSeconds()));
	object->Set(String::NewFromUtf8(isolate, "amount"), Integer::New(isolate, getAmount()));
//	object->Set(String::NewFromUtf8(isolate, "runningBalance"), Integer::New(isolate, getRunningBalance()));
	object->Set(String::NewFromUtf8(isolate, "posTerminalId"), String::NewFromUtf8(isolate, ConversionUtil::toCharArray(getPosTerminalId())));
	return object;
}