#include <iostream>
#include "model/TransactionsCriteria.h"
#include "../ErrorList.h"
#include "node.h"
#include "Validator.h"
#include "model/utils/ConversionUtil.h"

using v8::Isolate;
using v8::String;
using v8::Value;
using v8::Integer;
using v8::Local;
using v8::Object;

TransactionsCriteria::TransactionsCriteria()
{
}
string TransactionsCriteria::getSourceBranchID(){
	return this->posTerminalId;
}
void TransactionsCriteria::setSourceBranchID(string posTerminalId) {
	this->posTerminalId = posTerminalId;
}

long long TransactionsCriteria::getTimeInMilliSeconds(){
	return this->timeInMilliSeconds;
}

void TransactionsCriteria::setTimeInMilliSeconds(long long timeInMilliSeconds){
	this->timeInMilliSeconds = timeInMilliSeconds;
}

ErrorList TransactionsCriteria::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList;
	Local<Value>value = object->Get(String::NewFromUtf8(isolate, "sourceBranchId"));
	if (Validator::NotNullString(value, "sourceBranchId", "Invalid Source BranchID id", &errorList)) {
		this->setSourceBranchID(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "sourceBranchId"))->ToString()));
	}

	value = object->Get(String::NewFromUtf8(isolate, "timeInMilliSeconds"));
	if (Validator::NotNullNumber(value, "timeInMilliSeconds", "Invalid time in milli seconds", &errorList)) {
		this->setTimeInMilliSeconds(object->Get(String::NewFromUtf8(isolate, "timeInMilliSeconds"))->IntegerValue());
	}
	return errorList;
}
Local<Object> TransactionsCriteria::serialize() {
	Local<Object> object;
	return object;
}