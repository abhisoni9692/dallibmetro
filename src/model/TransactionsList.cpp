#include <iostream>
#include <list>
#include <iterator>
#include <algorithm>
#include "model/Transaction.h"
#include "model/TransactionsList.h"
#include "../ErrorList.h"
#include "node.h"
#include "Validator.h"

using v8::Isolate;
using v8::String;
using v8::Value;
using v8::Integer;

TransactionsList::TransactionsList()
{
	
}

vector<Transaction> TransactionsList::getTransactions()
{
	return this->transactions;
}

void TransactionsList::setTransactions(vector<Transaction> transactions)
{
	this->transactions = transactions;
}

ErrorList TransactionsList::deserialize(Local<Object> object)
{
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList;
	/*Local<Value>value = object->Get(String::NewFromUtf8(isolate, "posTerminalId"));
	if (Validator::NotNullString(value, "posTerminalId", "Invalid pos terminal id", &errorList)) {
		this->setPosTerminalId(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "posTerminalId"))->ToString()));
	}*/

	return errorList;
}

Local<Object> TransactionsList::serialize()
{
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> object = Object::New(isolate);
	Local<Object> transactionsObject = Object::New(isolate);
	for(int i = 0 ; i != transactions.size(); i++) {
		transactionsObject->Set(i,transactions[i].serialize());

	}
	object->Set(String::NewFromUtf8(isolate, "transactions"), transactionsObject);
	return object;
}
