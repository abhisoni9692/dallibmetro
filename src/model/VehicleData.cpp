#include "model/VehicleData.h"
#include "model/utils/ConversionUtil.h"
#include "model/utils/EnumUtil.h"
#include "model/enum/VehicleType.h"
#include "Validator.h"

using v8::Isolate;
using v8::String;
using v8::Value;
using v8::Integer;

VehicleType VehicleData::getVehicleType(){
	return VehicleData::vehicleType;
}

void VehicleData::setVehicleType(VehicleType vehicleType){
	this->vehicleType = vehicleType;
}

string VehicleData::getVehicleNumber(){
	return VehicleData::vehicleNumber;
}

void VehicleData::setVehicleNumber(string vehicleNumber){
	this->vehicleNumber = std::move(vehicleNumber);
}

long long VehicleData::getTimeInMilliSeconds(){
	return VehicleData::timeInMilliSeconds;
}

void VehicleData::setTimeInMilliSeconds(long long timeInMilliSeconds){
	this->timeInMilliSeconds = timeInMilliSeconds;
}

ErrorList VehicleData::deserialize(Local<Object> object) {
	Isolate* isolate = Isolate::GetCurrent();
	ErrorList errorList;

	Local<Value> value = object->Get(String::NewFromUtf8(isolate, "vehicleType"));
	if(Validator::NotNullString(value, "vehicleType", "Invalid vehicleType", &errorList)) {
		this->setVehicleType(EnumUtil::getVehicleType(ConversionUtil::toCharArray(object->Get(String::NewFromUtf8(isolate, "vehicleType"))->ToString())));
	}

	value = object->Get(String::NewFromUtf8(isolate, "vehicleNumber"));
	if(Validator::NotNullString(value, "vehicleNumber", "Invalid vehicleNumber", &errorList)) {
		this->setVehicleNumber(ConversionUtil::toStdString(object->Get(String::NewFromUtf8(isolate, "vehicleNumber"))->ToString()));
	}

	value = object->Get(String::NewFromUtf8(isolate, "timeInMilliSeconds"));
	if (Validator::NotNullNumber(value, "timeInMilliSeconds", "Invalid time in milli seconds", &errorList)) {
		this->setTimeInMilliSeconds(object->Get(String::NewFromUtf8(isolate, "timeInMilliSeconds"))->IntegerValue());
	}
	return errorList;
}

Local<Object> VehicleData::serialize() {
	Isolate* isolate = Isolate::GetCurrent();
	Local<Object> object = Object::New(isolate);
	object->Set(String::NewFromUtf8(isolate, "vehicleType"), String::NewFromUtf8(isolate, (const char *)EnumUtil::ToString(getVehicleType())));
	object->Set(String::NewFromUtf8(isolate, "vehicleNumber"), String::NewFromUtf8(isolate, ConversionUtil::toCharArray(getVehicleNumber())));
	object->Set(String::NewFromUtf8(isolate, "timeInMilliSeconds"), Integer::New(isolate, getTimeInMilliSeconds()));
	return object;
}