#include "../include/model/utils/ConversionUtil.h"
#include "model/enum/ServiceType.h"
#include "model/enum/LimitPeriodicity.h"
#include <node.h>
#include <iostream>
#include <string>

using v8::Local;
using v8::String;
using v8::Isolate;

using namespace std;

char* ConversionUtil::toCharArray(Local<String> v8String) {
	char* ca = ConversionUtil::toCharArray(ConversionUtil::toStdString(v8String));
    // cout<<"ca:" <<ca <<"\n";
	return ca;
}

unsigned char* ConversionUtil::toUnsignedCharArray(Local<String> v8String) {
	char* ca = ConversionUtil::toCharArray(v8String);
	return (unsigned char*)ca;
}

char* ConversionUtil::toCharArray(string str) {
	// char* ca = (char *)str.c_str();
	char* ca = new char[str.size() + 1];
	std::copy(str.begin(), str.end(), ca);
	ca[str.size()] = '\0';
	return ca;
}

unsigned char* ConversionUtil::toUnsignedCharArray(string str) {
	return (unsigned char*)ConversionUtil::toCharArray(str);
}

string ConversionUtil::toStdString(Local<String> v8String) {
	string str(*v8::String::Utf8Value(v8String));
	return str;
}

char ConversionUtil::toChar(Local<String> v8String) {
	string str = ConversionUtil::toStdString(v8String);
	if(str.length() >0 ) {
		return str.at(0);
	}
	return 0;
}

Local<String> ConversionUtil::toV8String(char ch) {
	char *pChar = &ch;
	return String::NewFromUtf8(Isolate::GetCurrent(), (const char*)pChar);
}

ServiceType ConversionUtil::toServiceType(int service)
{
	printf("return reached");
	ServiceType serviceType = (ServiceType)service;
	printf("%d\n",service );
	return serviceType;
}

LimitPeriodicity ConversionUtil::toLimitPeriodicity(int limit)
{
	LimitPeriodicity limitPeriodicity = (LimitPeriodicity)limit;
	return limitPeriodicity;
}