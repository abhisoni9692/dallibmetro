#include "../include/model/utils/EnumUtil.h"
#include "../include/model/enum/Action.h"
#include "../include/model/enum/CardStatusEnum.h"
#include "../include/model/enum/LimitPeriodicity.h"
#include "../include/model/enum/PassType.h"
#include "../include/model/enum/ReaderType.h"
#include "../include/model/enum/ServiceType.h"
#include "../include/model/enum/TransactionType.h"
#include "../include/model/enum/Gender.h"
#include "../include/model/enum/VehicleType.h"
#include <iostream>

using namespace std;

char* EnumUtil::ToString(Action action) {
    switch (action) {
        case Action::PAY :        return "PAY";
        case Action::RECHARGE :   return "RECHARGE";
        case Action::FINE :       return "FINE";
        default: return "UNKNOWN";
    }
}

Action EnumUtil::getAction(const char* action) {
    Action actionEnum = Action::UNKNOWN;
    if( strcmp(action,"PAY") == 0) 
        actionEnum = Action::PAY;
    else if( strcmp(action,"RECHARGE") == 0) 
        actionEnum = Action::RECHARGE;
    else if( strcmp(action,"FINE") == 0) 
        actionEnum = Action::FINE;
    return actionEnum;
}

char* EnumUtil::ToString(CardStatusEnum cardStatus) {
    switch (cardStatus) {
        case CardStatusEnum::personalize :  return "PERSONALIZE";
        case CardStatusEnum::allocate :     return "ALLOCATE";
        case CardStatusEnum::activate :     return "ACTIVATE";
        case CardStatusEnum::block :        return "BLOCK";
        case CardStatusEnum::UNKNOWN :      return "UNKNOWN";
        default: return "UNKNOWN";
    }
}

CardStatusEnum EnumUtil::getCardStatusEnum(const char* cardStatus) {
    CardStatusEnum cardStatusEnum = CardStatusEnum::UNKNOWN;
    if( strcmp(cardStatus,"PERSONALIZE") == 0) 
        cardStatusEnum = CardStatusEnum::personalize;
    else if( strcmp(cardStatus,"ALLOCATE") == 0) 
        cardStatusEnum = CardStatusEnum::allocate;
    else if( strcmp(cardStatus,"ACTIVATE") == 0) 
        cardStatusEnum = CardStatusEnum::activate;
    else if( strcmp(cardStatus,"BLOCK") == 0) 
        cardStatusEnum = CardStatusEnum::block;
    return cardStatusEnum;
}

char* EnumUtil::ToString(LimitPeriodicity limitPeriodicity) {
	switch (limitPeriodicity) {
        case LimitPeriodicity::HOURLY :         return "HOURLY";
        case LimitPeriodicity::DAILY :          return "DAILY";
        case LimitPeriodicity::WEEKLY :         return "WEEKLY";
        case LimitPeriodicity::MONTHLY :        return "MONTHLY";
        case LimitPeriodicity::QUARTLY :        return "QUARTLY";
        case LimitPeriodicity::HALFYEARLY :     return "HALFYEARLY";
        case LimitPeriodicity::YEARLY :         return "YEARLY";
        case LimitPeriodicity::UNKNOWN_PERIODICITY:         return "UNKNOWN";
        default: return "UNKNOWN";
    }
}

LimitPeriodicity EnumUtil::getLimitPeriodicity(const char* limitPeriodicity) {
    LimitPeriodicity limitPeriodicityEnum = LimitPeriodicity::UNKNOWN_PERIODICITY;
    if( strcmp(limitPeriodicity,"HOURLY") == 0) 
        limitPeriodicityEnum = LimitPeriodicity::HOURLY;
    else if( strcmp(limitPeriodicity,"DAILY") == 0) 
        limitPeriodicityEnum = LimitPeriodicity::DAILY;
    else if( strcmp(limitPeriodicity,"WEEKLY") == 0) 
        limitPeriodicityEnum = LimitPeriodicity::WEEKLY;
    else if( strcmp(limitPeriodicity,"MONTHLY") == 0) 
        limitPeriodicityEnum = LimitPeriodicity::MONTHLY;
    else if( strcmp(limitPeriodicity,"QUARTLY") == 0) 
        limitPeriodicityEnum = LimitPeriodicity::QUARTLY;
    else if( strcmp(limitPeriodicity,"HALFYEARLY") == 0) 
        limitPeriodicityEnum = LimitPeriodicity::HALFYEARLY;
    else if( strcmp(limitPeriodicity,"YEARLY") == 0) 
        limitPeriodicityEnum = LimitPeriodicity::YEARLY;
    return limitPeriodicityEnum;
}
    
char* EnumUtil::ToString(PassType passType) {
    switch (passType) {
        case PassType::DAILY :      return "DAILY";
        case PassType::MONTHLY :    return "MONTHLY";
        case PassType::LOCAL :      return "LOCAL";
        case PassType::UNKNOWN:     return "UNKNOWN";
        default: return "UNKNOWN";
    }
}

PassType EnumUtil::getPassType(const char* passType) {
    PassType passTypeEnum = PassType::UNKNOWN;
    if( strcmp(passType,"DAILY") == 0) 
        passTypeEnum = PassType::DAILY;
    else if( strcmp(passType,"MONTHLY") == 0) 
        passTypeEnum = PassType::MONTHLY;
    else if( strcmp(passType,"LOCAL") == 0) 
        passTypeEnum = PassType::LOCAL;
    return passTypeEnum;
}

char* EnumUtil::ToString(ReaderType readerType) {
    switch (readerType) {
        case ReaderType::ALL :              return "ALL";
        case ReaderType::SCSPRO_READER :    return "SCSPRO_READER";
        case ReaderType::SONY_READER :      return "SONY_READER";
        case ReaderType::BUAROH_READER :    return "BUAROH_READER";
        case ReaderType::DUALI_READER :     return "DUALI_READER";
        default: return nullptr;
    }
}

ReaderType EnumUtil::getReaderType(const char* readerType) {
    ReaderType readerTypeEnum = ReaderType::UNKNOWN;
    if( strcmp(readerType,"ALL") == 0) 
        readerTypeEnum = ReaderType::ALL;
    else if( strcmp(readerType,"SCSPRO_READER") == 0) 
        readerTypeEnum = ReaderType::SCSPRO_READER;
    else if( strcmp(readerType,"SONY_READER") == 0) 
        readerTypeEnum = ReaderType::SONY_READER;
    else if( strcmp(readerType,"BUAROH_READER") == 0) 
        readerTypeEnum = ReaderType::BUAROH_READER;
    else if( strcmp(readerType,"DUALI_READER") == 0) 
        readerTypeEnum = ReaderType::DUALI_READER;
    return readerTypeEnum;
}
    
char* EnumUtil::ToString(ServiceType serviceType) {
    switch (serviceType) {
        case ServiceType::TOLL :        return "TOLL";
        case ServiceType::BUS :         return "BUS";
        case ServiceType::METRO :       return "METRO";
        case ServiceType::RAIL :        return "RAIL";
        case ServiceType::PARKING :     return "PARKING";
        default: return nullptr;
    }
}

ServiceType EnumUtil::getServiceType(const char* serviceType) {
    ServiceType serviceTypeEnum = ServiceType::UNKNOWN;
    if( strcmp(serviceType,"TOLL") == 0) 
        serviceTypeEnum = ServiceType::TOLL;
    else if( strcmp(serviceType,"BUS") == 0) 
        serviceTypeEnum = ServiceType::BUS;
    else if( strcmp(serviceType,"METRO") == 0) 
        serviceTypeEnum = ServiceType::METRO;
    else if( strcmp(serviceType,"RAIL") == 0) 
        serviceTypeEnum = ServiceType::RAIL;
    else if( strcmp(serviceType,"PARKING") == 0) 
        serviceTypeEnum = ServiceType::PARKING;
    return serviceTypeEnum;
}

char* EnumUtil::ToString(TransactionType transactionType) {
    switch (transactionType) {
        case TransactionType::UNKNOWN :    return "UNKNOWN";
        case TransactionType::PAY :        return "PAY";
        case TransactionType::RECHARGE :   return "RECHARGE";
        case TransactionType::FINE :       return "FINE";
        default: return "UNKNOWN";
    }
}

TransactionType EnumUtil::getTransactionType(const char* transactionType) {
    TransactionType transactionTypeEnum = TransactionType::UNKNOWN;
    if( strcmp(transactionType,"PAY") == 0) 
        transactionTypeEnum = TransactionType::PAY;
    else if( strcmp(transactionType,"RECHARGE") == 0) 
        transactionTypeEnum = TransactionType::RECHARGE;
    else if( strcmp(transactionType,"FINE") == 0) 
        transactionTypeEnum = TransactionType::FINE;
    else if( strcmp(transactionType,"UNKNOWN") == 0) 
        transactionTypeEnum = TransactionType::UNKNOWN;
    return transactionTypeEnum;
}

char* EnumUtil::ToString(Gender gender){
     switch (gender) {
            case Gender::MALE :    return "MALE";
            case Gender::FEMALE :  return "FEMALE";
            default: return nullptr;
        }
}

Gender EnumUtil::getGender(const char* gender){
 Gender genderEnum = Gender::UNKNOWN;
    if( strcmp(gender,"MALE") == 0) 
        genderEnum = Gender::MALE;
    else if( strcmp(gender,"FEMALE") == 0) 
        genderEnum = Gender::FEMALE;
    return genderEnum;
}

char* EnumUtil::ToString(VehicleType vehicleType){
     switch (vehicleType) {
            case VehicleType::UNKNOWN:    return "UNKNOWN";
            case VehicleType::COMMERCIAL_CAR:    return "COMMERCIAL_CAR";
            case VehicleType::NON_COMMERCIAL_CAR:  return "NON_COMMERCIAL_CAR";
            case VehicleType::GOVT_VIP_CAR: return "GOVT_VIP_CAR";
            case VehicleType::COMMERCIAL_LCV: return "COMMERCIAL_LCV";
            case VehicleType::NON_COMMERCIAL_LCV: return "NON_COMMERCIAL_LCV";
            case VehicleType::GOVT_VIP_LCV: return "GOVT_VIP_LCV";
            case VehicleType::COMMERCIAL_BUS_TRUCK: return "COMMERCIAL_BUS_TRUCK";
            case VehicleType::NON_COMMERCIAL_BUS_TRUCK: return "NON_COMMERCIAL_BUS_TRUCK";
            case VehicleType::GOVT_VIP_BUS_TRUCK: return "GOVT_VIP_BUS_TRUCK";
            case VehicleType::SCHOOL_BUS_TRUCK: return "SCHOOL_BUS_TRUCK";
            case VehicleType::COMMERCIAL_UPTO_3_AXLE: return "COMMERCIAL_UPTO_3_AXLE";
            case VehicleType::NON_COMMERCIAL_UPTO_3_AXLE: return "NON_COMMERCIAL_UPTO_3_AXLE";
            case VehicleType::GOVT_VIP_UPTO_3_AXLE: return "GOVT_VIP_UPTO_3_AXLE";
            case VehicleType::COMMERCIAL_4_TO_6_AXLE: return "COMMERCIAL_4_TO_6_AXLE";
            case VehicleType::NON_COMMERCIAL_4_TO_6_AXLE: return "NON_COMMERCIAL_4_TO_6_AXLE";
            case VehicleType::GOVT_VIP_4_TO_6_AXLE: return "GOVT_VIP_4_TO_6_AXLE";
            case VehicleType::COMMERCIAL_HCM_EME: return "COMMERCIAL_HCM_EME";
            case VehicleType::NON_COMMERCIAL_HCM_EME: return "NON_COMMERCIAL_HCM_EME";
            case VehicleType::GOVT_VIP_HCM_EME: return "GOVT_VIP_HCM_EME";
            case VehicleType::COMMERCIAL_7_OR_MORE_AXLE: return "COMMERCIAL_7_OR_MORE_AXLE";
            case VehicleType::NON_COMMERCIAL_7_OR_MORE_AXLE: return "NON_COMMERCIAL_7_OR_MORE_AXLE";
            case VehicleType::GOVERNMENT_7_OR_MORE_AXLE: return "GOVERNMENT_7_OR_MORE_AXLE";
            default: return "UNKNOWN";
        }
}

VehicleType EnumUtil::getVehicleType(const char* vehicleType){
    VehicleType vehicleTypeEnum = VehicleType::UNKNOWN;
    if( strcmp(vehicleType,"UNKNOWN") == 0) 
        vehicleTypeEnum = VehicleType::UNKNOWN;
    else if( strcmp(vehicleType,"COMMERCIAL_CAR") == 0) 
        vehicleTypeEnum = VehicleType::COMMERCIAL_CAR;
    else if( strcmp(vehicleType,"NON_COMMERCIAL_CAR") == 0) 
        vehicleTypeEnum = VehicleType::NON_COMMERCIAL_CAR;
    else if( strcmp(vehicleType,"GOVT_VIP_CAR") == 0) 
        vehicleTypeEnum = VehicleType::GOVT_VIP_CAR;
    else if( strcmp(vehicleType,"COMMERCIAL_LCV") == 0) 
        vehicleTypeEnum = VehicleType::COMMERCIAL_LCV;
    else if( strcmp(vehicleType,"NON_COMMERCIAL_LCV") == 0) 
        vehicleTypeEnum = VehicleType::NON_COMMERCIAL_LCV;
    else if( strcmp(vehicleType,"GOVT_VIP_LCV") == 0) 
        vehicleTypeEnum = VehicleType::GOVT_VIP_LCV;
    else if( strcmp(vehicleType,"COMMERCIAL_BUS_TRUCK") == 0) 
        vehicleTypeEnum = VehicleType::COMMERCIAL_BUS_TRUCK;
    else if( strcmp(vehicleType,"NON_COMMERCIAL_BUS_TRUCK") == 0) 
        vehicleTypeEnum = VehicleType::NON_COMMERCIAL_BUS_TRUCK;
    else if( strcmp(vehicleType,"GOVT_VIP_BUS_TRUCK") == 0) 
        vehicleTypeEnum = VehicleType::GOVT_VIP_BUS_TRUCK;
    else if( strcmp(vehicleType,"SCHOOL_BUS_TRUCK") == 0) 
        vehicleTypeEnum = VehicleType::SCHOOL_BUS_TRUCK;
    else if( strcmp(vehicleType,"COMMERCIAL_UPTO_3_AXLE") == 0) 
        vehicleTypeEnum = VehicleType::COMMERCIAL_UPTO_3_AXLE;
    else if( strcmp(vehicleType,"NON_COMMERCIAL_UPTO_3_AXLE") == 0) 
        vehicleTypeEnum = VehicleType::NON_COMMERCIAL_UPTO_3_AXLE;
    else if( strcmp(vehicleType,"GOVT_VIP_UPTO_3_AXLE") == 0) 
        vehicleTypeEnum = VehicleType::GOVT_VIP_UPTO_3_AXLE;
    else if( strcmp(vehicleType,"COMMERCIAL_4_TO_6_AXLE") == 0) 
        vehicleTypeEnum = VehicleType::COMMERCIAL_4_TO_6_AXLE;
    else if( strcmp(vehicleType,"NON_COMMERCIAL_4_TO_6_AXLE") == 0) 
        vehicleTypeEnum = VehicleType::NON_COMMERCIAL_4_TO_6_AXLE;
    else if( strcmp(vehicleType,"GOVT_VIP_4_TO_6_AXLE") == 0) 
        vehicleTypeEnum = VehicleType::GOVT_VIP_4_TO_6_AXLE;
    else if( strcmp(vehicleType,"COMMERCIAL_HCM_EME") == 0) 
        vehicleTypeEnum = VehicleType::COMMERCIAL_HCM_EME;
    else if( strcmp(vehicleType,"NON_COMMERCIAL_HCM_EME") == 0) 
        vehicleTypeEnum = VehicleType::NON_COMMERCIAL_HCM_EME;
    else if( strcmp(vehicleType,"GOVT_VIP_HCM_EME") == 0) 
        vehicleTypeEnum = VehicleType::GOVT_VIP_HCM_EME;
    else if( strcmp(vehicleType,"COMMERCIAL_7_OR_MORE_AXLE") == 0) 
        vehicleTypeEnum = VehicleType::COMMERCIAL_7_OR_MORE_AXLE;
    else if( strcmp(vehicleType,"NON_COMMERCIAL_7_OR_MORE_AXLE") == 0) 
        vehicleTypeEnum = VehicleType::NON_COMMERCIAL_7_OR_MORE_AXLE;
    else if( strcmp(vehicleType,"GOVERNMENT_7_OR_MORE_AXLE") == 0) 
        vehicleTypeEnum = VehicleType::GOVERNMENT_7_OR_MORE_AXLE;
    return vehicleTypeEnum;
}