#include "reader/BuarohReader.h"

#pragma comment(lib, "winscard.lib")
#pragma comment(lib, "setupapi.lib")

//#define UNICODE

#include <winscard.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <stdbool.h>
#include "reader/Reader.h"
#include "sam/SAM.h"
#include "card/Card.h"
#include "../include/Util.h"
#include "model/enum/ReaderType.h"
#include "model/enum/TransactionType.h"
#include "model/enum/Action.h"
#include "model/constants/ErrorConstants.h"
#include "model/GenericData.h"
#include "model/Pass.h"
#include "model/PassCriteria.h"
#include "enum/VehicleType.h"
#include "model/TransactionsList.h"
#include "model/TransactionsCriteria.h"
#include "model/TollLog.h"
#include "model/ParkingService.h"
#include "enum/CardStatusEnum.h"
using namespace std;


SAM* BuarohReader::samRefSlot1 = NULL;
SAM* BuarohReader::samRefSlot2 = NULL;

struct LOG{
	int transactionType;
	int amount;
	long long timeinMills;
	long long terminalId;
};

struct PASS_LOG{
	long long branchId;
	long long inDateTime;
	long long expiryDateTime;
};



BuarohReader::BuarohReader() {
	type = ReaderType::BUAROH_READER;
}

long BuarohReader::initializeReader(int readerIndex) {

	vector<wstring> vPortNames;
	vector<HANDLE> vPorts;

	TCHAR szPort[32];
	DCB dcb;
	BOOL fSuccess = false;
	size_t j = 0;

		//vPortNames.size()
	if (FindBaoruhDevice(vPortNames)) {

		if(readerIndex == 1 && vPortNames.size() == 2){
			j = 1;
		}
		for (size_t i = j; i < j+1; i++) {

			while(!fSuccess){
				_sntprintf_s(szPort, sizeof(szPort), _T("\\\\.\\%s"), vPortNames[i].c_str());

			/* Try to open the port */
			hCom = CreateFile(szPort, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
			if (hCom != INVALID_HANDLE_VALUE) {
				//vPorts.push_back(hCom);
				_tprintf(_T("%s\n"), vPortNames[i].c_str());
			}

			dcb = { 0 };
			dcb.DCBlength = sizeof(dcb);

			fSuccess = GetCommState(hCom, &dcb);

			if (!fSuccess)
			{
				// Handle the error.
				printf("GetCommState failed with error %d.\n", GetLastError());
				CloseHandle(hCom);
				//return APP_ERROR;
			}

			// Fill in DCB: 57,600 bps, 8 data bits, no parity, and 1 stop bit.
			//dcb = { 0 };
			//dcb.DCBlength = sizeof(dcb);

			dcb.BaudRate = CBR_115200;     // set the baud rate
			dcb.ByteSize = 8;             // data size, xmit, and rcv
			dcb.Parity = NOPARITY;        // no parity bit
			dcb.StopBits = ONESTOPBIT;    // one stop bit

			fSuccess = SetCommState(hCom, &dcb);

			if (!fSuccess)
			{
				// Handle the error.
				printf("SetCommState failed with error %d.\n", GetLastError());
				CloseHandle(hCom);
				//return APP_ERROR;
			}

			}
			/* Form the Raw device name */
			
			vPorts.push_back(hCom);

			printf("Serial port successfully reconfigured.\n");
		}
	}

	if (vPorts.empty()) {
		printf("No Baoruh device found\n");
		return APP_ERROR;
	}
	else {
		printf("%u Baoruh devices found\n", vPorts.size());
	}
	return APP_SUCCESS;
}

long BuarohReader::disconnectReader() {
	return APP_ERROR;
}

long BuarohReader::sendToSAM(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int SAMType) {
	return APP_ERROR;
}

long BuarohReader::sendToCard(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int cardType) {
	return APP_ERROR;
}

long BuarohReader::detectCard(int *cardType) {

	unsigned char bLRC = 0;
	int nDataLen = 0;
	unsigned char apduBuffer[1040];
	unsigned char tmpBuf[262];
	unsigned long tmpBufLen;
	unsigned char sw1, sw2;
	// ActivateFelica, 11 01 P1=03 P2=00 Lc=03 +SysCode(2)+TimeSlots(1)
	// F0 00 08 11 01 03 00 03 92 CF 00 B5

	printf("\nTap Card\n");
	printf("<Press ESC Key to cancel>\n");
	cancelDetection=false;
	while(1){

		// Hit Esc to cancel operation
		if ((0 != kbhit()) && (SMPL_ESC_KEY == getch()))
		{
			printf("  -> Canceled\n");
			return APP_CANCEL;
		}

		if(cancelDetection == true){
			cancelDetection = false;
			return APP_ERROR;
		}

		bLRC = 0;
		nDataLen = 0;
		bLRC ^= apduBuffer[NAD_ADDR] = 0xF0;
		bLRC ^= apduBuffer[PCB_ADDR] = APDU_PCBTypeA;
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x11;    // CLA
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x01;    // INS
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x03;    // P1
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x00;	  // P2
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x03;    // Lc 0
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x92;    // System code 0
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0xcf;    // System code 1
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x00;    // time slots
		bLRC ^= apduBuffer[LEN_ADDR] = (unsigned char)(nDataLen & 0xFF);
		apduBuffer[DATA_BASE + nDataLen] = bLRC;



		WriteUsbData(hCom, apduBuffer, DATA_BASE + nDataLen + EDC_LEN);
		ReadUsbData(hCom, tmpBuf, &tmpBufLen, DATA_BASE + nDataLen + EDC_LEN, APDU_PCBTypeA);

		sw1 = tmpBuf[tmpBufLen - 3];

		sw2 = tmpBuf[tmpBufLen - 2];


		if ((sw1 == APDU_OK_SW1) && (sw2 == APDU_OK_SW2)) {
			printf("card found\n");
			break;
		}
	}

	PrintHexArray("\nUSB-->DAL: ", tmpBufLen, tmpBuf);

	*cardType = Card::unknownCard;
		// 3B 8F 80 01 80 4F 0C A0 00 00 03 06 11 00 3B 00 00 00 00 42
	unsigned char felicaATR[]  = { 0x3B,0x8F,0x80,0x01,0x80,0x4F,0x0C,0xA0,0x00,0x00,0x03,0x06,0x11,0x00,0x3B,0x00,0x00,0x00,0x00,0x42};
	unsigned char desfireATR[] = { 0x3B,0x89,0x80,0x01,0x80,0x5F,0x06,0x06,0x75,0x77,0x81,0x02,0x80,0xD6 };



	// TODO : check atr to differetiate cards
	Reader::cardTapped = this;
	*cardType = Card::FELICA_RCSA01_CARD;
	printf("felica found\n");

	return APP_SUCCESS;
}

//detectInstantCard
long BuarohReader::detectInstantCard(int *cardType) {

	unsigned char bLRC = 0;
	int nDataLen = 0, i= 0;
	unsigned char apduBuffer[1040];
	unsigned char tmpBuf[262];
	unsigned long tmpBufLen;
	unsigned char sw1, sw2;
	// ActivateFelica, 11 01 P1=03 P2=00 Lc=03 +SysCode(2)+TimeSlots(1)
	// F0 00 08 11 01 03 00 03 92 CF 00 B5

	printf("\nTap Card\n");
	printf("<Press ESC Key to cancel>\n");
	cancelDetection=false;
	for(i=0; i<=3; i++){

		// Hit Esc to cancel operation
		if ((0 != kbhit()) && (SMPL_ESC_KEY == getch()))
		{
			printf("  -> Canceled\n");
			return APP_CANCEL;
		}

		if(cancelDetection == true){
			cancelDetection = false;
			return APP_ERROR;
		}

		bLRC = 0;
		nDataLen = 0;
		bLRC ^= apduBuffer[NAD_ADDR] = 0xF0;
		bLRC ^= apduBuffer[PCB_ADDR] = APDU_PCBTypeA;
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x11;    // CLA
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x01;    // INS
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x03;    // P1
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x00;	  // P2
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x03;    // Lc 0
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x92;    // System code 0
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0xcf;    // System code 1
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x00;    // time slots
		bLRC ^= apduBuffer[LEN_ADDR] = (unsigned char)(nDataLen & 0xFF);
		apduBuffer[DATA_BASE + nDataLen] = bLRC;


		WriteUsbData(hCom, apduBuffer, DATA_BASE + nDataLen + EDC_LEN);
		ReadUsbData(hCom, tmpBuf, &tmpBufLen, DATA_BASE + nDataLen + EDC_LEN, APDU_PCBTypeA);

		sw1 = tmpBuf[tmpBufLen - 3];

		sw2 = tmpBuf[tmpBufLen - 2];


		if ((sw1 == APDU_OK_SW1) && (sw2 == APDU_OK_SW2)) {
			printf("card found\n");
			break;
		}
	}

	PrintHexArray("\nUSB-->DAL: ", tmpBufLen, tmpBuf);

	*cardType = Card::unknownCard;
		// 3B 8F 80 01 80 4F 0C A0 00 00 03 06 11 00 3B 00 00 00 00 42
	unsigned char felicaATR[]  = { 0x3B,0x8F,0x80,0x01,0x80,0x4F,0x0C,0xA0,0x00,0x00,0x03,0x06,0x11,0x00,0x3B,0x00,0x00,0x00,0x00,0x42};
	unsigned char desfireATR[] = { 0x3B,0x89,0x80,0x01,0x80,0x5F,0x06,0x06,0x75,0x77,0x81,0x02,0x80,0xD6 };



	// TODO : check atr to differetiate cards
	Reader::cardTapped = this;
	*cardType = Card::FELICA_RCSA01_CARD;
	printf("felica found\n");

	return APP_SUCCESS;
}

long BuarohReader::DisconnectFeliCaCard(void){

	// turn RF off
	return APP_ERROR;
};

bool BuarohReader::isCardRemoved(){
	
	unsigned char bLRC = 0;
	int nDataLen = 0, retryCnt = 3;
	unsigned char apduBuffer[1040];
	unsigned char tmpBuf[262];
	unsigned long tmpBufLen;
	unsigned char sw1, sw2;

	printf("checking removal of card\n");
	
	while(1){
		bLRC = 0;
		nDataLen = 0;
		bLRC ^= apduBuffer[NAD_ADDR] = 0xF0;
		bLRC ^= apduBuffer[PCB_ADDR] = APDU_PCBTypeA;
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x11;    // CLA
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x01;    // INS
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x03;    // P1
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x00;	  // P2
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x03;    // Lc 0
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x92;    // System code 0
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0xcf;    // System code 1
		bLRC ^= apduBuffer[DATA_BASE + nDataLen++] = 0x00;    // time slots
		bLRC ^= apduBuffer[LEN_ADDR] = (unsigned char)(nDataLen & 0xFF);
		apduBuffer[DATA_BASE + nDataLen] = bLRC;

		WriteUsbData(hCom, apduBuffer, DATA_BASE + nDataLen + EDC_LEN);
		ReadUsbData(hCom, tmpBuf, &tmpBufLen, DATA_BASE + nDataLen + EDC_LEN, APDU_PCBTypeA);

		printf("\n\n Data to Send \n\n");
		for (int i = 0; i < tmpBufLen ; i++)
			printf("%02X ", tmpBuf[i]);

		sw1 = tmpBuf[tmpBufLen - 3];

		sw2 = tmpBuf[tmpBufLen - 2];


		if ((sw1 != APDU_OK_SW1) && (sw2 != APDU_OK_SW2)) {
			printf("card removed: %d \n", retryCnt);
			retryCnt--;
		}
		if(retryCnt == 0){
			break;
		}
	}
	return true;
}

bool BuarohReader::stopDetection(){
	printf("checking removal of card\n");
	cancelDetection = true;
	return true;
}

void BuarohReader::ReadUsbData(HANDLE hCom, unsigned char SerialBuffer[], unsigned long *ReadBufferLen, unsigned long length, int PCB_Type) {
	unsigned char TempChar; //Temporary character used for reading
	DWORD NoBytesRead, TotalBytesRead = 0;
	int i = 0;
	unsigned int buff_len = 5;
	unsigned char num[2];
	unsigned int buff_size;

	do {
		ReadFile(hCom,           //Handle of the Serial port
			&TempChar,       //Temporary character
			sizeof(TempChar),//Size of TempChar
			&NoBytesRead,    //Number of bytes read
			NULL);
		if (PCB_Type == 0) {
			buff_len = SerialBuffer[2] + 3 + 1;
		}
		else {
			num[0] = SerialBuffer[3];
			num[1] = SerialBuffer[2];

			buff_size = CharArrayToInt(num,2);
			buff_len = buff_size + 4 + 1;
		}
		*ReadBufferLen = TotalBytesRead;
			SerialBuffer[i] = TempChar;// Store Tempchar into buffer
			i++;
			TotalBytesRead++;

		} while (i < buff_len);

		*ReadBufferLen = TotalBytesRead;
}

void BuarohReader::WriteUsbData(HANDLE hCom, unsigned char SerialBuffer[], DWORD WriteBufferLen) {
	DWORD bytes_written, total_bytes_written = 0;
	if (!WriteFile(hCom, SerialBuffer, WriteBufferLen, &bytes_written, NULL))
	{
		printf("Error\n");
		CloseHandle(hCom);
	}
}

bool BuarohReader::FindBaoruhDevice(vector<wstring> &vPortNames) {
	HDEVINFO                         hDevInfo;
	SP_DEVICE_INTERFACE_DATA         DevIntfData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA DevIntfDetailData;
	SP_DEVINFO_DATA                  DevData;

	DWORD dwSize, dwType, dwMemberIdx;
	HKEY hKey;
	BYTE lpData[1024];

	hDevInfo = SetupDiGetClassDevs(
		&GUID_DEVINTERFACE_USB_DEVICE, NULL, 0, DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);

	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DevIntfData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		dwMemberIdx = 0;

		SetupDiEnumDeviceInterfaces(hDevInfo, NULL, &GUID_DEVINTERFACE_USB_DEVICE,
			dwMemberIdx, &DevIntfData);

		while (GetLastError() != ERROR_NO_MORE_ITEMS) {
			DevData.cbSize = sizeof(DevData);

			SetupDiGetDeviceInterfaceDetail(
				hDevInfo, &DevIntfData, NULL, 0, &dwSize, NULL);

			DevIntfDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwSize);
			DevIntfDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

			if (SetupDiGetDeviceInterfaceDetail(hDevInfo, &DevIntfData,
				DevIntfDetailData, dwSize, &dwSize, &DevData))
			{
				if (NULL != _tcsstr((TCHAR*)DevIntfDetailData->DevicePath, _T("vid_15a2")) ||
					NULL != _tcsstr((TCHAR*)DevIntfDetailData->DevicePath, _T("vid_1fc9")))
				{
					hKey = SetupDiOpenDevRegKey(
						hDevInfo,
						&DevData,
						DICS_FLAG_GLOBAL,
						0,
						DIREG_DEV,
						KEY_READ
					);

					dwType = REG_SZ;
					dwSize = sizeof(lpData);
					RegQueryValueEx(hKey, _T("PortName"), NULL, &dwType, lpData, &dwSize);
					RegCloseKey(hKey);

					if (NULL != _tcsstr((TCHAR*)lpData, _T("COM"))) {
						vPortNames.push_back(wstring((wchar_t*)lpData));
					}
				}
			}

			HeapFree(GetProcessHeap(), 0, DevIntfDetailData);

			SetupDiEnumDeviceInterfaces(
				hDevInfo, NULL, &GUID_DEVINTERFACE_USB_DEVICE, ++dwMemberIdx, &DevIntfData);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}

	return !vPortNames.empty();
}

void b_getLogs(unsigned char hexData[], struct LOG historyData[5]){
	LOG temp;
	unsigned char buffer[20],workBuffer[16];
	for(int i=0;i<5;i++){
		memcpy(workBuffer,&hexData[16*i],16);
		memcpy(buffer,&workBuffer[0],1);
		temp.transactionType= (int) CharArrayToInt(buffer,1);
		memcpy(buffer,&workBuffer[1],6);
		temp.timeinMills=(long long)CharArrayTolonglong(buffer,6);
		memcpy(buffer,&workBuffer[7],4);
		temp.amount=(int)CharArrayToInt(buffer,4);
		memcpy(buffer,&workBuffer[11],4);
		temp.terminalId=(long long)CharArrayTolonglong(buffer,4);
		historyData[i]=temp;
	}
}

void b_getPassLogs(unsigned char hexData[], struct PASS_LOG historyData[7]){
	PASS_LOG temp;
	unsigned char buffer[20],workBuffer[16];
	for(int i=0;i<7;i++){
		memcpy(workBuffer,&hexData[16*i],16);
		for(int j =0; j<16; j++)
		printf("%02X ", workBuffer[j]);
		printf("\n");
		memcpy(buffer,&workBuffer[0],4);
		temp.branchId=(long long)CharArrayTolonglong(buffer,4);
		memcpy(buffer,&workBuffer[4],4);
		temp.inDateTime=(long long)CharArrayTolonglong(buffer,4);
		memcpy(buffer,&workBuffer[8],4);
		temp.expiryDateTime=(long long)CharArrayTolonglong(buffer,4);
		historyData[i]=temp;
	}
}

void b_getPasses(unsigned char passes[], GenericData *genericData, vector<Pass> *matchedPassesData, int *passesDataLength){
	*passesDataLength=0;
	for(int i=0;i<96;i+=32){
		long long srcBranchId=CharArrayTolonglong(&passes[i],4);
		long long destBranchId=CharArrayTolonglong(&passes[i+4],4);

		printf("\n %s \n",std::to_string(srcBranchId).c_str());	
		printf("\n %llu \n",destBranchId);	

		char* endptr=NULL;
		if(srcBranchId ==  strtoll(genericData->getBranchId().c_str(),&endptr,10) || destBranchId== strtoll(genericData->getBranchId().c_str(),&endptr,10)){
			*passesDataLength+=1;
			Pass* pass = new Pass();
			unsigned char passLimitsPeriodicity,passLimitsMAxCount1[2],passLimitsMAxCount[2],passLimitsRemainingCount[2],passLimitsRemainingCount1[2],passLimitsStartDate[4],passLimitsStartDate1[4];
			int periodicityLimit[2];

			pass->setSrcBranchId(std::to_string(srcBranchId).c_str());
			pass->setDestBranchId(std::to_string(destBranchId).c_str());
			pass->setMaxTrips(CharArrayToInt(&passes[i+4+4],2));
			pass->setExpiryDate((long long)CharArrayToInt(&passes[i+4+4+2],4));
			printf("\n%02X pass type issss\n",passes[i+4+4+2+4] );
			pass->setPassType((PassType) passes[i+4+4+2+4]);

			passLimitsPeriodicity=passes[i+4+4+2+4+1];
			periodicityLimit[0] = passLimitsPeriodicity>>4; //Taking last 4 bits
			periodicityLimit[1] = passLimitsPeriodicity & 0x0f; //Taking first four bits
			memcpy(passLimitsMAxCount,&passes[i+4+4+2+4+1+1],2);
			memcpy(passLimitsRemainingCount,&passes[i+4+4+2+4+1+1+2],2);
			memcpy(passLimitsStartDate,&passes[i+4+4+2+4+1+1+2+2],4);
			int limitsMaxCount[2],limitsRemainingCount[2];
			long long limitsStartDate[2];
			limitsMaxCount[0] = CharArrayToInt(passLimitsMAxCount,2);
			limitsRemainingCount[0] = CharArrayToInt(passLimitsRemainingCount,2);
			limitsStartDate[0] = (long long)CharArrayToInt(passLimitsStartDate,4);
			vector<Periodicity> periodicitiyValue;
			int count = 2;
			if(periodicityLimit[1] == 0)
				count = 1;
			if(count > 1)
			{
				memcpy(passLimitsMAxCount1,&passes[i+4+4+2+4+1+1+2+2+4],2);
				memcpy(passLimitsRemainingCount1,&passes[i+4+4+2+4+1+1+2+2+2],2);
				memcpy(passLimitsStartDate1,&passes[i+4+4+2+4+1+1+2+2+2],4);
				limitsMaxCount[1] = CharArrayToInt(passLimitsMAxCount1,2);
				limitsRemainingCount[1] = CharArrayToInt(passLimitsRemainingCount1,2);
				limitsStartDate[1] = (long long)CharArrayToInt(passLimitsStartDate1,4);
			}
			for(int i=0;i<count;i++){
				Periodicity *periodicityOfPass = new Periodicity();
				periodicityOfPass->setLimitPeriodicity((LimitPeriodicity)periodicityLimit[i]);
				periodicityOfPass->setLimitsMaxCount(limitsMaxCount[i]);
				periodicityOfPass->setLimitsRemainingCount(limitsRemainingCount[i]);
				periodicityOfPass->setLimitsStartTime(limitsStartDate[i]);
				periodicitiyValue.push_back(*periodicityOfPass);
			}
			pass->setPeriodicity(periodicitiyValue);
			(*matchedPassesData).push_back(*pass);
		}
	}
}

ErrorConstants BuarohReader::GetBalance(Card *card, long *amount) {

	unsigned char bLRC = 0;
	int nDataLen = 0;
	unsigned char apduBuffer[1040];
	unsigned char tmpBuf[262];
	unsigned long tmpBufLen;

	bLRC ^= apduBuffer[NAD_ADDR] = 0x01;
	bLRC ^= apduBuffer[PCB_ADDR] = APDU_PCBTypeB;
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x50;
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x03;
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x00;
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x00;
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x00;
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x00;
	bLRC ^= apduBuffer[LEN_ADDR] = (unsigned char)(nDataLen & 0xFF);
	bLRC ^= apduBuffer[LEN2_ADDR] = (unsigned char)((nDataLen >> 8) & 0xFF);
	apduBuffer[DATA2_BASE + nDataLen] = bLRC;

	PrintHexArray("\n\nDAL-->USB: ", DATA2_BASE + nDataLen + EDC_LEN, apduBuffer);

	WriteUsbData(hCom, apduBuffer, DATA2_BASE + nDataLen + EDC_LEN);
	ReadUsbData(hCom, tmpBuf, &tmpBufLen, DATA2_BASE + nDataLen + EDC_LEN, APDU_PCBTypeB);
	
	PrintHexArray("\nUSB-->DAL: ", tmpBufLen, tmpBuf);

	unsigned char sw1 = tmpBuf[tmpBufLen - 3];

	unsigned char sw2 = tmpBuf[tmpBufLen - 2];


	if ((sw1 != APDU_OK_SW1) && (sw2 != APDU_OK_SW2)) {
		return ErrorConstants::GET_VALUE_FAILED;
	}

	*amount = CharArrayToIntLE(&tmpBuf[DATA2_BASE], 4);
	
	return ErrorConstants::NO__ERROR;
}

ErrorConstants BuarohReader::GetGenericData(Card *card, GenericData *genericData) {
	unsigned char bLRC = 0;
	int nDataLen = 0;
	unsigned char apduBuffer[1040], workBuf[6];
	unsigned char tmpBuf[512];
	unsigned long tmpBufLen;
	unsigned char sw1, sw2;
	int    balance, vehicleType, cardStatus;
	char name[48], vehicleNo[16];
	long long phoneNo, adhaarNo, cardNo;
	unsigned char serviceType = 0x00;

	//Toll Pass related variables
	unsigned  char logs[256],passes[256], passLogs[128];
	vector<TollLog> tollLogs;
	vector<Pass>  matchedPassesData;
	int  passesDataLength=0;
	// Parking Related variables
	unsigned char parkinData[160];
	PARKING_DATA parkData;
	ParkingService *pkData = new ParkingService();

	if(genericData->getServiceType() == ServiceType::TOLL){
		serviceType = 0x03;
	}else{
		serviceType = 0x05;    // Parking generic data
	}

	

	bLRC ^= apduBuffer[NAD_ADDR] = 0x01;
	bLRC ^= apduBuffer[PCB_ADDR] = APDU_PCBTypeB;
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x50;	// CLA
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x03; // INS
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = serviceType; // P1
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x00; // P2
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x00; // Lc0
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x00; // Lc1

	bLRC ^= apduBuffer[LEN_ADDR] = (unsigned char)(nDataLen & 0xFF);
	bLRC ^= apduBuffer[LEN2_ADDR] = (unsigned char)((nDataLen >> 8) & 0xFF);
	apduBuffer[DATA2_BASE + nDataLen] = bLRC;


	PrintHexArray("\n\nDAL-->USB: ", DATA2_BASE + nDataLen + EDC_LEN, apduBuffer);
	WriteUsbData(hCom, apduBuffer, DATA2_BASE + nDataLen + EDC_LEN);
	ReadUsbData(hCom, tmpBuf, &tmpBufLen, DATA2_BASE + nDataLen + EDC_LEN, APDU_PCBTypeB);
	PrintHexArray("\nUSB-->DAL: ", tmpBufLen, tmpBuf);

	sw1 = tmpBuf[tmpBufLen - 3];

	sw2 = tmpBuf[tmpBufLen - 2];



	if ((sw1 == APDU_OK_SW1) && (sw2 == APDU_OK_SW2)) {
		printf("\nSuccess get generic data\n");
		// DATA WITHOUT ENC
		memcpy(name, &tmpBuf[4 + 3], 48);
		printf("\n \n name isss  %s", name);

		memcpy(workBuf, &tmpBuf[4 + 3 + 48], 1);
		vehicleType = tmpBuf[4 + 3 + 48];
		printf("\n \n vehicle type isss  %d", vehicleType);

		memcpy(workBuf, &tmpBuf[4 + 3 + 48 +1], 1);
		cardStatus = tmpBuf[4 + 3 + 48 + 1];
		printf("\n \n cardStatus is  %d", cardStatus);

		memcpy(vehicleNo, &tmpBuf[4 + 3 + 48 + 16], 16);
		printf("\n \n vehicle no isss  %s", vehicleNo);

		memcpy(workBuf, &tmpBuf[4 + 3 + 48 + 16 + 16], 16);
		cardNo = CharArrayTolonglong(workBuf, 16);
		printf("\n\n card number is iss %lld \n\n", cardNo);

		memcpy(workBuf, &tmpBuf[4 + 3 + 96], 4);
		//PrintHexArray("amount iss ", 4, workBuf);
		balance = CharArrayToIntLE(workBuf, 4);
		printf("\n\n balance iss %d \n\n", balance);


		memcpy(logs,&tmpBuf[4 + 3 + 96 + 4 + 12],80);
		struct LOG historyData[5];
		b_getLogs(logs,historyData);
		printf("history data isss %d\n", historyData[0].transactionType);
		vector<Transaction> transactions;

		for(int j = 0; j<5; j++){
			Transaction *transaction = new Transaction();
			transaction->setType((TransactionType)historyData[j].transactionType);
			transaction->setTimeInMilliSeconds(historyData[j].timeinMills);
			transaction->setAmount(historyData[j].amount);
			transaction->setPosTerminalId(std::to_string(historyData[j].terminalId).c_str());
			transactions.push_back(*transaction);
		}

		// ENCRYPTED DATA
		memcpy(workBuf, &tmpBuf[80 + 112 + 4 + 3 + 4 + 3], 6);
		phoneNo = CharArrayTolonglong(workBuf, 6);
		printf("\n\n phone number is iss %lld \n\n", phoneNo);

		memcpy(workBuf, &tmpBuf[80 + 112 + 4 + 3 + 4 + 3 + 6], 5);
		adhaarNo = CharArrayTolonglong(workBuf, 5);
		printf("\n\n adhaar number is iss %lld \n\n", adhaarNo);

			//PASSES DATA
		if(serviceType == 0x03){
			memcpy(passes,&tmpBuf[80 + 112 + 4 + 3 + 4 + 3 + 6 + 5 + 1],96);
			memcpy(passLogs,&tmpBuf[80 + 112 + 4 + 3 + 4 + 3 + 6 + 5 + 1 + 80],112);

			struct PASS_LOG passLogsData[7];
			b_getPassLogs(passLogs,passLogsData);
		//	vector<TollLog> tollLogs;
			char* endptr = NULL;

			for(int j = 0; j<7; j++){
				if(passLogsData[j].branchId ==  strtoll(genericData->getBranchId().c_str(),&endptr,10)){
					TollLog *tollLog = new TollLog();
					tollLog->setInDateTime(passLogsData[j].inDateTime);
					tollLog->setExpiryDateTime(passLogsData[j].expiryDateTime);
					tollLog->setBranchId(std::to_string(passLogsData[j].branchId).c_str());
					tollLogs.push_back(*tollLog);
				}
			}


			//vector<Pass>  matchedPassesData;
			//int  passesDataLength=0;
			b_getPasses(passes,genericData,&matchedPassesData,&passesDataLength);
			printf("/n Passes data length is %d",passesDataLength);
			genericData->setpassCount(passesDataLength);
			genericData->setPass(matchedPassesData);
			genericData->setTollLog(tollLogs);
			genericData->setParkingData(pkData);
		}

		if(serviceType == 0x05){
			memcpy(parkinData,&tmpBuf[80 + 112 + 4 + 3 + 4 + 3 + 6 + 5 + 1],160-16);
			
		//	PARKING_DATA parkData;
			parkData.inDateTime = CharArrayTolonglong(&parkinData[144-16],4);
			parkData.branchId =	CharArrayTolonglong(&parkinData[148-16],4);
			parkData.exitDateTime =	CharArrayTolonglong(&parkinData[152-16],4);

			pkData->setInDateTime(parkData.inDateTime);
			pkData->setExitDateTime(parkData.exitDateTime);
			pkData->setBranchId(std::to_string(parkData.branchId).c_str());

			genericData->setParkingData(pkData);
			genericData->setpassCount(passesDataLength);
			genericData->setPass(matchedPassesData);
			genericData->setTollLog(tollLogs);

		}
		
		genericData->setName(name);
		genericData->setVehicleType((VehicleType) vehicleType);
		genericData->setCardStatus((CardStatusEnum)cardStatus);
		genericData->setCardNumber(std::to_string(cardNo));
		genericData->setVehicleNumber(vehicleNo);
		genericData->setMobile(phoneNo);
		genericData->setBalance(balance);
		genericData->setAadhaar(adhaarNo);
		//genericData->setpassCount(passesDataLength);
		//genericData->setPass(matchedPassesData);
		genericData->setTransactionLog(transactions);
		//genericData->setTollLog(tollLogs);

		return ErrorConstants::NO__ERROR;
	}

	if ((sw1 == APDU_ERR_FELICA_TEST_SW1) && (sw2 == APDU_ERR_FELICA_TEST_POLLING)) {
		return ErrorConstants::POLLING_FAILED;
	}
	if ((sw1 == APDU_ERR_FELICA_TEST_SW1) && (sw2 == APDU_ERR_FELICA_TEST_READ_DATA)) {
		return ErrorConstants::READ_FROM_CARD_FAILED;
	}
	if ((sw1 == APDU_ERR_FELICA_TEST_SW1) && (sw2 == APDU_ERR_FELICA_TEST_AUTH)) {
		return ErrorConstants::CARD_AUTH_FAILED;
	}
	return ErrorConstants::GET_VALUE_FAILED;
}

ErrorConstants BuarohReader::Pay(Card *card, Activity *activity) {

	unsigned char bLRC = 0;
	int nDataLen = 0;
	unsigned char apduBuffer[1040];
	unsigned char tmpBuf[262];
	unsigned long tmpBufLen;
	unsigned char balance[4];
	unsigned char inTime[4], expDate[4];
	unsigned char newBalance[4];
	unsigned char amount[4];
	unsigned int u32Bal = 0;
	unsigned int u32NewBal = 0;
	unsigned int u32Amount; 
	int i = 0;
	unsigned char payLoad[32];
	unsigned char serviceType= 0x03;

	if(activity->getServiceType() == ServiceType::TOLL){
		serviceType = 0x03;
	}else{
		serviceType = 0x05;    // Parking generic data
	}

	u32Amount = activity->getAmount();
	IntToCharArrayLE(u32Amount, amount);

	long long inDateTime = activity->getTimeInMilliSeconds();
	inDateTime = trimMilliseconds(inDateTime);
	IntToCharArray(inDateTime,inTime);

	//unsigned char txnType = (unsigned char)TransactionType::PAY;

	unsigned char txnType = (unsigned char)activity->getAction();
	
	long long timeInMsec =  activity->getTimeInMilliSeconds();
	
	long long expiryTimeInMsec = timeInMsec + 86400000;   // adding 24 hours
	expiryTimeInMsec = trimMilliseconds(expiryTimeInMsec);
	IntToCharArray(expiryTimeInMsec,expDate);
	
	unsigned char time[6];
	longtobytes6(timeInMsec,time);
	
	char* endptr;
	string terminalId = activity->getPosTerminalId();
	string branchId = activity->getSrcBranchId();
	unsigned char terminalIdArr[4], branchIdArr[4];
	unsigned int branchIdInt = strtoll(activity->getSrcBranchId().c_str(),&endptr,10);
	unsigned int terminalIdInt = strtoll(activity->getSrcBranchId().c_str(),&endptr,10);
	
	IntToCharArray(branchIdInt,branchIdArr);
	IntToCharArray(terminalIdInt,terminalIdArr);
	

	memcpy(&payLoad[0], amount, 4);
	payLoad[4] = txnType;
	memcpy(&payLoad[5], time, 6);
	memcpy(&payLoad[11], terminalIdArr, 4);
	memcpy(&payLoad[15], branchIdArr, 4);
	memcpy(&payLoad[19], inTime, 4);
	memcpy(&payLoad[23], expDate, 4);
	payLoad[27] = serviceType;
	// Data to send 28bytes ---- 4 byte amount + 1 byte transaction type + 6 byte timeInMiliSec + 4 byte terminal id + 4 byte branch id + 4 byte inDateTime + 4 byte expiryDataTime + 1byte serviceType

	bLRC ^= apduBuffer[NAD_ADDR] = 0x01;
	bLRC ^= apduBuffer[PCB_ADDR] = APDU_PCBTypeB;
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x50;
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x03;
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x01;
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x00;
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x1B;
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x00;
	for (i = 0; i < 28; i++)
	{
		bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = payLoad[i];
	}
	bLRC ^= apduBuffer[LEN_ADDR] = (unsigned char)(nDataLen & 0xFF);
	bLRC ^= apduBuffer[LEN2_ADDR] = (unsigned char)((nDataLen >> 8) & 0xFF);
	apduBuffer[DATA2_BASE + nDataLen] = bLRC;

	PrintHexArray("\n\nDAL-->USB: ", DATA2_BASE + nDataLen + EDC_LEN, apduBuffer);
	WriteUsbData(hCom, apduBuffer, DATA2_BASE + nDataLen + EDC_LEN);
	
	ReadUsbData(hCom, tmpBuf, &tmpBufLen, DATA2_BASE + nDataLen + EDC_LEN, APDU_PCBTypeB);
	PrintHexArray("\nUSB-->DAL: ", tmpBufLen, tmpBuf);

	unsigned char sw1 = tmpBuf[tmpBufLen - 3];

	unsigned char sw2 = tmpBuf[tmpBufLen - 2];


	if ((sw1 != APDU_OK_SW1) && (sw2 != APDU_OK_SW2)) {
		printf("\nError in payment\n");

		if ((sw1 == APDU_ERR_FELICA_TEST_SW1) && (sw2 == APDU_ERR_FELICA_TEST_POLLING)) {
			return ErrorConstants::POLLING_FAILED;
		}
		if ((sw1 == APDU_ERR_FELICA_TEST_SW1) && (sw2 == APDU_ERR_FELICA_TEST_READ_DATA)) {
			return ErrorConstants::CARD_AUTH_FAILED;
		}
		if ((sw1 == APDU_ERR_FELICA_TEST_SW1) && (sw2 == APDU_ERR_FELICA_TEST_AUTH)) {
			return ErrorConstants::CARD_AUTH_FAILED;
		}
		if ((sw1 == APDU_ERR_FELICA_TEST_SW1) && (sw2 == APDU_ERR_FELICA_TEST_LOW_BAL)) {
			return ErrorConstants::LOW_BALANCE;
		}
		if ((sw1 == APDU_ERR_FELICA_TEST_SW1) && (sw2 == APDU_ERR_FELICA_TEST_WRITE_DATA)) {
			return ErrorConstants::PAY_FAILED;
		}
		return ErrorConstants::PAY_FAILED;
	}
	memcpy(balance, &tmpBuf[DATA2_BASE], 4);
	u32Bal = CharArrayToIntLE(balance, 4);

	printf("\n\n balance iss %d \n\n", u32Bal);

	memcpy(newBalance, &tmpBuf[DATA2_BASE + 4], 4);
	u32NewBal = CharArrayToIntLE(newBalance, 4);

	printf("\n\n new balance iss %d \n\n", u32NewBal);
	activity->setAmount(u32NewBal);

	return ErrorConstants::NO__ERROR;
}

long BuarohReader::changeObject (SAM *sam){
	// if(samRefSlot1!=NULL && samRefSlot1->type == SAM::FELICA_RCS500_SAM){
	// 	printf("slot 1 obj reinit\n");
	// 	samRefSlot1=sam;
	// 	return APP_SUCCESS;
	// }else if(samRefSlot2 !=NULL && samRefSlot2->type == SAM::FELICA_RCS500_SAM){
	// 	printf("slot 2 obj reinit\n");
	// 	samRefSlot2=sam;
	// 	return APP_SUCCESS;
	// }
	return APP_ERROR;
}

ErrorConstants BuarohReader::UsePass(Card *card, Pass *pass) {
	unsigned char bLRC = 0;
	int nDataLen = 0;
	unsigned char apduBuffer[1040];
	unsigned char tmpBuf[262];
	unsigned long tmpBufLen;
	unsigned char out[4];
	unsigned char balance[4], maxTrips[2], sourceBranchId[4], destinationBranchId[4], byteExpiryDate[4];
	unsigned char amount[4];
	unsigned char passData[32];
	unsigned int u32Bal = 0;
	unsigned int u32NewBal = 0;
	unsigned long u32Amount;
	char* endptr;

	u32Amount = pass->getAmount();

	IntToCharArrayLE((unsigned int)u32Amount, amount);
	//IntToCharArray(pass->getSrcBranchId(),sourceBranchId);
	//IntToCharArray(pass->getDestBranchId(),destinationBranchId);

	string strFromBranchID =pass->getSrcBranchId();
	//long amount = pass->getAmount();
	string strToBranchId =pass->getDestBranchId();
	int maxNoOfTrips = pass->getMaxTrips();
	PassType passType = pass->getPassType();
	cout << "passType " << (int)passType << endl;
	long long expiryDate = pass->getExpiryDate();
	cout<<"expiryDate "<<expiryDate<<endl;

	long long currentTime = pass->getTimeInMilliSeconds();
	currentTime = trimMilliseconds(currentTime);
	unsigned char byteConvertedNoOfTrips[2];
	int noOfTrips = maxNoOfTrips;
	IntToCharArrayToBytes(noOfTrips, byteConvertedNoOfTrips);
	expiryDate = trimMilliseconds(expiryDate);
	IntToCharArray(expiryDate,out);
	memcpy(byteExpiryDate,out,4);
	PrintHexArray("byteExpiryDate: ",4,byteExpiryDate);

	LimitPeriodicity limitPeriodicity; 
	unsigned char lPeriodicity = 0 ;
	int lmaxCount;
	int limitMaxCount[2] = { 0 };//, limitMaxCount1[2] = { 0};
	vector<Periodicity> periodicities = pass->getPeriodicity();
	bool isRenewal = pass->getIsRenewal();
	cout<<"isRenewal "<<isRenewal<<endl;
	auto i = 0;
	for (auto & periodicity : periodicities) {
		
		limitPeriodicity = periodicity.getLimitPeriodicity();
		cout<<"limitPeriodicity " <<static_cast<int>(limitPeriodicity)<<endl;
		lmaxCount = periodicity.getLimitsMaxCount();
		lPeriodicity = lPeriodicity + static_cast<int>(limitPeriodicity)*(16 - (i * 16) + i);
		cout<<"lPeriodicity inside "<<lPeriodicity<<endl;
		limitMaxCount[i] = lmaxCount;
		i++;
	}
	printf("lPeriodicity %02x \n",lPeriodicity);
	cout<<"limitPeriodicity out side " <<lPeriodicity<< endl;
	unsigned char limitStartTime[4];
	unsigned char CRISID[6] = {0};
	IntToCharArray(currentTime,out);
	memcpy(limitStartTime,out,4);


	// unsigned char *ptrByteConvertedFromBranchID =0;
	// ptrByteConvertedFromBranchID = (unsigned char *)strFromBranchID.c_str(); 
	unsigned char byteConvertedFromBranchID[4] = {0};
	unsigned char byteConvertedToBranchID[4] = {0};

	unsigned int fromBranchIdInt = strtoll(pass->getSrcBranchId().c_str(),&endptr,10);
	IntToCharArray(fromBranchIdInt,byteConvertedFromBranchID);

	unsigned int toBranchIdInt = strtoll(pass->getDestBranchId().c_str(),&endptr,10);
	IntToCharArray(toBranchIdInt,byteConvertedToBranchID);
	
	// for(int j=0;j<4;j++)
	// {
	// 	byteConvertedFromBranchID[j] =(unsigned char ) ptrByteConvertedFromBranchID[j];
	// }
	// unsigned char *ptrByteConvertedToBranchId =0;
	// ptrByteConvertedToBranchId = (unsigned char *)strToBranchId.c_str(); 
	
	// for(int j=0;j<4;j++)
	// {
	// 	byteConvertedToBranchID[j] = (unsigned char )ptrByteConvertedToBranchId[j];
	// }
	/*unsigned char byteConvertedFromBranchID[4],byteConvertedToBranchID[4];
	strncpy((char*)byteConvertedFromBranchID, strFromBranchID.c_str(),sizeof(byteConvertedFromBranchID));
	strncpy((char*)byteConvertedToBranchID, strToBranchId.c_str(),sizeof(byteConvertedToBranchID));*/
	PrintHexArray("byteConvertedToBranchID: ",4,byteConvertedToBranchID);
	PrintHexArray("byteConvertedFromBranchID: ",4,byteConvertedFromBranchID);

	unsigned char LimitsRemainingCount[2] = {0},LimitsRemainingCount1[2] = {0};
	unsigned char byteLimitsMaxCount[2] = { 0 }, byteLimitsMaxCount1[2] = { 0 };

	IntToCharArrayToBytes(limitMaxCount[0], byteLimitsMaxCount);
	IntToCharArrayToBytes(limitMaxCount[1], byteLimitsMaxCount1);
	printf("limitMaxCount-- %d\n", limitMaxCount[0]);
	printf("limitMaxCount1-- %d\n", limitMaxCount[1]);
	limitMaxCount[0] -= 1;
	limitMaxCount[1] -= 1;
	IntToCharArrayToBytes(limitMaxCount[0], LimitsRemainingCount);
	IntToCharArrayToBytes(limitMaxCount[1], LimitsRemainingCount1);



	// Pass data 32 bytes   4- from branch 4- to branch 2-num trpis 4-expiry date time 1- pass type 1- limit periodicity ---16 bytes
	//						2-limit max count 2-limit count remaining 4-limits start date time 2-limit max count 2-limit count remaining 4-limits start date time  --16 bytes
	memcpy(passData,byteConvertedFromBranchID,4);
	memcpy(&passData[4],byteConvertedToBranchID,4);
	memcpy(&passData[8],byteConvertedNoOfTrips,2);
	memcpy(&passData[10],byteExpiryDate,4);	
	passData[14] = (unsigned char)passType;
	passData[15] = lPeriodicity;
	memcpy(&passData[16],byteLimitsMaxCount,2);		
	memcpy(&passData[18],LimitsRemainingCount,2);
	memcpy(&passData[20],limitStartTime,4);
	memcpy(&passData[24],byteLimitsMaxCount1,2);		
	memcpy(&passData[26],LimitsRemainingCount1,2);
	memcpy(&passData[28],limitStartTime,4);

	bLRC ^= apduBuffer[NAD_ADDR] = 0x01;
	bLRC ^= apduBuffer[PCB_ADDR] = APDU_PCBTypeB;
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x50;     //CLA
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x03;		//INS
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x04;		//P1
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x00;		//P2
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x24;		//Lc1
	bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = 0x00;		//Lc2
	for (int i = 0; i < 36; i++)
	{	
		if(i<4){
			bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = amount[i];				// 4 bytes amount
		}else{
			bLRC ^= apduBuffer[DATA2_BASE + nDataLen++] = passData[i-4];			// 32 bytes pass data
		}
	}
	bLRC ^= apduBuffer[LEN_ADDR] = (unsigned char)(nDataLen & 0xFF);
	bLRC ^= apduBuffer[LEN2_ADDR] = (unsigned char)((nDataLen >> 8) & 0xFF);
	apduBuffer[DATA2_BASE + nDataLen] = bLRC;					//Le

	// printf("\n\n Data to Send \n\n");
	// for (int i = 0; i <= DATA2_BASE + nDataLen; i++)
	// 	printf("%02X ", apduBuffer[i]);
	PrintHexArray("\n\nDAL-->USB: ", DATA2_BASE + nDataLen + EDC_LEN, apduBuffer);
	WriteUsbData(hCom, apduBuffer, DATA2_BASE + nDataLen + EDC_LEN);
	
	ReadUsbData(hCom, tmpBuf, &tmpBufLen, DATA2_BASE + nDataLen + EDC_LEN, APDU_PCBTypeB);
	PrintHexArray("\nUSB-->DAL: ", tmpBufLen, tmpBuf);

	unsigned char sw1 = tmpBuf[tmpBufLen - 3];

	unsigned char sw2 = tmpBuf[tmpBufLen - 2];


	if ((sw1 != APDU_OK_SW1) && (sw2 != APDU_OK_SW2)) {
		printf("\nError in pass\n");

		if ((sw1 == APDU_ERR_FELICA_TEST_SW1) && (sw2 == APDU_ERR_FELICA_TEST_POLLING)) {
			return ErrorConstants::POLLING_FAILED;
		}
		if ((sw1 == APDU_ERR_FELICA_TEST_SW1) && (sw2 == APDU_ERR_FELICA_TEST_READ_DATA)) {
			return ErrorConstants::READ_FROM_CARD_FAILED;
		}
		if ((sw1 == APDU_ERR_FELICA_TEST_SW1) && (sw2 == APDU_ERR_FELICA_TEST_AUTH)) {
			return ErrorConstants::CARD_AUTH_FAILED;
		}
		if ((sw1 == APDU_ERR_FELICA_TEST_SW1) && (sw2 == APDU_ERR_FELICA_TEST_LOW_BAL)) {
			return ErrorConstants::LOW_BALANCE;
		}
		if ((sw1 == APDU_ERR_FELICA_TEST_SW1) && (sw2 == APDU_ERR_FELICA_TEST_WRITE_DATA)) {
			return ErrorConstants::PASS_USE_FAILED;
		}
		return ErrorConstants::PASS_USE_FAILED;
	}

	// memcpy(balance, &tmpBuf[DATA2_BASE], 4);
	// u32Bal = CharArrayToIntLE(balance, 4);

	// printf("\n\n balance iss %d \n\n", u32Bal);

	// memcpy(newBalance, &tmpBuf[DATA2_BASE + 4], 4);
	// u32NewBal = CharArrayToIntLE(newBalance, 4);

	// printf("\n\n new balance iss %d \n\n", u32NewBal);
	// pass->setAmount(u32NewBal);

	return ErrorConstants::NO__ERROR;
}

ErrorConstants BuarohReader::GetCardDetails(Card *card, CardDetail *cardDetail) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

ErrorConstants BuarohReader::ActivateCard(Card *card, CardActivation *cardActivation) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

ErrorConstants BuarohReader::UpdatePersonalData(Card *card, PersonalData *personalData) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

ErrorConstants BuarohReader::UpdateVehicleData(Card *card, VehicleData *vehicleData) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

ErrorConstants BuarohReader::UpdateCardStatus(Card *card, CardStatus *cardStatus) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

ErrorConstants BuarohReader::Recharge(Card *card, Activity *activity) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

ErrorConstants BuarohReader::PurchaseTicket(Card *card, Ticket *ticket) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

ErrorConstants BuarohReader::GetTicket(Card *card, Ticket *ticket) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

ErrorConstants BuarohReader::ActivatePass(Card *card, Pass *pass) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

ErrorConstants BuarohReader::GetPass(Card *card, Pass *pass,PassCriteria *passCriteria) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

ErrorConstants BuarohReader::GetTransactions(Card *card,TransactionsList *transactionsList,TransactionsCriteria *transactionsCriteria) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}
ErrorConstants BuarohReader::EncryptData(Card *card, unsigned char data[16]){
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}
