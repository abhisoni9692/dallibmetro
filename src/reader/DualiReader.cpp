#pragma comment(lib, "winscard.lib")
#define UNICODE

#include <winscard.h>
#include <stdio.h>
#include <conio.h>
#include <stdbool.h>
#include "reader/Reader.h"
#include "sam/SAM.h"
#include "card/Card.h"
#include "../include/Util.h"
#include "reader/DualiReader.h"
#include "model/enum/ReaderType.h"
#include "model/constants/ErrorConstants.h"
#include "model/PassCriteria.h"
#include "model/TransactionsCriteria.h"
#include "model/TransactionsList.h"
#include "model/TollLog.h"
#include "model/Transaction.h"
#include "model/GenericData.h"
using namespace std;

static const unsigned char PASORI_PCSC_NO_ERROR_HEADER[5] = {0xC0, 0x03, 0x00, 0x90, 0x00};


//static wchar_t CARD_IF_DUALI[] =  L"Duali DE-ABCM Contactless Reader 0";
//static wchar_t SAM_IF_GEM_DUALI[] = L"Duali DE-ABCM Sam1 Reader 0";

//SAM* DualiReader::samRefSlot1 = NULL;
// SCARDCONTEXT DualiReader::g_hContext = NULL;
// SCARDHANDLE	DualiReader::g_hCard = NULL;
// SCARDHANDLE	DualiReader::g_hSAM = NULL;
//LPTSTR DualiReader::g_mszReaders = NULL;


DualiReader::DualiReader()
{
	type = ReaderType::DUALI_READER;
}

long DualiReader::initializeReader(int readerIndex)
{
	long			_ret = 0;
	unsigned long	pcchReaders = SCARD_AUTOALLOCATE;
	unsigned long	dwActProtocolSAM;
	LPTSTR			pReader;

	disconnectReader();

	printf("Initialize Reader\n");
	std::wcout<<"printing..............\n";
	wsprintf(DualiReader::CARD_IF_DUALI, L"Duali DE-ABCM Contactless Reader 0");
	std::wcout<<DualiReader::CARD_IF_DUALI<<"\n";

	std::wcout<<"printing..............\n";
	wsprintf(DualiReader::SAM_IF_GEM_DUALI, L"Duali DE-ABCM Sam1 Reader 0");
	std::wcout<<DualiReader::SAM_IF_GEM_DUALI<<"\n";
	
	//printf("Initialize Reader %d\n",readerIndex);

	//Establish Context
	printf("Establish Context\n");
	_ret = SCardEstablishContext(SCARD_SCOPE_USER,
		NULL,
		NULL,
		&g_hContext);
	if (_ret != SCARD_S_SUCCESS) {
		printf(" -> Error\n");
		return APP_ERROR;
	}

	// List All Readers
	printf("List All Readers\n");
	_ret = SCardListReaders(g_hContext, NULL, (LPTSTR)&g_mszReaders, &pcchReaders);
	if (_ret != SCARD_S_SUCCESS) {
		printf(" -> Error\n");
		return APP_ERROR;
	}
	pReader = g_mszReaders;
	while ('\0' != *pReader) {
		printf(" %S\n", pReader);
		pReader = pReader + wcslen((wchar_t *)pReader) + 1;
	}

	SCardFreeMemory(g_hContext, g_mszReaders);
	
	// Connect to SAM interface
	printf("Connect SAM\n");
	_ret = SCardConnect(g_hContext,
		DualiReader::SAM_IF_GEM_DUALI,
		SCARD_SHARE_SHARED,
		SCARD_PROTOCOL_T1,
		&g_hSAM,
		&dwActProtocolSAM
	);
	printf("RET = %ld", _ret);
	if (_ret != SCARD_S_SUCCESS) {
		printf(" -> Error\n");
		return APP_ERROR;
	}
	samRefSlot1 =  initializeSAM();
	if (samRefSlot1 != NULL)
	{
		samRefSlot1->hostAuthentication(this);
	}
	
	printf("Sam initialized\n");
	
	return APP_SUCCESS;
}

long DualiReader::disconnectReader()
{

	SCardDisconnect(g_hCard, SCARD_UNPOWER_CARD);
	SCardDisconnect(g_hSAM, SCARD_UNPOWER_CARD);

	SCardReleaseContext(g_hContext);
	return APP_SUCCESS;
}

long DualiReader::sendToSAM(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int SAMType)
{
	long _ret = 0;
	unsigned char _send_buf[262];
	unsigned long _send_len;
	unsigned char tmpBuf[262];
	unsigned long tmpBufLen;
	unsigned char sw1, sw2;
	unsigned int i;

	_send_buf[0] = 0xA0; //CLA
	_send_buf[1] = 0x00; //INS
	_send_buf[2] = 0x00; //P1
	_send_buf[3] = 0x00; //P2
	_send_buf[4] = (unsigned char)cmdLen; //Lc
	
	for (i = 0; i<cmdLen; i++) {
		_send_buf[i+5] = cmdBuf[i];
	}

	_send_buf[cmdLen + 5] = 0x00; //Le
	_send_len = cmdLen + 6; 
	tmpBufLen = *resLen;

#ifdef DEBUG
	PrintHexArray("PC->SAM: ", _send_len, _send_buf);
#endif

	_ret = SCardTransmit(g_hSAM,
		NULL,
		_send_buf,
		_send_len,
		NULL,
		tmpBuf,
		&tmpBufLen);

	if (_ret != SCARD_S_SUCCESS)
	{
		printf("SCardTransmit Error\n");
		return APP_ERROR;
	}

#ifdef DEBUG
	PrintHexArray("SAM->PC: ", tmpBufLen, tmpBuf);
#endif
	sw1 = tmpBuf[tmpBufLen - 2];
	sw2 = tmpBuf[tmpBufLen - 1];

	if ((sw1 == 0x67) && (sw2 == 0x00)) {
		return RCS500_LCLE_ERROR;
	}
	if ((sw1 == 0x6A) && (sw2 == 0x86)) {
		return RCS500_P1P2_ERROR;
	}
	if ((sw1 == 0x6D) && (sw2 == 0x00)) {
		return RCS500_INS_ERROR;
	}
	if ((sw1 == 0x6E) && (sw2 == 0x00)) {
		return RCS500_CLA_ERROR;
	}

	// Remove SW1/SW2
	*resLen = tmpBufLen - 2;
	for (i = 0; i<*resLen; i++) {
		resBuf[i] = tmpBuf[i];
	}

	if ((sw1 == 0x90) && (sw2 == 0x00)) {
		return SAM_DATA_TRANSMIT_SUCCEEDED;
	}

	return UNKOWN_ERROR;
}

long DualiReader::sendToCard(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int cardType)
{
	long _ret = 0;
	unsigned char _send_buf[262];
	unsigned char receiveBuf[262];
	unsigned long _send_len, receiveLen;
	unsigned int i;
	//int nIdx = 0;


	//if (cmdLen > 0)
	//{	
		_send_buf[0] = 0xFE; // CLA
		_send_buf[1] = 0x50; // INS
		_send_buf[2] = 0xFE; // P1
		_send_buf[3] = 0xFE; // P2
		_send_buf[4] = (unsigned char)cmdLen + 2; // Lc
			
		_send_buf[5] = (unsigned char)cmdLen + 1;	// Length of FeliCa Command
	
		for (i = 0; i<cmdLen; i++)
		{
			_send_buf[6 + i] = cmdBuf[i];
		}
		_send_buf[6+cmdLen] = 0xC0; // timeout
		_send_len = cmdLen + 7;
	
		//nIdx += cmdLen;

		//_send_buf[nIdx++] = 0x00; //Le
	//}

	//_send_len = nIdx;
	
	receiveLen = 262;

#ifdef DEBUG
	PrintHexArray("PC->CARD: ", _send_len, _send_buf);
#endif

	_ret = SCardTransmit(g_hCard,
		NULL,
		_send_buf,
		_send_len,
		NULL,
		receiveBuf,
		&receiveLen);

	if (_ret != SCARD_S_SUCCESS) {
		printf("SCardTransmit Error\n");
		return CARD_READER_ERROR;
	}

#ifdef DEBUG
	PrintHexArray("CARD->PC: ", receiveLen, receiveBuf);
#endif
	
	memcpy(resBuf, &receiveBuf[2], receiveLen-2);
	*resLen = receiveLen - 4;

	return CARD_DATA_TRANSMIT_SUCCEEDED;
}

long DualiReader::detectCard(int *cardType)
{
	long			ret;
	unsigned int	nIdx = 0;

	//unsigned long	readerLen = 0;
	DWORD	readerLen = 0;
	wchar_t tempReadName[256];

	unsigned long	ATRLen;
	unsigned long	dwState=0;

	unsigned char	ATRVal[262] = { 0x00 };
	unsigned long	dwActProtocol;

	unsigned char	_send_buf[262] = { 0x00 };
	unsigned char	receiveBuf[262] = { 0x00 };

	unsigned long	_send_len = 0L;
	unsigned long	receiveLen = 0L;


	printf("\nTap Card\n");
	printf("<Press ESC Key to cancel>\n");
	while (1)
	{
		// Hit Esc to cancel operation
		if ((0 != kbhit()) && (SMPL_ESC_KEY == getch()))
		{
			printf("  -> Canceled\n");
			return APP_CANCEL;
		}	
		ret = SCardConnect(g_hContext,
			DualiReader::CARD_IF_DUALI,
			SCARD_SHARE_SHARED,
			SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
			&g_hCard,
			&dwActProtocol
		);
		if (ret != SCARD_S_SUCCESS)
		{
			//printf("no card found\n");
			continue;
		}
		Reader::cardTapped = this;
		Sleep(500);
		//Get ATR value to check status
		readerLen = sizeof(DualiReader::CARD_IF_DUALI);
		memset(tempReadName, 0x00, 256);
		memcpy(tempReadName, DualiReader::CARD_IF_DUALI, readerLen);

		ATRLen = (unsigned long)32;
		printf("dwState = %ld\n", dwState);
		ret = SCardStatus(g_hCard,
			DualiReader::SAM_IF_GEM_DUALI,
			&readerLen,
			&dwState,
			&dwActProtocol,
			ATRVal,
			&ATRLen);
		printf("dwState = %ld\n", dwState);
		printf("dwActProtocol = %ld", dwActProtocol);
		PrintHexArray("CARD ATR ", ATRLen, ATRVal);
		printf("ret = %ld \n", ret);
		//memcpy(atrVal, ATRVal, ATRLen);
		if (ret != SCARD_S_SUCCESS)
		{
			return APP_ERROR;
		}

		*cardType = Card::unknownCard;
		unsigned char desfireATR[] = { 0x3B,0x89,0x80,0x01,0x80,0x5F,0x06,0x06,0x75,0x77,0x81,0x02,0x80,0xD6 };
		unsigned char desfireATR1[] = { 0x3B,0x81 ,0x80 ,0x01 ,0x80 ,0x80 };
		if (Equals(desfireATR, 14, ATRVal, ATRLen))
		{
			*cardType = Card::DESFIRE_EV1_CARD;
		}
		if (Equals(desfireATR1, 6, ATRVal, ATRLen))
		{
			*cardType = Card::DESFIRE_EV1_CARD;
		}
		*cardType = Card::FELICA_RCSA01_CARD;

		// _send_buf[0] = 0xFE;
		// _send_buf[1] = 0x81;
		// _send_buf[2] = 0xFE;
		// _send_buf[3] = 0xFE;
		// _send_buf[4] = 0x02;
		// _send_buf[5] = 0xFF;
		// _send_buf[6] = 0x00;
		// _send_len = 7;
		// receiveLen = 262;
		// ret = SCardTransmit(g_hCard,
		// 	NULL,
		// 	_send_buf,
		// 	_send_len,
		// 	NULL,  
		// 	receiveBuf,
		// 	&receiveLen);

		// if(ret != SCARD_S_SUCCESS){
		// 	printf("Stop Auto Polling Error");
			
		// 	return APP_ERROR;
		// }
		
		break;
	}

	return APP_SUCCESS;
} 

// detectInstantCard
long DualiReader::detectInstantCard(int *cardType)
{
	long			ret;
	unsigned int	nIdx = 0;
	int i = 0;

	//unsigned long	readerLen = 0;
	DWORD	readerLen = 0;
	wchar_t tempReadName[256];

	unsigned long	ATRLen;
	unsigned long	dwState=0;

	unsigned char	ATRVal[262] = { 0x00 };
	unsigned long	dwActProtocol;

	unsigned char	_send_buf[262] = { 0x00 };
	unsigned char	receiveBuf[262] = { 0x00 };

	unsigned long	_send_len = 0L;
	unsigned long	receiveLen = 0L;


	printf("\nTap Card\n");
	printf("<Press ESC Key to cancel>\n");
	for (i=0; i<=3; i++)
	{

		// Hit Esc to cancel operation
		if ((0 != kbhit()) && (SMPL_ESC_KEY == getch()))
		{
			printf("  -> Canceled\n");
			return APP_CANCEL;
		}	
		ret = SCardConnect(g_hContext,
			CARD_IF_DUALI,
			SCARD_SHARE_SHARED,
			SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
			&g_hCard,
			&dwActProtocol
		);
		if (ret != SCARD_S_SUCCESS)
		{
			// if (Reader::cardTapped)
			// {
			// 	return APP_CANCEL;
			// }
			continue;
			//Sleep(1000);
		}
		Reader::cardTapped = this;
		//Sleep(500);
		//Get ATR value to check status
		readerLen = sizeof(CARD_IF_DUALI);
		memset(tempReadName, 0x00, 256);
		memcpy(tempReadName, CARD_IF_DUALI, readerLen);

		ATRLen = (unsigned long)32;
		printf("dwState = %ld\n", dwState);
		ret = SCardStatus(g_hCard,
			DualiReader::SAM_IF_GEM_DUALI,
			&readerLen,
			&dwState,
			&dwActProtocol,
			ATRVal,
			&ATRLen);
		printf("dwState = %ld\n", dwState);
		printf("dwActProtocol = %ld\n", dwActProtocol);
		PrintHexArray("CARD ATR ", ATRLen, ATRVal);
		printf("ret = %ld \n", ret);
		//memcpy(atrVal, ATRVal, ATRLen);
		if (ret != SCARD_S_SUCCESS)
		{
			return APP_ERROR;
		}

		*cardType = Card::unknownCard;
		unsigned char desfireATR[] = { 0x3B,0x89,0x80,0x01,0x80,0x5F,0x06,0x06,0x75,0x77,0x81,0x02,0x80,0xD6 };
		unsigned char desfireATR1[] = { 0x3B,0x81 ,0x80 ,0x01 ,0x80 ,0x80 };
		unsigned char felicaAtr[] = {0x3B,0x8A,0x80,0x01,0xFC,0x01,0x2E,0x34,0xF3,0x47,0x82,0x1D,0x3B,0x00,0xFC};
		if (Equals(desfireATR, 14, ATRVal, ATRLen))
		{
			*cardType = Card::DESFIRE_EV1_CARD;
		}
		if (Equals(desfireATR1, 6, ATRVal, ATRLen))
		{
			*cardType = Card::DESFIRE_EV1_CARD;
		}
		*cardType = Card::FELICA_RCSA01_CARD;
		// _send_buf[0] = 0xFE;
		// _send_buf[1] = 0x81;
		// _send_buf[2] = 0xFE;
		// _send_buf[3] = 0xFE;
		// _send_buf[4] = 0x02;
		// _send_buf[5] = 0xFF;
		// _send_buf[6] = 0x00;
		// _send_len = 7;
		// receiveLen = 262;
		// ret = SCardTransmit(g_hCard,
		// 	NULL,
		// 	_send_buf,
		// 	_send_len,
		// 	NULL,  
		// 	receiveBuf,
		// 	&receiveLen);

		// if(ret != SCARD_S_SUCCESS){
		// 	printf("Stop Auto Polling Error");
			
		// 	return APP_ERROR;
		// }
		break;
	}

	return APP_SUCCESS;
}

SAM* DualiReader::initializeSAM() {
	int samType = SAM::FELICA_RCS500_SAM;
	//TODO: check desfire or felica SAM with _sam_res(ATR)
	SAM *sam = SAM::getInstance(samType);
	return sam;
}

bool DualiReader::isCardPresent() {
	long			lReturn,ret;
	unsigned long	dwActProtocol;
	lReturn = SCardConnect(g_hContext,
		DualiReader::CARD_IF_DUALI,
		SCARD_SHARE_SHARED,
		SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
		&g_hCard,
		&dwActProtocol
	);
	printf("retRETRETEERERERE = %ld", lReturn);
	DWORD	readerLen = 0;
	wchar_t tempReadName[256];

	unsigned long	ATRLen;
	unsigned long	dwState = 0;

	unsigned char	ATRVal[262] = { 0x00 };
	readerLen = sizeof(DualiReader::CARD_IF_DUALI);
	memset(tempReadName, 0x00, 256);
	memcpy(tempReadName, DualiReader::CARD_IF_DUALI, readerLen);
	ret = SCardStatus(g_hCard,
		DualiReader::CARD_IF_DUALI,
		&readerLen,
		&dwState,
		&dwActProtocol,
		ATRVal,
		&ATRLen);
	printf("dwState = %ld\n", dwState);
	printf("dwActProtocol = %ld", dwActProtocol);
	PrintHexArray("CARD ATR ", ATRLen, ATRVal);
	if (lReturn != SCARD_S_SUCCESS)
	{
		return false;
	}
	else {
		return true;
	}

}

long DualiReader::DisconnectFeliCaCard(void){
	//printf("Success Dis\n");

	// SCardDisconnect(g_hCard, SCARD_UNPOWER_CARD);
	// return APP_SUCCESS;

	long	lReturn;
	unsigned char	sendBuf[262];
	unsigned char	receiveBuf[262];
	unsigned long	sendLen, receiveLen;
	// Reset RF
	sendBuf[0] = 0xFE;
	sendBuf[1] = 0x20;
	sendBuf[2] = 0xFE;
	sendBuf[3] = 0xFE;
	sendBuf[4] = 0x02;
	sendBuf[5] = 0x10;
	sendBuf[6] = 0x00;

	sendLen = 7;
	receiveLen = 0;
	lReturn = SCardTransmit(g_hCard,
							NULL,
							sendBuf,
							sendLen,
							NULL,  
							receiveBuf,
							&receiveLen);

	if(lReturn != SCARD_S_SUCCESS){
		return APP_ERROR;
	}
	//SCardDisconnect(g_hCard, SCARD_UNPOWER_CARD);
	//lReturn =  startAutoPolling();
	//return 1; lReturn;
	printf("Success DisconnectFeliCaCard\n");
	return APP_SUCCESS;
	// turn RF off
	//return APP_ERROR;
};

bool DualiReader::isCardRemoved(){
	printf("checking removal of card\n");
	cancelDetection = false;
	long			ret = -1;
	unsigned long	dwActProtocol = 0L;
	unsigned char sendBuf[262];
	unsigned char receiveBuf[262];
	unsigned long sendLen, receiveLen;
	// FF 00 00 00 06 00 00 18 00 00

	sendBuf[0] = 0xFE;
	sendBuf[1] = 0x50;
	sendBuf[2] = 0xFE;
	sendBuf[3] = 0xFE;
	sendBuf[4] = 0x02;
	sendBuf[5] = 0x92;
	sendBuf[6] = 0xCF;
	sendBuf[7] = 0x00;
	sendLen = 8;
	while (1)
	{
		//SCardFreeMemory(g_hContext, g_hCard);
		ret = SCardConnect(g_hContext,
			DualiReader::CARD_IF_DUALI,
			SCARD_SHARE_SHARED,
			SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
			&g_hCard,
			&dwActProtocol);
		
		if (ret != SCARD_S_SUCCESS)
		{
			printf("1 card removed\n");
			printf("value for ret :%ld\n", ret);
			printf("hex value for ret :%02x\n", ret);
			//break;
		}		

		ret = SCardTransmit(g_hCard,
		NULL,
		sendBuf,
		sendLen,
		NULL,
		receiveBuf,
		&receiveLen);

		if (ret != SCARD_S_SUCCESS)
		{
			printf("card removed\n");
			printf("value for ret :%ld\n", ret);
			printf("hex value for ret :%02x\n", ret);
			break;
		}
		
		if(cancelDetection == true){
			cancelDetection = false;
			return false;
		}
	}
	return true;
}

bool DualiReader::stopDetection(){
	printf("checking removal of card\n");
	cancelDetection = true;
	return true;
}

long DualiReader::changeObject (SAM *sam){
	// if(samRefSlot1!=NULL && samRefSlot1->type == SAM::FELICA_RCS500_SAM){
	// 	printf("slot 1 obj reinit\n");
	// 	samRefSlot1=sam;
	// 	return APP_SUCCESS;
	// }else if(samRefSlot2 !=NULL && samRefSlot2->type == SAM::FELICA_RCS500_SAM){
	// 	printf("slot 2 obj reinit\n");
	// 	samRefSlot2=sam;
	// 	return APP_SUCCESS;
	// }
	return APP_ERROR;
}

ErrorConstants DualiReader::GetBalance(Card *card, long *amount) {
	return card->GetBalance(this, amount);
}

ErrorConstants DualiReader::GetCardDetails(Card *card, CardDetail *cardDetail) {
	return card->GetCardDetails(this, cardDetail);
}

ErrorConstants DualiReader::GetGenericData(Card *card, GenericData *genericData) {
	vector <TollLog> tollLog;
	vector <Transaction> transactionLogs;
	genericData->setTransactionLog(transactionLogs);
	genericData->setTollLog(tollLog);
	return card->GetGenericData(this, genericData);
}

ErrorConstants DualiReader::ActivateCard(Card *card, CardActivation *cardActivation) {
	return card->ActivateCard(this, cardActivation);
}

ErrorConstants DualiReader::UpdatePersonalData(Card *card, PersonalData *personalData) {
	return card->UpdatePersonalData(this, personalData);
}

ErrorConstants DualiReader::UpdateVehicleData(Card *card, VehicleData *vehicleData) {
	return card->UpdateVehicleData(this, vehicleData);
}

ErrorConstants DualiReader::UpdateCardStatus(Card *card, CardStatus *cardStatus) {
	return card->UpdateCardStatus(this, cardStatus);
}

ErrorConstants DualiReader::Pay(Card *card, Activity *activity) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

ErrorConstants DualiReader::Recharge(Card *card, Activity *activity) {
	return card->Recharge(this, activity);
}

ErrorConstants DualiReader::PurchaseTicket(Card *card, Ticket *ticket) {
	return card->PurchaseTicket(this, ticket);
}

ErrorConstants DualiReader::GetTicket(Card *card, Ticket *ticket) {
	return card->GetTicket(this, ticket);
}

ErrorConstants DualiReader::ActivatePass(Card *card, Pass *pass) {
	return card->ActivatePass(this, pass);
}

ErrorConstants DualiReader::UsePass(Card *card, Pass *pass) {
	return card->UsePass(this, pass);
}

ErrorConstants DualiReader::GetPass(Card *card, Pass *pass,PassCriteria *passCriteria) {
	return card->GetPass(this, pass,passCriteria);
}

ErrorConstants DualiReader::GetTransactions(Card *card,TransactionsList *transactionsList,TransactionsCriteria *transactionsCriteria) {
	return card->GetTransactions(this,transactionsList ,transactionsCriteria);
}
ErrorConstants DualiReader::EncryptData(Card *card, unsigned char data[16]) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

