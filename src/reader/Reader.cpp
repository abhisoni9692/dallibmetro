#pragma comment(lib, "winscard.lib")
#include <iostream>
#include <winscard.h>

#include "reader/Reader.h"
#include "Util.h"
#include "reader/BuarohReader.h"
#include "reader/SCSProReader.h"
#include "reader/SonyReader.h"
#include "reader/DualiReader.h"
#include "sam/SAM.h"

Reader* Reader::cardTapped;

Reader* Reader::getInstance(ReaderType type) {
	if (type == ReaderType::SCSPRO_READER) {
		return new SCSProReader();
	}
	if (type == ReaderType::SONY_READER) {
		return new SonyReader();
	}
	if (type == ReaderType::BUAROH_READER) {
		return new BuarohReader();
	}
	if (type == ReaderType::DUALI_READER) {
		return new DualiReader();
	}
	return NULL;
}