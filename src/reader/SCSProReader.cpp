#pragma comment(lib, "winscard.lib")
#define UNICODE

#include <winscard.h>
#include <stdio.h>
#include <conio.h>
#include <stdbool.h>
#include "reader/Reader.h"
#include "sam/SAM.h"
#include "card/Card.h"
#include "../include/Util.h"
#include "reader/SCSProReader.h"
#include "card/DesfireCardCmdUtil.h"
#include "sam/MifareSAMCmdUtil.h"
#include "reader/ScsProReaderSpecificCmdUtil.h"
#include "model/enum/ReaderType.h"
#include "model/constants/ErrorConstants.h"
#include "model/PassCriteria.h"
#include "model/TollLog.h"
#include "model/ParkingService.h"
#include "model/Transaction.h"
#include "model/GenericData.h"
#include "model/TransactionsList.h"
#include "model/TransactionsCriteria.h"
#include <TIME.h>
using namespace std;


unsigned char g_CMD_SAM_APDU = PCSC_CMD_SAM_1_APDU;

static const unsigned char PASORI_PCSC_NO_ERROR_HEADER[5] = {0xC0, 0x03, 0x00, 0x90, 0x00};


SCSProReader::SCSProReader() 
{
	type = ReaderType::SCSPRO_READER;
}

long SCSProReader::initializeReader(int readerIndex) {
	long			ret = 0;
	unsigned long	pcchReaders = SCARD_AUTOALLOCATE;
	LPTSTR			pReader;
	unsigned int	nIdx = 0;

	disconnectReader();
	std::wcout<<"printing..............\n";
	wsprintf(SCSProReader::CARD_IF_SDR10, L"SCSpro SDR-10 USB Smart Card Reader %d", readerIndex);
	std::wcout<<SCSProReader::CARD_IF_SDR10<<"\n";
	printf("Initialize Reader %d\n",readerIndex);

	//Establish Context
	printf("Establish Context\n");
	ret = SCardEstablishContext(SCARD_SCOPE_USER,
		NULL,
		NULL,
		&g_hContext);
	if (ret != SCARD_S_SUCCESS)
	{
		printf(" -> Error\n");
		return APP_ERROR;
	}

	// List All Readers
	printf("List All Readers\n");
	ret = SCardListReaders(g_hContext,
		NULL,
		(LPTSTR)&g_mszReaders,
		&pcchReaders);
	if (ret != SCARD_S_SUCCESS)
	{
		printf(" -> Error: %ld\n", ret);
		return APP_ERROR;
	}
	pReader = g_mszReaders;
	while ('\0' != *pReader)
	{
		printf(" %S\n", pReader);
		pReader = pReader + wcslen((wchar_t *)pReader) + 1;
	}

	SCardFreeMemory(g_hContext, g_mszReaders);

	// Connect to SAM interface
	PrintText("Connect SAM\n");
	ret = _ConnectToSAMInDirectMode();
	printf("ret:%ld\n",ret);
	if (ret != APP_SUCCESS)
	{
		PrintText(" -> Error");

		_DisconnectWithSAMInDirectMode();
		return APP_ERROR;
	}
	printf("\n\n\n\nInitializeSAM\n\n\n\n");

	samRefSlot1 = initializeSAM(slot1);
	samRefSlot2 = initializeSAM(slot2);

	printf("Sam Powered ON\n");
	if (samRefSlot1 == NULL && samRefSlot2 == NULL)
	{
		_DisconnectWithSAMInDirectMode();
		return APP_ERROR;
	}

	if (samRefSlot1 != NULL)
	{
		printf("Sam 1 auth \n");
		samRefSlot1->hostAuthentication(this);
	}
	if (samRefSlot2 != NULL)
	{
		printf("Sam 2 auth \n");
		samRefSlot2->hostAuthentication(this);
	}
	printf("Sam initialized\n");

	ret = _DisconnectWithSAMInDirectMode();
	if (ret != SCARD_S_SUCCESS)
	{
		return APP_ERROR;
	}
	printf("disconnected\n");

	SCSProReader::scsproReaderIniti = true;
	playBuzzer(BUZZER_GOOD);
	return APP_SUCCESS;
}

SAM* SCSProReader::initializeSAM(int slotNo) {
	long			ret = 0;
	unsigned char	_sam_res[262];
	unsigned long	_sam_res_len;
	unsigned int	nIdx = 0;
	int i;
	unsigned char felicaATR[]  = {0x3B, 0xF9, 0x15, 0x00, 0xFF, 0x91, 0x01, 0x31, 0xFE, 0x43};
	

	//TODO: detect felica SAM
	printf("Send SAM#%d Power ON \n",slotNo);
	_sam_res_len = sizeof(_sam_res);
	ret = _send_SAMPowerON(slotNo, _sam_res, &_sam_res_len);
	if (ret != APP_SUCCESS)
	{
		return NULL;
	}

	if(_sam_res_len < 10){
		return NULL;
	}
	if (Equals(felicaATR, 10, _sam_res, 10))
	{

		SAM *sam = SAM::getInstance(SAM::FELICA_RCS500_SAM);
		printf("felica SAM found\n");
		return sam;
	} else {

		int samType = SAM::MIFARE_AV2_SAM;
		//TODO: check desfire or felica SAM with _sam_res(ATR)
		SAM *sam = SAM::getInstance(samType);
		printf("Mifare SAM found\n");
		return sam;
	}

	/*PrintText("Send SAM#2 Power ON");
	_sam_res_len = sizeof(_sam_res);
	ret = _send_SAMPowerON(2, _sam_res, &_sam_res_len);
	if (ret != APP_SUCCESS)
	{
		PrintText(" -> Error");
	}
	else
	{
		samRefSlot2 = SAM::getInstance(SAM::desfireSAM_CONST);
	}

	if (ScsProReader::samRefSlot1 == NULL && samRefSlot2 == NULL)
	{
		return APP_ERROR;
	}

	printf("connected\n");
	if (ScsProReader::samRefSlot1 != NULL && ScsProReader::samRefSlot1->type == SAM::desfireSAM_CONST) {
		samRefSlot1->hostAuthentication(this);
	}

	if (samRefSlot2 != NULL && samRefSlot2->type == SAM::desfireSAM_CONST) {
		samRefSlot2->hostAuthentication(this);
	}
	*/
}
 
long SCSProReader::disconnectReader()
{
	SCardDisconnect(g_hCardReader, SCARD_UNPOWER_CARD);
	SCardReleaseContext(g_hContext);
	return APP_SUCCESS;
}

long SCSProReader::detectCard(int *cardType)
{
	long			ret = -1;
	unsigned int	nIdx = 0;

	//unsigned long	readerLen = 0;
	DWORD	readerLen = 0;
	wchar_t tempReadName[256];

	unsigned long	ATRLen = 0;
	unsigned long	dwState = 0;

	unsigned char	ATRVal[262] = { 0x00 };
	unsigned long	dwActProtocol = 0L;

	unsigned char	_send_buf[262] = { 0x00 };
	unsigned char	receiveBuf[262] = { 0x00 };

	unsigned long	_send_len = 0L;
	unsigned long	receiveLen = 0L;


	printf("\nTap Card\n");
	printf("<Press ESC Key to cancel>\n");
	cancelDetection=false;
	while (1)
	{

		// Hit Esc to cancel operation
		if ((0 != kbhit()) && (SMPL_ESC_KEY == getch()))
		{
			printf("  -> Canceled\n");
			return APP_CANCEL;
		}

		if(cancelDetection == true){
			printf("this is the errrorrrrr\n");
			cancelDetection = false;
			return APP_ERROR;
		}

		ret = SCardConnect(g_hContext,
			SCSProReader::CARD_IF_SDR10,
			SCARD_SHARE_SHARED,
			SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
			&g_hCardReader,
			&dwActProtocol);
		if (ret != SCARD_S_SUCCESS)
		{
			// if(Reader::cardTapped)
			// {
			// 	return APP_CANCEL;
			// }
			continue;
			//Sleep(1000);
		}
		Reader::cardTapped = this;
		//Sleep(500);
		//Get ATR value to check status
		readerLen = sizeof(SCSProReader::CARD_IF_SDR10);
		memset(tempReadName, 0x00, 256);
		memcpy(tempReadName, SCSProReader::CARD_IF_SDR10, readerLen);

		ATRLen = (unsigned long)32;
		ret = SCardStatus(g_hCardReader,
			tempReadName,
			&readerLen,
			&dwState,
			&dwActProtocol,
			ATRVal,
			&ATRLen);
		PrintHexArray("CARD ATR ", ATRLen, ATRVal);
		//memcpy(atrVal, ATRVal, ATRLen);
		if (ret != SCARD_S_SUCCESS)
		{
			return APP_ERROR;
		}

		*cardType = Card::unknownCard;
		// 3B 8F 80 01 80 4F 0C A0 00 00 03 06 11 00 3B 00 00 00 00 42
		unsigned char felicaATR[]  = { 0x3B,0x8F,0x80,0x01,0x80,0x4F,0x0C,0xA0,0x00,0x00,0x03,0x06,0x11,0x00,0x3B,0x00,0x00,0x00,0x00,0x42};
		unsigned char desfireATR[] = { 0x3B,0x89,0x80,0x01,0x80,0x5F,0x06,0x06,0x75,0x77,0x81,0x02,0x80,0xD6 };
		if (Equals(desfireATR, 14, ATRVal, ATRLen))
		{
			*cardType = Card::DESFIRE_EV1_CARD;
		}

		if (Equals(felicaATR, 20, ATRVal, ATRLen))
		{
			*cardType = Card::FELICA_RCSA01_CARD;
			printf("felica found\n");

			
			//Get IDM    // Reset felica card for continuous transactions 
			nIdx = 0;
			_send_buf[nIdx++] = 0xFF;
			_send_buf[nIdx++] = 0xCA;
			_send_buf[nIdx++] = 0x00;
			_send_buf[nIdx++] = 0x00;
			_send_len = nIdx;
			receiveLen = sizeof(receiveBuf);
			ret = SCardTransmit(g_hCardReader,
				NULL,
				_send_buf,
				_send_len,
				NULL,  
				receiveBuf,
				&receiveLen);

			if (ret != SCARD_S_SUCCESS || receiveLen < 8)
			{
				printf("returnung 1\n");
				return APP_ERROR;
			}
			//Reset Mode
			nIdx = 0;
			_send_buf[nIdx++] = 0xFF;
			_send_buf[nIdx++] = 0x00;
			_send_buf[nIdx++] = 0x00;
			_send_buf[nIdx++] = 0x00;
			_send_buf[nIdx++] = 0x0C;//Length
			_send_buf[nIdx++] = 0x3E;//Reset Mode (FeliCa Card)

			memcpy(&_send_buf[nIdx], receiveBuf, 8);
			nIdx += 8;
			_send_buf[nIdx++] = 0x00;
			_send_buf[nIdx++] = 0x00;
			
			_send_len = nIdx++;
			receiveLen = sizeof(receiveBuf);
			ret = SCardTransmit(g_hCardReader,
				NULL,
				_send_buf,
				_send_len,
				NULL,  
				receiveBuf,
				&receiveLen);

			if (ret != SCARD_S_SUCCESS)
			{
				printf("returnung 2\n");
				return APP_ERROR;
			}
		}
		printf("returnung 3\n");
		break;
	}
	playBuzzer(0x03);
	printf("returnung 4\n");
	return APP_SUCCESS;
}

long SCSProReader::detectInstantCard(int *cardType)
{
	long			ret = -1;
	unsigned int	nIdx = 0;
	int i=0;

	//unsigned long	readerLen = 0;
	DWORD	readerLen = 0;
	wchar_t tempReadName[256];

	unsigned long	ATRLen = 0;
	unsigned long	dwState = 0;

	unsigned char	ATRVal[262] = { 0x00 };
	unsigned long	dwActProtocol = 0L;

	unsigned char	_send_buf[262] = { 0x00 };
	unsigned char	receiveBuf[262] = { 0x00 };

	unsigned long	_send_len = 0L;
	unsigned long	receiveLen = 0L;


	printf("\nTap Card\n");
	printf("<Press ESC Key to cancel>\n");
	cancelDetection=false;
	for (i=0; i<=3; i++)
	{

		// Hit Esc to cancel operation
		if ((0 != kbhit()) && (SMPL_ESC_KEY == getch()))
		{
			printf("  -> Canceled\n");
			return APP_CANCEL;
		}

		if(cancelDetection == true){
			printf("this is the errrorrrrr\n");
			cancelDetection = false;
			return APP_ERROR;
		}

		ret = SCardConnect(g_hContext,
			SCSProReader::CARD_IF_SDR10,
			SCARD_SHARE_SHARED,
			SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
			&g_hCardReader,
			&dwActProtocol);
		if (ret != SCARD_S_SUCCESS)
		{
			// if(Reader::cardTapped)
			// {
			// 	return APP_CANCEL;
			// }
			continue;
			//Sleep(1000);
		}
		Reader::cardTapped = this;
		//Sleep(500);
		//Get ATR value to check status
		readerLen = sizeof(SCSProReader::CARD_IF_SDR10);
		memset(tempReadName, 0x00, 256);
		memcpy(tempReadName, SCSProReader::CARD_IF_SDR10, readerLen);

		ATRLen = (unsigned long)32;
		ret = SCardStatus(g_hCardReader,
			tempReadName,
			&readerLen,
			&dwState,
			&dwActProtocol,
			ATRVal,
			&ATRLen);
		PrintHexArray("CARD ATR ", ATRLen, ATRVal);
		//memcpy(atrVal, ATRVal, ATRLen);
		if (ret != SCARD_S_SUCCESS)
		{
			return APP_ERROR;
		}

		*cardType = Card::unknownCard;
		// 3B 8F 80 01 80 4F 0C A0 00 00 03 06 11 00 3B 00 00 00 00 42
		unsigned char felicaATR[]  = { 0x3B,0x8F,0x80,0x01,0x80,0x4F,0x0C,0xA0,0x00,0x00,0x03,0x06,0x11,0x00,0x3B,0x00,0x00,0x00,0x00,0x42};
		unsigned char desfireATR[] = { 0x3B,0x89,0x80,0x01,0x80,0x5F,0x06,0x06,0x75,0x77,0x81,0x02,0x80,0xD6 };
		if (Equals(desfireATR, 14, ATRVal, ATRLen))
		{
			*cardType = Card::DESFIRE_EV1_CARD;
		}

		if (Equals(felicaATR, 20, ATRVal, ATRLen))
		{
			*cardType = Card::FELICA_RCSA01_CARD;
			printf("felica found\n");

			
			//Get IDM    // Reset felica card for continuous transactions 
			nIdx = 0;
			_send_buf[nIdx++] = 0xFF;
			_send_buf[nIdx++] = 0xCA;
			_send_buf[nIdx++] = 0x00;
			_send_buf[nIdx++] = 0x00;
			_send_len = nIdx;
			receiveLen = sizeof(receiveBuf);
			ret = SCardTransmit(g_hCardReader,
				NULL,
				_send_buf,
				_send_len,
				NULL,  
				receiveBuf,
				&receiveLen);

			if (ret != SCARD_S_SUCCESS || receiveLen < 8)
			{
				printf("returnung 1\n");
				return APP_ERROR;
			}
			//Reset Mode
			nIdx = 0;
			_send_buf[nIdx++] = 0xFF;
			_send_buf[nIdx++] = 0x00;
			_send_buf[nIdx++] = 0x00;
			_send_buf[nIdx++] = 0x00;
			_send_buf[nIdx++] = 0x0C;//Length
			_send_buf[nIdx++] = 0x3E;//Reset Mode (FeliCa Card)

			memcpy(&_send_buf[nIdx], receiveBuf, 8);
			nIdx += 8;
			_send_buf[nIdx++] = 0x00;
			_send_buf[nIdx++] = 0x00;
			
			_send_len = nIdx++;
			receiveLen = sizeof(receiveBuf);
			ret = SCardTransmit(g_hCardReader,
				NULL,
				_send_buf,
				_send_len,
				NULL,  
				receiveBuf,
				&receiveLen);

			if (ret != SCARD_S_SUCCESS)
			{
				printf("returnung 2\n");
				return APP_ERROR;
			}
		}
		printf("returnung 3\n");
		break;
	}
	printf("returnung 4\n");
	return APP_SUCCESS;
}

long SCSProReader::TransmitDataToSAM(unsigned char samCmdBuf[], unsigned long samCmdLen, unsigned char samResBuf[], unsigned long* samResLen, unsigned char sdr10_cmd)
{
	long ret = 0;
	unsigned char _send_buf[262];
	unsigned long _send_len;
	unsigned char tmpBuf[262];
	unsigned long tmpBufLen;
	unsigned char sw1, sw2;
	unsigned int i;
	unsigned int nIdx = 0;
	
	
	readerSpecific(sdr10_cmd,_send_buf,&nIdx);


	if (samCmdLen > 0)
	{
		/*_send_buf[nIdx++] = 0xA0; //CLA
		_send_buf[nIdx++] = 0x00; //INS
		_send_buf[nIdx++] = 0x00; //P1
		_send_buf[nIdx++] = 0x00; //P2
		_send_buf[nIdx++] = (unsigned char)samCmdLen; //Lc*/
		for (i=0; i<samCmdLen; i++)
		{
			_send_buf[nIdx + i] = samCmdBuf[i];
		}
		nIdx += samCmdLen;

		//_send_buf[nIdx++] = 0x00; //Le
	}

	_send_len = nIdx;
	tmpBufLen = *samResLen;

#ifdef DEBUG
	PrintHexArray("PC->SAM: ", _send_len, _send_buf);
#endif

	ret = SCardTransmit(g_hCardReader,
		NULL,
		_send_buf,
		_send_len,
		NULL,  
		tmpBuf,
		&tmpBufLen);
	printf("ret = %ld\n",ret );
	if (ret != SCARD_S_SUCCESS)
	{
		printf("SCardTransmit Error\n");
		return APP_ERROR;
	}

#ifdef DEBUG
	PrintHexArray("SAM->PC: ", tmpBufLen, tmpBuf);
#endif

	if (PCSC_CMD_SAM_1_APDU != sdr10_cmd && PCSC_CMD_SAM_2_APDU != sdr10_cmd)
	{
		if ((PCSC_CMD_SAM_1_ON == sdr10_cmd || PCSC_CMD_SAM_2_ON == sdr10_cmd) && (0x3B == tmpBuf[0]))
		{
			return SCARD_S_SUCCESS;
		}
	}

	sw1 = tmpBuf[tmpBufLen - 2];
	sw2 = tmpBuf[tmpBufLen - 1];

	if ((sw1 == 0x67) && (sw2 == 0x00))
	{
		return RCS500_LCLE_ERROR;
	}
	if ((sw1 == 0x6A) && (sw2 == 0x86))
	{
		return RCS500_P1P2_ERROR;
	}
	if ((sw1 == 0x6D) && (sw2 == 0x00))
	{
		return RCS500_INS_ERROR;
	}
	if ((sw1 == 0x6E) && (sw2 == 0x00))
	{
		return RCS500_CLA_ERROR;
	}

	// Remove SW1/SW2
	*samResLen = tmpBufLen-2;
	for (i=0; i<*samResLen; i++)
	{
		samResBuf[i] = tmpBuf[i];
	}

	if ((sw1 == 0x90) && (sw2 == 0x00))
	{
		return SCARD_S_SUCCESS;
	}

	return UNKOWN_ERROR;
}


long SCSProReader::TransmitDataToFelicaSAM(unsigned char samCmdBuf[], unsigned long samCmdLen, unsigned char samResBuf[], unsigned long* samResLen, unsigned char sdr10_cmd)
{
	long ret = 0;
	unsigned char _send_buf[262];
	unsigned long _send_len;
	unsigned char tmpBuf[262];
	unsigned long tmpBufLen;
	unsigned char sw1, sw2;
	unsigned int i;
	unsigned int nIdx = 0;
	
	
	readerSpecific(sdr10_cmd,_send_buf,&nIdx);


	if (samCmdLen > 0)
	{
		_send_buf[nIdx++] = 0xA0; //CLA
		_send_buf[nIdx++] = 0x00; //INS
		_send_buf[nIdx++] = 0x00; //P1
		_send_buf[nIdx++] = 0x00; //P2
		_send_buf[nIdx++] = (unsigned char)samCmdLen; //Lc
		for (i=0; i<samCmdLen; i++)
		{
			_send_buf[nIdx + i] = samCmdBuf[i];
		}
		nIdx += samCmdLen;

		_send_buf[nIdx++] = 0x00; //Le
	}

	_send_len = nIdx;
	tmpBufLen = *samResLen;

#ifdef DEBUG
	PrintHexArray("PC->SAM: ", _send_len, _send_buf);
#endif

	ret = SCardTransmit(g_hCardReader,
		NULL,
		_send_buf,
		_send_len,
		NULL,  
		tmpBuf,
		&tmpBufLen);

	if (ret != SCARD_S_SUCCESS)
	{
		printf("SCardTransmit Error\n");
		return SAM_BRIDGE_ERROR;
	}

#ifdef DEBUG
	PrintHexArray("SAM->PC: ", tmpBufLen, tmpBuf);
#endif

	/* -------Ambigous L.O.C--------*/
	if (PCSC_CMD_SAM_1_APDU != sdr10_cmd && PCSC_CMD_SAM_2_APDU != sdr10_cmd)
	{
		if ((PCSC_CMD_SAM_1_ON == sdr10_cmd || PCSC_CMD_SAM_2_ON == sdr10_cmd) && (0x3B == tmpBuf[0]))
		{
			return SAM_DATA_TRANSMIT_SUCCEEDED;
		}
	}
	/************************/

	sw1 = tmpBuf[tmpBufLen - 2];
	sw2 = tmpBuf[tmpBufLen - 1];

	if ((sw1 == 0x67) && (sw2 == 0x00))
	{
		return SAM_DATA_TRANSMIT_LCLE_ERROR;
	}
	if ((sw1 == 0x6A) && (sw2 == 0x86))
	{
		return SAM_DATA_TRANSMIT_P1P2_ERROR;
	}
	if ((sw1 == 0x6D) && (sw2 == 0x00))
	{
		return SAM_DATA_TRANSMIT_INS_ERROR;
	}
	if ((sw1 == 0x6E) && (sw2 == 0x00))
	{
		return SAM_DATA_TRANSMIT_CLA_ERROR;
	}

	// Remove SW1/SW2
	*samResLen = tmpBufLen-2;
	for (i=0; i<*samResLen; i++)
	{
		samResBuf[i] = tmpBuf[i];
	}

	if ((sw1 == 0x90) && (sw2 == 0x00))
	{
		return SAM_DATA_TRANSMIT_SUCCEEDED;
	}

	return SAM_DATA_TRANSMIT_UNKOWN_ERROR;
}

long SCSProReader::sendToSAM(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int SAMType) {
	printf("reached\n");
	printf("%d\n",scsproReaderIniti);

	if (!scsproReaderIniti)
	{
		if (samRefSlot1 != NULL && samRefSlot1->type == SAM::MIFARE_AV2_SAM && SAMType == SAM::MIFARE_AV2_SAM) {
			printf(" 0.1 case \n");
			return EscapeDataToSAM(cmdBuf, cmdLen, resBuf, resLen, PCSC_CMD_SAM_1_APDU);
		}
		else if (samRefSlot2 != NULL && samRefSlot2->type == SAM::MIFARE_AV2_SAM && SAMType == SAM::MIFARE_AV2_SAM) {
			printf(" 0.2 case \n");
			return EscapeDataToSAM(cmdBuf, cmdLen, resBuf, resLen, PCSC_CMD_SAM_2_APDU);
		}
		else if (samRefSlot1 != NULL && samRefSlot1->type == SAM::FELICA_RCS500_SAM && SAMType == SAM::FELICA_RCS500_SAM) {
			printf(" 0.3 case \n");
			return EscapeDataToFelicaSAM(cmdBuf, cmdLen, resBuf, resLen, PCSC_CMD_SAM_1_APDU);
		}
		else if (samRefSlot2 != NULL && samRefSlot2->type == SAM::FELICA_RCS500_SAM && SAMType == SAM::FELICA_RCS500_SAM) {
			printf(" 0.4 case \n");
			return EscapeDataToFelicaSAM(cmdBuf, cmdLen, resBuf, resLen, PCSC_CMD_SAM_2_APDU);
		}
	}else {
		printf(" 1 case \n");
		if (samRefSlot1 != NULL && samRefSlot1->type == SAM::MIFARE_AV2_SAM && SAMType == SAM::MIFARE_AV2_SAM) {
			printf(" 1.1 case \n");
			return TransmitDataToSAM(cmdBuf, cmdLen, resBuf, resLen, PCSC_CMD_SAM_1_APDU);
		}
		else if(samRefSlot2 != NULL && samRefSlot2->type == SAM::MIFARE_AV2_SAM && SAMType == SAM::MIFARE_AV2_SAM) {
			printf(" 1.2 case \n");
			return TransmitDataToSAM(cmdBuf, cmdLen, resBuf, resLen, PCSC_CMD_SAM_2_APDU);
		}
		else if(samRefSlot1 != NULL && samRefSlot1->type == SAM::FELICA_RCS500_SAM && SAMType == SAM::FELICA_RCS500_SAM) {
			printf(" 1.3 case \n");
			return TransmitDataToFelicaSAM(cmdBuf, cmdLen, resBuf, resLen, PCSC_CMD_SAM_1_APDU);
		}
		else if(samRefSlot2 != NULL && samRefSlot2->type == SAM::FELICA_RCS500_SAM && SAMType == SAM::FELICA_RCS500_SAM) {
			printf(" 1.4 case \n");
			return TransmitDataToFelicaSAM(cmdBuf, cmdLen, resBuf, resLen, PCSC_CMD_SAM_2_APDU);
		}
	}
	return APP_ERROR;
}


long SCSProReader::sendToCard(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int cardType) {
	if(cardType == Card::DESFIRE_EV1_CARD){
		return TransmitDataToCard(cmdLen, cmdBuf, resLen,  resBuf);
	}else{
		return TransmitDataToFeliCaCard(cmdLen, cmdBuf, resLen,  resBuf);
	}
}

long SCSProReader::TransmitDataToCard(unsigned long cmdLen, unsigned char cmdBuf[], unsigned long* resLen, unsigned char resBuf[])
{

	hasAdditionalFrames = false;
	long ret = 0;
	unsigned char _send_buf[262];
	unsigned char receiveBuf[262];
	unsigned long _send_len, receiveLen;
	unsigned int i = 0;
	
	unsigned int nIdx = 0;

	for (i=0; i<cmdLen; i++)
	{
		_send_buf[nIdx++] = cmdBuf[i];
	}

	_send_len = nIdx;
	receiveLen = 262;

#ifdef DEBUG
	PrintHexArray("PC->CARD: ", _send_len, _send_buf);
#endif

	ret = SCardTransmit(g_hCardReader,
		NULL,
		_send_buf,
		_send_len,
		NULL,  
		receiveBuf,
		&receiveLen);
	if (ret != SCARD_S_SUCCESS)
	{
		printf("SCardTransmit Error\n");
		return APP_ERROR;
	}

#ifdef DEBUG
	PrintHexArray("CARD->PC: ", receiveLen, receiveBuf);
#endif

	*resLen = receiveLen;
	memcpy(resBuf, receiveBuf, receiveLen);
	printf("receiveLen:%ld\n", receiveLen);

	unsigned char sw1 = receiveBuf[receiveLen - 2];
	unsigned char sw2 = receiveBuf[receiveLen - 1];

	*resLen = receiveLen - 2;

	if ((sw1 == 0x90) && (sw2 == 0x00))
	{
		return APP_SUCCESS;
	}
	if ((sw1 == 0x91) && (sw2 == 0xAF))
	{
		hasAdditionalFrames = true;
		return APP_SUCCESS;
	}
	if ((sw1 == 0x91) && (sw2 == 0x00))
	{
		return APP_SUCCESS;
	}

	return APP_ERROR;
}


long SCSProReader::TransmitDataToFeliCaCard(unsigned long felicaCmdLen, unsigned char felicaCmdBuf[], unsigned long* felicaResLen, unsigned char felicaResBuf[])
{
	long ret = 0;
	unsigned char _send_buf[262];
	unsigned char receiveBuf[262];
	unsigned long _send_len, receiveLen;
	unsigned int i = 0;
	
	unsigned int nIdx = 0;

	//PCSC_CMD_CARD_FECLICA
	_send_buf[nIdx++] = 0xFF; //SDR-10 Header
	_send_buf[nIdx++] = 0x00;
	_send_buf[nIdx++] = 0x00;
	_send_buf[nIdx++] = 0x00;
	
	_send_buf[nIdx++] = (unsigned char)felicaCmdLen + 1;	// Length of FeliCa Command
	for (i=0; i<felicaCmdLen; i++)
	{
		_send_buf[nIdx++] = felicaCmdBuf[i];
	}

	_send_len = nIdx;
	receiveLen = 262;


#ifdef DEBUG
	PrintHexArray("PC->CARD: ", _send_len, _send_buf);
#endif
	ret = SCardTransmit(g_hCardReader,
		NULL,
		_send_buf,
		_send_len,
		NULL,  
		receiveBuf,
		&receiveLen);

	if (ret != SCARD_S_SUCCESS)
	{
		printf("SCardTransmit Error\n");
		return CARD_READER_ERROR;
	}

#ifdef DEBUG
	PrintHexArray("CARD->PC: ", receiveLen, receiveBuf);
#endif

	*felicaResLen = receiveLen;
	memcpy(felicaResBuf, receiveBuf, receiveLen);

	if (receiveLen <= 2)
	{
		return APP_ERROR;
	}

	*felicaResLen = receiveLen - 2;

	return CARD_DATA_TRANSMIT_SUCCEEDED;
}


long SCSProReader::_ConnectToSAMInDirectMode()
{
	long			ret = 0;
	unsigned long	dwActProtocolSAM;
	unsigned char	sendBuf[262];
	unsigned char	recvBuf[262];
	DWORD			recvLength = 0;
	unsigned int	nIdx = 0;

	// Connect to SAM interface
	PrintText("Connect To SAM In DirectMode \n");
	std::wcout<<this->CARD_IF_SDR10<<"\n";
	ret =	SCardConnect(g_hContext,
	SCSProReader::CARD_IF_SDR10,
	SCARD_SHARE_DIRECT,
	SCARD_PROTOCOL_UNDEFINED,
	&g_hSAM,
	&dwActProtocolSAM);
	printf("ret:%ld\n",ret);
	if (ret != SCARD_S_SUCCESS)
	{
		PrintText(" -> Error\n");
		return APP_ERROR;
	}

	//Reader Version
	nIdx = 0;
	sendBuf[nIdx++] = 0xE0;
	sendBuf[nIdx++] = 0x00;
	sendBuf[nIdx++] = 0x00;
	sendBuf[nIdx++] = 0x18;
	sendBuf[nIdx++] = 0x00;

	recvLength = sizeof(recvBuf);
	memset(recvBuf, 0x00, recvLength);

	ret = SCardControl(g_hSAM, IOCTL_CCID_ESCAPE, sendBuf, nIdx, recvBuf, sizeof(recvBuf), &recvLength);
	if (ret != SCARD_S_SUCCESS)
	{
		PrintText(" -> Error\n");
		return APP_ERROR;
	}

	if (recvLength < 4)
	{
		PrintText("   -> Error\n");
		return APP_ERROR;
	}

#ifdef DEBUG
	PrintText("  Reader FW Ver: %.250s \n", &recvBuf[4]);
#endif

	return ret;
}

long SCSProReader::_DisconnectWithSAMInDirectMode(void)
{
	long			ret = 0;

	ret = SCardDisconnect(g_hSAM, SCARD_LEAVE_CARD);

	return ret;
}

long SCSProReader::_send_SAMPowerON(unsigned char samNo, unsigned char samResBuf[], unsigned long* samResLen)
{
	long ret = 0;
		
	if (1 == samNo)
	{
		g_CMD_SAM_APDU = PCSC_CMD_SAM_1_APDU;
		ret = EscapeDataToSAM(NULL, 0, samResBuf, samResLen, PCSC_CMD_SAM_1_ON);
	}
	else if (2 == samNo)
	{
		g_CMD_SAM_APDU = PCSC_CMD_SAM_2_APDU;
		ret = EscapeDataToSAM(NULL, 0, samResBuf, samResLen, PCSC_CMD_SAM_2_ON);
	}
	else
	{
		return -1;
	}

	return ret;
}

long SCSProReader::EscapeDataToSAM(unsigned char samCmdBuf[], unsigned long samCmdLen, unsigned char samResBuf[], unsigned long* samResLen, unsigned char sdr10_cmd)
{
	long ret = 0;
	unsigned char _send_buf[262];
	unsigned long _send_len;
	unsigned char tmpBuf[262];
	unsigned long tmpBufLen;
	unsigned char sw1, sw2;
	unsigned int i;
	unsigned int nIdx = 0;

	readerSpecific(sdr10_cmd,_send_buf,&nIdx);


	if (samCmdLen > 0)
	{

		//	_send_buf[nIdx++] = (unsigned char)samCmdLen; //Lc
		for (i = 0; i<samCmdLen; i++)
		{
			_send_buf[nIdx + i] = samCmdBuf[i];
		}
		nIdx += samCmdLen;

		//_send_buf[nIdx++] = 0x00; //Le
	}

	_send_len = nIdx;
	tmpBufLen = *samResLen;

#ifdef DEBUG
	PrintHexArray("PC->SAM: ", _send_len, _send_buf);
#endif

	ret = SCardControl(g_hSAM,
		IOCTL_CCID_ESCAPE,
		_send_buf,
		_send_len,
		tmpBuf,
		sizeof(tmpBuf),
		&tmpBufLen);

	if (ret != SCARD_S_SUCCESS)
	{
		printf("SCardTransmit Error\n");
		return APP_ERROR;
	}


#ifdef DEBUG
	PrintHexArray("SAM->PC: ", tmpBufLen, tmpBuf);
#endif

	memcpy(samResBuf, tmpBuf, tmpBufLen);
	*samResLen = tmpBufLen - 2;

	if (PCSC_CMD_SAM_1_APDU != sdr10_cmd && PCSC_CMD_SAM_2_APDU != sdr10_cmd)
	{
		if ((PCSC_CMD_SAM_1_ON == sdr10_cmd || PCSC_CMD_SAM_2_ON == sdr10_cmd) && (0x3B == tmpBuf[0]))
		{
			return SCARD_S_SUCCESS;
		}
	}

	printf("escape 1\n");
	sw1 = tmpBuf[tmpBufLen - 2];
	sw2 = tmpBuf[tmpBufLen - 1];

	if ((sw1 == 0x67) && (sw2 == 0x00))
	{
		return RCS500_LCLE_ERROR;
	}
	if ((sw1 == 0x6A) && (sw2 == 0x86))
	{
		return RCS500_P1P2_ERROR;
	}
	if ((sw1 == 0x6D) && (sw2 == 0x00))
	{
		return RCS500_INS_ERROR;
	}
	if ((sw1 == 0x6E) && (sw2 == 0x00))
	{
		return RCS500_CLA_ERROR;
	}

	// Remove SW1/SW2
	*samResLen = tmpBufLen - 2;
	for (i = 0; i<*samResLen; i++)
	{
		samResBuf[i] = tmpBuf[i];
		printf("%02X ",samResBuf[i]);
	}

	if (((sw1 == 0x90) && (sw2 == 0xAF)) || ((sw1 == 0x90) && (sw2 == 0x00)))
	{
		return SCARD_S_SUCCESS;
	}

	return UNKOWN_ERROR;
}


long SCSProReader::EscapeDataToFelicaSAM(unsigned char samCmdBuf[], unsigned long samCmdLen, unsigned char samResBuf[], unsigned long* samResLen, unsigned char sdr10_cmd)
{
	long ret = 0;
	unsigned char _send_buf[262];
	unsigned long _send_len;
	unsigned char tmpBuf[262];
	unsigned long tmpBufLen;   
	unsigned char sw1, sw2;
	unsigned int i;
	unsigned int nIdx = 0;

	readerSpecific(sdr10_cmd,_send_buf,&nIdx);

	if (samCmdLen > 0)
	{
		_send_buf[nIdx++] = 0xA0; //CLA
		_send_buf[nIdx++] = 0x00; //INS
		_send_buf[nIdx++] = 0x00; //P1
		_send_buf[nIdx++] = 0x00; //P2
		_send_buf[nIdx++] = (unsigned char)samCmdLen; //Lc
		//	_send_buf[nIdx++] = (unsigned char)samCmdLen; //Lc
		for (i = 0; i<samCmdLen; i++)
		{
			_send_buf[nIdx + i] = samCmdBuf[i];
		}
		nIdx += samCmdLen;

		_send_buf[nIdx++] = 0x00; //Le
	}
	
	_send_len = nIdx;
	tmpBufLen = *samResLen;

#ifdef DEBUG
	PrintHexArray("PC->SAM: ", _send_len, _send_buf);
#endif

	ret = SCardControl(g_hSAM,
		IOCTL_CCID_ESCAPE,
		_send_buf,
		_send_len,
		tmpBuf,
		sizeof(tmpBuf),
		&tmpBufLen);

	if (ret != SCARD_S_SUCCESS)
	{
		printf("SCardTransmit Error\n");
		return SAM_BRIDGE_ERROR;
	}

#ifdef DEBUG
	PrintHexArray("SAM->PC: ", tmpBufLen, tmpBuf);
#endif

	if (PCSC_CMD_SAM_1_APDU != sdr10_cmd && PCSC_CMD_SAM_2_APDU != sdr10_cmd)
	{
		if ((PCSC_CMD_SAM_1_ON == sdr10_cmd || PCSC_CMD_SAM_2_ON == sdr10_cmd) && (0x3B == tmpBuf[0]))
		{
			return SAM_DATA_TRANSMIT_SUCCEEDED;
		}
	}

	sw1 = tmpBuf[tmpBufLen - 2];
	sw2 = tmpBuf[tmpBufLen - 1];

	if ((sw1 == 0x67) && (sw2 == 0x00))
	{
		return SAM_DATA_TRANSMIT_LCLE_ERROR;
	}
	if ((sw1 == 0x6A) && (sw2 == 0x86))
	{
		return SAM_DATA_TRANSMIT_P1P2_ERROR;
	}
	if ((sw1 == 0x6D) && (sw2 == 0x00))
	{
		return SAM_DATA_TRANSMIT_INS_ERROR;
	}
	if ((sw1 == 0x6E) && (sw2 == 0x00))
	{
		return SAM_DATA_TRANSMIT_CLA_ERROR;
	}

	// Remove SW1/SW2
	*samResLen = tmpBufLen - 2;
	for (i = 0; i<*samResLen; i++)
	{
		samResBuf[i] = tmpBuf[i];
	}

	if (((sw1 == 0x90) && (sw2 == 0xAF)) || ((sw1 == 0x90) && (sw2 == 0x00)))
	{
		return SAM_DATA_TRANSMIT_SUCCEEDED;
	}

	return SAM_DATA_TRANSMIT_UNKOWN_ERROR;
}

void SCSProReader::readerSpecific(unsigned char sdr10_cmd,unsigned char _send_buf[],unsigned int* nIdx)
{
	switch (sdr10_cmd)
	{
	case PCSC_CMD_GET_FW_VER:
		{
			memcpy(_send_buf,ScsProReaderSpecificCmdUtil::CMD_GET_FW_VER,sizeof(ScsProReaderSpecificCmdUtil::CMD_GET_FW_VER));
			*nIdx = sizeof(ScsProReaderSpecificCmdUtil::CMD_GET_FW_VER);
			
		}
		break;
	case PCSC_CMD_SAM_1_ON:
		{
			memcpy(_send_buf,ScsProReaderSpecificCmdUtil::CMD_SAM_1_ON,sizeof(ScsProReaderSpecificCmdUtil::CMD_SAM_1_ON));
			*nIdx = sizeof(ScsProReaderSpecificCmdUtil::CMD_SAM_1_ON);
			
		}
		break;
	case PCSC_CMD_SAM_2_ON:
		{
			memcpy(_send_buf,ScsProReaderSpecificCmdUtil::CMD_SAM_2_ON,sizeof(ScsProReaderSpecificCmdUtil::CMD_SAM_2_ON));
			*nIdx = sizeof(ScsProReaderSpecificCmdUtil::CMD_SAM_2_ON);
		}
		break;
	case PCSC_CMD_SAM_1_OFF:
		{
			memcpy(_send_buf,ScsProReaderSpecificCmdUtil::CMD_SAM_1_OFF,sizeof(ScsProReaderSpecificCmdUtil::CMD_SAM_1_OFF));
			*nIdx = sizeof(ScsProReaderSpecificCmdUtil::CMD_SAM_1_OFF);
		}
		break;
	case PCSC_CMD_SAM_2_OFF:
		{
			memcpy(_send_buf,ScsProReaderSpecificCmdUtil::CMD_SAM_2_OFF,sizeof(ScsProReaderSpecificCmdUtil::CMD_SAM_2_OFF));
			*nIdx = sizeof(ScsProReaderSpecificCmdUtil::CMD_SAM_2_OFF);
		}
		break;
	case PCSC_CMD_SAM_1_APDU:
		{
			memcpy(_send_buf,ScsProReaderSpecificCmdUtil::CMD_SAM_1_APDU,sizeof(ScsProReaderSpecificCmdUtil::CMD_SAM_1_APDU));
			*nIdx = sizeof(ScsProReaderSpecificCmdUtil::CMD_SAM_1_APDU);
		}
		break;
	case PCSC_CMD_SAM_2_APDU:
		{
			memcpy(_send_buf,ScsProReaderSpecificCmdUtil::CMD_SAM_2_APDU,sizeof(ScsProReaderSpecificCmdUtil::CMD_SAM_2_APDU));	
			*nIdx = sizeof(ScsProReaderSpecificCmdUtil::CMD_SAM_2_APDU);
		}
		break;
	default:
		break;
	}

}


long SCSProReader::DisconnectFeliCaCard(void) 
{
	printf("success disconnect felica card\n");
	return APP_SUCCESS;
}

bool SCSProReader::isCardRemoved(){
	printf("checking removal of card\n");
	cancelDetection = false;
	long			ret = -1;
	unsigned long	dwActProtocol = 0L;
	
	while (1)
	{

		ret = SCardConnect(g_hContext,
			SCSProReader::CARD_IF_SDR10,
			SCARD_SHARE_SHARED,
			SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
			&g_hCardReader,
			&dwActProtocol);
		if (ret != SCARD_S_SUCCESS)
		{
			printf("card removed\n");
			break;
			//Sleep(1000);
		}
		
		if(cancelDetection == true){

			cancelDetection = false;
			return false;
		}
		Sleep(100);

	}
	return true;
}


bool SCSProReader::stopDetection(){
	printf("checking removal of card\n");
	cancelDetection = true;
	cancel_thread = true;
	return true;
}


/* TODO: Add logic for mifore also */
long SCSProReader::changeObject (SAM *sam){
	if(samRefSlot1!=NULL && samRefSlot1->type == SAM::FELICA_RCS500_SAM){
		printf("slot 1 obj reinit\n");
		samRefSlot1=sam;
		return APP_SUCCESS;
	}else if(samRefSlot2 !=NULL && samRefSlot2->type == SAM::FELICA_RCS500_SAM){
		printf("slot 2 obj reinit\n");
		samRefSlot2=sam;
		return APP_SUCCESS;
	}
	return APP_ERROR;
}

bool SCSProReader::playBuzzer (unsigned char buzzerType){
	long ret=0;
	unsigned char	sendBuf[262];
	unsigned char	recvBuf[262];
	unsigned int	nIdx = 0;
	DWORD			recvLength = 0;
	sendBuf[nIdx++] = 0xE0;
	sendBuf[nIdx++] = 0x00;
	sendBuf[nIdx++] = 0x00;
	sendBuf[nIdx++] = 0x28;
	sendBuf[nIdx++] = 0x01;
	sendBuf[nIdx++] = buzzerType;

	recvLength = sizeof(recvBuf);
	memset(recvBuf, 0x00, recvLength);

	ret = SCardControl(g_hSAM, IOCTL_CCID_ESCAPE, sendBuf, nIdx, recvBuf, sizeof(recvBuf), &recvLength);
	if (ret != SCARD_S_SUCCESS)
	{
		printf("%02X",ret);
		PrintText("Buzzer -> Error in ret\n");
		return false;
	}
	if(recvBuf[0]!=0x90 && recvBuf[1]!=0x00){
		("Buzzer -> Error in response\n");
		return false;
	}
	return true;

}
ErrorConstants SCSProReader::GetBalance(Card *card, long *amount) {
	return card->GetBalance(this, amount);
}

ErrorConstants SCSProReader::GetCardDetails(Card *card, CardDetail *cardDetail) {

	return card->GetCardDetails(this, cardDetail);
}

ErrorConstants SCSProReader::GetGenericData(Card *card, GenericData *genericData) {
	vector <TollLog> tollLog;
	vector <Transaction> transactionLogs;
	ParkingService *parkingData;
	genericData->setTransactionLog(transactionLogs);
	genericData->setTollLog(tollLog);
	printf("working fine till parking\n");
	genericData->setParkingData(parkingData);
	printf("working fine afer parking\n"); 
	return card->GetGenericData(this, genericData);
}

ErrorConstants SCSProReader::ActivateCard(Card *card, CardActivation *cardActivation) {
	return card->ActivateCard(this, cardActivation);
}

ErrorConstants SCSProReader::UpdatePersonalData(Card *card, PersonalData *personalData) {
	return card->UpdatePersonalData(this, personalData);
}

ErrorConstants SCSProReader::UpdateVehicleData(Card *card, VehicleData *vehicleData) {
	return card->UpdateVehicleData(this, vehicleData);
}

ErrorConstants SCSProReader::UpdateCardStatus(Card *card, CardStatus *cardStatus) {
	return card->UpdateCardStatus(this, cardStatus);
}

ErrorConstants SCSProReader::Pay(Card *card, Activity *activity) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

ErrorConstants SCSProReader::Recharge(Card *card, Activity *activity) {
	return card->Recharge(this, activity);
}

ErrorConstants SCSProReader::PurchaseTicket(Card *card, Ticket *ticket) {
	return card->PurchaseTicket(this, ticket);
}

ErrorConstants SCSProReader::GetTicket(Card *card, Ticket *ticket) {
	return card->GetTicket(this, ticket);
}

ErrorConstants SCSProReader::ActivatePass(Card *card, Pass *pass) {
	return card->ActivatePass(this, pass);
}

ErrorConstants SCSProReader::UsePass(Card *card, Pass *pass) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

ErrorConstants SCSProReader::GetPass(Card *card, Pass *pass,PassCriteria *passCriteria) {
	return card->GetPass(this, pass,passCriteria);
}

ErrorConstants SCSProReader::GetTransactions(Card *card,TransactionsList *transactionsList,TransactionsCriteria *transactionsCriteria) {
	return card->GetTransactions(this, transactionsList,transactionsCriteria);
}
ErrorConstants SCSProReader::EncryptData(Card *card, unsigned char data[16]) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}
