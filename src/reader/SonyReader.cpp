#pragma comment(lib, "winscard.lib")
#define UNICODE

#include <winscard.h>
#include <stdio.h>
#include <conio.h>
#include <stdbool.h>
#include "reader/Reader.h"
#include "sam/SAM.h"
#include "card/Card.h"
#include "../include/Util.h"
#include "reader/SonyReader.h"
#include "model/enum/ReaderType.h"
#include "model/constants/ErrorConstants.h"
#include "model/PassCriteria.h"
#include "model/TransactionsCriteria.h"
#include "model/TransactionsList.h"
using namespace std;


wchar_t CARD_IF_PASORI[] = L"Sony FeliCa Port/PaSoRi 3.0 0";

wchar_t SAM_IF_GEM[] = L"Gemalto USB Smart Card Reader 0";

SAM* SonyReader::samRefSlot1 = NULL;
SCARDCONTEXT SonyReader::g_hContext = NULL;
SCARDHANDLE	SonyReader::g_hCard = NULL;
SCARDHANDLE	SonyReader::g_hSAM = NULL;
LPTSTR SonyReader::g_mszReaders = NULL;


SonyReader::SonyReader()
{
	type = ReaderType::SONY_READER;
}

long SonyReader::initializeReader(int readerIndex)
{
	long			_ret = 0;
	unsigned long	pcchReaders = SCARD_AUTOALLOCATE;
	unsigned long	dwActProtocolSAM;
	LPTSTR			pReader;

	disconnectReader();

	printf("Initialize Reader\n");

	//Establish Context
	printf("Establish Context\n");
	_ret = SCardEstablishContext(SCARD_SCOPE_USER,
		NULL,
		NULL,
		&g_hContext);
	if (_ret != SCARD_S_SUCCESS) {
		printf(" -> Error\n");
		return APP_ERROR;
	}

	// List All Readers
	printf("List All Readers\n");
	_ret = SCardListReaders(g_hContext, NULL, (LPTSTR)&g_mszReaders, &pcchReaders);
	if (_ret != SCARD_S_SUCCESS) {
		printf(" -> Error\n");
		return APP_ERROR;
	}
	pReader = g_mszReaders;
	while ('\0' != *pReader) {
		printf(" %S\n", pReader);
		pReader = pReader + wcslen((wchar_t *)pReader) + 1;
	}

	SCardFreeMemory(g_hContext, g_mszReaders);
	
	// Connect to SAM interface
	printf("Connect SAM\n");
	_ret = SCardConnect(g_hContext,
		SAM_IF_GEM,
		SCARD_SHARE_SHARED,
		SCARD_PROTOCOL_T1,
		&g_hSAM,
		&dwActProtocolSAM
	);
	printf("RET = %ld", _ret);
	if (_ret != SCARD_S_SUCCESS) {
		printf(" -> Error\n");
		return APP_ERROR;
	}
	samRefSlot1 =  initializeSAM();
	if (samRefSlot1 != NULL)
	{
		samRefSlot1->hostAuthentication(this);
	}
	
	printf("Sam initialized\n");
	
	return APP_SUCCESS;
}

long SonyReader::disconnectReader()
{

	SCardDisconnect(g_hCard, SCARD_UNPOWER_CARD);
	SCardDisconnect(g_hSAM, SCARD_UNPOWER_CARD);

	SCardReleaseContext(g_hContext);
	return APP_SUCCESS;
}

long SonyReader::sendToSAM(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int SAMType)
{
	long _ret = 0;
	unsigned char _send_buf[262];
	unsigned long _send_len;
	unsigned char tmpBuf[262];
	unsigned long tmpBufLen;
	unsigned char sw1, sw2;
	unsigned int i;

	
	for (i = 0; i<cmdLen; i++) {
		_send_buf[i] = cmdBuf[i];
	}
	_send_len = cmdLen ;
	tmpBufLen = *resLen;

#ifdef DEBUG
	PrintHexArray("PC->SAM: ", _send_len, _send_buf);
#endif

	_ret = SCardTransmit(g_hSAM,
		NULL,
		_send_buf,
		_send_len,
		NULL,
		tmpBuf,
		&tmpBufLen);

	if (_ret != SCARD_S_SUCCESS)
	{
		printf("SCardTransmit Error\n");
		return APP_ERROR;
	}

#ifdef DEBUG
	PrintHexArray("SAM->PC: ", tmpBufLen, tmpBuf);
#endif
	sw1 = tmpBuf[tmpBufLen - 2];
	sw2 = tmpBuf[tmpBufLen - 1];

	if ((sw1 == 0x67) && (sw2 == 0x00)) {
		return RCS500_LCLE_ERROR;
	}
	if ((sw1 == 0x6A) && (sw2 == 0x86)) {
		return RCS500_P1P2_ERROR;
	}
	if ((sw1 == 0x6D) && (sw2 == 0x00)) {
		return RCS500_INS_ERROR;
	}
	if ((sw1 == 0x6E) && (sw2 == 0x00)) {
		return RCS500_CLA_ERROR;
	}

	// Remove SW1/SW2
	*resLen = tmpBufLen - 2;
	for (i = 0; i<*resLen; i++) {
		resBuf[i] = tmpBuf[i];
	}

	if ((sw1 == 0x90) && (sw2 == 0x00)) {
		return SCARD_S_SUCCESS;
	}

	return UNKOWN_ERROR;
}

long SonyReader::sendToCard(unsigned char cmdBuf[], unsigned long cmdLen, unsigned char resBuf[], unsigned long* resLen, int cardType)
{
	long _ret = 0;
	unsigned char _send_buf[262];
	unsigned char receiveBuf[262];
	unsigned long _send_len, receiveLen;
	unsigned int i;
	int nIdx = 0;
	if (cmdLen > 0)
	{
		
		for (i = 0; i<cmdLen; i++)
		{
			_send_buf[nIdx + i] = cmdBuf[i];
		}
		nIdx += cmdLen;

		//_send_buf[nIdx++] = 0x00; //Le
	}

	_send_len = nIdx;
	
	receiveLen = 262;

#ifdef DEBUG
	PrintHexArray("PC->CARD: ", _send_len, _send_buf);
#endif

	_ret = SCardTransmit(g_hCard,
		NULL,
		_send_buf,
		_send_len,
		NULL,
		receiveBuf,
		&receiveLen);

	if (_ret != SCARD_S_SUCCESS) {
		printf("SCardTransmit Error\n");
		return CARD_READER_ERROR;
	}

#ifdef DEBUG
	PrintHexArray("CARD->PC: ", receiveLen, receiveBuf);
#endif
	memcpy(resBuf, receiveBuf, receiveLen);
	*resLen = receiveLen - 2;

	return CARD_DATA_TRANSMIT_SUCCEEDED;
}

long SonyReader::detectCard(int *cardType)
{
	long			ret;
	unsigned int	nIdx = 0;

	//unsigned long	readerLen = 0;
	DWORD	readerLen = 0;
	wchar_t tempReadName[256];

	unsigned long	ATRLen;
	unsigned long	dwState=0;

	unsigned char	ATRVal[262] = { 0x00 };
	unsigned long	dwActProtocol;

	unsigned char	_send_buf[262] = { 0x00 };
	unsigned char	receiveBuf[262] = { 0x00 };

	unsigned long	_send_len = 0L;
	unsigned long	receiveLen = 0L;


	printf("\nTap Card\n");
	printf("<Press ESC Key to cancel>\n");
	while (1)
	{

		// Hit Esc to cancel operation
		if ((0 != kbhit()) && (SMPL_ESC_KEY == getch()))
		{
			printf("  -> Canceled\n");
			return APP_CANCEL;
		}	
		ret = SCardConnect(g_hContext,
			CARD_IF_PASORI,
			SCARD_SHARE_SHARED,
			SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
			&g_hCard,
			&dwActProtocol
		);
		if (ret != SCARD_S_SUCCESS)
		{
			if (Reader::cardTapped)
			{
				return APP_CANCEL;
			}
			continue;
			Sleep(1000);
		}
		Reader::cardTapped = this;
		Sleep(500);
		//Get ATR value to check status
		readerLen = sizeof(CARD_IF_PASORI);
		memset(tempReadName, 0x00, 256);
		memcpy(tempReadName, CARD_IF_PASORI, readerLen);

		ATRLen = (unsigned long)32;
		printf("dwState = %ld\n", dwState);
		ret = SCardStatus(g_hCard,
			tempReadName,
			&readerLen,
			&dwState,
			&dwActProtocol,
			ATRVal,
			&ATRLen);
		printf("dwState = %ld\n", dwState);
		printf("dwActProtocol = %ld", dwActProtocol);
		PrintHexArray("CARD ATR ", ATRLen, ATRVal);
		printf("ret = %ld", ret);
		//memcpy(atrVal, ATRVal, ATRLen);
		if (ret != SCARD_S_SUCCESS)
		{
			return APP_ERROR;
		}

		*cardType = Card::unknownCard;
		unsigned char desfireATR[] = { 0x3B,0x89,0x80,0x01,0x80,0x5F,0x06,0x06,0x75,0x77,0x81,0x02,0x80,0xD6 };
		unsigned char desfireATR1[] = { 0x3B,0x81 ,0x80 ,0x01 ,0x80 ,0x80 };
		if (Equals(desfireATR, 14, ATRVal, ATRLen))
		{
			*cardType = Card::DESFIRE_EV1_CARD;
		}
		if (Equals(desfireATR1, 6, ATRVal, ATRLen))
		{
			*cardType = Card::DESFIRE_EV1_CARD;
		}
		break;
	}

	return APP_SUCCESS;
} 

// detectInstantCard
long SonyReader::detectInstantCard(int *cardType)
{
	long			ret;
	unsigned int	nIdx = 0;
	int i = 0;

	//unsigned long	readerLen = 0;
	DWORD	readerLen = 0;
	wchar_t tempReadName[256];

	unsigned long	ATRLen;
	unsigned long	dwState=0;

	unsigned char	ATRVal[262] = { 0x00 };
	unsigned long	dwActProtocol;

	unsigned char	_send_buf[262] = { 0x00 };
	unsigned char	receiveBuf[262] = { 0x00 };

	unsigned long	_send_len = 0L;
	unsigned long	receiveLen = 0L;


	printf("\nTap Card\n");
	printf("<Press ESC Key to cancel>\n");
	for (i=0; i<=3; i++)
	{

		// Hit Esc to cancel operation
		if ((0 != kbhit()) && (SMPL_ESC_KEY == getch()))
		{
			printf("  -> Canceled\n");
			return APP_CANCEL;
		}	
		ret = SCardConnect(g_hContext,
			CARD_IF_PASORI,
			SCARD_SHARE_SHARED,
			SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
			&g_hCard,
			&dwActProtocol
		);
		if (ret != SCARD_S_SUCCESS)
		{
			if (Reader::cardTapped)
			{
				return APP_CANCEL;
			}
			continue;
			Sleep(1000);
		}
		Reader::cardTapped = this;
		Sleep(500);
		//Get ATR value to check status
		readerLen = sizeof(CARD_IF_PASORI);
		memset(tempReadName, 0x00, 256);
		memcpy(tempReadName, CARD_IF_PASORI, readerLen);

		ATRLen = (unsigned long)32;
		printf("dwState = %ld\n", dwState);
		ret = SCardStatus(g_hCard,
			tempReadName,
			&readerLen,
			&dwState,
			&dwActProtocol,
			ATRVal,
			&ATRLen);
		printf("dwState = %ld\n", dwState);
		printf("dwActProtocol = %ld", dwActProtocol);
		PrintHexArray("CARD ATR ", ATRLen, ATRVal);
		printf("ret = %ld", ret);
		//memcpy(atrVal, ATRVal, ATRLen);
		if (ret != SCARD_S_SUCCESS)
		{
			return APP_ERROR;
		}

		*cardType = Card::unknownCard;
		unsigned char desfireATR[] = { 0x3B,0x89,0x80,0x01,0x80,0x5F,0x06,0x06,0x75,0x77,0x81,0x02,0x80,0xD6 };
		unsigned char desfireATR1[] = { 0x3B,0x81 ,0x80 ,0x01 ,0x80 ,0x80 };
		if (Equals(desfireATR, 14, ATRVal, ATRLen))
		{
			*cardType = Card::DESFIRE_EV1_CARD;
		}
		if (Equals(desfireATR1, 6, ATRVal, ATRLen))
		{
			*cardType = Card::DESFIRE_EV1_CARD;
		}
		break;
	}

	return APP_SUCCESS;
}

SAM* SonyReader::initializeSAM() {
	int samType = SAM::MIFARE_AV2_SAM;
	//TODO: check desfire or felica SAM with _sam_res(ATR)
	SAM *sam = SAM::getInstance(samType);
	return sam;
}

bool SonyReader::isCardPresent() {
	long			lReturn,ret;
	unsigned long	dwActProtocol;
	lReturn = SCardConnect(g_hContext,
		CARD_IF_PASORI,
		SCARD_SHARE_SHARED,
		SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
		&g_hCard,
		&dwActProtocol
	);
	printf("retRETRETEERERERE = %ld", lReturn);
	DWORD	readerLen = 0;
	wchar_t tempReadName[256];

	unsigned long	ATRLen;
	unsigned long	dwState = 0;

	unsigned char	ATRVal[262] = { 0x00 };
	readerLen = sizeof(CARD_IF_PASORI);
	memset(tempReadName, 0x00, 256);
	memcpy(tempReadName, CARD_IF_PASORI, readerLen);
	ret = SCardStatus(g_hCard,
		CARD_IF_PASORI,
		&readerLen,
		&dwState,
		&dwActProtocol,
		ATRVal,
		&ATRLen);
	printf("dwState = %ld\n", dwState);
	printf("dwActProtocol = %ld", dwActProtocol);
	PrintHexArray("CARD ATR ", ATRLen, ATRVal);
	if (lReturn != SCARD_S_SUCCESS)
	{
		return false;
	}
	else {
		return true;
	}

}

long SonyReader::DisconnectFeliCaCard(void){

	// turn RF off
	return APP_ERROR;
};

bool SonyReader::isCardRemoved(){
	printf("checking removal of card\n");
	if(cancelDetection == true){
			cancelDetection = false;
			return false;
		}

	return true;
}

bool SonyReader::stopDetection(){
	printf("checking removal of card\n");
	cancelDetection = true;
	return true;
}

long SonyReader::changeObject (SAM *sam){
	// if(samRefSlot1!=NULL && samRefSlot1->type == SAM::FELICA_RCS500_SAM){
	// 	printf("slot 1 obj reinit\n");
	// 	samRefSlot1=sam;
	// 	return APP_SUCCESS;
	// }else if(samRefSlot2 !=NULL && samRefSlot2->type == SAM::FELICA_RCS500_SAM){
	// 	printf("slot 2 obj reinit\n");
	// 	samRefSlot2=sam;
	// 	return APP_SUCCESS;
	// }
	return APP_ERROR;
}

ErrorConstants SonyReader::GetBalance(Card *card, long *amount) {
	return card->GetBalance(this, amount);
}

ErrorConstants SonyReader::GetCardDetails(Card *card, CardDetail *cardDetail) {
	return card->GetCardDetails(this, cardDetail);
}

ErrorConstants SonyReader::GetGenericData(Card *card, GenericData *genericData) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

ErrorConstants SonyReader::ActivateCard(Card *card, CardActivation *cardActivation) {
	return card->ActivateCard(this, cardActivation);
}

ErrorConstants SonyReader::UpdatePersonalData(Card *card, PersonalData *personalData) {
	return card->UpdatePersonalData(this, personalData);
}

ErrorConstants SonyReader::UpdateVehicleData(Card *card, VehicleData *vehicleData) {
	return card->UpdateVehicleData(this, vehicleData);
}

ErrorConstants SonyReader::UpdateCardStatus(Card *card, CardStatus *cardStatus) {
	return card->UpdateCardStatus(this, cardStatus);
}

ErrorConstants SonyReader::Pay(Card *card, Activity *activity) {
	return card->Pay(this, activity);
}

ErrorConstants SonyReader::Recharge(Card *card, Activity *activity) {
	return card->Recharge(this, activity);
}

ErrorConstants SonyReader::PurchaseTicket(Card *card, Ticket *ticket) {
	return card->PurchaseTicket(this, ticket);
}

ErrorConstants SonyReader::GetTicket(Card *card, Ticket *ticket) {
	return card->GetTicket(this, ticket);
}

ErrorConstants SonyReader::ActivatePass(Card *card, Pass *pass) {
	return card->ActivatePass(this, pass);
}

ErrorConstants SonyReader::UsePass(Card *card, Pass *pass) {
	return card->UsePass(this, pass);
}

ErrorConstants SonyReader::GetPass(Card *card, Pass *pass,PassCriteria *passCriteria) {
	return card->GetPass(this, pass,passCriteria);
}

ErrorConstants SonyReader::GetTransactions(Card *card,TransactionsList *transactionsList,TransactionsCriteria *transactionsCriteria) {
	return card->GetTransactions(this,transactionsList ,transactionsCriteria);
}
ErrorConstants SonyReader::EncryptData(Card *card, unsigned char data[16]) {
	return ErrorConstants::FUNCTION_NOT_SUPPORTED;
}

