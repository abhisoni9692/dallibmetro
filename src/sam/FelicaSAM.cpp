#pragma comment(lib, "winscard.lib")
#define UNICODE

#include <winscard.h>
#include<stdio.h>
#include "sam/SAM.h"
#include "reader/Reader.h"
#include "../include/Util.h"
#include "Parameter.h"
#include "openssl/cmac.h"
#include "sam/FelicaSAM.h"
#include "../include/Util.h"

FelicaSAM::FelicaSAM()
{
	type = SAM::FELICA_RCS500_SAM;
}


SCARDHANDLE		g_hSAM;
// Transaction data to communicate with SAM
unsigned char g_RwSamNumber[8];
unsigned char Rar[16];
unsigned char Rbr[16];
unsigned char Rcr[4];
unsigned char KYtr[16];	// Transaction Key
unsigned char Snr[4];	// Sequence number

/** AES key for authentication between PC and SAM */
AES_KEY _auth_key;

/** AES key to encrypt transaction packets between PC and SAM */
AES_KEY _tran_key;

/** AES key to generate MAC for transaction between PC and SAM */
AES_KEY _mac_key;

//========================================
// Extern
//----------------------------------------
extern SCARDCONTEXT	g_hContext;
extern SCARDHANDLE	g_hCardReader;;
extern wchar_t CARD_IF_SDR10[];

//========================================


//========================================
// Public functions
//========================================


long FelicaSAM::hostAuthentication(Reader *reader,bool AuthOnlyFlag)
{
	long			ret = 0;
	unsigned char	_sam_res[262];
	unsigned long	_sam_res_len;
	unsigned int	nIdx = 0;
	unsigned char	_send_buf[262];
	unsigned long	_send_len;


	if(AuthOnlyFlag==false){
	PrintText("Initialize Felica SAM");

		//Send Set RWSAM Mode command to RC-S500
	PrintText("Send RWSAM Mode");
	ret = _send_SetNormalMode(_sam_res, &_sam_res_len, reader);
	if (ret != SAM_COMMAND_SUCCEEDED)
	{
		PrintText(" -> Error");

		_DisconnectWithSAMInDirectMode();
		return ret;
	}

	//Send Attention command to RC-S500
	PrintText("Send Attention");
	ret = _send_Attention(_sam_res, &_sam_res_len, reader);
	if (ret != SAM_COMMAND_SUCCEEDED)
	{
		PrintText(" -> Error");

		_DisconnectWithSAMInDirectMode();
		return ret;
	}
	
	//Set RW SAM ID
	memcpy(g_RwSamNumber, &_sam_res[4], 8);
}
	//Send Authentication 1 to RC-S500
	PrintText("Send Auth1");
	ret = _send_Auth1(_sam_res, &_sam_res_len, reader);
	if (ret != SAM_COMMAND_SUCCEEDED)
	{
		PrintText(" -> Error");

		_DisconnectWithSAMInDirectMode();
		return ret;
	}

	//Check Authentication 1 response
	ret = _check_Auth1_result(_sam_res, _sam_res_len);
	if (ret != APP_SUCCESS)
	{
		PrintText(" -> Error");

		_DisconnectWithSAMInDirectMode();
		return APP_ERROR;
	}

	//Send Authentication 2 to RC-S500
	PrintText("Send Auth2");
	ret = _send_Auth2(_sam_res, &_sam_res_len, reader);
	if (ret != SAM_COMMAND_SUCCEEDED)
	{
		PrintText(" -> Error");

		_DisconnectWithSAMInDirectMode();
		return ret;
	}

	//Check Authentication 2 response
	ret = _check_Auth2_result(_sam_res, _sam_res_len);
	if (ret != SAM_COMMAND_SUCCEEDED)
	{
		PrintText(" -> Error");

		_DisconnectWithSAMInDirectMode();
		return APP_ERROR;
	}

	return APP_SUCCESS;
}


long FelicaSAM::getVersion(Reader *reader) {
	unsigned char apdu[256], samres[256];
	//memcpy(apdu,Cmd::samVersion,sizeof(Cmd::samVersion));
	unsigned long ret, samres_len = 256;
	PrintHexArray("SAM VERSION: ", 5, apdu);
	ret = reader->sendToSAM(apdu, 5, samres, &samres_len, 2);

	if (ret != APP_SUCCESS) {
		return APP_ERROR;
	}
	return APP_SUCCESS;
}

long AskFeliCaCmdToSAM(unsigned char commandCode, 
					   unsigned long felicaCmdParamsLen,
					   unsigned char felicaCmdParams[],
					   unsigned long* felicaCommandLen,
					   unsigned char felicaCommand[],
					   Reader *reader)
{
	return AskFeliCaCmdToSAMSC(commandCode,
							 0x00,
							 felicaCmdParamsLen,
							 felicaCmdParams,
							 felicaCommandLen,
							 felicaCommand,
							 reader);
}

long AskFeliCaCmdToSAMSC(unsigned char commandCode, 
					   unsigned char subCommandCode, 
					   unsigned long felicaCmdParamsLen,
					   unsigned char felicaCmdParams[],
					   unsigned long* felicaCommandLen,
					   unsigned char felicaCommand[],
					   Reader *reader)
{
	long			ret;
	unsigned char	_payload[512], _mac[8], _send_buf[262], _sam_res[262];
	unsigned long	_send_len, _sam_res_len;
	unsigned int	nIdx = 0;

	long returnValue = checkAndHandleForSNROverFlow(reader);
	if (returnValue != APP_SUCCESS)
	{
		return returnValue;
	}

	// Encrypt command payload data
	encrypt_payload(commandCode, subCommandCode, felicaCmdParamsLen, felicaCmdParams, _payload, _mac);

	//construct command packet sent to SAM
	_send_buf[nIdx++] = 0x00;			// Dispatcher
	_send_buf[nIdx++] = 0x00;			// Reserved
	_send_buf[nIdx++] = 0x00;			// Reserved
	_send_buf[nIdx++] = commandCode;	// Command Code
	_send_buf[nIdx++] = subCommandCode;	// Sub Command Code
	_send_buf[nIdx++] = 0x00;			// Reserved
	_send_buf[nIdx++] = 0x00;			// Reserved
	_send_buf[nIdx++] = 0x00;			// Reserved
	memcpy(&_send_buf[nIdx], Snr, 4);	// Snr
	nIdx += 4;
	memcpy(&_send_buf[nIdx], _payload, felicaCmdParamsLen);	// encrypted data
	nIdx += felicaCmdParamsLen;
	memcpy(&_send_buf[nIdx], _mac, 8);	// encrypted MAC
	nIdx += 8;

	_send_len = nIdx;
	_sam_res_len = 0xFF;

	// Send packets to SAM
	ret = reader-> sendToSAM(_send_buf, _send_len, _sam_res, &_sam_res_len, SAM::FELICA_RCS500_SAM);
	if (ret != SAM_DATA_TRANSMIT_SUCCEEDED)
	{

		PrintText("AskFeliCaCmdToSAMSC - TransmitDataToSAM; failed with error code %ld", ret);
		return ret;
	}

	long hasSyntaxError = checkAndHandleSyntaxErrorInSamResponse(_sam_res, &_sam_res_len,reader);
	if (hasSyntaxError != SAM_RESPONSE_NO_SYNTAX_ERROR)
	{
		return hasSyntaxError;
	}

	// Extract FeliCa command packets from SAM response
	*felicaCommandLen = _sam_res_len - 3;
	memcpy(felicaCommand, &_sam_res[3], *felicaCommandLen);

	return SAM_COMMAND_SUCCEEDED;
}

long SendRWSAMCmd(unsigned char commandCode,
				  unsigned char subcommndCode,
				  unsigned long felicaCmdParamsLen,
				  unsigned char felicaCmdParams[],
				  unsigned long* felicaCommandLen,
				  unsigned char felicaCommand[],
				  Reader *reader)
{
	unsigned char	_sam_res[262];
	unsigned long	_sam_res_len;
	long ret;

	ret = AskFeliCaCmdToSAMSC(commandCode, subcommndCode, felicaCmdParamsLen, felicaCmdParams, &_sam_res_len, _sam_res, reader);
	if (ret != APP_SUCCESS)
	{
		return ret;
	}

	// decrypt and verify SAM response
	ret = _decrypt_sam_response(_sam_res, _sam_res_len,felicaCommand );
	*felicaCommandLen = _sam_res_len - (1+1+3+4) - 8;

	return ret;	

}

long SendPollingResToSAM(unsigned long felicaResLen,
						 unsigned char felicaResponse[], Reader *reader)
{
	long			ret = 0;
	unsigned char	_send_buf[262], _sam_res[262];
	unsigned long	_send_len, _sam_res_len;
	unsigned char	_result[256];
	unsigned int	nIdx = 0;

	//Send back the response from FeliCa Card to RW-SAM(RC-S500)
	_send_buf[nIdx++] =	0x01;							// Dispatcher
	_send_buf[nIdx++] =	0x00;							// Reserved
	_send_buf[nIdx++] =	0x00;							// Reserved
	_send_buf[nIdx++] =	0x01;							// Reserved
	_send_buf[nIdx++] = 0x01;							// Number of Target 1<=n<=4
	memcpy(&_send_buf[nIdx], &felicaResponse[1], 8);	// IDm
	nIdx += 8;
	memcpy(&_send_buf[nIdx], &felicaResponse[1+8], 8);	// PMm
	nIdx += 8;

	_send_len = nIdx;
	_sam_res_len = 0xFF;

	// Send packets to SAM
	ret = reader -> sendToSAM(_send_buf, _send_len, _sam_res, &_sam_res_len, SAM::FELICA_RCS500_SAM);
	if (ret != SAM_DATA_TRANSMIT_SUCCEEDED)
	{
		printf("Error in data from SAM");
		return ret;
	}

	long hasSyntaxError = checkAndHandleSyntaxErrorInSamResponse(_sam_res, &_sam_res_len,reader);
	if (hasSyntaxError != SAM_RESPONSE_NO_SYNTAX_ERROR)
	{
		return hasSyntaxError;
	}

	//Check Result
	ret = _decrypt_sam_response(&_sam_res[1+2], _sam_res_len-(1+2), _result);
	if(ret != DECRYPTION_SUCCEEDED){
			return ret;
	}
	return SAM_COMMAND_SUCCEEDED;
}

long SendAuth1V2ResultToSAM(unsigned long felicaResLen, 
							unsigned char felicaResponse[], 
							unsigned long* auth2V2CommandLen,
							unsigned char auth2V2Command[],
							Reader *reader)
{
	long			ret = 0;
	unsigned char	_send_buf[262], _sam_res[262];
	unsigned long	_send_len, _sam_res_len;
	unsigned int	nIdx = 0;

	//Send back the response from FeliCa Card to RW-SAM(RC-S500)
	_send_buf[nIdx++] =	0x01;	// Dispatcher
	_send_buf[nIdx++] =	0x00;	// Reserved
	_send_buf[nIdx++] = 0x00;	// Reserved
	memcpy(&_send_buf[nIdx], felicaResponse, felicaResLen); //response of FeliCa Card
	nIdx += felicaResLen;

	_send_len = nIdx;
	_sam_res_len = 0xFF;

	// Send packets to SAM
	ret = reader -> sendToSAM(_send_buf, _send_len, _sam_res, &_sam_res_len, SAM::FELICA_RCS500_SAM);
	if (ret != SAM_DATA_TRANSMIT_SUCCEEDED)
	{
		PrintText("Card result Error\n");
		return APP_ERROR;
	}

	*auth2V2CommandLen = _sam_res_len - 3;
	memcpy(auth2V2Command, &_sam_res[3], *auth2V2CommandLen);

	return ret;
}


long SendCardResultToSAM(unsigned long felicaResLen, 
						 unsigned char felicaResponse[], 
						 unsigned long* resultLen, 
						 unsigned char result[],
						 Reader *reader)
{
	long			ret = 0;
	unsigned char	_send_buf[512], _sam_res[262];
	unsigned long	_send_len, _sam_res_len;
	unsigned int	nIdx = 0;

	//Send back the response from FeliCa Card to RW-SAM(RC-S500)
	_send_buf[nIdx++] =	0x01; // Dispatcher
	_send_buf[nIdx++] =	0x00; // Reserved
	_send_buf[nIdx++] =	0x00; // Reserved
	memcpy(&_send_buf[nIdx], felicaResponse, felicaResLen);	//response of FeliCa Card
	nIdx += felicaResLen;

	_send_len = nIdx;
	_sam_res_len = 0xFF;
	
#ifdef DEBUG
	PrintText("\n     - TransmitDataToSAM \n");
	PrintHexArray("Snr= ", 4, Snr);
#endif

	// Send packets to SAM
	ret = reader-> sendToSAM(_send_buf, _send_len, _sam_res, &_sam_res_len,  SAM::FELICA_RCS500_SAM);
	if (ret != SAM_DATA_TRANSMIT_SUCCEEDED)
	{
		PrintText("Card result Error\n");
		return APP_ERROR;
	}

	long hasSyntaxError = checkAndHandleSyntaxErrorInSamResponse(_sam_res, &_sam_res_len,reader);
	if (hasSyntaxError != SAM_RESPONSE_NO_SYNTAX_ERROR)
	{
		return hasSyntaxError;
	}
	
#ifdef DEBUG
	PrintText("\n     - _decrypt_sam_response \n");
	PrintHexArray("Snr= ", 4, Snr);
#endif
	// decrypt and verify SAM response
	ret = _decrypt_sam_response(&_sam_res[1+2], _sam_res_len-(1+2), result);
	if(ret != DECRYPTION_SUCCEEDED){
		return ret;
	}
	*resultLen = _sam_res_len - (1+2+1+1+3+4) - 8;

	return SAM_COMMAND_SUCCEEDED;
}



long SendCardErrorToSAM(unsigned long* resultLen, 
						unsigned char result[],
						Reader *reader)
{
	long			ret = 0;
	unsigned char	_send_buf[262], _sam_res[262];
	unsigned long	_send_len, _sam_res_len;
	unsigned int	nIdx = 0;
	
#ifdef DEBUG
	PrintText("\n SendCardErrorToSAM --- \n");
#endif

	// "card No Response packet"
	_send_buf[nIdx++] = 0x01; // Dispatcher
	_send_buf[nIdx++] = 0x00; // Reserved
	_send_buf[nIdx++] = 0x00; // Reserved

	_send_len = nIdx;
	_sam_res_len = 0xFF;

	// Send packets to SAM
	ret = reader-> sendToSAM(_send_buf, _send_len, _sam_res, &_sam_res_len,  SAM::FELICA_RCS500_SAM);
	if (ret != SAM_DATA_TRANSMIT_SUCCEEDED)
	{
		return APP_ERROR;
	}

	return SAM_DATA_TRANSMIT_SUCCEEDED;
}

//========================================
// private functions
//========================================


/**
Send Authentication 1 command to RC-S500.

\return PC/SC API Return Values
*/
long FelicaSAM:: _send_Auth1(unsigned char samResBuf[], unsigned long* samResLen, Reader *reader)
{
	long			hasSyntaxError, ret = 0;
	unsigned char	_send_buf[262];
	unsigned long	_send_len;
	unsigned int	nIdx = 0;

	//Send Authentication1 command
	_send_buf[nIdx++] = 0x00; // Dispatcher
	_send_buf[nIdx++] = 0x00; // Reserved
	_send_buf[nIdx++] = 0x00; // Reserved
	_send_buf[nIdx++] = 0x02; // Command code
	_send_buf[nIdx++] = 0x00; // Reserved
	_send_buf[nIdx++] = 0x00; // Reserved
	//Set RW SAM ID
	memcpy(&_send_buf[nIdx], g_RwSamNumber, 8);
	nIdx += 8;
	//Generate Rar
	GenerateRandom(Rar,16);
	//Set Rar
	memcpy(&_send_buf[nIdx], Rar, 16);
	nIdx += 16;

	_send_len = nIdx;
	*samResLen = 1+2+1+32+4 + 2; // Response Data(1+2+1+32+4) + SW1 SW2
	ret = reader-> sendToSAM(_send_buf, _send_len, samResBuf, samResLen, SAM::FELICA_RCS500_SAM);
	hasSyntaxError = checkAndHandleSyntaxErrorInSamResponse(samResBuf, samResLen,reader);
	if(hasSyntaxError != SAM_RESPONSE_NO_SYNTAX_ERROR){
		return hasSyntaxError;
	}
	return SAM_COMMAND_SUCCEEDED;
}

/**
Check Authentication 1 response of SAM

\return  APP_SUCCESS, APP_ERROR
*/
long FelicaSAM:: _check_Auth1_result(unsigned char samResBuf[], unsigned long samResLen)
{
	unsigned char	_Kab[16];
	unsigned char	_received_Rar[16];
	unsigned char	_encrypted_M2r[32];
	unsigned char	_decrypted_M2r[32];
	unsigned char	_iv[16];
	int i = 0;
	
	//extract M2r(encrypted)
	memcpy(_encrypted_M2r, &samResBuf[4], 32);

	//extract Rcr
	memcpy(Rcr, &samResBuf[4+32], 4);

	// generate Kab
	for (i = 0; i < 4; i++)
	{
		_Kab[i] = AuthNormalKey[i] ^ Rcr[i];
	}
	for (i = 4; i < 16; i++)
	{
		_Kab[i] = AuthNormalKey[i];
	}

	//decrypt M2r
	AES_set_decrypt_key(_Kab, 128, &_auth_key);
	memset (_iv, 0, sizeof (_iv));
    memset (_decrypted_M2r, 0, 32);
    AES_cbc_encrypt (_encrypted_M2r, _decrypted_M2r, 32, &_auth_key, _iv, AES_DECRYPT);

	//extract Rar, Rbr
	memcpy(Rbr, _decrypted_M2r, 16);				// Rbr
	memcpy(_received_Rar, &_decrypted_M2r[16], 16);	// Rar

	//compare Rar
	for (i = 0; i < 16; i++)
	{
		if (Rar[i] != _received_Rar[i])
		{
			return APP_ERROR;
		}
	}

	return APP_SUCCESS;
}


/**
Send Authentication 2 command to SAM.

\return PC/SC API Return Values
*/
long FelicaSAM:: _send_Auth2(unsigned char samResBuf[], unsigned long* samResLen, Reader *reader)
{
 	long			hasSyntaxError, ret = 0;
	unsigned char	_buf[32];
	unsigned char	_Kab[16];
	unsigned char	_iv[16];
	unsigned char	_M3r[32];
	unsigned char	_send_buf[262];
	unsigned long	_send_buf_len;
	unsigned int	nIdx = 0;
	int i = 0;

	// concatinate Rar, Rbr
	memcpy(&_buf[0], Rar, 16);
	memcpy(&_buf[16], Rbr, 16);

	// generate Kab
	for (i = 0; i < 4; i++)
	{
		_Kab[i] = AuthNormalKey[i] ^ Rcr[i];
	}
	for (i = 4; i < 16; i++)
	{
		_Kab[i] = AuthNormalKey[i];
	}
	
	//encrypt M3r
	AES_set_encrypt_key(_Kab, 128, &_auth_key);
	memset (_iv, 0, sizeof (_iv));
    memset (_M3r, 0, 32);
    AES_cbc_encrypt (_buf, _M3r, 32, &_auth_key, _iv, AES_ENCRYPT);

	//Send Authentication2 command
	_send_buf[nIdx++] = 0x00;					// Dispatcher
	_send_buf[nIdx++] = 0x00;					// Reserved
	_send_buf[nIdx++] = 0x00;					// Reserved
	_send_buf[nIdx++] = 0x04;					// Command code
	_send_buf[nIdx++] = 0x00;					// Reserved
	_send_buf[nIdx++] = 0x00;					// Reserved
	memcpy(&_send_buf[nIdx], g_RwSamNumber, 8);	// RW SAM ID
	nIdx += 8;
	memcpy(&_send_buf[nIdx], _M3r, 32);			// M3r
	nIdx += 32;

	_send_buf_len = nIdx;
	*samResLen = 1+2+1+1 + 2; //Response Data(1+2+1+1) + SW1 SW2

	ret = reader -> sendToSAM(_send_buf, _send_buf_len, samResBuf, samResLen, SAM::FELICA_RCS500_SAM);
	hasSyntaxError=checkAndHandleSyntaxErrorInSamResponse(samResBuf,samResLen,reader);
	if(hasSyntaxError != SAM_RESPONSE_NO_SYNTAX_ERROR){
		return hasSyntaxError;
	}
	return SAM_COMMAND_SUCCEEDED;
}

/**
Check Authentication 2 response of SAM.

\return  APP_SUCCESS, APP_ERROR
*/
long FelicaSAM:: _check_Auth2_result(unsigned char samResBuf[], unsigned long samResLen)
{
	if (samResBuf[4] != 0x00)
	{
		return APP_ERROR;
	}

	// Initialize Snr (Initial value for RC-S500 driver is 1 )
	Snr[0] = 0x01;
	Snr[1] = 0x00;
	Snr[2] = 0x00;
	Snr[3] = 0x00;

	// if(AuthOnlyFlag){
	// Snr[0] = 0x01;
	// Snr[1] = 0x00;
	// Snr[2] = 0x00;
	// Snr[3] = 0x00;
	// }
	// Set KYtr
	memcpy(KYtr, Rbr, 16);

	return SAM_COMMAND_SUCCEEDED;
}


// /**
// Calculate CBC-MAC of message.

// \param [in] key		MAC key
// \param [in] msg		message data to calculate MAC
// \param [in] msgLen	length of "msg" 
// \param [out] MAC	calculated MAC
// */
void  _calc_mac(	unsigned char key[16], unsigned char msg[], unsigned long msgLen, unsigned char MAC[16])
{
	unsigned long	_enc_size;
	unsigned char	_last_block[16];
	unsigned char	_encypted_msg[16] = {0};
	AES_KEY			_aes_key;
	unsigned long i;
		
	AES_set_encrypt_key(key, 128, &_aes_key);

	// Copy last block data for Zero Padding
	if (msgLen == 0)
	{
		return;
	}
	else if (msgLen % 16 == 0)
	{
		_enc_size = msgLen - 16;
		memcpy(_last_block, &msg[_enc_size], 16);
	}
	else
	{
		_enc_size = (msgLen / 16) * 16;
		memset(_last_block, 0x00, 16);
		memcpy(_last_block, &msg[_enc_size], msgLen % 16);
	}
	
	// CBC encryption
	for (i = 0; i < _enc_size; i += 16)
	{
		AES_cbc_encrypt(&msg[i], _encypted_msg, 16, &_aes_key, _encypted_msg, AES_ENCRYPT);
	}
	AES_cbc_encrypt(_last_block, MAC, 16, &_aes_key, _encypted_msg, AES_ENCRYPT);
	
	return;
}


/**
Encrypt command payload to SAM.

\param commandCode				Command code for SAM
\param subCommandCode			Sub command code for SAM
\param [in] felicaCmdParamsLen	length of "felicaCmdParams"
\param [in] felicaCmdParams		command parameters after Snr of command gereration request packtes to SAM
\param [out] payload			encrypted payload to SAM
\param [out] mac				encrypted MAC to SAM

\return  APP_SUCCESS, APP_ERROR
*/
void  encrypt_payload (unsigned char commandCode, 
						  unsigned char subCommandCode, 
						  unsigned long felicaCmdParamsLen,
						  unsigned char felicaCmdParams[],
						  unsigned char payload[],
						  unsigned char	mac[8])
{
	unsigned char	_b0[16], _b1[16], _raw_mac[16], _ctr_block[16], _iv[16], work_buf[256];
	AES_KEY			_aes_key;
	unsigned int	_num;

#ifdef DEBUG
	PrintHexArray("PC->SAM [Raw Payload]: ", felicaCmdParamsLen, felicaCmdParams);
	PrintHexArray("                [Snr]: ", 4, Snr);
	PrintHexArray("                [Rar]: ", 16, Rar);
	PrintHexArray("                [Rcr]: ", 4, Rcr);
	PrintHexArray("               [KYtr]: ", 16, KYtr);
#endif

	// Create B0
	_b0[0] =			0x59;		// Flags
	memcpy(&_b0[1],		Snr, 4);	// Snr
	memcpy(&_b0[1+4],	Rcr, 4);	// Rcr
	memcpy(&_b0[1+4+4], Rar, 5);	// Rar
	_b0[14] = (unsigned char)(felicaCmdParamsLen / 0x0100);	// Packet data size
	_b0[15] = (unsigned char)(felicaCmdParamsLen % 0x0100);	// Packet data size

	// Create B1
    _b1[0] =				0x00;			// Associated Data Size
    _b1[1] =				0x09;			// Associated Data Size
	_b1[2] =				commandCode;	// Command Code
	_b1[2+1] =				subCommandCode;	// Sub Command Code
	_b1[2+1+1] =			0x00;			// Reserved
	_b1[2+1+1+1] =			0x00;			// Reserved
	_b1[2+1+1+2] =			0x00;			// Reserved
	memcpy(&_b1[2+1+1+3],	Snr, 4);		// Snr
	memset(&_b1[2+1+1+3+4],	0x00, 5);		// All Zero	

	// Generate Ctr1
	_ctr_block[0] =				0x01;		// Flags
	memcpy(&_ctr_block[1],		Snr, 4);	// Snr
	memcpy(&_ctr_block[1+4],	Rcr, 4);	// Rcr
	memcpy(&_ctr_block[1+4+4],	Rar, 5);	// Rar
	_ctr_block[14] =			0x00;		// i
    _ctr_block[15] =			0x01;		// i

	// Encrypt packet data
	memset(_iv, 0x00, 16);
	memset(payload, 0x00, 256);
	_num = 0;
	AES_set_encrypt_key(KYtr, 128, &_aes_key);
	AES_ctr128_encrypt(felicaCmdParams, payload, felicaCmdParamsLen, &_aes_key, _ctr_block, _iv, &_num);

    // Calc CBC-MAC
	memset(work_buf, 0x00, 256);
	memcpy(&work_buf[0],		_b0, 16);
	memcpy(&work_buf[16],		_b1, 16);
    memcpy(&work_buf[16+16],	felicaCmdParams, felicaCmdParamsLen);
	_calc_mac(KYtr, work_buf, 16+16+felicaCmdParamsLen, _raw_mac);
	
	// Encrypt MAC
    _ctr_block[14] = 0x00;
    _ctr_block[15] = 0x00;
	_num = 0;
	AES_ctr128_encrypt(_raw_mac, mac, 8, &_aes_key, _ctr_block ,_iv, &_num);
}

/**
Decrypt SAM response packets and verify MAC value.

\param [in] samResponse		SAM response packets (starts with response code, sub response code, ...) 
\param [in] samResLen		length of "samResponse" 
\param [out] plainPackets	decrypted response data (packets after Snr)

\return  APP_SUCCESS, APP_ERROR
*/
long  _decrypt_sam_response(unsigned char samResponse[], unsigned int samResLen, unsigned char plainPackets[])
{
	printf("Decrypting RESPONSE \n");
	unsigned char	_received_snr[4];
	unsigned int	_snr_value, _received_snr_value;
	unsigned int	_enc_data_len;

	unsigned char	_b0[16], _b1[16],  _ctr_block[16], _iv[16], work_buf[512];
	unsigned char	_mac[16], _received_mac[8];
	unsigned int	_num;
	AES_KEY			_aes_key;
	
	// Extract Snr
	memcpy(_received_snr, &samResponse[1+1+3], 4);

	// Calc encrypted packtes data length
	_enc_data_len = samResLen - (1+1+3+4) - 8;
	// Generate Ctr1
	_ctr_block[0] =				0x01;				// Flags
	memcpy(&_ctr_block[1],		_received_snr, 4);	// Snr
	memcpy(&_ctr_block[1+4],	Rcr, 4);			// Rcr
	memcpy(&_ctr_block[1+4+4],	Rar, 5);			// Rar
	_ctr_block[14] =			0x00;				// i
    _ctr_block[15] =			0x01;				// i

	// Decrypt packet data
	memset(_iv, 0x00, 16);
	memset(work_buf, 0x00, 512);
	_num = 0;
	AES_set_encrypt_key(KYtr, 128, &_aes_key);
	AES_ctr128_encrypt(&samResponse[1+1+3+4], plainPackets, _enc_data_len, &_aes_key, _ctr_block, _iv, &_num);
#ifdef DEBUG
	PrintHexArray("SAM->PC [Raw Payload]: ", _enc_data_len, plainPackets);
#endif
	// Decrypt MAC
	memset(_iv, 0x00, 16);
	_ctr_block[14] = 0x00;
    _ctr_block[15] = 0x00;
	_num = 0;
	AES_ctr128_encrypt(&samResponse[samResLen-8], _received_mac, 8, &_aes_key, _ctr_block, _iv, &_num);
	// Create B0
	_b0[0] =			0x59;							// Flags
	memcpy(&_b0[1],		_received_snr, 4);				// Snr
	memcpy(&_b0[1+4],	Rcr, 4);						// Rcr
	memcpy(&_b0[1+4+4],	Rar, 5);						// Rar
	_b0[14] = (unsigned char)(_enc_data_len / 0x0100);	// Packet data size
	_b0[15] = (unsigned char)(_enc_data_len % 0x0100);	// Packet data size

	// Create B1
    _b1[0] =				0x00;					// Associated Data Size
    _b1[1] =				0x09;					// Associated Data Size
	memcpy(&_b1[2],			samResponse, 1+1+3+4);	// Response Code, Sub Response Code, Reserved(3), Snr(4)
	memset(&_b1[2+1+1+3+4],	0x00, 5);				// All Zero	

    // Calc CBC-MAC
	memcpy(&work_buf[0],		_b0, 16);
	memcpy(&work_buf[16],		_b1, 16);
    memcpy(&work_buf[16+16],	plainPackets, _enc_data_len);
	_calc_mac(KYtr, work_buf, 16+16+_enc_data_len, _mac);
	// Verify Snr
	_received_snr_value = CharArrayToIntLE(_received_snr, 4);
	_snr_value = CharArrayToIntLE(Snr, 4);
	printf("\n Recieved SNR is %d and snr_value+1 is %d \n",_received_snr_value,_snr_value+1);	
	if (_received_snr_value != _snr_value + 1)
	{
		return SNR_VERIFICATION_FAILED;
	}
	// Update Snr
	// If _received_snr_value = 0xffffffff, incrementing _snr_value will take it to zero. Semantically is not correct.
	// Sending next command without incrementing snr, will cause SAM to throw syntax error.
	// This syntax error is handled and treated as snr overflow error.
	// Motivation was to have snr over flow error check at one place.
	if (_received_snr_value != 0xffffffff) {
		_snr_value += 2;
	}

	IntToCharArrayLE(_snr_value, Snr);

    // Verify MAC
	if (memcmp(_received_mac, _mac, 8) == 0)
	{
		return DECRYPTION_SUCCEEDED;
	}
	else
	{
		return DECRYPTION_FAILED;
	}
}



//////////////DEBUG purpose function. Not in use
long _get_error_reason(unsigned char samResBuf[], unsigned long* samResLen,Reader *reader)
{
	long _ret;
	unsigned char _send_buf[262];
	unsigned long _send_len;

	_send_buf[0] = 0x00; //Dispatcher
	_send_buf[1] = 0x00; //Reserved
	_send_buf[2] = 0x00; //Reserved
	_send_buf[3] = 0x28; //Command code
	_send_buf[4] = 0x00; //Reserved
	_send_buf[5] = 0x00; //Reserved
	_send_len = 6;
	*samResLen = 8; //Response Data + SW1 +SW2

	PrintText("Getting Error reason");

	_ret = reader ->sendToSAM(_send_buf, _send_len, samResBuf, samResLen,SAM::FELICA_RCS500_SAM);
	if( _ret != SAM_DATA_TRANSMIT_SUCCEEDED)
	{
		return SAM_ERROR_REASON_FETCH_ERROR;
	}

	long hasSyntaxError = hasSyntaxErrorInSamResponse(samResBuf, samResLen);
	if (hasSyntaxError == SAM_RESPONSE_HAS_SYNTAX_ERROR)
	{
		return hasSyntaxError;
	}

	return SAM_COMMAND_SUCCEEDED;
}

long FelicaSAM:: _DisconnectWithSAMInDirectMode(void)
{
	long			ret = 0;
	
	ret = SCardDisconnect(g_hSAM, SCARD_LEAVE_CARD);

	return ret;
}


long FelicaSAM:: _send_SetNormalMode(unsigned char samResBuf[], unsigned long* samResLen, Reader *reader)
{
	long			hasSyntaxError,ret = 0;
	unsigned char	_send_buf[262];
	unsigned long	_send_len;
	unsigned int	nIdx = 0;

	//Send Set RWSAM Mode command
	_send_buf[nIdx++] = 0x00; // Dispatcher
	_send_buf[nIdx++] = 0x00; // Reserved
	_send_buf[nIdx++] = 0x00; // Reserved
	_send_buf[nIdx++] = 0xE6; // Command code
	_send_buf[nIdx++] = 0x02; // Sub command code
	_send_buf[nIdx++] = 0x02; // RW SAM mode(0x02: Normal Mode)

	_send_len = nIdx;
	*samResLen = 0xFF; // Expect the length of response (Data + SW1 +SW2)

	ret = reader ->sendToSAM(_send_buf, _send_len, samResBuf, samResLen, SAM::FELICA_RCS500_SAM);
	hasSyntaxError=checkAndHandleSyntaxErrorInSamResponse(samResBuf,samResLen,reader);
	if(hasSyntaxError !=SAM_RESPONSE_NO_SYNTAX_ERROR){
		return hasSyntaxError;
	}
	return SAM_COMMAND_SUCCEEDED;
}

/**
Send Attention command to SAM.

\return PC/SC API Return Values
*/
long FelicaSAM:: _send_Attention(unsigned char samResBuf[], unsigned long* samResLen, Reader* reader)
{
	long			hasSyntaxError, ret = 0;
	unsigned char	_send_buf[262];
	unsigned long	_send_len;
	unsigned int	nIdx = 0;

	//Send Attention Command
	_send_buf[nIdx++] = 0x00; // Dispatcher
	_send_buf[nIdx++] = 0x00; // Reserved
	_send_buf[nIdx++] = 0x00; // Reserved
	_send_buf[nIdx++] = 0x00; // Command code
	_send_buf[nIdx++] = 0x00; // Reserved
	_send_buf[nIdx++] = 0x00; // Reserved

	_send_len = nIdx;
	*samResLen = 24; // Response Data + SW1 +SW2

	ret = reader ->sendToSAM(_send_buf, _send_len, samResBuf, samResLen, SAM::FELICA_RCS500_SAM);
	hasSyntaxError=checkAndHandleSyntaxErrorInSamResponse(samResBuf,samResLen,reader);
	if(hasSyntaxError != SAM_RESPONSE_NO_SYNTAX_ERROR){
		return hasSyntaxError;
	}
	return SAM_COMMAND_SUCCEEDED;
}

void update_SNR_For_NextCommand( Reader* reader)
{
	unsigned int snr_value = CharArrayToIntLE(Snr, 4);
	// Update Snr
	snr_value += 2;
	IntToCharArrayLE(snr_value, Snr);
	checkAndHandleForSNROverFlow(reader);
}

long hasSyntaxErrorInSamResponse(unsigned char samResBuf[], unsigned long* samResLen)
{
	if (samResBuf[3] == 0x7f) {
		return SAM_RESPONSE_HAS_SYNTAX_ERROR;
	}
	else {
		return SAM_RESPONSE_NO_SYNTAX_ERROR;
	}
}

long checkAndHandleSyntaxErrorInSamResponse(unsigned char _sam_res[], unsigned long* _sam_res_len, Reader *reader)
{
	long hasSyntaxError = hasSyntaxErrorInSamResponse(_sam_res, _sam_res_len);
	if (hasSyntaxError == SAM_RESPONSE_HAS_SYNTAX_ERROR)
	{
		PrintText("Syntax Error occured.");
		long errorReasonCode = _get_error_reason(_sam_res,_sam_res_len,reader);
		if (errorReasonCode != SAM_COMMAND_SUCCEEDED)
		{
			PrintText("Unable to fetch error reason.");
		}
		else
		{
			// Syntax error reasone here.
			// Sample response: 00 0000 29 0000 (Dispatcher(1), Reserved(2), ResponseCode(1), ErrorCode(2))
			hasSyntaxError = CharArrayToIntLE(&_sam_res[4], 2);
			PrintText("Error Reason Code:%0x", hasSyntaxError);
		}
	}

	return hasSyntaxError;
}

long checkAndHandleForSNROverFlow(Reader *reader)
{
	unsigned int snr_value = CharArrayToIntLE(Snr, 4);
	if (snr_value > 0xfffffffd)
	{
		PrintText("SNR Reached limit, re-authentication between SAM and FeliCa driver; resetting SNR.");

		SAM *sam = SAM::getInstance(SAM::FELICA_RCS500_SAM);
		sam->hostAuthentication(reader,true);
		reader -> changeObject(sam);
	}

	return APP_SUCCESS;
}