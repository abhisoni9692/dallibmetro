#pragma comment(lib, "ssleay32MDd.lib")
#pragma comment(lib, "libeay32MDd.lib")
#include<cstdio>
#include "sam/SAM.h"
#include "reader/Reader.h"
#include "../include/Util.h"
#include "Parameter.h"
#include "sam/MifareAV2SAM.h"
#include "sam/MifareSAMCmdUtil.h"
#include "openssl/cmac.h"
#include "openssl/aes.h"

MifareAV2SAM::MifareAV2SAM()
{
	type = SAM::MIFARE_AV2_SAM;
}

long MifareAV2SAM::hostAuthentication(Reader *reader,bool AuthOnlyFlag) {
	long			ret = 0;
	unsigned char	_send_buf[262];
	unsigned long	_send_len;
	unsigned int	nIdx = 0; 
	unsigned char samResBuf[262];
	unsigned long samResLen = 262;
	
	memcpy(_send_buf,MifareSAMCmdUtil::ISO_fourPass_1,sizeof(MifareSAMCmdUtil::ISO_fourPass_1));

	_send_len = sizeof(MifareSAMCmdUtil::ISO_fourPass_1);

	samResLen = 0x32; // Expect the length of response (Data + SW1 +SW2)

	ret = reader->sendToSAM(_send_buf, _send_len, samResBuf, &samResLen, SAM::MIFARE_AV2_SAM);//Mifare_EscapeDataToSAM(_send_buf, _send_len, samResBuf, samResLen, g_CMD_SAM_APDU);

	nIdx = 0;
	int i;
	unsigned char key[16] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	};
	unsigned char zeros_16[16] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	};
	unsigned char mact[16] = { 0 };
	unsigned char mact1[16] = { 0 };
	unsigned char macHostPart[8];
	unsigned char rand2[256];
	size_t mactlen;
	size_t mact1len;
	int j;
	unsigned char protect[4] = { 0x02,0x00,0x00,0x00 };
	_send_len = samResLen;

	for (i = 0; i<(int)_send_len; i++)
	{
		_send_buf[i] = samResBuf[i];
		rand2[i] = samResBuf[i];

	}
	//_send_buf = samResBuf;
	_send_len = samResLen;

	PrintHexArray("RAND2  ", _send_len, _send_buf);
	nIdx = _send_len;
	memcpy(&_send_buf[nIdx],protect,sizeof(protect));
	
	_send_len = nIdx;
	printf("%lu", _send_len);

	samResLen = 24; // Response Data + SW1 +SW2

					 //samResBuf = NULL;

	PrintHexArray("RAND CMD: ", _send_len, _send_buf);

	//ret = EscapeDataToSAM(_send_buf, _send_len, samResBuf, samResLen, g_CMD_SAM_APDU);

	//*samResLen = 24; // Response Data + SW1 +SW2

	//samResBuf = NULL;
	CMAC_CTX *ctx = CMAC_CTX_new();
	CMAC_Init(ctx, key, 16, EVP_aes_128_cbc(), NULL);
	// printf("message length = %lu bytes (%lu bits)\n", sizeof(_send_buf), sizeof(_send_buf) * 8);
	CMAC_Update(ctx, _send_buf, 16);
	CMAC_Final(ctx, mact, &mactlen);
	PrintHexArray("machost: ", mactlen, mact);
	//	CMAC_CTX_free(ctx);
	unsigned char rand1[12];
	unsigned char rand1Apend[16];
	for (i = 0; i < 12; i++)
	{
		rand1[i] = (unsigned)rand();
		rand1Apend[i] = rand1[i];
	}
	PrintHexArray("Rand1 ", 12, rand1);
	for (i = 12, j = 0; i < 16; i++, j++)
	{
		rand1Apend[i] = protect[j];
	}

	PrintHexArray("Rand1Appended ", 16, rand1Apend);
	//ctx = CMAC_CTX_new();

	CMAC_Init(ctx, key, 16, EVP_aes_128_cbc(), NULL);
	// printf("message length = %lu bytes (%lu bits)\n", sizeof(rand1Apend), sizeof(rand1Apend) * 8);
	CMAC_Update(ctx, rand1Apend, 16);
	CMAC_Final(ctx, mact1, &mact1len);
	PrintHexArray("machost: ", mact1len, mact1);
	CMAC_CTX_free(ctx);
	for (int i = 0; i<8; i++)
	{
		macHostPart[i] = mact[i * 2 + 1];
	}
	PrintHexArray("machost: ", sizeof(macHostPart), macHostPart);
	unsigned char apdu[256];
	memcpy(apdu,MifareSAMCmdUtil::ISO_fourPass_2,sizeof(MifareSAMCmdUtil::ISO_fourPass_2));
	for (i = 5, j = 0; i < 13; i++, j++)
	{
		apdu[i] = macHostPart[j];
	}
	for (i = 13, j = 0; i < sizeof(rand1) + 13; i++, j++)
	{
		apdu[i] = rand1[j];
	}
	apdu[i++] = 0x00;
	PrintHexArray("mac APDU ", 26, apdu);
	samResLen = 30;

	unsigned char sv1[256];

	for (i = 7, j = 0; i < 12; i++, j++)
	{
		sv1[j] = rand1[i];
		//printf("j===%d\n", j);
	}
	for (i = 5, j = 7; j < 12; j++, i++)
	{
		sv1[i] = rand2[j];
		//printf("i===%d\n", i);
	}
	for (i = 0, j = 10; i < 5; i++, j++)
	{
		sv1[j] = rand1[i] ^ rand2[i];
		//printf("%d\n", j);
	}
	//	PrintHexArray("rand1 : ", 12, rand1);
	//PrintHexArray("SAM AUTHENTICATE SV: ", 16, sv1);
	sv1[15] = 0x91;

	PrintHexArray("SAM AUTHENTICATE SV: ", 16, sv1);

	unsigned char enc_out[16] = { 0 }, dec_out[16] = { 0 };
	unsigned char iv[16] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	};
	AES_KEY enc_key, dec_key;

	AES_set_encrypt_key(iv, 128, &enc_key);
	AES_cbc_encrypt(sv1, enc_out, 16, &enc_key, iv, AES_ENCRYPT);
	//AES_encrypt(sv1, enc_out, &enc_key);
	PrintHexArray("SAM AUTHENTICATE Kxe: ", 16, enc_out);
	unsigned char kxe[16];
	memcpy(kxe, enc_out, 16);
	ret = reader->sendToSAM(apdu, 26, samResBuf, &samResLen, SAM::MIFARE_AV2_SAM);


	unsigned char encryptedRndB[16];
	for (i = 8, j = 0; i < 24, j<16; j++, i++)
	{
		encryptedRndB[j] = samResBuf[i];
	}

	PrintHexArray("encryptedRndB: ", 16, encryptedRndB);

	memcpy(iv, zeros_16, 16);
	PrintHexArray("IV : ", 16, iv);
	AES_set_decrypt_key(enc_out, 128, &dec_key);
	AES_cbc_encrypt(encryptedRndB, dec_out, 16, &dec_key, iv, AES_DECRYPT);

	PrintHexArray("RndB: ", 16, dec_out);

	unsigned char rotRandB[16];
	rotRandB[14] = dec_out[0];
	rotRandB[15] = dec_out[1];
	for (i = 0, j = 2; i < 14; i++, j++)
	{
		rotRandB[i] = dec_out[j];
	}
	PrintHexArray("RotRndB: ", 16, rotRandB);
	unsigned char randA[16];
	for (i = 0; i < 16; i++)
	{
		randA[i] = (unsigned)rand();

	}
	PrintHexArray("RandA: ", 16, randA);
	unsigned char rndAPlusRndB2LR[32];
	for (i = 0; i < 16; i++)
	{
		rndAPlusRndB2LR[i] = randA[i];
	}
	for (i = 16, j = 0; i < 32; i++, j++)
	{
		rndAPlusRndB2LR[i] = rotRandB[j];
	}
	PrintHexArray("rndAPlusRndB2LR: ", 32, rndAPlusRndB2LR);

	unsigned char encryptRndAPlusRndB2LR[32];
	memcpy(iv, zeros_16, 16);
	PrintHexArray("kxe: ", 16, enc_out);
	AES_set_encrypt_key(enc_out, 128, &enc_key);
	AES_cbc_encrypt(rndAPlusRndB2LR, encryptRndAPlusRndB2LR, 32, &enc_key, iv, AES_ENCRYPT);

	PrintHexArray("encrypted rndAPlusRndB2LR: ", 32, encryptRndAPlusRndB2LR);

	unsigned char Apd3[256];
	memcpy(Apd3,MifareSAMCmdUtil::ISO_fourPass_3,sizeof(MifareSAMCmdUtil::ISO_fourPass_3));
	for (i = 5, j = 0; i < 37; i++, j++)
	{
		Apd3[i] = encryptRndAPlusRndB2LR[j];
	}
	Apd3[37] = 0x00;
	PrintHexArray("SAM APDU_3: ", 38, Apd3);
	samResLen = 18;
	ret = reader->sendToSAM(Apd3, 38, samResBuf, &samResLen, SAM::MIFARE_AV2_SAM);
	getVersion(reader);
	return ret;
}

long MifareAV2SAM::getVersion(Reader *reader) {
	unsigned char apdu[256], samres[256];
	memcpy(apdu,MifareSAMCmdUtil::ISO_samVersion,sizeof(MifareSAMCmdUtil::ISO_samVersion));
	unsigned long ret, samres_len = 256;
	PrintHexArray("SAM VERSION: ", 5, apdu);
	ret = reader->sendToSAM(apdu, 5, samres, &samres_len, 1);

	if (ret != APP_SUCCESS) {
		return APP_ERROR;
	}
	return APP_SUCCESS;
}
