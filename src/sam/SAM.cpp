#include<cstdio>
#include "sam/SAM.h"
#include "reader/Reader.h"
#include "Util.h"
#include "Parameter.h"
#include "sam/MifareAV2SAM.h"
#include "sam/FelicaSAM.h"

SAM* SAM::getInstance(int type)
{
	if (type == SAM::MIFARE_AV2_SAM)
	{
		return new MifareAV2SAM();
	}
	if (type == SAM::FELICA_RCS500_SAM)
	{
		return new FelicaSAM();
	}
	return NULL;
}